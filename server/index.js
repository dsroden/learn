'use strict';

var config = require('../config.js');
var NEEDED_PARAMS = [
  "APP_PORT"
];

const  jwtSecret = 'jwtSecret';

config.checkConfig(NEEDED_PARAMS);




var port    = global.APP_PORT,
    express = require('express'),
    app     = express(),
    http = require('http'),
    // sio = require('socket.io'),
    socketClusterServer = require('socketcluster-server'),
    LOGGER = require('./lib/logger'),
    // socketioJwt = require('socketio-jwt'),
    server = http.createServer(app);



// sio = sio(server);
// app.use(function(req, res, next) {
//   req.sio = sio;
//   // console.log('sio', req.sio);
//   next();
// });

// sio.set('authorization', socketioJwt.authorize({
//   secret: jwtSecret,
//   handshake: true
// }));

// sio.use(socketioJwt.authorize({
//   secret: jwtSecret,
//   handshake: true
// }));

// sio.on('connection', require('./sockets/connection'));
// sio.set('origins', '*:*');




// Attach socketcluster-server to our httpServer
var scServer = socketClusterServer.attach(server);
// scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_IN, function (req, next) {
//   var authToken = req.socket.authToken;
//   console.log('publish auth', authToken)
//   if (authToken) {
//     next();
//   } else {
//     next('You are not authorized to publish to ' + req.channel);
//   }
// });

scServer.addMiddleware(scServer.MIDDLEWARE_SUBSCRIBE, function (req, next) {
  // console.log('AN ATTEMPT TO SUBSCRIBE',req.socket);
  // ...
  let permit = false
  if(req.socket.authState === 'authenticated'){
    // console.log("authenticated", req.channel.split('personal-'));
    let name = req.channel.split('personal-')[1];
    if(name){
      // console.log('this is a personal subscription');
      if(req.socket.authToken.username === name){
        permit = true;
      }
    } else {
      permit = true;
    }
  }
  if (permit) {
    next(); // Allow
  } else {
    next(req.socket.id + ' is not allowed to subscribe to ' + req.channel); // Block
  }
});

// scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_IN, function (req, next) {
//   // ...
//   if (...) {
//     next(); // Allow
//   } else {
//     next(req.socket.id + ' is not allowed to publish to the ' + req.channel + ' channel'); // Block
//   }
// });

// scServer.addMiddleware(scServer.MIDDLEWARE_EMIT, function (req, next) {
//   // ...
//   if (...) {
//     next(); // Allow
//   } else {
//     next(req.socket.id + ' is not allowed to emit the ' + req.event + ' req.event'); // Block
//   }
// });

scServer.on('connection', require('./sockets/connection'));


//LOAD THE ROUTES
require('./routes/routes')(app, express);
//CONNECT THE DB AND LISTEN
require('./lib/mongodb')(function(){
  LOGGER.log('APP LISTENING AT PORT ' + port);
  server.listen(port);
});

module.exports = app;

