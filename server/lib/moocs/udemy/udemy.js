// https://about.coursera.org/affiliates
'use strict';

const request = require('request-promise');  
const fs = require('fs');
const MongoClient   = require('mongodb').MongoClient;
const mongodb     = require('mongodb');
const f       = require('util').format;
const Utils = require('../../utils.js')
const asyncLoop   = require('node-async-loop');
const Crawler = require("node-webcrawler");

const c = new Crawler({
        normalizeWhitespace: true,
        xmlMode: true
    });
var scrape = require('html-metadata');


const NEEDED_PARAMS = [
  "MONGODB_USER",
  "MONGODB_USER_PWD",
  "MONGODB_DB_NAME",
  "MONGODB_URL"
];
const config    = require('../../../../config.js');
config.checkConfig(NEEDED_PARAMS);

const url = f('mongodb://' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const dbName = global.MONGODB_DB_NAME;


function Udemy(){}

Udemy.convertCsvsToJson = () =>{
  Utils.listFiles('csv', './Udemy\ Courses\ Data/', (fileArray)=>{
    console.log(fileArray);
    for(var i = 0; i < fileArray.length; i++){
      Utils.csvToJson('./Udemy\ Courses\ Data/' + fileArray[i]);
    }
  });
}

Udemy.saveCoursesToDB = (courses, cb) =>{
  MongoClient.connect(url, (err, client)=>{
    const db = client.db(dbName);
    asyncLoop(courses, (course, next)=> {
      db.collection('udemy').save(course, (err, saved)=>{
            if (err){
                next(err);
                return;
            }
            next();     
        })
    }, (err) => {
      if (err){
          console.error('Error: ' + err.message);
          return;
      }

      cb(true);
    });
  });
}

Udemy.gatherCatalogCourses = (cb)=>{
  Utils.listFiles('json', './Udemy\ Courses\ Data/', (fileArray)=>{
    let sections = [];
    for(var i = 0; i < fileArray.length; i++){
      let courses = Utils.readJsonFile('./Udemy\ Courses\ Data/' + fileArray[i]);
      sections.push(courses);
    }
    let courses = [];
    // console.log(sections);
    for(var j = 0; j < sections.length; j++){
      for(var k = 0; k < sections[j].length; k++){
        courses.push(sections[j][k]);
      }
    }
    cb(courses);
  });
}

Udemy.consolidateTopics = () =>{
     MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName);

        var moocCursor = db.collection('udemy').find({});

        var objs = {};
        moocCursor.forEach(function(c) {
          let topics = consolidateTopics(c);
          console.log('topics', topics);
          c.topics = topics;
          db.collection('udemy').findOneAndUpdate({_id: c._id}, c, function(err, updated){

          });
        }, function(err) {
            if(err){
              console.log('eerror', err);
            } else {
              console.log('done');
            }
        });
    });
}


module.exports = Udemy


function consolidateTopics(c){
  let topics = [];
  let cat1 = c["Category 1"]
  cat1 = cat1.toLowerCase().trim();
  let cat2 = c["Category 2"];
  cat2 = cat2.toLowerCase().trim();
  topics.push(cat1, cat2);
  return topics;
}

// Udemy.gatherCatalogCourses((courses)=>{
//   Udemy.saveCoursesToDB(courses, ()=>{
    // Udemy.consolidateTopics()
//   })
// });




