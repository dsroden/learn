// https://about.coursera.org/affiliates
'use strict';

const request = require('request-promise');  
const fs = require('fs');
const MongoClient   = require('mongodb').MongoClient;
const mongodb     = require('mongodb');
const f       = require('util').format;
const asyncLoop   = require('node-async-loop');
const Crawler = require("node-webcrawler");
const c = new Crawler({});
const _ = require('underscore');

const NEEDED_PARAMS = [
  "MONGODB_USER",
  "MONGODB_USER_PWD",
  "MONGODB_DB_NAME",
  "MONGODB_URL"
];
const config    = require('../../../../config.js');
config.checkConfig(NEEDED_PARAMS);

const url = f('mongodb://' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const dbName = global.MONGODB_DB_NAME;


function Coursera(){}

Coursera.getCatalog = () =>{
	console.log('get catalog')
	let url = "https://api.coursera.org/api/courses.v1?start=0&limit=2150&includes=instructorIds,partnerIds,specializations,s12nlds,v1Details,v2Details&fields=courseType,instructorIds,partnerIds,specializations,s12nlds,description"
	  const options = {
	    method: 'GET',
	    uri: url
	  };
	let prom = new Promise((resolve, reject)=>{
    request(options)
    .then(response => {
      if(!response){
        reject();
      }
      response = JSON.parse(response);
      console.log('response from coursera', response);
      const content = JSON.stringify(response);
		fs.writeFile("./coursera_catalog2.json", content, 'utf8', function (err) {
		    if (err) {
		        return console.log(err);
		    }

		    console.log("The file was saved!");
          resolve(response);

		});
    }).catch((err)=>{
      reject(err)
    });
  })

  prom.then((response)=>{
  }).catch((err)=>{
    console.log('ERROR GETTTING LONG TOKEN' + JSON.stringify(err))
  })

  return prom;

}

Coursera.readCatalog = (cb) =>{
  fs.readFile("./coursera_catalog.json", 'utf8', function (err, data) {
      if(err){

      }
      var catalog = JSON.parse(data);
      cb(catalog.elements);
  });
}

Coursera.saveCatalogToDB = (courses, cb) =>{
  MongoClient.connect(url, (err, client)=>{
    const db = client.db(dbName);
    asyncLoop(courses, (course, next)=> {
      db.collection('coursera').save(course, (err, saved)=>{
            if (err){
                next(err);
                return;
            }
            next();     
        })
    }, (err) => {
      if (err){
          console.error('Error: ' + err.message);
          return;
      }

      cb(true);
    });
  });
}

Coursera.crawlUrl = (uri, cb)=>{
    c.queue([{
        uri: uri,
        jQuery: true,
     
        // The global callback won't be called 
        callback: function (error, result, $) {
            if(error){
                console.log('error', error);
                return;
            }
            let breadCrumbs = $(".rc-BannerBreadcrumbs a");
            let hrefs = [];
            for(var b in breadCrumbs){
              if(breadCrumbs.hasOwnProperty(b)){
                if(breadCrumbs[b].attribs){
                  hrefs.push(breadCrumbs[b].attribs.href)
                }
              }
            }
            //get and parse full href to extract course topics
            let topics = null;
            let rawTopics = null;
            let hrefcount = 0;
            if(hrefs[1]){
              hrefcount = 1;
            }
            if(hrefs[2]){
              hrefcount = 2;
            }
            if(hrefs[1] || hrefs[2]){
              rawTopics = hrefs[hrefcount].split('/');
              topics = _.filter(rawTopics, (topic)=>{
                if(topic !== 'browse' && topic !== ''){
                  return topic;
                }
              })
            }


            //get image associated with course page
            let bodyContainer = $(".body-container");
            let style = null;
            for(var b in bodyContainer){
              if(bodyContainer.hasOwnProperty(b)){
                if(bodyContainer[b].attribs){
                  style = bodyContainer[b].attribs.style;
                }
              }
            }
            let image_url = null;
            if(style){
              image_url = style.split('(')[1].split('?')[0];
            }
            
            cb({topics: topics, image_url: image_url});
        }
    }]);
}

Coursera.addTagsAndImageToDBCourses = (cb)=>{
  let counter = 0;
  let base_url = 'https://coursera.org/learn/';
  MongoClient.connect(url, function(err, client) {
    const db = client.db(dbName);
    let courseraCursor = db.collection('coursera').find({image_url: {$exists: false}});
    courseraCursor.forEach(function(course) {
        Coursera.crawlUrl(base_url + course.slug, (topicsImageUrl)=>{
          course.topics = topicsImageUrl.topics;
          course.image_url = topicsImageUrl.image_url;
          counter = counter + 1;
          console.log('counter', counter);
          db.collection('coursera').save(course);
        })
    }, function(err) {
        if (!err) {
          cb('done');
        }
    });

  });
}


module.exports = Coursera



// Coursera.getCatalog().then(()=>{
//   Coursera.readCatalog((courses)=>{
//     Coursera.saveCatalogToDB(courses, ()=>{
//       Coursera.addTagsAndImageToDBCourses((response)=>{
//         console.log(response);
//       })
//     })
//   });
// });




