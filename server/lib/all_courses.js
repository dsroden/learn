const request = require('request-promise');  
const fs = require('fs');
const MongoClient   = require('mongodb').MongoClient;
const mongodb     = require('mongodb');
const f       = require('util').format;
const asyncLoop   = require('node-async-loop');
const _     = require('underscore');
const Utils = require('./utils');
const CATEGORIES = require('../references/categories')


const NEEDED_PARAMS = [
  "MONGODB_USER",
  "MONGODB_USER_PWD",
  "MONGODB_DB_NAME",
  "MONGODB_URL"
];
const config    = require('../../config.js');
config.checkConfig(NEEDED_PARAMS);

const url = f('mongodb://' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const dbName = global.MONGODB_DB_NAME;



function Course(course){
	this.title = course.title;
	this.url = course.url;
	this.provider = course.provider;
	this.tags = course.tags || [];
	this.instuctors = course.instructors || null;
	this.description = course.description || null;
	this.image_url = course.image_url || null;
    this.type = course.type || null;
}


function CoursesLib(course){

};

CoursesLib.addFromCoursera = () => {
	 MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName);

        var courseraCursor = db.collection('coursera').find({});

        var objs = {};
        courseraCursor.forEach(function(c) {
        	let course = {};
        	course.title = c.name;
        	course.url = 'https://www.coursera.org/learn/' + c.slug;
        	course.tags = c.topics;
        	course.instructors = c.instructorIds; // url to get instructor id is https://www.coursera.org/instructor/~ + instructorId
        	course.description = c.description;
        	course.image_url = c.image_url;
        	course.provider = 'coursera';
        	course = new Course(course);
            db.collection('courses').save(course)

        }, function(err) {
            if(err){
            	console.log('eerror', err);
            } else {
            	console.log('done');
            }
        });
    });
}

CoursesLib.addFromUdemy = () => {
	MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName);

        var moocCursor = db.collection('udemy').find({});

        var objs = {};
        moocCursor.forEach(function(c) {
        	let course = {};
        	course.title = c["Course Name"];
        	course.url = c["Product Url"];
        	course.tags = c.topics;
        	course.instructors = c["Instructor Name"]; 
        	course.description = null;
        	course.image_url = null;
        	course.provider = 'udemy';
        	course = new Course(course);
            db.collection('courses').save(course)

        }, function(err) {
            if(err){
            	console.log('eerror', err);
            } else {
            	console.log('done');
            }
        });
    });
}


CoursesLib.collectAllTags = () =>{
    let tags = [];
    MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName);
        var moocCursor = db.collection('courses').find({});
        moocCursor.forEach(function(c) {
           tags.push(c.tags);
        }, function(err) {
            if(err){
                console.log('eerror', err);
            } else {
                tags = _.uniq(_.flatten(tags));
                console.log('done', tags.length);
                collectedTags = {tags: tags};
                Utils.jsonToCsv(collectedTags, ['tags'], '../references/tags')
                Utils.csvToJson('../references/tags.csv');

            }
        });
    });
};

CoursesLib.addProviderInTags = () =>{
    MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName);
        var moocCursor = db.collection('courses').find({});
        moocCursor.forEach(function(c) {
            let tags = c.tags;
            let exists = _.find(tags, (t)=>{
                t === c.provider.toLowerCase();
            });
            if(!exists){
                tags.unshift(c.provider.toLowerCase())
                tags = _.uniq(tags);
                db.collection('courses').findOneAndUpdate({_id: c._id}, {$set: {tags: tags}});
            } else {
                console.log('done already');
            }
        }, function(err) {
            if(err){
                console.log('eerror', err);
            } else {
               

            }
        });
    });
};

CoursesLib.addCategories = () =>{
    MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName);

        // var moocCursor = db.collection('udemy').find({});

        var objs = {};
        CATEGORIES.list.forEach(function(c) {
            let course = {};
            course.title = c["title"];
            course.url = c["url"];
            course.tags = c.tags;
            course.instructors = c.instructors; 
            course.description = c["description"];
            course.image_url = c["image_url"];
            course.provider = c.provider;
            course.type = c.type;
            course = new Course(course);
            db.collection('courses').findOne({title: course.title, type: 'category'}, (err, category)=>{
                if(err){
                    console.log('error inserting category');
                } 
                if(category){
                    db.collection('courses').findOneAndUpdate({title: course.title}, course, (err, updated)=>{
                        console.log('updated')
                    })
                } else {
                    db.collection('courses').save(course)
                }
            })
        }, function(err) {
            if(err){
                console.log('eerror', err);
            } else {
                console.log('done');
            }
        });
    });
}

module.exports = CoursesLib;

// CoursesLib.addProviderInTags();
CoursesLib.collectAllTags();
// CoursesLib.addCategories();
