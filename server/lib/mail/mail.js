'use strict';

var NEEDED_PARAMS = [
  "SENDGRID_USERNAME",
  "SENDGRID_KEY",
  "BASE_URL",
  "CLIENT_URL"
];

var config = require('../../../config.js');
config.checkConfig(NEEDED_PARAMS);

var nodemailer = require('nodemailer');

var client = nodemailer.createTransport({
  service: 'SendGrid',
  host: 'smtp.sendgrid.net',
  port: 587,
  auth: {
    user: global.SENDGRID_USERNAME,
    pass: global.SENDGRID_KEY
  }
});


const EmailTemplate = require('email-templates-v2').EmailTemplate;
var path = require('path');
var templateDir = path.join(__dirname, 'eTemplates', 'welcome-email');



exports.sendMail = function(address, type, options, cb){
  var message;
  if(options.message !== ''){
    message = '' + options.message;
  }
  var msg, bodyText;
  switch(type){
    case 'verify': 
      msg = {
        msg: 
        {
          subject:   'Welcome',
          callToActionText: 'Validate', 
          callToActionUrl: global.CLIENT_URL + '/verification?token=' + options.verification_token, 
          bodyText: 'Thank you for signing up to LearnLabs. Please validate your account.', 
          headingText: 'Your LearnLabs account is ready',
        }
      };
    break;
        case 'reset_password': 
      msg = {
        msg: 
        {
          subject:   'LearnLabs password reset',
          callToActionText: 'Reset password', 
          callToActionUrl: global.CLIENT_URL + '/reset?token=' + options.token, 
          bodyText: 'We received your request to reset your password.', 
          headingText: 'Keep on learning',
        }
      };
    break;
   
  }
  
  var email = {address: address, html: false, subject: false, text: false};
  // console.log('MSG', msg);
  var sendEmail = new EmailTemplate(templateDir);
  // console.log('send email', sendEmail);
  sendEmail.render(msg, function (err, result) {
    // console.log('RESULT, EMIAL', result)
    email.text = result.text;
    email.html = result.html;
    email.subject = result.subject;
    mailing(email.address, email.subject, email.text, email.html, function(info){
      console.log('mail', info);
      if(!info){
        return cb();
      } else {
        return cb(info);
      }
    });
  });

};


function mailing(address, subject, text, html, cb){
  var email = {
    from: 'info@learnlabs.city',
    to: address,
    subject: subject,
    text: text,
    html: html
  };

  client.sendMail(email, function(err, info){
    if (err ){
      console.log(err);
      return cb();
    }
    else {
      // console.log('Message sent: ' + info.response);
      return cb(info.response);
    }
  });
}


