'use strict';
const request = require('request-promise');  
const LOGGER = require('./logger.js');
const config = require('../../config.js');
var NEEDED_PARAMS = [
  "FB_APP_ID",
  "FB_APP_SECRET"
];

config.checkConfig(NEEDED_PARAMS);

function Facebook(){

}

Facebook.getLongToken = function(short_token){
  const options = {
    method: 'GET',
    uri: `https://graph.facebook.com/oauth/access_token?client_id=${global.FB_APP_ID}&client_secret=${global.FB_APP_SECRET}&grant_type=fb_exchange_token&&fb_exchange_token=${short_token}`
  };
  let prom = new Promise((resolve, reject)=>{
    request(options)
    .then(fbRes => {
      if(!fbRes){
        reject();
      }
      var res = JSON.parse(fbRes);
      resolve(res);
    }).catch((err)=>{
      reject(res)
    });
  })

  prom.then((res)=>{
    return res;
  }).catch((err)=>{
    console.log('ERROR GETTTING LONG TOKEN' + JSON.stringify(err))
  })

  return prom;
};


Facebook.getUserPhotos = function(user_access_token){

  var prom = new Promise((resolve, reject)=>{
    const userFieldSet = 'id, name, albums';

    const options = {
      method: 'GET',
      uri: 'https://graph.facebook.com/v2.8/me?',
      qs: {
        access_token: user_access_token,
        fields: userFieldSet
      }
    };
    request(options)
      .then(fbRes => {
        if(!fbRes){
          reject();
        }
        var res = JSON.parse(fbRes);
        // console.log(res);
        //find profile pictures album
        var profileAlbumId;
        // console.log('res', res);
        if(!res.albums){
          resolve([]);
        } else {
          for(var i = 0; i < res.albums.data.length; i++){
            if(res.albums.data[i].name === 'Profile Pictures'){
              profileAlbumId = res.albums.data[i].id; 
            }
          }

          const albumOptions = {
            method: 'Get',
            uri: 'https://graph.facebook.com/v2.8/' + profileAlbumId + '/photos',
            qs: {
              access_token: user_access_token,
              fields: 'images'
            }
          };
          request(albumOptions).then((photos)=>{
            // console.log('photos', photos);
            if(!fbRes){
              reject();
            }
            photos = JSON.parse(photos);
            var res = [];
            for(var j = 0; j < photos.data.length; j++){
              var p = {};
              p.id = photos.data[j].id;
              p.picture = photos.data[j].images[0];
              res.push(p);
            }
            resolve(res);
          });
        }
      });
  });

  return prom;
};

Facebook.getAll = function(user_access_token){
    // user_access_token = global.FACEBOOK_TEST_TOKEN || user_access_token;

  var prom = new Promise((resolve, reject)=>{
    const userFieldSet = 'ids_for_apps, token_for_business, ids_for_business, email, first_name, last_name, about, gender, work, birthday, location, education, albums, books, movies, music, photos, likes';

    const options = {
      method: 'GET',
      uri: 'https://graph.facebook.com/v2.9/me?',
      qs: {
        access_token: user_access_token,
        fields: userFieldSet
      }
    };
    request(options)
    .then(fbRes => {
    var res = JSON.parse(fbRes);
      if(!fbRes){
        reject();
      }
      // console.log('facebook res', res.ids_for_business.data);
      resolve(res);
    });
  });

  prom.catch((location)=>{
    return location;
  }).catch((err)=>{
    LOGGER.error('error getting faeboook fields'+ JSON.stringify(err));
    //errror
  });

  return prom;
};

Facebook.getInterests = function(user_access_token){
	const userFieldSet = 'books, movies, music';

	const options = {
	    method: 'GET',
	    uri: 'https://graph.facebook.com/v2.8/me?',
	    qs: {
	      access_token: user_access_token,
	      fields: userFieldSet
	    }
	  };
  	request(options)
    .then(fbRes => {
    var res = JSON.parse(fbRes);
    	return res;
    });
};

module.exports = Facebook