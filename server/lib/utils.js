'use strict';

const  fs = require('fs');
const json2csv = require('json2csv').parse;
const csv2json = require('csv2json');
const _ = require('underscore');

function Utils(){}

Utils.readJsonFile = (filePath)=>{
	var contents = fs.readFileSync(filePath);
	console.log('contents', contents);
	return jsonContent = JSON.parse(contents);
}

Utils.listFiles = (type, path, cb)=>{
	console.log('path', path);
	let fileArray = [];
	fs.readdir(path,function(err,files){
	    if(err) throw err;
	    if(type === 'json'){
		    fileArray = _.filter(files, function(f){
		    	if(f.split('.')[1] === 'json'){
		    		return f;
		    	}
		    });
	    } else if (type === 'csv'){
	    	fileArray = _.filter(files, function(f){
		    	if(f.split('.')[1] === 'csv'){
		    		return f;
		    	} else {
		    		console.log('f', f);
		    	}
		    });
	    }
	    cb(fileArray)
	});
}

  
Utils.jsonToCsv = (items, fields, path) =>{
	console.log('tiems fields path', items, fields, path)
    var result = json2csv(items, {fields});
    fs.writeFile(path + '.csv', result, function(err) {
    	console.log('err writing csv files')
    });
}


Utils.csvToJson = (filePath) =>{
	fs.createReadStream(filePath)
	.pipe(csv2json({
	// Defaults to comma. 
	}))
	.pipe(fs.createWriteStream(filePath.split('.csv')[0] + '.json'));

}



module.exports = Utils;

