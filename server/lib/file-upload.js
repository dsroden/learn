'use strict';

//node modules
const crypto = require('crypto'),
    path = require('path'),
    fs = require('fs'),
    S3FS = require('s3fs'),
    AWS = require('aws-sdk');


const config = require('../../config.js');
const NEEDED_PARAMS = [
  "AWS_BUCKET",
  "AWS_ID",
  "AWS_SECRET",
  "S3_BUCKET_URL",
];

let awsConfig = { "accessKeyId": global.AWS_ID, "secretAccessKey": global.AWS_SECRET, "region": "us-east-1" }
AWS.config.update(awsConfig);
var s3 = new AWS.S3();

console.log('aws bucket', global.AWS_BUCKET);

var s3fsImpl = new S3FS(global.AWS_BUCKET, {
    accessKeyId: global.AWS_ID,
    secretAccessKey: global.AWS_SECRET
});

s3fsImpl.create();

function Upload(){};

Upload.file = function(file, filename, cb){
  crypto.randomBytes(48, function(ex, buf){
    let hex = buf.toString('hex'),
      originalFilename = file.name,
      ext = path.extname(originalFilename),
      filePath = filename + '.png'; //path.extname(originalFilename);

      // filePath =  hex + path.extname(originalFilename);
        // s3fsImpl.writeFile(filePath , file.data, {CacheControl: "no-cache", "ContentType": (file.mimetype)}).then(function (e) {
        // let s3Link = global.S3_BUCKET_URL + '/' + filePath; 
        // console.log('S3>>>>>', s3);
      console.log('file mimetype', file.mimetype)
      s3.putObject({
        Bucket: global.AWS_BUCKET,
        Key: filePath,
        Body: file.data,
        ACL: 'public-read',
        Expires: new Date,
        CacheControl: 'no-store',
        ContentType: file.mimetype
      },function (resp) {
        let s3Link = global.S3_BUCKET_URL + '/' + filePath; 
        cb(s3Link);
      });


    // });
  });
};


module.exports = Upload;