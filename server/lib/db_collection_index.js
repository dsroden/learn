'use strict';

const args = [];
process.argv.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
  args.push(val);
});
const COLLECTION_NAME = args[4]
const INDEX_NAME = args[5]
console.log('requires colelction name and index name...')
if(!INDEX_NAME){
	console.log('No index name provided')
	process.exit(99);
	return;
}
const NEEDED_PARAMS = [
  "MONGODB_USER",
  "MONGODB_USER_PWD",
  "MONGODB_DB_NAME",
  "MONGODB_URL"
];
const config 		= require('../../config.js');
const MongoClient 	= require('mongodb').MongoClient;
const mongodb 		= require('mongodb');
const f 			= require('util').format;
const _ 			= require('underscore');

config.checkConfig(NEEDED_PARAMS);

// const url = f('mongodb://' + global.MONGODB_USER + ':' + global.MONGODB_USER_PWD + '@' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const url = f('mongodb://' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const dbName = global.MONGODB_DB_NAME;

MongoClient.connect(url, (err, client)=>{
	const db = client.db(dbName);
	console.log(`Creating Index ${INDEX_NAME} on Collection ${COLLECTION_NAME}...`);
	let collection  = db.collection(COLLECTION_NAME);
	collection.createIndex({[INDEX_NAME]: 1})
	console.log('Finished!');
});