'use strict';

const args = [];
process.argv.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
  args.push(val);
});
const filePath = args[4];
const DB_COLLECTION = args[5]
console.log('requires filepath argument, processing recommendations...')
const NEEDED_PARAMS = [
  "MONGODB_USER",
  "MONGODB_USER_PWD",
  "MONGODB_DB_NAME",
  "MONGODB_URL"
];
const config 		= require('../../config.js');
const MongoClient 	= require('mongodb').MongoClient;
const mongodb 		= require('mongodb');
const f 			= require('util').format;
const _ 			= require('underscore');
const csvjson 		= require('csvjson');
const fs 			= require('fs');
const asyncLoop 	= require('node-async-loop');

config.checkConfig(NEEDED_PARAMS);

// const url = f('mongodb://' + global.MONGODB_USER + ':' + global.MONGODB_USER_PWD + '@' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const url = f('mongodb://' + global.MONGODB_URL + '/' + global.MONGODB_DB_NAME);
const dbName = global.MONGODB_DB_NAME;


let createTags = (tagsString) =>{
	let tags = tagsString.split(',');
	let cleanTags = [];
	for(var i = 0; i < tags.length; i++){
		cleanTags.push(tags[i].trim())
	}
	return cleanTags;
}

let creatInfoObj = (rec) =>{
	let info = {};
	for(var p in rec){
		if(rec.hasOwnProperty(p)){
			if(p !== 'title' && p !== 'tags' && p !== ''){
				info[p] = rec[p];
			}
		}
	}
	return info;
}

let mergeRecs = (oldRec, newRec)=>{
	let mergedRec = {}
	mergedRec.title = newRec.title;
	mergedRec.tags = _.union(oldRec.tags, newRec.tags);
	let info = oldRec.info;
	for(var p in newRec.info){
		if(newRec.info.hasOwnProperty(p)){
			info[p] = newRec.info[p];
		}
	}
	mergedRec.info = info;
	return mergedRec;
}


let saveRecs = (recs, cb) =>{
	MongoClient.connect(url, (err, client)=>{
		const db = client.db(dbName);
		asyncLoop(recs, (rec, next)=> {
			db.collection(DB_COLLECTION).findOne({title: rec.title}, (err, foundRec)=>{
				if(foundRec){
					let updateRec = mergeRecs(foundRec, rec);
					db.collection(DB_COLLECTION).findOneAndUpdate({title: rec.title}, updateRec, (err, updated)=>{
					    if (err){
					        next(err);
					        return;
					    }
					    next();			
					})
				} else {
					db.collection(DB_COLLECTION).save(rec, (err, rec) => {
					    if (err){

					        next(err);
					        return;
					    }
					    next();
					});
				}
			})

		}, (err) => {
			if (err){
			    console.error('Error: ' + err.message);
			    return;
			}

			cb(true);
		});
	});
}


const Rekr = () =>{};


Rekr.fromCSV = (fileP, cb) => {
	let options = {
	  delimiter : ',', // optional
	  quote     : '"' // optional
	};
	let data = fs.readFileSync((fileP), { encoding : 'utf8'});
	let rArray = csvjson.toObject(data, options);
	let fArray = [];
	//go through result array and push formatted recs into formatted array
	rArray.forEach((rec)=>{
		let fRec = {};
		if(rec.title && rec.title !== ''){
			fRec.tags = createTags(rec.tags);
			fRec.title = rec.title;
			fRec.info = creatInfoObj(rec);
			fArray.push(fRec);
		}
	})

	saveRecs(fArray, ()=>{
		cb()
	})
}



module.exports = Rekr

//testing with filepath from args
// Rekr.fromCSV(filePath, (result)=>{
// 	console.log('Finished!');
// 	process.exit(0);
// })