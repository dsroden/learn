'use strict';

const app_secret = 'secret0129';
const jwt    = require('jsonwebtoken');

exports.authenticate = (req)=>{
	// console.log('authetnicating', req);
	return new Promise((res, rej)=>{
		var token = req.body.access_token;

		  // decode token
		  if (token) {
		  	// console.log('verified')
		    // verifies secret and checks exp
		    jwt.verify(token, app_secret, function(err, decoded) {      
		      if (err) {
		        rej()  
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.decoded = decoded;    
				res(true);
		      }
		    });

		  } else {
		  	console.log('error', err);
		    // if there is no token
		    // return an error
		   rej()
		  }

	}).then(()=>{
		return true;
	}).catch(()=>{
		return false;
	})
}