'use strict';


//app is launched using a config file (check package.json scripts and change accordingly)
const config = require('../../config.js');
const NEEDED_PARAMS = [
  "BASE_URL",
  "S3_BUCKET_URL"
];
config.checkConfig(NEEDED_PARAMS);
// console.log('APP BASE_URL: ', global.BASE_URL);


//node modules
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const multer = require('multer');
const upload = multer();

//controllers
const examples = require('../controllers/examples');
const courses = require('../controllers/courses');
const users = require('../controllers/users');
const searches = require('../controllers/searches');
const extensions = require('../controllers/extensions');
const google = require('../controllers/google');
const authenticate = require('./authenticate');
const channels = require('../controllers/channels');
const groups = require('../controllers/groups');
const messages = require('../controllers/messages');
const geolocation = require('../controllers/geolocation');
const coaches = require('../controllers/coaches');
const meetings = require('../controllers/meetings');
const categories = require('../controllers/categories');


module.exports = function(app, express){
    app.set('trust proxy', true);

	// app.use(function (req, res, next) {
	//     // Website you wish to allow to connect
	//     res.setHeader('Access-Control-Allow-Origin', '*');
	//     // Request methods you wish to allow
	//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	//     // Request headers you wish to allow
	//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	//     // Set to true if you need the website to include cookies in the requests sent
	//     // to the API (e.g. in case you use sessions)
	//     res.setHeader('Access-Control-Allow-Credentials', true);
	//     // Pass to next layer of middleware
	//     next();
	// });


	//setup router that will handle interactions with bots/user agents
	const nonSPArouter = express.Router();    

	// to track user agents and provide dynamic content, i.e meta headers get passed to jade template to create twitter/facebook cards
	nonSPArouter.get('/', function(req,res) {
		var url, title, descriptionText, imageUrl;
		url = global.BASE_URL;
		title = 'For the learners';
		descriptionText = 'A place to connect with other learners based on interests, locations, and classes.';
		imageUrl = global.S3_BUCKET_URL + '/assets/circle_logo.png'

		res.render('bot', { 
		  img       : imageUrl,
		  url       : url,
		  title     : title, 
		  descriptionText : descriptionText,
		  imageUrl  : imageUrl
		});
	});

		//checks the request headers to see if user-agent is a bot, if bot send to nonSPArouter otherwise continue
	app.use(function(req,res,next) {
		let ua = req.headers['user-agent'];
		// console.log('UA', ua);
		if(ua === 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko)' || ua === 'WhatsApp/2.18.22 i'){
			nonSPArouter(req,res,next);
		} else if (/^(facebookexternalhit)|(Twitterbot)|(WhatsApp)|(LinkedInBot)|(Facebot)|(Pinterest)/gi.test(ua)) {
		  nonSPArouter(req,res,next);
		} else {
		  next();
		}
	});

	app.use(function (req, res, next) {
	    // Website you wish to allow to connect
	    res.setHeader('Access-Control-Allow-Origin', '*');
	    // Request methods you wish to allow
	    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	    // Request headers you wish to allow
	    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	    // Set to true if you need the website to include cookies in the requests sent
	    // to the API (e.g. in case you use sessions)
	    res.setHeader('Access-Control-Allow-Credentials', true);
	    // Pass to next layer of middleware
	    next();
	});


	

	//set render engine to jade
	app.set('view engine', 'jade');
	//parse req.body from client
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(methodOverride());
	//in case app needs to handle file uploads
	app.use(fileUpload());


	// app.post('/api/examples/create',  examples.createOne);
	// app.get('/api/examples/fetch',  examples.fetch);
	// app.post('/api/examples/delete',  examples.delete);

	//API CALLS
	app.get('/api/courses/fetch',  courses.fetch);

	app.post('/api/user/signup', users.signup);
	app.post('/api/user/login', users.login);
	app.post('/api/user/sociallogin', users.socialLogin);
	app.post('/api/user/verify', users.verifyUser);
	app.post('/api/user/sendVerificationEmail', users.sendVerificationEmail);
	app.post('/api/search', searches.topicAndLocation);
	app.get('/api/search/topics/?', searches.topics);
	app.post('/api/user/checkUsername', users.checkUsername);
	app.post('/api/extension/createone', extensions.createOne);

	app.post('/api/googleapi/places', google.queryGooglePlaces)
	app.post('/api/googleapi/geo', google.queryGoogleGeo)

	app.post('/api/user/setSocketToken', users.setSocketToken);

	app.post('/api/user/resetPasswordRequest', users.resetPasswordRequest);
	app.post('/api/user/resetPassword', users.resetPassword);

	app.post('/api/courses/suggestion', courses.courseSuggestion);
	app.post('/api/course/group/recent/messages', courses.fetchGroupRecentMessages)
	app.post('/api/course/fetch/title', courses.fetchByTitle)

	app.post('/api/geolocation/iplookup', geolocation.ipLookup);

	app.post('/api/fetch/course/group/title', courses.fetchGroupByTitleAndLocation);
	app.post('/api/course/groups/local/searchbyplace', courses.searchLocalGroupsByPlace)
	app.post('/api/courses/groups/view', courses.view);
	app.post('/api/course/group/local/create', courses.createLocalGroup);
	app.post('/api/channels/messages/load', channels.loadMessages);
	app.post('/api/course/group/local/view', courses.viewLocalGroup);
	app.post('/api/course/group/global', courses.loadCourseGroupGlobal);

	//API CALLS that require authentication

	app.post('/api/auth/*', (req, res, next)=>{
		authenticate.authenticate(req).then((authenticated)=>{
			// console.log('authenticated', authenticated);
			if(authenticated){
				next();
			} else {
				return res.status(400).end();
			}
		});
	});

	app.post('/api/categories/fetch', categories.fetchAll);


	app.post('/api/auth/courses/groups/join', courses.join);
	app.post('/api/auth/courses/groups/global/member', courses.loadCourseGroupsGlobalMember);
	app.post('/api/auth/courses/groups/load/all', courses.loadCoursesGroups);
	app.post('/api/auth/courses/groups/load/one', courses.loadCourseGroups);
	app.post('/api/auth/course/group/global', courses.loadCourseGroupGlobal);
	app.post('/api/auth/course/group/local', courses.loadCourseGroupLocal); 
	app.post('/api/auth/fetch/course/group/title', courses.fetchGroupByTitleAndLocation);
	app.post('/api/auth/fetch/course/group/global/member', courses.fetchGlobalGroupByCourseIdMemberId)
	app.post('/api/auth/course/groups/local/member', courses.loadCourseGroupsLocalMember)
	app.post('/api/auth/course/groups/local/searchbyplace', courses.searchLocalGroupsByPlace)
	app.post('/api/auth/course/group/local/join', courses.joinLocalGroup);
	app.post('/api/auth/course/group/local/create', courses.createLocalGroup);
	app.post('/api/auth/channels/messages/load', channels.loadMessages);
	app.post('/api/auth/users/fetch/one', users.fetchOneByUsername);
	app.post('/api/auth/users/fetch/self', users.fetchSelf);
	app.post('/api/auth/user/conversation/history', users.fetchConversationHistory);
	app.post('/api/auth/user/conversations/contacts', users.fetchContacts);
	app.post('/api/auth/user/group/global/leave', users.leaveGlobalGroup);
	app.post('/api/auth/user/group/local/leave', users.leaveLocalGroup);
	app.post('/api/auth/user/course/leave', users.leaveCourse);
	app.post('/api/auth/fetch/group/recent', groups.fetchRecent)
	app.post('/api/auth/fetch/group/global/member', groups.fetchGlobalGroupByMember)
	app.post('/api/auth/fetch/group/local/member', groups.fetchLocalGroupByMember)
	app.post('/api/auth/user/profile/update', users.updateUserProfile);
	app.post('/api/auth/user/profile/update/photo', users.updateUserProfilePhoto);
	app.post('/api/auth/user/block', users.blockUser);
	app.post('/api/auth/message/expire', messages.expireMessage);
	app.post('/api/auth/user/flag', users.flagUser);
	app.post('/api/auth/group/fetch/members', groups.fetchGroupMembers);
	app.post('/api/auth/user/createUsername', users.createUsername);

	app.post('/api/auth/coach/groups/join', coaches.joinGroup)
	app.post('/api/auth/coach/groups/fetch', coaches.fetchGroups)

	app.post('/api/auth/search/people', searches.fetchPeople)
	app.post('/api/auth/group/meeting/submit', meetings.createOne)
	app.post('/api/auth/group/meetings/fetch', meetings.fetchByGroupId)
	app.post('/api/auth/group/meeting/fetchDetails', meetings.fetchDetails);
	app.post('/api/auth/group/meeting/join', meetings.joinMeeting);
	app.post('/api/auth/group/meeting/leave', meetings.leaveMeeting);
	app.post('/api/auth/group/member/check', groups.checkIfMember);
}