'use strict';

const crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

const Mongo = require('mongodb');
const _ = require('underscore');
const encrypt = require('./EncryptionHelper').encrypt;
const decrypt = require('./EncryptionHelper').decrypt;
const asyncLoop   = require('node-async-loop');

function Message(message){
  this.created_at = message.created_at;
  this.text = encrypt(message.text);
  this.sender_username = message.username;
  this.receiver_username = (message.receiver) ? message.receiver : null;
  this.channel_id = message.channelId;
}

Object.defineProperty(Message, 'collection', {
  get: function(){return global.mongodb.collection('messages');}
});

Message.saveFromChannel = (messageData) => {
  let newMessage = new Message(messageData);
  Message.collection.save(newMessage)
};

Message.fetchForChannel = (data, cb)=>{
  let query = null;
  if(data.last_id){
    query = {$and: [{channel_id: data.channel_id}, {_id: { $lt: Mongo.ObjectID(data.last_id)}}]}
  } else {
    query = {channel_id: data.channel_id};
  }
  Message.collection.find(query).sort({created_at: -1}).limit((data.limit || 10)).toArray((err, messages)=>{
    if(err){
      console.log('ERROR fetching messages for channel ' + JSON.stringify(err));
      return cb()
    }
    if(!messages){
      return cb([])
    }
    messages = decryptMessages(messages);
    cb(messages);
  })
};

Message.fetchConversationHistory = (data, cb)=>{
  //data is {sender: , receiver: }
  let sender = data.sender;
  let receiver = data.receiver
  Message.collection.find({$and: [{expired: {$exists: false}}, {$or: [{sender_username: sender, receiver_username: receiver}, {sender_username: receiver, receiver_username: sender}]}]}).sort({created_at: -1}).limit(10).toArray((err, messages)=>{
    if(err){
      console.log('ERROR fetching conversation history ' + JSON.stringify(err));
      return cb()
    }
    if(!messages){
      return cb([]);
    }
    messages = decryptAndFormatPrivateMessages(messages);
    cb(messages); 
  });
}


Message.fetchConversationsContacts = (data, cb)=>{
  //get all messages sent out by user to private users
  Message.collection.find({$and: [{sender_username: data.username}, {receiver_username: {$ne: null}}, {expired: {$exists: false}}]}).sort({created_at: -1}).toArray((err, messages_sent)=>{

    let contacts_messages_sent = _.map(messages_sent, (ms)=>{
      return ms.receiver_username;
    });

    //get all messages received
    Message.collection.find({$and: [{receiver_username: data.username}, {expired: {$exists: false}}]}).sort({createdAt: -1}).toArray((err, messages_received)=>{

      let contacts_messages_received = _.map(messages_received, (mr)=>{
        return mr.sender_username;
      });

      //merge contact names
      let contacts = _.union(contacts_messages_received, contacts_messages_sent);
      // console.log('contacts', contacts);
      //create contacts object with all messages related to contact
      let contacts_messages = {};
      for(var j = 0; j < contacts.length; j++){
        let contact_name = contacts[j];
        contacts_messages[contact_name] = [];
        for(var i = 0; i < messages_sent.length; i++){
          if(messages_sent[i].receiver_username === contact_name){
            contacts_messages[contact_name].push(messages_sent[i]);
          }
        }
        for(var k = 0; k < messages_received.length; k++){
          if(messages_received[k].sender_username === contact_name){
            contacts_messages[contact_name].push(messages_received[k]);
          }
        }
      }

      //sort the messages per contact by data created
      for(var c in contacts_messages){
        if(contacts_messages.hasOwnProperty(c)){
          contacts_messages[c] = _.sortBy(contacts_messages[c], (m)=>{
            return m.created_at;
          });
        }
      }

      // format final contact data with most recent msg to/from each contact
      let contacts_data = [];
      // console.log(contacts_messages);
      for(var cm in contacts_messages){
        if(contacts_messages.hasOwnProperty(cm)){
          let contactData = {};
          let message = contacts_messages[cm][contacts_messages[cm].length - 1];
          contactData._id = cm;
          contactData.created_at = message.created_at;
          contactData.sender_username = message.sender_username;
          contactData.text = decrypt(message.text); 
          contacts_data.push(contactData);
        }        
      }

      cb(contacts_data);

    });
  });
}

Message.expireOne = (data, cb)=>{
  Message.collection.findOneAndUpdate({created_at: data.created_at}, {$set: {expired: true}}, (err, updated)=>{
    if(err){
      cb();
    }
    cb(true);
  });
}

module.exports = Message;


function decryptAndFormatPrivateMessages(messages){
  messages.forEach((m)=>{
    // console.log(m.text);
    m.text = decrypt(m.text);
    m.username = m.sender_username;
  });
  return messages;
}

function decryptMessages(messages){
  messages.forEach((m)=>{
    // console.log(m.text);
    m.text = decrypt(m.text);
    m.username = m.sender_username;
  });
  return messages;
}

