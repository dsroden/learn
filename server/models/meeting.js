'use strict';

const Mongo = require('mongodb');
const User = require('./user');

function Meeting(meeting){
	this.group_id = Mongo.ObjectID(meeting.group_id)
	this.date = new Date(meeting.date);
	this.time = meeting.time;
	this.goal = meeting.goal;
	this.place = meeting.place;
	this.location = (meeting.place) ? {type: "Point", coordinates: [meeting.place.location.lng, meeting.place.location.lat] } : null
	this.creator_id = Mongo.ObjectID(meeting.creator_id);
	this.attending = [Mongo.ObjectID(meeting.creator_id)];
	this.created_at = new Date();
}

Object.defineProperty(Meeting, 'collection', {
  get: function(){return global.mongodb.collection('meetings');}
});

Meeting.createOne = (meeting, cb) => {
	//form contains title attribute
	let newMeeting = new Meeting(meeting);
	Meeting.collection.save(newMeeting, cb(newMeeting))
};

Meeting.fetchByGroupId = (group_id, cb)=>{
	var today = new Date();
	var yesterday = new Date(today);
	yesterday.setDate(today.getDate() - 1);
	Meeting.collection.find({$and: [{date: {$gte: (yesterday)}}, {group_id: Mongo.ObjectID(group_id)}]}).sort({date: 1}).toArray((err, meetings)=>{
		if(err){
			return cb()
		}
		cb(meetings);
	})
}

Meeting.fetchDetails = (meeting_id, cb) =>{
	Meeting.collection.findOne({_id: Mongo.ObjectID(meeting_id)}, (err, meeting)=>{
		User.findUsersByIdArray(meeting.attending, (users)=>{
			meeting.users = users;
			cb(meeting);
		});
	});
}

Meeting.joinMeeting = (data, cb)=>{
	Meeting.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.meeting_id)}, {$addToSet: {attending: Mongo.ObjectID(data.user_id)}}, (err, updatedMeeting)=>{
		if(err){
			return cb();
		}
		cb(updatedMeeting);
	})
}

Meeting.leaveMeeting = (data, cb)=>{
	Meeting.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.meeting_id)}, {$pull: {attending: Mongo.ObjectID(data.user_id)}}, (err, updatedMeeting)=>{
		if(err){
			return cb();
		}
		cb(updatedMeeting);
	})
}

module.exports = Meeting;

