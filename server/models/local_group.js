'use strict';

const Mongo = require('mongodb');
const User = require('./user');

function LocalGroup(data){
	this._id = data._id
	this.created_at = new Date();
	this.address = data.course.place.address;
	this.location = {type: "Point", coordinates: [data.course.place.location.lng, data.course.place.location.lat] } // location should be -> { type: "Point", coordinates: [-73.856077, 40.848447]}
	this.course_id = Mongo.ObjectID(data.course._id);
	this.tags = data.course.tags;
	this.members = data.members || [];
	this.private = data.privacy_status || false;
	this.type = data.type;
	this.views = 1;
}

Object.defineProperty(LocalGroup, 'collection', {
  get: function(){return global.mongodb.collection('localgroups');}
});


LocalGroup.create =(data, cb)=>{
	// console.log('create local', data);
	// data.members = [data.user_id];
	data._id = new Mongo.ObjectID();
	data.type = data.course.type;
	let newGroup = new LocalGroup(data);
	LocalGroup.collection.insert(newGroup, (err, group)=>{
		cb(group.ops[0])
	});
}


LocalGroup.addView = (data, cb)=>{
	//data includes {user_id: ObjectId , course: {_id: , title: , provider: ,...}}
	//check if data.course.place exists, if not then kick out because the query did not include place
	if(!data.course.place){
		return cb();
	}

	LocalGroup.collection.findOneAndUpdate({course_id: Mongo.ObjectId(data.course._id)}, {$inc: {views: 1}}, (err, updated_group)=>{
		if(updated_group.value){
			return cb(updated_group.value);
		}
		//if no group to update create one 
		LocalGroup.create(data, (new_local_group)=>{
			// console.log('new global group', new_local_group);
			cb(new_local_group);
		});
	});
}


LocalGroup.addMember = (data, cb)=>{
	//data includes {user_id: ObjectId , course: {_id: , title: , provider: ,...}}
	// console.log('data to add member to local group', data);
	//check if data.course.place exists, if not then kick out because the query did not include place
	if(!data.course.place){
		return cb();
	}

	LocalGroup.collection.findOneAndUpdate({_id: Mongo.ObjectId(data.course._id)}, {$addToSet: {members: Mongo.ObjectID(data.user_id)}}, (err, updated_group)=>{
		// console.log('updated group', updated_group);
		if(updated_group.value){
			return cb(updated_group.value);
		}
		//if no group to update create one 
		LocalGroup.create(data, (new_local_group)=>{
			// console.log('new global group', new_local_group);
			cb(new_local_group);
		});
	});
}

LocalGroup.removeMember = (data, cb) =>{
	//data is {user_id: , group_id: }
	LocalGroup.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.group_id)}, {$pull: {members: Mongo.ObjectID(data.user_id)}}, (err, updated)=>{
		if(err){
			return cb();
		}
		cb(updated);
	});
};

LocalGroup.findAllByMemberId = (user_id, cb)=>{
	// console.log('id', user_id)
	LocalGroup.collection.find({ members: { "$in" : [Mongo.ObjectID(user_id)]}}).toArray((err, local_groups)=>{
		// console.log('local groups', local_groups)
		if(err){
			return cb();
		}
		if(!local_groups){
			return cb([])
		}
		cb(local_groups);
	})
}

LocalGroup.findByCourseIdAndMemberId = (data, cb) =>{
	//data is {user_id : , course_id: }
	LocalGroup.collection.find({$and: [{course_id: Mongo.ObjectID(data.course_id)}, {members: {$in: [Mongo.ObjectID(data.user_id)]}}]}).toArray((err, local_groups)=>{
		if(err){
			return cb()
		}
		if(!local_groups){
			return cb([])
		}
		// console.log(local_groups);
		cb(local_groups);
	});
}

LocalGroup.loadCourseGroupLocal = (data, cb) =>{
	LocalGroup.collection.findOne({_id: Mongo.ObjectID(data.group_id)}, (err, local_group)=>{
		if(err){
			return cb();
		}
		if(!local_group){
			return cb();
		}
		// console.log('local group', local_group);
		cb(local_group);
	})
}

LocalGroup.findByCourseAndPlace = (data, cb)=>{ 
	//data includes {place: {address: , location: }, course_id: }
	console.log('data to find local group for a course by place', data);
	let query = { 
		course_id: Mongo.ObjectID(data.course_id), 
		location :
	    { $near :
	       { $geometry :
	        	{ 
	        		type : "Point" ,
	            	coordinates : [ parseFloat(data.place.location.lng), parseFloat(data.place.location.lat) ] 
	        	},
	         	$maxDistance : 2000
	  		} 
	  	}
	}

	LocalGroup.collection.find(query).toArray((err, local_groups)=>{
		// console.log('fetching local groups by place and course id - data and result', data, local_groups);
		if(err){
			console.log('error', err);
			return cb();
		}
		if(!local_groups){
			return cb([])
		}
		cb(local_groups);
	});
}

LocalGroup.joinGroup = (data, cb)=>{
	User.findByToken(data.access_token, (user)=>{
		if(!user){
			return cb();
		}
		LocalGroup.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.group_id)}, {$addToSet: {members: user._id}}, (err, updated_group)=>{
			if(err){
				return cb();
			}
			if(!updated_group.value){
				return cb();
			}
			User.recentGroup({user_id: user._id, recent_group: {group_type: 'local', group_id: Mongo.ObjectID(data.group_id)}}, ( updated_user)=>{
				cb(updated_group.value);
			});
		});
	});
}

LocalGroup.viewGroup = (data, cb)=>{

		LocalGroup.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.group_id)}, {$inc: {views: 1}}, (err, updated_group)=>{
			if(err){
				return cb();
			}
			if(!updated_group.value){
				return cb();
			}
			// User.recentGroup({user_id: user._id, recent_group: {group_type: 'local', group_id: Mongo.ObjectID(data.group_id)}}, ( updated_user)=>{
			// 	cb(updated_group.value);
			// });
			cb(updated_group.value);
		});
	// });
}






LocalGroup.createGroup = (data, cb)=>{
	// User.findByToken(data.access_token, (user)=>{
		// if(!user){
		// 	return cb();
		// }
		// data.user_id = user._id;
		data.course.place = data.place;
		LocalGroup.create(data, (new_local_group)=>{
			// console.log('new global group', new_local_group);
			// User.recentGroup({user_id: user._id, recent_group: {group_type: 'local', group_id: new_local_group._id}}, ( updated_user)=>{
				cb(new_local_group);
			// });
		});
	// });

}

LocalGroup.findNearLocation = (data, cb) =>{
	if(!data.place){
		return cb([]);
	}
	let tagQuery = null;
	if(!data.topics || data.topics.length < 1){
		data.topics = [];
		tagQuery = {$nin: data.topics};
	} else {
		// tagQuery = {$in: data.topics}
		tagQuery = {$all: data.topics}
	}
	// let providerQuery = {provider: {$exists: true}};
	// if(data.provider){
	// 	providerQuery = {provider: data.provider};
	// }
	let matchQuery = null 
	if(data.last_id){
		// matchQuery = {tags: tagQuery, _id: {$lt: Mongo.ObjectID(data.last_id)}};
		matchQuery = {$and: [{tags: tagQuery}, {_id: {$lt: Mongo.ObjectID(data.last_id)}}]};

	} else {
		matchQuery = {tags: tagQuery}
	}

	let geoQuery = 
	{
		$geoNear: {
			near: { type: "Point", coordinates: [ data.place.location.lng, data.place.location.lat ] },
			distanceField: "dist.calculated",
			maxDistance: 2,
			includeLocs: "dist.location",
			spherical: true
		}
	}
	// console.log('local gorup search data', data);
	if(data.categories){
		matchQuery.type = 'category';
	} else {
		matchQuery.type = {$ne: 'category'}
	}


	LocalGroup.collection.aggregate([
		geoQuery,
	    {
	        $lookup: {
	           from: "courses",
	           localField: "course_id",
	           foreignField: "_id",
	           as: "course"
	        }
	    },
	    {
	        $unwind: "$course"
	    },
	    {
	    	$match: matchQuery
	    },
		{
        	$project : { course: "$course", address: "$address", views: "$views", members: "$members", member_count: {$size: { "$ifNull": [ "$members", [] ] } } }
	    },
   	    {
   	    	$sort: {"member_count": -1}
   	    } 
	]).toArray(function(err, localGroups){
        if(err){
        	console.log('ERROR FETCHING LOCAL GROUPS: ' + JSON.stringify(err));
            return cb([]);
        }
        if(!localGroups){
        	console.log('NO LOCAL GROUPS FOUND');
        	localGroups = [];
        }
 		cb(localGroups)
    });
}


LocalGroup.findById = (id, cb)=>{
	LocalGroup.collection.findOne({_id: Mongo.ObjectID(id)}, (err, group)=>{
		if(err){
			return cb();
		}
		cb(group);
	});
}

LocalGroup.findByIdAndMember = (data, cb)=>{
	LocalGroup.collection.findOne({_id: Mongo.ObjectID(data.group_id), members: {$in: [Mongo.ObjectID(data.member_id)]}}, (err, group)=>{
		if(err){
			return cb();
		}
		cb(group);
	});	
}

LocalGroup.removeMemberByCourseIdMemberId = (data, cb)=>{
	LocalGroup.collection.updateMany({course_id: Mongo.ObjectID(data.course_id)}, {$pull: {members: Mongo.ObjectID(data.user_id)}}, (err, updated_group)=>{
		if(err){
			return cb();
		}
		cb(updated_group);
	});
}

LocalGroup.fetchMembers = (group, cb) =>{
	require('./user').findUsersByIdArray(group.members, (users)=>{
		if(!users){
			return cb();
		}
		cb(users);
	});
	// });
}

LocalGroup.fetchMember = (data, cb) =>{
	LocalGroup.collection.findOne({_id: Mongo.ObjectID(data.group._id), members: {$in: [Mongo.ObjectID(data.user._id)]}}, (err, group)=>{
		if(err){
			return cb();
		}
		return cb(group);
	})
}



module.exports = LocalGroup;

