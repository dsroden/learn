'use strict';

const Mongo = require('mongodb');

function Suggestion(suggestion){
	this.course_title = suggestion.course_title;
	this.email = suggestion.email;
	this.note = suggestion.note;
	this.course_url = suggestion.course_url;
}

Object.defineProperty(Suggestion, 'collection', {
  get: function(){return global.mongodb.collection('suggestions');}
});

Suggestion.saveOne = (suggestion, cb) => {
	//form contains title attribute
	let newSuggestion = new Suggestion(suggestion);
	Suggestion.collection.save(newSuggestion, cb(newSuggestion))
};


module.exports = Suggestion;

