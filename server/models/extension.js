'use strict';

const Mongo = require('mongodb');

function Extension(extension){
	this.created_at = new Date();
	this.client_id = extension.client_id;
}

Object.defineProperty(Extension, 'collection', {
  get: function(){return global.mongodb.collection('extensions');}
});


Extension.createOne = (extensionInfo, cb) => {
	//extension info is {client_id: client_id}
	let newExtension = new Extension(extensionInfo);
	Extension.collection.save(newExtension, cb(newExtension))
};


module.exports = Extension;

