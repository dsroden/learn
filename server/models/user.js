'use strict';

const Mongo = require('mongodb');
const Facebook = require('../lib/facebook')
const  bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const app_secret = 'secret0129';
const Upload = require('../lib/file-upload');
const Flag = require('./flag');
const Mail = require('../lib/mail/mail.js');
function User(user){
	this.created_at = new Date();
	this.access_token = user.access_token;

	this.social_provider = user.social_provider;
	this.social_profile = user.social_profile;
	this.social_token = user.social_token;

	// this.first_name = user.first_name;
	// this.last_name = user.last_name;
	this.birthdate = user.birthdate;
	this.email = user.email;
	this.username = user.username;
	this.password = user.password;
	this.verified = false;
	this.verification_token = user.verification_token;

	this.education = null;
	this.work = null;

	this.coach = null;
	this.sponsor = null;

	this.interests = [];
}

Object.defineProperty(User, 'collection', {
  get: function(){return global.mongodb.collection('users');}
});

User.checkUsername = (data, cb)=>{
	//check if a username is valid
	if(data.username.length < 6){
		return cb(true, 'Username invalid');
	}
	User.collection.findOne({username: data.username}, {username: 1}, (err, user)=>{
		if(err){
			return cb(true);
		}
		if(user){
			return cb(true, 'Username taken')
		}
		cb();
	});
}

User.createUsername = (data, cb)=>{
	User.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.user_id)}, {$set: {username: data.username}}, (err, updated_user)=>{
		if(err){
			return cb();
		}
		if(!updated_user){
			return cb();
		}
		cb(updated_user);
	});
}

User.signup = (user, cb) =>{
	User.collection.findOne({email: user.email.toLowerCase()}, (err, foundUser)=>{
		if(err){
			return cb(null, 'Network error');
		}
		if(foundUser){
			return cb(null, 'Email exists');
		}
		let saltRounds = 10;
		bcrypt.hash(user.password, saltRounds, function(err, hash) {
		  	user.password = hash;
	    	user.email = user.email.toLowerCase();
	    	user.username = user.username.toLowerCase();
		    user.access_token = token(user.email);
		    user.verification_token = token(user.username);
		    let newUser = new User(user);
		    User.collection.save(newUser, (err, saved)=>{
		    	//send a verification email
		    	Mail.sendMail(user.email, 'verify', {verification_token: user.verification_token}, ()=>{
			    	cb(newUser)
		    	});
		    });
		});
	});
};

User.login = (user, cb) =>{
	User.collection.findOne({$or: [{email: user.email.toLowerCase()}, {username: user.email}]}, (err, foundUser)=>{
		if(err){
			return cb();
		}
		if(!foundUser){
			return cb();
		}

		bcrypt.compare(user.password, foundUser.password, function(err, res) {
			if(err){
				return cb();
			}
		  	if(res) {
				// Passwords match
				//update user access token
			    foundUser.access_token = token(foundUser.email);
			    User.collection.findOneAndUpdate({_id: foundUser._id}, {$set: {access_token: foundUser.access_token}}, (err, updated)=>{
			   		return cb(foundUser)
			    })
		  	} else {
			   // Passwords don't match
		   		return cb()
		  	} 
		});

	});
}

User.socialLogin = (user, cb) => {
	let newUser = {};
	newUser.social_profile = JSON.parse(user._profile);
	newUser.social_provider = user._provider;
	newUser.social_token = JSON.parse(user._token);
	newUser.access_token = token(newUser.social_profile.id);
	newUser = new User(newUser);

	User.collection.findOne({social_provider: newUser.social_provider, "social_profile.id": newUser.social_profile.id}, (err, foundUser)=>{
		if(err){
			return cb()
		}
		if(foundUser){
			return cb(foundUser)
		} else {
			//no user found, save user - check for provider if facebook change tokens
			if(newUser.social_provider === 'facebook'){
				Facebook.getLongToken(newUser.social_token.accessToken).then((facebookResponse)=>{
					newUser.social_token.long_live_token = facebookResponse.access_token;
					User.collection.save(newUser, cb(newUser))
				})
			} else {
				User.collection.save(newUser, cb(newUser))				
			}
		}
	});
};

User.findByToken = (token, cb)=>{
	User.collection.findOne({access_token: token}, (err, user)=>{
		if(err){
			return cb();
		}
		if(!user){
			return cb();
		}
		delete user.password;
		cb(user);
	});
};

User.fetchOneByUsername = (data, cb)=>{
	//data includes {access_token: , username: } where username is the name of the user being looked up 
	User.collection.findOne({username: data.username}, {fields: {access_token: 0, password: 0}}, (err, user)=>{
		if(err){
			return cb();
		}
		if(!user){
			return cb();
		}
		cb(user);
	});
}

User.recentGroup = (data, cb)=>{
	//data includes {user_id, group_id}
	User.collection.findOneAndUpdate({_id: data.user_id}, {$set: {recent_group: data.recent_group}}, (err, updatedUser)=>{
		if(err){
			return cb();
		}
		cb(updatedUser);
	});
}


User.updateProfile = (data, cb) =>{
	let updateFields = {};
	// {status: data.status, bio: data.bio, place: data.place}

	for(var p in data){
		if(data.hasOwnProperty(p)){
			if(data[p] || data[p] === ''){
				updateFields[p] = data[p];
			}
		}
	}

	if(data.place.address){
		console.log('data.place', data);
		updateFields["location"] = 	{type: "Point", coordinates: [data.place.location.lng, data.place.location.lat] } // location should be -> { type: "Point", coordinates: [-73.856077, 40.848447]}
	}

	updateFields.last_updated = new Date();
	User.collection.findOneAndUpdate({access_token: data.access_token}, {$set : updateFields}, {new: true, returnOriginal: false}, (err, updated_user)=>{
		if(err){
			return cb();
		}
		if(!updated_user){
			return cb()
		}
		cb(updated_user.value);
	});
}

User.updateProfilePhoto = (data, cb)=>{
	User.findByToken(data.body.access_token, (user)=>{
		if(!user){
			return cb();
		}
		Upload.file(data.files.file, 'profile_photos/' + user.username, function(filePath){
			User.collection.findOneAndUpdate({_id: user._id}, {$set: {profile_photo: filePath}}, {new: true}, (err, updated_user)=>{
				updated_user.value.profile_photo = filePath;
				cb(updated_user.value);
			});
		});
	});
}

User.blockUser = (data, cb)=>{
	//add a username to a user's blocekd list, and add the user to the blocked_by list of the newly blocked user
	//or remove block and blocked_by
	let action = null;
	let action2 = null;
	if(!data.block){
		action = {$addToSet: {blocked: data.blocked.username}};
		action2 = {$addToSet: {blocked_by: data.blocker.username}};
	} else {
		action = {$pull: {blocked: data.blocked.username}};
		action2 = {$pull: {blocked_by: data.blocker.username}};
	}
	User.collection.findOneAndUpdate({access_token: data.blocker.access_token}, action, {upsert: true}, (err, updated_user)=>{
		if(err){
			cb();
		}
		if(!updated_user){
			cb();
		}
		User.collection.findOneAndUpdate({username: data.blocked.username}, action2, {upsert: true}, (err, updated_blocked_user)=>{
			if(err){
				return cb();
			}
			if(!updated_blocked_user){
				return cb();
			}
			cb(updated_user);
		});
	});
}

User.flagUser = (data, cb)=>{
	Flag.createOne(data, (flag)=>{
		cb(flag);
	});
}

User.findUsersByIdArray = (id_array, cb)=>{
	let id_arr = [];
	for(var i = 0; i < id_array.length; i++ ){
		let id = Mongo.ObjectID(id_array[i]);
		id_arr.push(id);
	}
	User.collection.find({_id: {$in: id_arr}}, {profile_photo: 1, username: 1, _id: 1}).toArray((err, users)=>{
		if(err){
			return cb();
		}
		if(!users){
			return cb([]);
		}
		cb(users);
	})
}


User.verify = (data, cb)=>{
	User.collection.findOneAndUpdate({verification_token: data.verification_token}, {$set: {verified: true}}, (err, updated_user)=>{
		if(err){
			return cb();
		}
		if(!updated_user){
			return cb()
		}
		cb(updated_user);
	});
}


User.sendVerificationEmail = (data, cb)=>{
	User.collection.findOne({email: data.email}, (err, user)=>{
		if(err){
			return cb()
		}
		if(!user){
			return cb();
		}
		Mail.sendMail(user.email, 'verify', {verification_token: user.verification_token}, (sent)=>{
			cb(sent);
		});
	});
}


User.resetPassword = (data, cb)=>{
	let saltRounds = 10;
	bcrypt.hash(data.password, saltRounds, function(err, hash) {
	    User.collection.findOneAndUpdate({email: data.email}, {$set: {password: hash}}, (err, updated_user)=>{
	    	if(err){
	    		return cb();
	    	}
	    	if(!updated_user){
	    		return cb();
	    	}
	    	cb(updated_user);
	    });
	});
}

User.fetchMany = (data, cb)=>{
	let matchQuery = null 
	let tagQuery = null;
	if(!data.topics || data.topics.length < 1){
		data.topics = [];
		tagQuery = {$nin: data.topics};
	} else {
		// tagQuery = {$in: data.topics}
		tagQuery = {$all: data.topics}
	}
	if(data.last_id){
		// matchQuery = {tags: tagQuery, _id: {$lt: Mongo.ObjectID(data.last_id)}};
		matchQuery = {$and: [{username: {$ne: null}}, {interests: tagQuery}, {_id: {$lt: Mongo.ObjectID(data.last_id)}}]};

	} else {
		matchQuery = {$and: [{username: {$ne: null}}, {interests: tagQuery}]};
	}

	var finalQuery = [
	    {
	    	$match: matchQuery
	    },
	]

	let geoQuery = {};
	if(data.place){
		geoQuery = {
		$geoNear: {
			near: { type: "Point", coordinates: [ data.place.location.lng, data.place.location.lat ] },
			distanceField: "dist.calculated",
			maxDistance: 15000,
			includeLocs: "dist.location",
			spherical: true
		}
		}

		finalQuery = [
			geoQuery,
		    {
		    	$match: matchQuery
		    },
			]

	}

	User.collection.aggregate(finalQuery).limit(100).sort({createdAt: -1}).toArray((err, users)=>{
			if(err){
				console.log('ERROR FETCHING USERS ' + JSON.stringify(err));
				return cb()
			}
			if(!users){
				return cb([])
			}
			cb(users);
		});
}



module.exports = User;


//helper functions
function rand(){
    return Math.random().toString(36).substr(2); // remove `0.`
}

function token(identifier){

	 var token = jwt.sign({identifier: identifier}, app_secret, {
	      expiresIn: '8760h'  // expires in 24 hours * 30
	    });

	 return token;
    // return rand() + rand(); // to make it longer
}
