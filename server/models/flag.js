'use strict';

const Mongo = require('mongodb');
const encrypt = require('./EncryptionHelper').encrypt;
const decrypt = require('./EncryptionHelper').decrypt;
function Flag(flag){
	this.flagged_user = encrypt(flag.flagged_user);
	this.flagged_by = encrypt(flag.flagged_by);
	this.text = encrypt(flag.text)
}

Object.defineProperty(Flag, 'collection', {
  get: function(){return global.mongodb.collection('flags');}
});

Flag.createOne = (flag, cb) => {
	//form contains title attribute
	let newFlag = new Flag(flag);
	Flag.collection.save(newFlag, cb(newFlag))
};


module.exports = Flag;

