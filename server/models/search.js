'use strict';

const Mongo = require('mongodb');

function Search(search){
	this.created_at = new Date();
	this.place = search.place;
	this.topics = search.topics;
	// this.provider = search.provider || null;
	this.username = search.username || null;
}

Object.defineProperty(Search, 'collection', {
  get: function(){return global.mongodb.collection('searches');}
});


Search.save =(data, cb)=>{
	// console.log('data', data);
	if(!data){
		return cb();
	}
	let newSearch = new Search(data);
	Search.collection.save(newSearch, cb);
}


module.exports = Search;

