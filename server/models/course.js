'use strict';

const Mongo = require('mongodb');
const tags = require('../references/references.js').getTags();
const User = require('./user');
const GG = require('./global_group');
const LG = require('./local_group');

function Course(course){
	this.created_at = new Date();
	this.title = course.title 
	this.city = course.city
	this.status = 0,
	this.tags = course.tags || [];
	this.global_group_members = 1;
	this.type = course.type || null;
}

Object.defineProperty(Course, 'collection', {
  get: function(){return global.mongodb.collection('courses');}
});

Course.createOne = (form, cb) => {
	//form contains title attribute
	let newCourse = new Course(form);
	Course.collection.save(newCourse, cb(newCourse))
};

Course.fetch = (cb) => {
	Course.collection.find({}).toArray((err, courses)=>{
		// console.log(courses.length);
		if(err){
			console.log('ERROR FETCHING COURSES: ' + JSON.stringify(err))
			return cb();
		}
		cb(courses);
	});
};


Course.delete = (example, cb) => {
	Course.collection.remove({_id: Mongo.ObjectId(example._id)}, (err, res)=>{
		cb(res);
	})
};

Course.findByTagsAndLocation = (data, cb) =>{
	// console.log('find by tags and location', data);
	let tagQuery = null;
	if(!data.topics || data.topics.length < 1){
		data.topics = [];
		tagQuery = {$nin: data.topics};
	} else {
		// tagQuery = {$in: data.topics}
		tagQuery = {$all: data.topics}
	}
	// console.log('find ', data);
	LG.findNearLocation(data, (local_groups)=>{
		// console.log('local groups', local_groups);
		if(local_groups.length > 0){
			// flip local groups to return associated courses with members
			localGroupsToCourses(local_groups, (courses)=>{
				cb(courses);
			});
		} else {
			//consider adding last_id when querying on multiple pages
			let query = {};
			var sort = {global_group_views: -1};		
			var limit = 10;
			if(data.last_id && !data.categories){
				query = {$and: [{type: {$exists: false}}, {tags: tagQuery}, {_id: {$gt: Mongo.ObjectID(data.last_id)}}]};
				// sort = {global_group_members: -1};
			} else if(!data.last_id && !data.categories){
				query = {tags: tagQuery, type: {$exists: false}}
			} else if(!data.last_id && data.categories) {
				query.tags = tagQuery;
				query.type = 'category';
				limit = 100;
			} else if(data.last_id && data.categories){
				query = {$and: [{type: 'category'}, {tags: tagQuery}, {_id: {$lt: Mongo.ObjectID(data.last_id)}}]};
				// sort = {global_group_views: -1};		
				limit = 1000;		
			}
			// console.log('query', JSON.stringify(query));
			Course.collection.find(query).limit(limit).sort(sort).toArray((err, courses)=>{
				if(!courses){
					return cb([]);
				}
				// console.log('courses', courses);
				cb(courses);
			});

		}
	});
}

function localGroupsToCourses(local_groups, cb){
	let courses = [];
	for(var i = 0; i < local_groups.length; i++){
		let course = local_groups[i].course;
		course.local_group_members = local_groups[i].members.length;
		course.local_group_views = local_groups[i].views;
		course.local_group = true;
		courses.push(course);
	}
	cb(courses);
}


Course.view = (data, cb)=>{
	//if the global group does not exist then create a global group for this course with the user as first member
	// console.log('retrieved user now adding to glboal group, user id : ', user._id)
	GG.addView({course: data.course}, (global_group)=>{
		Course.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.course._id)}, {$set: {global_group_views: global_group.views}}, (err, updated_course)=>{

			// console.log('added user as member to global group ' + JSON.stringify(global_group));
			//if the request to join comes in from a course that appeared in a local query and if a local group does not exist, create a local group with the user as first memeber
			LG.addView({course: data.course}, (local_group)=>{
				//if local group is null it means that the request to join had no place attribute (data.course.place), basically the operation was skipped
				// console.log('added user as member to local group ' + JSON.stringify(local_group));

				// stamp the user with most recent group
				// let recent_group = (local_group) ? {group_type: 'local', group_id: local_group._id} : {group_type: 'global', group_id: global_group._id};
				// User.recentGroup({user_id: user._id, recent_group: recent_group}, (updatedUser)=>{
				// 	cb(data.course);
				// });
				cb(data.course)
			});
		});
	});

}

Course.join = (data, cb) =>{
	User.findByToken(data.access_token, (user)=>{
		if(!user){
			return cb();
		}
		//add this user as a member of a global group associated with this course 
		//if the global group does not exist then create a global group for this course with the user as first member
		// console.log('retrieved user now adding to glboal group, user id : ', user._id)
		console.log('data', data);
		GG.addMember({user_id: user._id, course: data.course}, (global_group)=>{
			Course.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.course._id)}, {$set: {global_group_members: global_group.members.length}}, (err, updated_course)=>{

				// console.log('added user as member to global group ' + JSON.stringify(global_group));
				//if the request to join comes in from a course that appeared in a local query and if a local group does not exist, create a local group with the user as first memeber
				console.log('join memember')
				LG.addMember({user_id: user._id, course: data.course}, (local_group)=>{
					//if local group is null it means that the request to join had no place attribute (data.course.place), basically the operation was skipped
					// console.log('added user as member to local group ' + JSON.stringify(local_group));

					// stamp the user with most recent group
					let recent_group = (local_group) ? {group_type: 'local', group_id: local_group._id} : {group_type: 'global', group_id: global_group._id};
					User.recentGroup({user_id: user._id, recent_group: recent_group}, (updatedUser)=>{
						cb(data.course);
					});
				});
			});
		});
	})

}

Course.loadCourseGroupsGlobalMember = (data, cb)=>{
	// console.log('data to load groups', data);
	User.findByToken(data.access_token, (user)=>{
		if(!user){
			return cb();
		}
		// find all the global groups that the user belongs to
		// find all the local gorups the user belongs to 
		// iterate through local groups by global groups and wherever the course_id matches attach the local group to the global group object
		let groups = [];
		GG.findAllByMemberId(user._id, (global_groups)=>{
			if(!global_groups){
				return cb();
			}
			let global_groups_course_ids = [];
			global_groups.forEach((g)=>{
				global_groups_course_ids.push(g.course_id);
			})
			Course.collection.find({_id: {$in: global_groups_course_ids}}).toArray((err, courses)=>{
				//iterate through courses and assign course info to group with matching course_id
				for(var l = 0; l < courses.length; l++){
					for(var k = 0; k < global_groups.length; k++){
						if(courses[l]._id.toString() === global_groups[k].course_id.toString()){
							global_groups[k].course_info = courses[l];
						}
					}
				}
				//delete user password before sending user back
				delete user.password;

				cb({global_groups: global_groups, user: user});
			});
		});
	});
}

Course.loadCoursesGroups = (data,cb) =>{
	// console.log('data to load groups', data);
	User.findByToken(data.access_token, (user)=>{
		if(!user){
			return cb();
		}
		// find all the global groups that the user belongs to
		// find all the local gorups the user belongs to 
		// iterate through local groups by global groups and wherever the course_id matches attach the local group to the global group object
		let groups = [];
		GG.findAllByMemberId(user._id, (global_groups)=>{
			LG.findAllByMemberId(user._id, (local_groups)=>{
				for(var i = 0; i < local_groups.length; i++){
					for(var j = 0; j < global_groups.length; j++){
						global_groups[j].local_groups = [];
						if(local_groups[i].course_id.toString() === global_groups[j].course_id.toString()){
							global_groups[j].local_groups.push(local_groups[i]._id);
						}
					}
				}
				//retrieve course info for each group before sending
				let global_groups_course_ids = [];
				global_groups.forEach((g)=>{
					global_groups_course_ids.push(g.course_id);
				})
				Course.collection.find({_id: {$in: global_groups_course_ids}}).toArray((err, courses)=>{
					//iterate through courses and assign course info to group with matching course_id
					for(var l = 0; l < courses.length; l++){
						for(var k = 0; k < global_groups.length; k++){
							if(courses[l]._id.toString() === global_groups[k].course_id.toString()){
								global_groups[k].course_info = courses[l];
							}
						}
					}
					//delete user password before sending user back
					delete user.password;

					cb({global_groups: global_groups, user: user});
				});


			});
		});
	});
}

Course.loadCourseGroups = (data, cb) =>{
	User.findByToken(data.access_token, (user)=>{
		if(!user){
			return cb();
		}
		let courseIdUserId = {user_id: user._id, course_id: Mongo.ObjectID(data.course_id)};
		GG.findOneByCourseIdAndMemberId(courseIdUserId, (global_group)=>{
			// console.log('global', global_group);
			LG.findByCourseIdAndMemberId(courseIdUserId, (local_groups)=>{
				global_group.local_groups = [];
				for(var i = 0; i < local_groups.length; i++){
					global_group.local_groups.push(local_groups[i]._id);
				}
				Course.collection.findOne({_id: courseIdUserId.course_id}, (err, course)=>{
					global_group.course_info = course;
					cb(global_group);
				});
			});
		});
	});
}

Course.loadCourseGroupsLocalMember = (data, cb)=>{
	// console.log('data for fetching local groups', data); // includes user_id, course_id
	data.user_id = Mongo.ObjectID(data.user_id);
	data.course_id = Mongo.ObjectID(data.course_id);
	//find local gorups belonging to this course in which the user is a member
	LG.findByCourseIdAndMemberId(data, (local_groups)=>{
		if(!local_groups){
			return cb([]);
		}
		cb(local_groups)
	});

};

Course.loadCourseGroupGlobal = (data, cb)=>{
	// console.log('data course', data)
	Course.collection.findOne({_id: Mongo.ObjectID(data.course_id)}, (err, course)=>{
		GG.findOneByCourseId(Mongo.ObjectID(data.course_id), (group)=>{
			if(!group){
				return cb()
			}
			cb({course: course, group: group})
		});
	});
};

Course.findById = (id, cb) =>{
	Course.collection.findOne({_id: id}, (err, course)=>{
		if(!course){
			return cb();
		}
		cb(course);
	});
};

Course.findByTitle = (title, cb)=>{
	Course.collection.findOne({title: title}, (err, course)=>{
		if(!course){
			return cb();
		}
		cb(course);
	});
}

Course.fetchMainCategories = (data, cb)=>{
	var query = {type: 'category'}
	if(data.topics && data.topics.length > 0){
		var tagQuery = {$all: data.topics}
		query.tags = tagQuery;
	}
	console.log('query', query);
	Course.collection.find(query).toArray((err, categories)=>{
		if(err){
			return cb();
		}
		cb(categories);
	});
}


module.exports = Course;

