'use strict';

const Mongo = require('mongodb');
const Mail = require('../lib/mail/mail.js')
function ResetToken(token){
	this.created_at = new Date();
	this.token = token.token;
	this.email = token.email;
}

Object.defineProperty(ResetToken, 'collection', {
  get: function(){return global.mongodb.collection('resettokens');}
});

ResetToken.createOne = (data, cb) => {
	//form contains title attribute
	data.token = rand();
	let resetToken = new ResetToken(data);
	ResetToken.collection.findOneAndUpdate({email: data.email}, resetToken, {upsert: true}, ()=>{
		//send an email then callback
		Mail.sendMail(data.email, 'reset_password', data, (info)=>{
			if(!info){
				return cb()
			}
			cb(resetToken);
		});
	});
};

ResetToken.findToken = (data, cb)=>{
	ResetToken.collection.findOne({token: data.token, email: data.email}, (err, token)=>{
		if(err){
			return cb();
		}
		if(!token){
			return cb();
		}
		ResetToken.collection.deleteOne({token: data.token, email: data.email}, (err, deleted_token)=>{
			cb(token);
		})
	});
};



module.exports = ResetToken;

function rand(){
    return Math.random().toString(36).substr(2); // remove `0.`
}
