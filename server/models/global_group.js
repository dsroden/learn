'use strict';

const Mongo = require('mongodb');

function GlobalGroup(data){
	this.created_at = new Date();
	this.course_id = Mongo.ObjectID(data.course._id);
	this.tags = data.course.tags;
	this.members = data.members || [];
	this.type = data.type
	this.views = 1;
}

Object.defineProperty(GlobalGroup, 'collection', {
  get: function(){return global.mongodb.collection('globalgroups');}
});


GlobalGroup.create =(data, cb)=>{
	// data.members = [data.user_id];
	data.type = data.course.type;
	let newGroup = new GlobalGroup(data);
	GlobalGroup.collection.insert(newGroup, (err, group)=>{
		cb(group.ops[0])
	});
}


GlobalGroup.addView = (data, cb)=>{
	//data includes {user_id: ObjectId , course: {_id: , title: , provider: ,...}}
	GlobalGroup.collection.findOneAndUpdate({course_id: Mongo.ObjectId(data.course._id)}, {$inc: {views: 1}}, (err, updated_group)=>{
		// console.log('updated group', updated_group);
		if(updated_group.value){
			return cb(updated_group.value);
		}
		GlobalGroup.create(data, (new_global_group)=>{
			// console.log('new global group', new_global_group);
			cb(new_global_group);
		});
	});
}

GlobalGroup.addMember = (data, cb)=>{
	//data includes {user_id: ObjectId , course: {_id: , title: , provider: ,...}}
	console.log('add member to global group', data);
	GlobalGroup.collection.findOneAndUpdate({course_id: Mongo.ObjectId(data.course.course_id)}, {$addToSet: {members: data.user_id}}, (err, updated_group)=>{
		// console.log('updated group', updated_group);
		if(updated_group.value){
			return cb(updated_group.value);
		}
		GlobalGroup.create(data, (new_global_group)=>{
			// console.log('new global group', new_global_group);
			cb(new_global_group);
		});
	});
}


GlobalGroup.removeMember = (data, cb) =>{
	//data is {user_id: , group_id: }

	GlobalGroup.collection.findOneAndUpdate({_id: Mongo.ObjectID(data.group_id)}, {$pull: {members: Mongo.ObjectID(data.user_id)}}, (err, updated)=>{
		console.log('err', err);
		if(err){
			return cb();
		}
		console.log('udpated group, removed member', updated)
		cb(updated);
	});
};



GlobalGroup.findAllByMemberId = (user_id, cb)=>{
	GlobalGroup.collection.find({ members: { "$in" : [Mongo.ObjectID(user_id)]}}).toArray((err, global_groups)=>{
		if(err){
			return cb();
		}
		cb(global_groups);
	})
}

GlobalGroup.findOneByCourseIdAndMemberId = (data, cb) =>{
	//data is {user_id : , course_id: }
	GlobalGroup.collection.findOne({$and: [{course_id: Mongo.ObjectID(data.course_id)}, {members: {"$in": [Mongo.ObjectID(data.user_id)]}}]}, (err, global_group)=>{
		if(err){
			return cb()
		}
		cb(global_group);
	});
}

GlobalGroup.findOneByCourseId = (course_id, cb)=>{
	GlobalGroup.collection.findOne({course_id: Mongo.ObjectID(course_id)}, (err, global_group)=>{
		if(err){
			console.log('err', err);
			return cb()
		}
		cb(global_group);
	});
}

GlobalGroup.findById = (id, cb)=>{
	GlobalGroup.collection.findOne({_id: Mongo.ObjectID(id)}, (err, group)=>{
		if(err){
			return cb();
		}
		cb(group);
	});
}

GlobalGroup.findByIdAndMember = (data, cb)=>{
	GlobalGroup.collection.findOne({_id: Mongo.ObjectID(data.group_id), members: {$in: [Mongo.ObjectID(data.member_id)]}}, (err, group)=>{
		if(err){
			return cb();
		}
		cb(group);
	});	
}

GlobalGroup.removeMemberByCourseIdMemberId = (data, cb)=>{
	GlobalGroup.collection.findOneAndUpdate({course_id: Mongo.ObjectID(data.course_id)}, {$pull: {members: Mongo.ObjectID(data.user_id)}}, (err, updated_group)=>{
		if(err){
			return cb();
		}
		cb(updated_group);
	});
}

GlobalGroup.fetchMembers = (group, cb) =>{
		
	require('./user').findUsersByIdArray(group.members, (users)=>{
		if(!users){
			return cb();
		}
		cb(users);
	});

}

GlobalGroup.addCoach = (data, cb)=>{
	//data includes {user_id: ObjectId , course: {_id: , title: , provider: ,...}}
	GlobalGroup.collection.findOneAndUpdate({course_id: Mongo.ObjectId(data.course._id)}, {$addToSet: {coaches: data.user_id}}, (err, updated_group)=>{
		// console.log('updated group', updated_group);
		if(updated_group.value){
			return cb(updated_group.value);
		}
		GlobalGroup.create(data, (new_global_group)=>{
			// console.log('new global group', new_global_group);
			cb(new_global_group);
		});
	});
}

GlobalGroup.findAllByCoachId = (coach_id, cb)=>{
	GlobalGroup.collection.aggregate([
	    {
	        $lookup: {
	           from: "courses",
	           localField: "course_id",
	           foreignField: "_id",
	           as: "course"
	        }
	    },
	    {
	        $unwind: "$course"
	    },
	    {
	    	$match: { coaches: { "$in" : [Mongo.ObjectID(coach_id)]}}
	    }
    ]).toArray(function(err, groups){
        if(err){
        	console.log('ERROR FETCHING LOCAL GROUPS: ' + JSON.stringify(err));
            return cb([]);
        }
        if(!groups){
        	console.log('NO LOCAL GROUPS FOUND');
        	groups = [];
        }
 		cb(groups)
    });
}

GlobalGroup.fetchMember = (data, cb) =>{
	GlobalGroup.collection.findOne({_id: Mongo.ObjectID(data.group._id), members: {$in: [Mongo.ObjectID(data.user._id)]}}, (err, group)=>{
		if(err){
			return cb();
		}
		return cb(group);
	})
}




module.exports = GlobalGroup;
