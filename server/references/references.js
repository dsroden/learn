'use strict';
const Utils = require('../lib/utils');
const fs = require('fs');

function Ref(){}

Ref.getTags = ()=>{
	let data = require('./tags.json');
	let tags = JSON.parse(data[0].tags);
	let namedTags = []
	for(var i = 0; i < tags.length; i++){
		namedTags.push({name: tags[i]});
	}
	return namedTags;
}

module.exports = Ref;
