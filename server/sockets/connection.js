'use strict';

const Message = require('../models/message');

module.exports = function(socket){
  let scServer = this;

  socket.on('setAuth', require('./authenticate'))
  socket.on('publish', (data)=>{
    // console.log(data)
  })
  socket.on('private_message', (data, respond)=>{
    let channelId = data.channelId;
    // console.log(channelId)
    let subscribed = scServer.exchange.isSubscribed(channelId);
    // console.log('channelid>>>>>>>>>', channelId);
    if(subscribed){
      scServer.exchange.publish(channelId, data);
      respond();
    } else {
      //subscribe and watch this channel
      scServer.exchange.subscribe(channelId).watch((data)=>{
        //data being watched here shoudl be stored
        Message.saveFromChannel(data)
        respond();
      });
      scServer.exchange.publish(channelId, data);

    }

  })
  socket.on('subscribe', (channelId, other, other1)=>{
    let subscribed = scServer.exchange.isSubscribed(channelId);
    // console.log('channelid>>>>>>>>>', channelId);
    if(subscribed){
      // console.log('exchange already subscribed to this channel, new client side subscription', channelId);
    } else {
      // console.log('not subscribed yet');
      //subscribe and watch this channel
      scServer.exchange.subscribe(channelId).watch((data)=>{
        //data being watched here shoudl be stored
        // console.log('data watched', data);
        Message.saveFromChannel(data)
      });
    }
  });



  // socket.emit('online');
  // socket.on('my_room', require('./myRoom'));
  // socket.on('disconnect', require('./disconnect'));
  // socket.on('test', require('./test'));
  // socket.on('join_chat', require('./joinChat'));
  // socket.on('message', require('./message'));
  // socket.on('leave_chat', require('./leaveChat'));
};
