'use strict';
// var LOGGER = require('../lib/logger.js');
module.exports = function(data){
  var socket = this;
  //check that this room hasn't been closed by one of the users due to blocking a connection 

  //data should include: sender, receiver, room (connectionId), payload (text, img, etc)
  socket.leave(data.connectionId);
};