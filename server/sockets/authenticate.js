'use strict';

const User = require('../models/user');

module.exports = function(credentials, respond){
  var socket = this;
  // console.log('credentials', credentials);
  User.findByToken(credentials.access_token, (user)=>{
  	if(!user){
  		respond('login failed');
  	} else {
  	  respond();

      // This will give the client a token so that they won't
      // have to login again if they lose their connection
      // or revisit the app at a later time.
      socket.setAuthToken({username: credentials.username});
  	}
  });
};