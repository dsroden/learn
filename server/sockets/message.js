'use strict';
// var LOGGER = require('../lib/logger.js'),
var   Message = require('../models/message');

module.exports = function(data){
  // console.log('message emitted on client', data);
  var socket = this;
  //data should include: sender, receiver, room (connectionId), payload (text, img, etc)
  // LOGGER.log('DATA FROM MESSAGE' + JSON.stringify(data.room.id));
 
  //message needs to be saved and then emitted to chat
  Message.saveChatMsg(data, function(){
  	if(data.count < 1){
	  	socket.in(data.receiver._id).emit('update_user', {msg: data});
  	} else {
    	socket.in(data.connectionId).emit('update_chat', {text: data.payload.text});
	}
  });
};