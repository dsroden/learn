var geolite2 = require('geolite2');
var maxmind = require('maxmind');

exports.ipLookup = (req, res)=>{
	var lookup = maxmind.openSync(geolite2.paths.city); // or geolite2.paths.country
	var info = lookup.get(req.ip);
	if(!info){
		return res.status(400).end()
	}
	let city_name = (info.city && info.city.names) ? info.city.names.en : null;
	let country_name = (info.country && info.country.names.en) ? info.country.names.en : null;
	let address = null;
	if(city_name && country_name){
		address = city_name + ', ' + country_name;
	}
	let location = null;
	if(info.location){
		location = {lat: info.location.latitude, long: info.location.longitude};
	}
	if(location && address){
		res.send({address: address, location: location}).end();
	} else {
		res.status(400).end();
	}

}