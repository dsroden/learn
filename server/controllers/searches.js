'use strict';


var Example = require('../models/example');
const tags = require('../references/references.js').getTags();
const Course = require('../models/course');
const Search = require('../models/search');
const User = require('../models/user');

console.log('tags', tags.length)
exports.topicAndLocation = (req, res)=>{
	Search.save(req.body, ()=>{
		Course.findByTagsAndLocation(req.body, (items)=>{
			res.send({courses: items}).end();
		});
	});
}

exports.topics = (req, res)=>{
	res.send(tags).end();
};


exports.fetchPeople = (req, res)=>{
	User.fetchMany(req.body, (users)=>{
		if(!users){
			return res.status(400).end()
		} else {
			return res.send(users).end();
		}
	});
};

