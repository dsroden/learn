'use strict';

const Course = require('../models/course');

exports.fetchAll = (req, res)=>{
	Course.fetchMainCategories(req.body, (categories)=>{
		if(!categories){
			return res.status(400).end();
		} else {
			return res.send(categories).end();
		}
	});
}

