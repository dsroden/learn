'use strict';


var Message = require('../models/message');

//fetch documents
exports.expireMessage = (req, res)=>{
	// console.log('load them', req.body);
	Message.expireOne(req.body, function(expired_msg){
		if(!expired_msg){
			res.status(400).end();
		} else {
			res.send(expired_msg).end();
		}
	});
};

