'use strict';


var Message = require('../models/message');

//fetch documents
exports.loadMessages = (req, res)=>{
	// console.log('load them', req.body);
	Message.fetchForChannel(req.body, function(messages){
		if(!messages){
			res.status(400).end();
		} else {
			res.send(messages).end();
		}
	});
};

