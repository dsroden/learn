'use strict';

const User = require('../models/user');
const GG = require('../models/global_group');
const LG = require('../models/local_group');

exports.joinGroup = (req, res)=>{
	User.findByToken(req.body.access_token, (user)=>{
		GG.addCoach({user_id: user._id, course: req.body.course}, (global_group)=>{
			if(!global_group){
				res.status(400).end()
			} else {
				res.send(global_group).end();
			}
		});
	});
}

exports.fetchGroups = (req, res)=>{
	GG.findAllByCoachId(req.body.coach_id, (global_groups)=>{
		if(!global_groups){
			res.status(400).end()
		} else {
			res.send(global_groups).end();
		}
	});
}