'use strict';


const GG = require('../models/global_group');
const LG = require('../models/local_group');
const Course = require('../models/course');
const User = require('../models/user');

//fetch documents
exports.fetchRecent = (req, res)=>{
	// console.log('load them', req.body);
	// User.findByToken(req.body.access_token, (user)=>{
	// 	if(user){
	// 		return res.status(400).end();
	// 	}
		// console.log('req body', req.body);
		if(req.body.recent_group.group_type === 'global'){
			GG.findByIdAndMember({member_id: req.body.user_id, group_id: req.body.recent_group.group_id}, (group)=>{
				if(!group){
					return res.status(400).end();
				}
				Course.findById(group.course_id, (course)=>{
					if(!course){
						return res.status(400).end();
					}
					res.send({group: group, course: course}).end();
				});
			});
		} else if(req.body.recent_group.group_type === 'local'){
			LG.findByIdAndMember({member_id:  req.body.user_id, group_id: req.body.recent_group.group_id}, (group)=>{
				if(!group){
					return res.status(400).end();
				}
				Course.findById(group.course_id, (course)=>{
					if(!course){
						return res.status(400).end();
					}
					res.send({group: group, course: course}).end();
				});
			});
		} else {
			return res.status(400).end();
		}
	// });
};

exports.fetchGlobalGroupByMember = (req, res)=>{
	GG.findAllByMemberId(req.body.user_id, (groups)=>{
		console.log('groups', groups);
		if(!groups || groups.length < 1){
			return res.status(400).end();
		}
		let group = groups[0]
		Course.findById(group.course_id, (course)=>{
			if(!course){
				return res.status(400).end();
			}
			res.send({group: group, course: course}).end();
		});
	});
}

exports.fetchLocalGroupByMember = (req, res)=>{
	LG.findAllByMemberId(req.body.user_id, (groups)=>{
		console.log('groups', groups);
		if(!groups || groups.length < 1){
			return res.status(400).end();
		}
		let group = groups[0]
		Course.findById(group.course_id, (course)=>{
			if(!course){
				return res.status(400).end();
			}
			res.send({group: group, course: course}).end();
		});
	});
}

exports.fetchGroupMembers = (req, res)=>{
	if(req.body.group.address){
		LG.fetchMembers(req.body.group, (members)=>{
			if(!members){
				res.status(400).end();
			} else {
				res.send(members).end();
			}
		});
	} else {
		GG.fetchMembers(req.body.group, (members)=>{
			if(!members){
				return res.status(400).end();
			} else {
				res.send(members).end();
			}
		});
	}
}

exports.checkIfMember = (req, res)=>{
	if(req.body.group.address){
		LG.fetchMember(req.body, (member)=>{
			if(!member){
				return res.status(400).end()
			}
			res.send(member).end();
		})
	} else {
		GG.fetchMember(req.body, (member)=>{
			if(!member){
				return res.status(400).end()
			}
			res.send(member).end();
		})
	}
}



