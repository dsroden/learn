'use strict';


var Meeting = require('../models/meeting');

//fetch documents
exports.createOne = (req, res)=>{
	Meeting.createOne(req.body, (meeting)=>{
		if(!meeting){
			return res.status(400).end()
		}
		res.send(meeting).end();
	})
};

exports.fetchByGroupId = (req, res)=>{
	Meeting.fetchByGroupId(req.body.group_id, (meetings)=>{
		if(!meetings){
			return res.status(400).end()
		}
		res.send(meetings).end();
	})
};

exports.fetchDetails = (req, res) =>{
	Meeting.fetchDetails(req.body.meeting_id, (meeting)=>{
		if(!meeting){
			return res.status(400).end()
		}
		res.send(meeting).end();
	});
};

exports.joinMeeting = (req, res) =>{
	Meeting.joinMeeting(req.body, (joined)=>{
		if(!joined){
			return res.status(400).end()
		}
		res.send(joined).end();
	});	
}

exports.leaveMeeting = (req, res) =>{
	Meeting.leaveMeeting(req.body, (left)=>{
		if(!left){
			return res.status(400).end()
		}
		res.send(left).end();
	});	
}
