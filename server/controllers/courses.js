'use strict';


var Course = require('../models/course');
var LocalGroup = require('../models/local_group')
var GlobalGroup = require('../models/global_group')
const Suggestion = require('../models/suggestion');
const Message = require('../models/message')
//fetch documents
exports.fetch = (req, res)=>{
	Course.fetch(function(courses){
		if(!courses){
			res.status(400).end();
		} else {
			res.send({courses: courses}).end();
		}
	});
};

exports.join = (req, res)=>{
	Course.join(req.body, (courseJoined)=>{
		if(!courseJoined){
			res.status(400).end();
		} else {
			res.send(courseJoined).end();
		}
	});
};


exports.view = (req, res)=>{
	Course.view(req.body, (courseViewed)=>{
		if(!courseViewed){
			res.status(400).end();
		} else {
			res.send(courseViewed).end();
		}
	});
};


exports.loadCourseGroupsGlobalMember = (req, res) =>{
	Course.loadCourseGroupsGlobalMember(req.body, (groups)=>{
		if(!groups){
			return res.status(400).end();
		}
		res.send(groups).end();
	});	
}

exports.loadCoursesGroups = (req, res) => {
	Course.loadCoursesGroups(req.body, (groups)=>{
		if(!groups){
			return res.status(400).end();
		}
		res.send(groups).end();
	});
};

exports.loadCourseGroups = (req, res) => {
	Course.loadCourseGroups(req.body, (groups)=>{
	 if(!groups){
	 	return res.status(400).end();
	 }
	 res.send(groups).end();
	});
};

exports.loadCourseGroupsLocalMember = (req, res) =>{
	Course.loadCourseGroupsLocalMember(req.body, (groups)=>{
	 if(!groups){
	 	return res.status(400).end();
	 }
	 res.send(groups).end();
	});
}

exports.loadCourseGroupGlobal = (req, res) =>{
	Course.loadCourseGroupGlobal(req.body, (groups)=>{
		if(!groups){
			return res.status(400).end();
		}
		res.send(groups).end();
	});
}

exports.loadCourseGroupLocal = (req, res) =>{
	LocalGroup.loadCourseGroupLocal(req.body, (local_group)=>{
		if(!local_group){
			return res.status(400).end();
		}
		res.send(local_group).end();
	});
}

exports.searchLocalGroupsByPlace = (req, res) =>{
	LocalGroup.findByCourseAndPlace(req.body, (local_groups)=>{
		if(!local_groups){
			return res.status(400).end();
		}
		res.send(local_groups).end();
	});
}

exports.joinLocalGroup = (req, res) =>{
	LocalGroup.joinGroup(req.body, (joined_group)=>{
		if(!joined_group){
			return res.status(400).end();
		}
		res.send(joined_group).end();
	});
}

exports.viewLocalGroup = (req, res) =>{
	LocalGroup.viewGroup(req.body, (view_group)=>{
		if(!view_group){
			return res.status(400).end();
		}
		res.send(view_group).end();
	});
}

exports.createLocalGroup = (req, res) =>{
	LocalGroup.createGroup(req.body, (created_group)=>{
		if(!created_group){
			return res.status(400).end();
		}
		res.send(created_group).end();
	});
}

exports.fetchGlobalGroupByCourseIdMemberId = (req, res)=>{
	GlobalGroup.findOneByCourseIdAndMemberId(req.body, (group)=>{
		if(!group){
			return res.status(400).end();
		}
		res.send(group).end();		
	});
}

exports.fetchGroupByTitleAndLocation = (req, res)=>{
	Course.findByTitle(req.body.title, (course)=>{
		if(!course){
			return res.status(400).end();
		}
		if(req.body.address){
			let data = {course_id: course._id, place: {location: {lat: req.body.lat, lng: req.body.lng}}}
			LocalGroup.findByCourseAndPlace(data, (local_groups)=>{
				if(!local_groups){
					return res.status(400).end();
				} else {
					res.send({course: course, group: local_groups[0]}).end();
				}
			});
		} else {
			GlobalGroup.findOneByCourseId(course._id, (global_group)=>{
				if(!global_group){
					return res.status(400).end()
				} else {
					res.send({course: course, group: global_group}).end();
				}
			});
		}
	})	
}

exports.courseSuggestion = (req, res)=>{
	Suggestion.saveOne(req.body, (saved)=>{
		if(!saved){
			return res.status(400).end();
		} else {
			res.status(200).end()
		}
	})
}

exports.fetchGroupRecentMessages = (req, res)=>{
	let place = req.body.place;
	let channel_id = '';
	if(place){
		LocalGroup.findByCourseAndPlace(req.body, (local_groups)=>{
			//get recent messages 
			if(!local_groups || local_groups.length < 1 || !local_groups[0]._id){
				return res.status(400).end();
			} else {

				channel_id = local_groups[0]._id.toString();
				Message.fetchForChannel({channel_id: channel_id, limit: 3}, (messages)=>{
					if(!messages){
						return res.status(400).end();
					} else {
						res.send(messages).end();
					}
				});
			}
		});
	} else {
		GlobalGroup.findOneByCourseId(req.body.course_id, (global_group)=>{
			if(!global_group){
				return res.status(400).end()
			} else {
				channel_id = global_group._id.toString();
				Message.fetchForChannel({channel_id: channel_id, limit: 3}, (messages)=>{
					if(!messages){
						return res.status(400).end();
					} else {
						res.send(messages).end();
					}
				});
			}
		});
	}
}

exports.fetchByTitle = (req, res)=>{
	// console.log('fetch by title', req.body);
	Course.findByTitle(req.body.course_title, (course)=>{
		if(!course){
			return res.status(400).end();
		}
		res.send(course).end();
	});
}
