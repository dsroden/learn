'use strict';

const request = require('request-promise');  
const api_key = 'AIzaSyCa7tM-SXkFtCJw3rBy3FkBtRvO34jSyxU'
// const place_url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCa7tM-SXkFtCJw3rBy3FkBtRvO34jSyxU&libraries=places"
let place_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Vict&types=geocode&language=fr&key=" + api_key

exports.queryGooglePlaces = (req, res) =>{
	console.log('REQUEST BODY IN GOOGLE QUERY', req.body);
	let query = req.body.query;
	let google_api_url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${query}&types=geocode&language=en&key=${api_key}`;
	  const options = {
	    method: 'GET',
	    uri: google_api_url
	  };
	let prom = new Promise((resolve, reject)=>{
    request(options)
    .then(response => {
      if(!response){
        reject();
      }
      response = JSON.parse(response);
      console.log('RESPONSE FROM GOOGLE API')
      resolve(response);
    }).catch((err)=>{
      reject(err)
    });
  })

  prom.then((response)=>{
    res.send(response).end();
  }).catch((err)=>{
    console.log('ERROR GETTTING LONG TOKEN' + JSON.stringify(err))
    res.status(400).end();
  })

  return prom;

}

// https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

exports.queryGoogleGeo = (req, res) =>{
	console.log('REQUEST BODY IN GOOGLE QUERY', req.body);
	let query = req.body.query;
	let google_api_url = `https://maps.googleapis.com/maps/api/geocode/json?address=${query}&key=${api_key}`;
	  const options = {
	    method: 'GET',
	    uri: google_api_url
	  };
	let prom = new Promise((resolve, reject)=>{
    request(options)
    .then(response => {
      if(!response){
        reject();
      }
      response = JSON.parse(response);
      console.log('RESPONSE FROM GOOGLE API')
      resolve(response);
    }).catch((err)=>{
      reject(err)
    });
  })

  prom.then((response)=>{
    res.send(response).end();
  }).catch((err)=>{
    console.log('ERROR GETTTING LONG TOKEN' + JSON.stringify(err))
    res.status(400).end();
  })

  return prom;

}