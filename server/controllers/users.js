'user strict';

const User = require('../models/user');
const Message  = require('../models/message');
const  jwt = require('jsonwebtoken');
const  jwtSecret = 'jwtSecret';
const GG = require('../models/global_group.js');
const LG = require('../models/local_group.js');
const ResetToken = require('../models/reset-token.js')

exports.checkUsername = function(req, res){
	User.checkUsername(req.body, function(user, err){
		if(!user){
			res.status(200).end();
		} else {
			res.status(400).send({message: err});
		}
	});	
}


exports.signup = function(req, res){
	User.signup(req.body, function(user, err){
		if(!user){
			res.status(400).send({message: err});
		} else {
			res.send(user).end();
		}
	});
};

exports.login = function(req, res){
	User.login(req.body, function(user){
		if(!user){
			res.status(400).end();
		} else {
			res.send(user).end();
		}
	});
};


exports.socialLogin = function(req, res){
	User.socialLogin(req.body, function(user){
		if(!user){
			res.status(400).end();
		} else {
			res.send(user).end();
		}
	});
};

exports.createUsername = function(req, res){
	User.createUsername(req.body, function(user){
		if(!user){
			res.status(400).end();
		} else {
			res.send(user).end();
		}
	});	
}


exports.setSocketToken = function(req, res){
	// console.log('req.body', req.body);
	var token = jwt.sign(req.body, jwtSecret, { expiresIn : 60*60*24 });
	// console.log('token set', token);
	// res.send({token: token}).end();
	res.json({token: token});
}

exports.fetchOneByUsername = (req, res)=>{
	User.fetchOneByUsername(req.body, (user)=>{
		if(!user){
			res.status(400).end();
		} else {
			res.send(user).end();
		}
	});
};

exports.fetchConversationHistory = (req, res) =>{
	Message.fetchConversationHistory(req.body, (messages)=>{
		if(!messages){
			return res.status(400).end()
		}
		res.send(messages).end();
	});
};

exports.fetchContacts = (req, res) =>{
	Message.fetchConversationsContacts(req.body, (contacts)=>{
		if(!contacts){
			return res.status(400).end()
		}
		res.send(contacts).end();		
	});
}

exports.leaveGlobalGroup = (req, res) =>{
	GG.removeMember(req.body, (removed)=>{
		if(!removed){
			return res.status(400).end();
		}
		res.send(removed).end();
	});
}

exports.leaveLocalGroup = (req, res) =>{
	LG.removeMember(req.body, (removed)=>{
		if(!removed){
			return res.status(400).end();
		}
		res.send(removed).end();
	});
}

exports.leaveCourse = (req, res)=>{
	GG.removeMemberByCourseIdMemberId(req.body, (removed)=>{
		LG.removeMemberByCourseIdMemberId(req.body, (removed_local)=>{
			res.status(200).end()
		});
	});
}

exports.fetchSelf = (req, res) =>{
	User.findByToken(req.body.access_token, (user)=>{
		if(!user){
			return res.status(400).end();
		}
		res.send(user).end();
	})
}

exports.updateUserProfile = (req, res) =>{
	User.updateProfile(req.body, (user)=>{
		if(!user){
			return res.status(200).end();
		} else {
			res.send(user).end()
		}
	})
}

exports.updateUserProfilePhoto = (req, res) =>{
	User.updateProfilePhoto(req, (user)=>{
		if(!user){
			return res.status(400).end();
		}
		res.send(user).end();
	});
}

exports.blockUser = (req, res) =>{
	User.blockUser(req.body, (user_blocked)=>{
		if(!user_blocked){
			return res.status(400).end();
		}
		res.send(user_blocked).end();
	});
}

exports.flagUser = (req, res) =>{
	User.flagUser(req.body, (user_blocked)=>{
		if(!user_blocked){
			return res.status(400).end();
		}
		res.send(user_blocked).end();
	});	
}

exports.verifyUser = (req, res) =>{
	User.verify(req.body, (user_verified)=>{
		if(!user_verified){
			return res.status(400).end();
		}
		res.send(user_verified).end();
	});	
}

exports.sendVerificationEmail = (req, res) =>{
	User.sendVerificationEmail(req.body, (sent)=>{
		if(sent){
			return res.status(400).end();
		}
		res.send(sent).end();
	});	
} 

exports.resetPasswordRequest = (req, res)=>{
	console.log('reset password request', req.body);
	ResetToken.createOne(req.body, (sent)=>{
		console.log('sent', sent);
		if(!sent){
			return res.status(400).end();
		}
		res.send(sent).end();		
	});
}

exports.resetPassword = (req, res)=>{
	console.log('reset password', req.body);
	ResetToken.findToken(req.body, (token)=>{
		console.log('token retrieved', token);
		if(!token){
			return res.status(400).end();
		}
		User.resetPassword(req.body, (user)=>{
			console.log('user updated', user);
			if(!user){
				return res.status(400).end();
			}
			res.send(user).end();
		});
	});
}
