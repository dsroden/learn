'use strict';

const Extension = require('../models/extension')
var jwt = require('jsonwebtoken');

exports.createOne = (req, res)=>{
	Extension.createOne(req.body, (extension)=>{
		if(!extension){
			return res.status(400).end();
		}
		res.send(extension).end();
	});
}
