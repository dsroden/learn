import { createStore } from 'redux';
// import { googleAnalytics } from './reactGAMiddlewares'

import AppReducer from './Reducers'

const store = createStore(
	AppReducer

	);

export default store;
