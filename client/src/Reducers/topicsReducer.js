import { updateSearchTopic } from "../Actions/actionTypes";

const initialState = { topic: null, };

const topicsReducer = (state = initialState, action) => {
  switch (action.type) {
    case updateSearchTopic:
	    state = Object.assign({}, state, { topic: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default topicsReducer;