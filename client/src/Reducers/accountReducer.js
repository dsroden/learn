import { accountStateUpdated, blockUser, unblockUser, createdUsername, updateUser} from "../Actions/actionTypes";

const initialState = { user: null, account_state: null, blocked_user: null, unblocked_user: null, username: null};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case accountStateUpdated:
	    state = Object.assign({}, state, { account_state: action.data, loading:false });
	    return state;
    case blockUser:
	    state = Object.assign({}, state, { blocked_user: action.data, loading:false });
	    return state;
    case unblockUser:
      state = Object.assign({}, state, { unblocked_user: action.data, loading:false });
      return state;
    case createdUsername:
      state = Object.assign({}, state, { username: action.data, loading:false });
      return state;
    case updateUser:
      // console.log('reducer user', action.data);
      state = Object.assign({}, state, { user_updated: action.data, loading:false });
      return state;    
    default:
      return state;
  }
};

export default accountReducer;