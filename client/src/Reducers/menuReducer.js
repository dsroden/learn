import { toggleMenu } from "../Actions/actionTypes";

const initialState = { menu_state: null};

const menuReducer = (state = initialState, action) => {
  switch (action.type) {
    case toggleMenu:
	    state = Object.assign({}, state, { menu_state: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default menuReducer;