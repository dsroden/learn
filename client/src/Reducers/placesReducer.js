import { updateSearchPlace } from "../Actions/actionTypes";

const initialState = { topic: null, };

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case updateSearchPlace:
	    state = Object.assign({}, state, { place: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default placesReducer;