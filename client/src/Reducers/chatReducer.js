import { 
  sendMessage,
  receiveMessage, 
  globalLocalToggle, 
  courseToggle, 
  switchCourse, 
  switchGroup, 
  closeCourseList, 
  openCourseList, 
  closeLocalList, 
  openLocalList, 
  openPrivateConversationsList,
  closePrivateConversationsList,
  openUserProfileView,
  closeUserProfileView,
  showUserProfile,
  receivePrivateMessage,
  sendPrivateMessage,
  reloadChatPage
} from "../Actions/actionTypes";

const initialState = { 
  message: null,
  received_message: null,
  global_local_toggle: null,
  courses_toggle: null, 
  group: null, 
  course: null, 
  course_list_state: null, 
  local_list_state: null, 
  private_conversations_list_state: null,
  user_profile_view: null,
  user_profile: null,
  private_message: null,
  received_private_message: null,
  chat_page_state: null
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case sendMessage:
	    state = Object.assign({}, state, { message: action.data, loading:false });
	    return state;
    case receiveMessage:
	    state = Object.assign({}, state, { received_message: action.data, loading:false });
	    return state;
    case globalLocalToggle:
      state = Object.assign({}, state, { global_local_toggle: action.data, loading:false });
      return state;
    case courseToggle:
      state = Object.assign({}, state, { courses_toggle: action.data, loading:false });
      return state;
    case switchCourse:
      state = Object.assign({}, state, { course: action.data, loading:false });
      return state;
    case switchGroup:
      state = Object.assign({}, state, { group: action.data, loading:false });
      return state;
    case closeCourseList:
      state = Object.assign({}, state, { course_list_state: action.data, loading:false });
      return state;
    case openCourseList:
      state = Object.assign({}, state, { course_list_state: action.data, loading:false });
      return state;
    case closeLocalList:
      state = Object.assign({}, state, { local_list_state: action.data, loading:false });
      return state;
    case openLocalList:
      state = Object.assign({}, state, { local_list_state: action.data, loading:false });
      return state;
    case openPrivateConversationsList:
      state = Object.assign({}, state, { private_conversations_list_state: action.data, loading:false });
      return state;
    case closePrivateConversationsList:
      state = Object.assign({}, state, { private_conversations_list_state: action.data, loading:false });
      return state;
    case openUserProfileView:
      state = Object.assign({}, state, { user_profile_view: action.data, loading:false });
      return state;
    case closeUserProfileView:
      state = Object.assign({}, state, { user_profile_view: action.data, loading:false });
      return state;
    case showUserProfile:
      state = Object.assign({}, state, { user_profile: action.data, loading:false });
      return state;
    case receivePrivateMessage:
      state = Object.assign({}, state, { received_private_message: action.data, loading:false });
      return state;
    case sendPrivateMessage:
      state = Object.assign({}, state, { private_message: action.data, loading:false });
      return state;
    case reloadChatPage:
      state = Object.assign({}, state, { chat_page_state: action.data, loading:false });
      return state;
    default:
      return state;
  }
};

export default chatReducer;