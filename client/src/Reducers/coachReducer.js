import { coachGroupAdded } from "../Actions/actionTypes";

const initialState = { new_group: null};

const coachReducer = (state = initialState, action) => {
  switch (action.type) {
    case coachGroupAdded:
    console.log('coach reducer');
	    state = Object.assign({}, state, { new_group: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default coachReducer;