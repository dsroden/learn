import { removeTag } from "../Actions/actionTypes";

const initialState = { tag: null};

const tagReducer = (state = initialState, action) => {
  switch (action.type) {
    case removeTag:
	    state = Object.assign({}, state, { tag: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default tagReducer;