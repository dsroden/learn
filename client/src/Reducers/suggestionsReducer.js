import { openSuggestionBox } from "../Actions/actionTypes";

const initialState = { open: null};

const suggestionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case openSuggestionBox:
	    state = Object.assign({}, state, { open: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default suggestionsReducer;