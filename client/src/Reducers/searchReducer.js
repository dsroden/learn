import { courseSearchDone, topicsUpdated, placeUpdated } from "../Actions/actionTypes";

const initialState = { courses: null, topics: null, location: null};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case courseSearchDone:
	    state = Object.assign({}, state, { courses: action.data, loading:false });
	    return state;
	case topicsUpdated: 
	    state = Object.assign({}, state, { topics: action.data, loading:false });
	    return state;
	case placeUpdated: 
	    state = Object.assign({}, state, { place: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default searchReducer;