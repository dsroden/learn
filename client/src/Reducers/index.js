import { combineReducers } from "redux";
import ExamplesReducer from "./examplesReducer";
import SearchReducer from "./searchReducer";
import TopicsReducer from "./topicsReducer";
import PlacesReducer from "./placesReducer";
import TagReducer from "./tagReducer";
import AccountReducer from "./accountReducer";
import ChatReducer from "./chatReducer"
import MenuReducer from "./menuReducer"
import CoachReducer from "./coachReducer"
import SuggestionsReducer from "./suggestionsReducer"
import MeetingsReducer from "./meetingsReducer"

const AppReducer = combineReducers({
  AccountReducer,
  ExamplesReducer,
  SearchReducer,
  TopicsReducer,
  PlacesReducer,
  TagReducer,
  ChatReducer,
  MenuReducer,
  CoachReducer,
  SuggestionsReducer,
  MeetingsReducer
});

export default AppReducer;