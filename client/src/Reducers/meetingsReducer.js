import { toggleCreateMeeting, toggleMeetingsList } from "../Actions/actionTypes";

const initialState = {create_meeting: 'reset', meetings_list: 'reset'};

const meetingReducer = (state = initialState, action) => {
  switch (action.type) {
    case toggleCreateMeeting:
	    state = Object.assign({}, state, { create_meeting: action.data, loading:false });
	    return state;
    case toggleMeetingsList:
	    state = Object.assign({}, state, { meetings_list: action.data, loading:false });
	    return state;
    default:
      return state;
  }
};

export default meetingReducer;