import axios from 'axios';
import Constants from '../Constants';

// load all course groups that hte user is a member of 

function Api(){}

Api.callLanding = ()=>{
  return new Promise((res, rej)=>{
    axios.get(Constants.BASE_URL).then((response)=>{
      res(response);
    }).catch((err)=>{
      rej(err);
    })
  }).then((searchResults)=>{
    return searchResults;
  }).catch((err)=>{
    console.log('ERROR: ' + err);
    return err;
  });
}

Api.search = (data)=>{
  data.access_token = getUserToken();
  data.username = getUsername();
  // console.log('data to send', data);
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/search', data).then((response)=>{
      res(response);
    }).catch((err)=>{
      rej(err);
    })
  }).then((searchResults)=>{
    return searchResults;
  }).catch((err)=>{
    console.log('ERROR: ' + err);
    return err;
  });
}

Api.searchPeople = (data)=>{
  data.access_token = getUserToken();
  data.username = getUsername();
  // console.log('data to send', data);
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/auth/search/people', data).then((response)=>{
      res(response);
    }).catch((err)=>{
      rej(err);
    })
  }).then((searchResults)=>{
    return searchResults;
  }).catch((err)=>{
    console.log('ERROR: ' + err);
    return err;
  }); 
}

Api.checkUsername = (data)=>{
  return new Promise((resolve, rej)=>{
    axios.post(Constants.API_URL + '/user/checkUsername', data).then((res)=>{
      resolve(res);
    }).catch((err)=>{
      rej(err.response)
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR: ' + err);
    return err;
  });  
}

Api.login = (data)=>{
  return new Promise((resolve, rej)=>{
    axios.post(Constants.API_URL + '/user/login', data).then((res)=>{
      resolve(res);
    }).catch((err)=>{
      // console.log('error logging in ', err);
      rej(err.response);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR: ' + err);
    return err;
  });
      
}

Api.socialLogin = (data)=>{
  return new Promise((resolve, rej)=>{
    axios.post(Constants.API_URL + '/user/sociallogin', data).then((res)=>{
      // console.log('res', res);
      resolve(res)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    console.log('ERROR: ' + err);
    return err;
  });

}

Api.signup = (data)=>{
  return new Promise((resolve, rej)=>{
    axios.post(Constants.API_URL + '/user/signup', data).then((res)=>{
      resolve(res);
    }).catch((error)=>{
      console.log('error creating example', error);
       if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            // console.log(error.response.status);
            // console.log(error.response.headers);
            rej(error.response);
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log(error.request);
            rej(error)
        } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
            rej(error)

        }
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    console.log('ERROR: ' + err.message);
    return err;
  });
}

Api.createUsername = (data)=>{
  data.access_token = getUserToken();
  return new Promise((resolve, rej)=>{
    axios.post(Constants.API_URL + '/auth/user/createUsername', data).then((res)=>{
      resolve(res);
    }).catch((err)=>{
      // console.log('error creating example', err);
      rej(err)
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    console.log('ERROR: ' + err);
    return err;
  });  
}

// Api.joinCourse
//     axios.post(Constants.API_URL + '/auth/courses/groups/join', {access_token: access_token, course: course}).then((res)=>{
//       // console.log('response from joining course', res);
//       this.nav(course);
//     }).catch((err)=>{
//       // console.log('error creating example', err);
//       window.localStorage.removeItem('access_token');
//       this.joinCourse()
//     })

Api.fetchCourseByTitle =  (data)=>{
  console.log('data', data);
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/course/fetch/title', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });

}

Api.joinGroup = (data)=>{
  data.access_token = getUserToken();
  console.log('data to join', data);
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/courses/groups/join', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });

}

Api.viewGroup = (data)=>{
  // data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/courses/groups/view', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });

}

Api.loadCoursesGlobalGroupsMemberAll = ()=>{
    let data = {access_token: getUserToken()}
    return new Promise((res, rej)=>{
       axios.post(Constants.API_URL + '/auth/courses/groups/global/member', data)
      .then((global_groups)=>{
        // if the user arrived at chat page via a specific course id
        // get the specifics associated with that course id - global group with attribute of local group ids where the user is a member
        if(!global_groups){
          rej()
        } else {
          res(global_groups);
        }
      })
      .catch((err)=>{
        return err;
      });
    }).then((global_groups)=>{
      return global_groups;
    }).catch((err)=>{
      console.log('ERROR FETCHING GLOBAL GROUPS: ' + err);
    });
}

Api.loadCourseLocalGroupsMember = (data)=>{
    return new Promise((res, rej)=>{
       axios.post(Constants.API_URL + '/auth/course/groups/local/member', data)
      .then((local_groups)=>{
        if(!local_groups){
          rej()
        } else {
          res(local_groups);
        }
      })
      .catch((err)=>{
        return err;
      });
    }).then((local_groups)=>{
      return local_groups;
    }).catch((err)=>{
      console.log('ERROR FETCHING LOCAL GROUPS: ' + err);
    });
}

Api.loadCoursesGroupsOne = (data) =>{
    return new Promise((res, rej)=>{
      axios.post(Constants.API_URL + '/auth/courses/groups/load/one', data)
      .then((localres)=>{
        res(localres);
      })
      .catch((err)=>{
        // console.log('ERROR, unable to load group', err);
        rej(err);
      });
    }).then((localres)=>{
      return localres;
    }).catch((err)=>{
      console.log('ERROR FETCHING GROUP: ' + err);
    });  
}

Api.loadCourseGlobalGroup = (data) =>{
    return new Promise((res, rej)=>{
      axios.post(Constants.API_URL + '/course/group/global', data)
      .then((courseInfoGlobalGroup)=>{
        res(courseInfoGlobalGroup);
      })
      .catch((err)=>{
        // console.log('ERROR, unable to load group', err);
        rej(err);
      });
    }).then((courseInfoGlobalGroup)=>{
      return courseInfoGlobalGroup;
    }).catch((err)=>{
      console.log('ERROR FETCHING GROUP: ' + err);
    });  
}

Api.loadCourseLocalGroup = (data) =>{
    return new Promise((res, rej)=>{
      axios.post(Constants.API_URL + '/auth/course/group/local', data)
      .then((local_group)=>{
        res(local_group);
      })
      .catch((err)=>{
        // console.log('ERROR, unable to load group', err);
        rej(err);
      });
    }).then((local_group)=>{
      return local_group;
    }).catch((err)=>{
      console.log('ERROR FETCHING GROUP: ' + err);
    });    
}

Api.findLocalGroupsForCourseBasedOnLocation= (data)=>{
  // data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/course/groups/local/searchbyplace', data)
    .then((local_group)=>{
      res(local_group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((local_group)=>{
    return local_group;
  }).catch((err)=>{
    console.log('ERROR FETCHING GROUP bASED ON LOCATION: ' + err);
  });     
}


Api.joinLocalGroup = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/auth/course/group/local/join', data)
    .then((joinedGroup)=>{
      res(joinedGroup);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((joinedGroup)=>{
    return joinedGroup;
  }).catch((err)=>{
    console.log('ERROR JOINING GROUP: ' + err);
  });   
}

Api.viewLocalGroup = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/course/group/local/view', data)
    .then((result)=>{
      res(result);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((result)=>{
    return result;
  }).catch((err)=>{
    console.log('ERROR JOINING GROUP: ' + err);
  });   
}

Api.createLocalGroupForCourse = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/course/group/local/create', data)
    .then((joinedGroup)=>{
      res(joinedGroup);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((joinedGroup)=>{
    return joinedGroup;
  }).catch((err)=>{
    console.log('ERROR JOINING GROUP: ' + err);
  });  
}


Api.fetchUserProfile = (username) =>{
  let data = {access_token: getUserToken(), username: username}
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/auth/users/fetch/one', data)
    .then((user)=>{
      res(user);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((user)=>{
    return user;
  }).catch((err)=>{
    console.log('ERROR JOINING GROUP: ' + err);
  }); 
}

Api.getPrivateConversationHistory = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/auth/user/conversation/history', data)
    .then((user)=>{
      res(user);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((user)=>{
    return user;
  }).catch((err)=>{
  });   
}

Api.loadChannelMessages = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/channels/messages/load', data).then((response)=>{
      res(response);
    }).catch((err)=>{
      // console.log('error creating example', err);
      rej(err);
    });
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    return err;
  });   
}

Api.fetchRecentConversations = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + '/auth/user/conversations/contacts', data)
    .then((user)=>{
      res(user);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((user)=>{
    return user;
  }).catch((err)=>{
    console.log('ERROR JOINING GROUP: ' + err);
  });     
}

Api.leaveGroup = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/user/group/${data.group_type}/leave`, data)
    .then((left)=>{
      console.log('left')
      res(left);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((left)=>{
    return left;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.leaveCourse = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/user/course/leave`, data)
    .then((left)=>{
      // console.log('left')
      res(left);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((left)=>{
    return left;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}


//chat page 

Api.fetchUserSelf = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/users/fetch/self`, data)
    .then((user)=>{
      res(user);
    })
    .catch((err)=>{
      rej(err);
    });
  }).then((user)=>{
    return user;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  }); 
}

Api.fetchRecentGroup = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/fetch/group/recent`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.fetchGlobalGroupByMember = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/fetch/group/global/member`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}


Api.fetchLocalGroupByMember = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/fetch/group/local/member`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.fetchClobalGroupByCourseIdAndMemberId = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/fetch/course/group/global/member`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.fetchGroupByCourseTitleAndLocation = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/fetch/course/group/title`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.updateUserProfile = (data)=>{
  // data.append('access_token', getUserToken());
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios({
      method: 'post',
      url: Constants.API_URL + `/auth/user/profile/update`,
      data: data,
      // config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then((results)=>{
      res(results);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    return err;
  }); 
}

Api.updateUserProfilePhoto = (data)=>{
  data.append('access_token', getUserToken());
  // console.log('data', data);
  return new Promise((res, rej)=>{
    axios({
      method: 'post',
      url: Constants.API_URL + `/auth/user/profile/update/photo`,
      data: data,
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then((updated)=>{
      res(updated);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((updated)=>{
    return updated;
  }).catch((err)=>{
    return err;
  }); 
}


Api.blockUser = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/user/block`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.expireMessage = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/message/expire`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });   
}

Api.flagUser = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/user/flag`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });   
}

Api.getGroupMembers = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/auth/group/fetch/members`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.sendVerificationEmail = (data)=>{
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/user/sendVerificationEmail`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.verifyUser = (data)=>{
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/user/verify`, data)
    .then((group)=>{
      res(group);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((group)=>{
    return group;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.resetPasswordRequest = (data)=>{
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/user/resetPasswordRequest`, data)
    .then((response)=>{
      res(response);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((result)=>{
    return result;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });
}

Api.resetPassword = (data)=>{
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/user/resetPassword`, data)
    .then((response)=>{
      res(response);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((result)=>{
    return result;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });   
}

Api.submitCourseSuggestion = (data)=>{
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/courses/suggestion`, data)
    .then((response)=>{
      res(response);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((result)=>{
    return result;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.fetchRecentActivityForCourse =  (data)=>{
  return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/course/group/recent/messages`, data)
    .then((response)=>{
      res(response);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((result)=>{
    return result;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  });  
}

Api.lookupIp = ()=>{
   return new Promise((res, rej)=>{
    axios.post(Constants.API_URL + `/geolocation/iplookup`, {lookup: true})
    .then((response)=>{
      res(response);
    })
    .catch((err)=>{
      // console.log('ERROR, unable to load group', err);
      rej(err);
    });
  }).then((result)=>{
    return result;
  }).catch((err)=>{
    // console.log('ERROR LEAVING GROUP: ' + err);
    return err;
  }); 
}

Api.coachJoinGroup = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/coach/groups/join', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });
}

Api.fetchCoachGroups = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/coach/groups/fetch', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });  
}

Api.submitCoachingApplication = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/coach/application/submit', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });  
}

Api.submitMeeting = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/group/meeting/submit', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });  
}

Api.fetchGroupMeetings = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/group/meetings/fetch', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });   
}

Api.fetchMeetingDetails = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/group/meeting/fetchDetails', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });   
}

Api.joinMeeting = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/group/meeting/join', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });   
}

Api.leaveMeeting = (data)=>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/group/meeting/leave', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH JOIN GROUP: ' + err);
    return err;
  });   
}


Api.searchCategories = (data) =>{
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/categories/fetch', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH FETCHING CATEGORIES: ' + err);
    return err;
  });   
}

Api.isGroupMember = (data) =>{
  data.access_token = getUserToken();
  return new Promise((res, rej)=>{
     axios.post(Constants.API_URL + '/auth/group/member/check', data).then((response)=>{
      res(response)
    }).catch((err)=>{
      rej(err);
    })
  }).then((results)=>{
    return results;
  }).catch((err)=>{
    // console.log('ERROR WITH FETCHING CATEGORIES: ' + err);
    return err;
  });   
}


function getUserToken(){
  return window.localStorage.getItem("access_token")
}
function getUsername(){
  return window.localStorage.getItem("username")
}

export default Api;