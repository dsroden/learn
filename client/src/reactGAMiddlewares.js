import ReactGA from 'react-ga'

const options = {}

const trackPage = (page) => {
  ReactGA.set({
    page,
  })
  ReactGA.pageview(page)
}

let currentPage = ''

export const googleAnalytics = store => next => action => {
  // console.log('google analytics GOOGLE ANALYTICS', action)
  if (action.type === '@@router/LOCATION_CHANGE') {
    const nextPage = `${action.payload.pathname}${action.payload.search}`

    if (currentPage !== nextPage) {
      currentPage = nextPage
      trackPage(nextPage)
    }
  }

  return next(action)
}