/*eslint no-useless-escape: "off"*/

import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import FA from '@fortawesome/react-fontawesome';
import { faCheck, faTimes} from '@fortawesome/fontawesome-free-solid';
import Api from '../Api'
import {openSuggestionBoxAction} from "../Actions/actionCreator";
import TopicSearch from "./TopicSearch"

class CourseSuggestionBox extends Component  {
  constructor (props) {
    super(props)
    this.handleSubmission = this.handleSubmission.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.state = {
      email: '',
      course: '',
      courseurl: '',
      note: '',
      showForm: false,
      emailValid: false,
      errorMsg: false,
      successMsg: false,
      listState: 'local-list-close',
      coach: this.props.coach
    }
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  componentWillReceiveProps(newProps){
    if(newProps.open_box){
      this.toggleForm()
      this.props.openSuggestionBoxAction(false);
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    // console.log('value', name, value);
    this.setState({
      [name]: value
    });

    if(name === 'email'){
      let regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      let emailValid = (regex.test(value)) ? true : false;
      this.setState({emailValid: emailValid})
    }
  }

  handleSubmission(event){
    event.preventDefault();
    let $this = this;
    let data = {};
    data.course_title = $this.state.course;
    data.course_url = $this.state.courseurl;
    data.email = $this.state.email;

    if(!$this.state.emailValid || $this.state.course === '' || $this.state.courseurl === ''){
      $this.setState({errorMsg: 'Please check the required input fields.'}, ()=>{
        setTimeout(()=>{
          $this.setState({errorMsg: false})
        }, 3000)
      })
      return;
    }
    Api.submitCourseSuggestion(data).then((res)=>{
      if(res.status === 200){
        $this.setState({course: '', courseurl: '', note: '', successMsg: 'Submission received!'}, ()=>{
          setTimeout(()=>{
            $this.setState({successMsg: false})
          }, 3000)
        })
      } else {
        $this.setState({errorMsg: 'There was an error processing your submission'}, ()=>{
          setTimeout(()=>{
            $this.setState({errorMsg: false})
          }, 3000)
        })
      }
    });

  }

  toggleForm(){
    let show = (this.state.showForm) ? false : true;
    // this.setState({showForm: show});
    let listState = show ? 'local-list-open' : 'local-list-close';
    this.setState({showForm: show, listState: listState});
  }

  render(){
      let suggestionForm = (<div></div>);
      if(this.state.showForm){
        suggestionForm = (
          <div style={styles.suggestionBox}>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Submit a course and we'll review it and notify you when it has been added and users can join.</b></div>
              </div>
            </div>
           <div style={styles.formContainer} >
            <div className="row">
              <div className="col-12">
                <label style={{width: "100%"}}>
                <div style={{fontSize: "12px", fontWeight: "bold"}}>Course title (required):</div>
                </label>
                <input style={styles.input} type="text" placeholder="Enter the course title" ref="course" name="course" value={this.state.course} onChange={this.handleInputChange} />

              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <label style={{width: "100%"}}>
                <div style={{fontSize: "12px", fontWeight: "bold"}}>Course URL (required):</div>
                </label>
                <input style={styles.input} type="text" placeholder="Enter the course url" ref="courseurl" name="courseurl" value={this.state.courseurl} onChange={this.handleInputChange} />

              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <label style={{width: "100%"}}>
                <div style={{fontSize: "12px", fontWeight: "bold"}}>Add tags:</div>
                </label>
                  <TopicSearch />
              </div>
            </div>
            <div style={{height: "10px"}}></div>
            <div className="row">
              <div className="col-12">
               <label style={{width: "100%"}}>
                <div style={{fontSize: "12px", fontWeight: "bold"}}>Valid Email (required):</div>
                </label>
                <input style={styles.input} type="email" placeholder="Enter your email address" ref="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
                <div style={{display: ((this.state.emailValid) ? 'inherit' : 'none'), position: 'absolute', right: '20px', top: '0', lineHeight: '40px', color: Constants.BRAND_GREEN, fontSize: '1.2em' }}>
                  <FA icon={faCheck}/>
                </div>
              </div>
            </div>
           <div className="row">
              <div className="col-12">
               <label style={{width: "100%"}}>
                <div style={{fontSize: "12px", fontWeight: "bold"}}>Add a note (optional):</div>
              </label>
                <textarea style={styles.textarea} type="text" placeholder="Tell us about the course" ref="note" name="note" value={this.state.note} onChange={this.handleInputChange} />
              </div>
            </div>
      
            <div className="row">
              <div className="col-12 ">    
                <button 
                        onClick={this.handleSubmission}
                        // style={((this.state.buttonIsHovered) ? styles.signUpBtnHover : styles.signUpBtn)} 
                        className="general-button color-border"
                        type="submit">Submit</button>
              </div>
            </div>
          </div>   
          <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
          <div style={{display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             
          </div>
        )
      }

    let suggestionButton = (<div></div>)
      if(this.state.showForm || this.state.coach){
        // suggestionButton = (
        //   <div onClick={this.toggleForm.bind(this)} style={styles.suggestionButton}> x </div>
        // )
      } else {
        suggestionButton = (
          <div>
          <button className="general-button color-border" onClick={this.toggleForm.bind(this)}  style={styles.suggestionButtonDescription} >Add a course</button>
          </div>
        )
      }

    // let learningProviderSponsor = (<div></div>)
    // if(!this.state.coach){
    //   learningProviderSponsor = (
    //       <div className="row">
    //         <div className="col-12">
    //         <div onClick={this.toggleForm.bind(this)} style={{textAlign: 'center', padding: "20px", marginTop: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
    //           <div>Are you a learning provider or sponsor?</div>
    //           <button onClick={this.nav.bind(this, 'providers')} className="general-button color-border center-item" style={{marginTop: "10px"}}>Add or sponsor courses</button>
    //         </div>
    //         </div>
    //       </div>
    //   )
    // }


    return (
      <div >
        <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{color: "white", backgroundColor: Constants.BRAND_RED,}}>
          <div className="col-12">
          <div onClick={this.toggleForm.bind(this)} style={{ textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
            <FA icon={faTimes} style={{float: "left", marginLeft: "10px"}} />
            Suggestions
          </div>
          </div>
        </div>
        <div style={styles.innerSideMenuContents}>
          {suggestionForm}
        </div>
        </div>
        {suggestionButton}

      </div>
    )
  }

}


const mapStateToProps = state => ({
  open_box: state.SuggestionsReducer.open
});

const mapDispatchToProps = {
  openSuggestionBoxAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseSuggestionBox);


const styles = {
  suggestionBox: {
    zIndex: "99",
    bottom: "60px",
    right: "0",
    padding: "20px",
  },
  suggestionButton: {
    position: "fixed",
    bottom: "10px",
    right: "10px",
    width: "50px",
    height: "50px",
    borderRadius: "100%",
    boxShadow: "0px 0px 5px " + Constants.BRAND_DARKGRAY,
    textAlign: "center",
    fontSize: "1.5em",
    fontWeight: "700",
    lineHeight: "45px",
    border: "1px solid " + Constants.BRAND_RED,
    cursor: "pointer",
    backgroundColor: "white",
    zIndex: "99"
  },
  suggestionButtonDescription: {
    position: "fixed",
    bottom: "10px",
    right: "10px",
    boxShadow: "0px 0px 5px " + Constants.BRAND_MIDDLEGRAY,
    textAlign: "center",
    fontSize: "1.1em",
    cursor: "pointer" ,
    backgroundColor: "white",
    padding: "10px",
    zIndex: "99",
    maxWidth: "150px",
  },
  formContainer: {
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  // input: {
  //   "height": "50px",
  //   width: "100%",
  //   maxWidth: "400px",
  //   border: '1px solid #c6c6c6',
  //   padding: '15px',
  //   marginBottom: '10px',
  // },
  listContainer: {
      top: "0px",
      width: "100%",
      maxWidth: "400px",
      position: "fixed",
      height: window.innerHeight + 60 + "px",
      right:  "-415px",
      // paddingBottom: "40px",
      background: Constants.BRAND_LIGHTGRAY,
      zIndex: "2147483647",
      boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
    },
    innerSideMenuContents: {
    height: window.innerHeight - 60 + "px",
    overflowY: "scroll",
    paddingBottom: "100px"
  },
    "input": {border: "1px solid #ccc",  borderRadius: "5px", padding: "15px", width: '100%', height: '40px', marginBottom: "10px"},
  "textarea": {border: "1px solid #ccc", width: '100%', borderRadius: "5px", padding: "15px", minHeight: "200px"},
 
}

