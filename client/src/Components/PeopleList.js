import React, { Component } from 'react';
import { connect } from 'react-redux';
import { placeUpdatedAction} from "../Actions/actionCreator";
import Api from "../Api";
// import Infinite from 'react-infinite'
// import loadingGif from "../assets/loading.gif"
import PeopleListItem from "./PeopleListItem";
import Masonry from 'react-masonry-component';

const masonryOptions = {
    transitionDuration: 0
};

class PeopleList extends Component {
  constructor (props) {
    super(props)
    this.state = {people: [], place: null, topics: null, loading: false};
  }

 componentDidMount() {
    // window.addEventListener('scroll', this.onScroll, false);
    this.callApi()
 }



  componentWillUnmount() {
    // window.removeEventListener('scroll', this.onScroll, false);
  }


  onPaginatedSearch = ()=>{
    let $this = this;
    if($this.state.loading){
      // console.log('still loading');
    }
    $this.setState({loading: true}, ()=>{
      // console.log('paginate');
      Api.searchPeople({topics: $this.state.topics, place: $this.state.place, last_id: null}).then((res)=>{
        // console.log('loaded more', res.data.courses.length);
        if(res.data.length < 1){
          // $this.setState({loading: false})
          return;
        } else {
          let people = $this.state.people;
          // courses = courses.concat(res.data.courses);
          // let merge = _.unionBy(courses, res.data, '_id');
          // console.log(merge);
          // $this.cleanCourses(courses, (cleanCourses)=>{
            setTimeout(()=>{
              // console.log('length ', cleanCourses.length);
              // console.log('merge length', merge.length);
              $this.setState({people: people, last_id: null, loading: false});
            }, 2000)
          // });
        }
      });
    })
    
  }

 
 componentWillReceiveProps(newProps){
    let $this = this;
    let place = (!newProps.place || newProps.place.address.length < 4) ? null : newProps.place;
    let searchData = {topics: [].concat.apply([], newProps.topics), place: place};
    $this.setState({topics: searchData.topics, place: searchData.place}, ()=>{
      $this.callApi(searchData);
    })
 }

 callApi = (data) => {
    let $this = this;
    $this.setState({people: []}, ()=>{
      data = (data) ? data : {topics: null, place: null};
      Api.searchPeople(data).then(res =>{
        if(res.status === 200){
          // $this.props.courseSearchDoneAction(res.data.courses);
          $this.setState({people: res.data})
        }
      });
    })

  };

 render() {
    var people = (<div></div>)
    if(this.state.people.length > 0){

      let place = this.state.place;
      if(!place){
        place = null
      }
      people = this.state.people.map((person) =>(
        <PeopleListItem history={this.props.history} profile={person} key={person._id} place={place}/>
      ))
 
      // let loading = (<div></div>)
      // if(this.state.loading){
      //   loading = (<div className="row">
      //       <div className="col-12">
      //         <div style={{textAlign: 'center', padding: "30px"}}><img alt="loading" src={loadingGif} width="80" height="80" style={{display: "block", marginLeft: "auto", marginRight: "auto"}}/></div>
      //       </div>
      //     </div>)
      // }
      // let loadMore = (<div></div>)
      // if(!this.state.loading){
      //   loadMore = (
      //     <button onClick={this.onPaginatedSearch.bind(this)} className="general-button color-border center-item" style={{marginTop: "10px", maxWidth: "300px", display: "block", marginLeft: "auto", marginRight: "auto"}}>Load More</button>
      //   )
      // }
      // list = (
      //     <div className="container-fluid" style={styles.listContainer}>
      //     {people}
      //     {loadMore}
      //     {loading}
      //     </div>
      // )
          
    } else {
        people= (
          <div className="row">
            <div className="col-12">
              <div style={{textAlign: 'center', padding: "30px"}}>No users matched your search queries at this time.</div>
            </div>
          </div>
          )
      }



    return (
        <div className="container-fluid">
            <Masonry
                className={'people-masonry'} // default ''
                // elementType={'ul'} // default 'div'
                options={masonryOptions} // default {}
                disableImagesLoaded={false} // default false
                updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
                // imagesLoadedOptions={} // default {}
            >
                {people}
            </Masonry>
        </div>
    );
  }
}


const mapStateToProps = state => ({
  courses: state.SearchReducer.courses,
  topics: state.SearchReducer.topics,
  place: state.SearchReducer.place

});

const mapDispatchToProps = {
  placeUpdatedAction
  
};

export default connect(mapStateToProps, mapDispatchToProps)(PeopleList);

