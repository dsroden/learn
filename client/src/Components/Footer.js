import React, { Component } from 'react';

import Constants from '../Constants'

class Footer extends Component {

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  render() {

    return (
        <div className="container-fluid" style={styles.footerContainer}>
          <div className="row">
            <div className="col-12 col-sm-4 col-md-4 ">
              <div>© Copyright LearnLabs 2018 </div>            
            </div> 
            <div className="col-12 col-sm-4 col-md-4 ">
              <a style={styles.link} href="/privacy">Privacy Policy</a>
            </div> 
            <div className="col-12 col-sm-4 col-md-4 ">
              <a style={styles.link}  href="/terms">Terms of Use</a>
            </div> 
          </div>
        </div>
    );
  }
}

export default Footer;

const styles = {
  "footerContainer": {
    width: "100%",
    backgroundColor: Constants.BRAND_GRAY,
    color: Constants.BRAND_WHITE,
    textAlign: 'center',
    padding: "10px"
  },
  link: {
    display: "block",
    textAlign: "center",
    color: "white"
  }
}