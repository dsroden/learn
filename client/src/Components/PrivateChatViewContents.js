import React, {Component} from 'react'
import { connect } from 'react-redux';
import ReactChatView from 'react-chatview'
import PrivateChatViewMessage from './PrivateChatViewMessage'
import Api from '../Api'

class PrivateChatViewContents extends Component {
   constructor (props) {
    super(props);
    this.state = { 
      messages: false, 
      sender: this.props.sender,
      receiver: this.props.receiver,
    };
  }


  componentWillReceiveProps(newProps){
    this.addMessage(newProps.received_private_message);
  }

  componentDidMount(){
    // console.log('chat mounted')
    if(this.state.receiver){
      this.callApi();
    }
  }

  callApi(){
    let $this = this;
    let data = {sender: $this.state.sender.username, receiver: $this.state.receiver.username};
    Api.getPrivateConversationHistory(data).then((results)=>{
      // console.log(results);
      if(results.status === 200){
        $this.setState({messages: results.data})
      } else {
        return;
      }
    });
  };


  addMessage(messageData){
    // var messages = this.state.messages.concat([messageData]);
    var messages = [messageData].concat(this.state.messages);
    this.setState({messages: []}, ()=>{
      this.setState({messages: messages})
    });
  }

  loadMoreHistory () {
    // console.log('load more')
    return new Promise((resolve, reject) => {
      // let more = _.range(20).map(v=>'yo');
      // this.setState({ messages: this.state.messages.concat(more)});
      resolve();
    });
  }

  render () {
    let chatlist = (<div></div>)
    if(this.state.messages && this.state.messages.length > 0){
      // console.log('loading messages');
      chatlist = (
        <ReactChatView 
          className="private-content"
          flipped={true}
          scrollLoadThreshold={50}
          onInfiniteLoad={this.loadMoreHistory.bind(this)}>
          {this.state.messages.map((m, ix) => <PrivateChatViewMessage isself={(m.username === this.state.sender.username)} previoustime={(this.state.messages[ix + 1]) ? this.state.messages[ix  + 1].created_at : ''} message={m} key={ix}/>)}
        </ReactChatView>
        )
    }
    return (
      <div style={styles.chatViewContainer}>
        {chatlist}
      </div>
    )  
  }
}




const mapStateToProps = state => ({
  received_private_message: state.ChatReducer.received_private_message
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateChatViewContents);


const styles = {
  chatViewContainer: {
    width: "100%",
    height: window.innerHeight - 200 + 'px',
    overflow: "auto",
    backgroundColor: 'white'
  }
}

