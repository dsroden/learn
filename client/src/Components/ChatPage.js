
import React, { Component } from 'react';
import Constants from '../Constants';
import ChatView from './ChatView';
import Api from '../Api';
import CourseGroupList from './CourseGroupList'
import LocalGroupsList from './LocalGroupsList'
import PrivateConversationsList from './PrivateConversationsList'
import { connect } from 'react-redux';
import UserProfileView from './UserProfileView'
import Nav from './Nav';
import {reloadChatPageAction, accountStateUpdatedAction} from "../Actions/actionCreator";
import Socket from './Socket'
import PrivateSocket from "./PrivateSocket";
import _ from "lodash";
import CreateUsername from "./CreateUsername"
import MeetForm from "./MeetForm"
import MeetingsList from "./MeetingsList"

class ChatPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: null,
      global_groups: null,
      current_group: null,
      current_course: null,
      chat_state: null,
      user: null,
      no_groups: false,
      no_username: false,
    };
  }

  componentDidMount(){
    //collect course specific groups and user
    //each row is a global group with 
    //attribute local_groups, an array 0, 1, or more local groups ids
    //and attribute course_info that describes the course (title, description, url, etc)
    this.callApi();
  }

  nav(location){
    this.props.history.push('/' + location);
  }

  callApi(){
    let $this = this;
    let currentUrl = $this.props.history.location
    let parsedUrl = new URLSearchParams(currentUrl.search);
    let parsed = {};
    parsed.course = parsedUrl.get("course");
    parsed.place = parsedUrl.get("place");
    parsed.lng = parsedUrl.get("lng");
    parsed.lat = parsedUrl.get("lat");

    let access_token = window.localStorage.getItem('access_token')
    let data = {access_token: access_token, title: parsed.course, address: parsed.place, lng: parsed.lng, lat: parsed.lat};

    //first fetch the user
    Api.fetchUserSelf(data).then((user_res)=>{
      // if(res.status !==200){
      //   window.location.href = '/';
      //   return;
      // }
      let user = null;
      if(user_res.status === 200){
        user = user_res.data;
      }

      // if(!user.verified){
      //   //user is not verified
      //   window.location.href = '/verification/'
      //   return;
      // }
      if(user && !user.username){
        $this.setState({user: user, no_username: true});
        return;
      }

      if(data.title){
        //find the specific group by the url (course id and possibly location);
        Api.fetchGroupByCourseTitleAndLocation(data).then((res)=>{
          //check if the user is a member of the group
          //if not, add the member ot the group
          // console.log('res', res.data);
          let member = null;
          if(user && res.data && res.data.group){
            member = _.find(res.data.group.members, (m)=>{
            return m ===  user._id;
          })
          }
          let current_course = res.data.course;
          let current_group = res.data.group;
          let chat_state = (current_group.address) ? 'local' : 'global';
          $this.setState({chat_state: chat_state , user: user, current_group: current_group, current_course: current_course}) 
          if(!member){
            let course = res.data.course;
            if(parsed.place){
              course.place = {address: parsed.place, location: {lng: parsed.lng, lat: parsed.lat}};
            }
            // Api.joinGroup({course: course}).then((join_res)=>{
              // console.log('joined response', join_res);
            // });
          }
    
        });
      } else if(user && user.recent_group){
        //find the most recent group
    

        Api.fetchRecentGroup({recent_group: user.recent_group, user_id: user._id}).then((group)=>{
          if(group.status !== 200){
            //need to fetch a global group that the user is a member of
            Api.fetchGlobalGroupByMember({user_id: user._id}).then((global_group)=>{
              if(global_group.status !== 200){
                Api.fetchLocalGroupByMember({user_id: user._id}).then((local_group)=>{
                  if(local_group.status !==200){
                    $this.setState({no_groups: true});
                  }
                });
              } else {
                // console.log('here')
                group = global_group.data;
                let current_course = group.course;
                let current_group = group.group;
                let chat_state = (current_group.address) ? 'local' : 'global';
                $this.setState({chat_state: chat_state , user: user, current_group: current_group, current_course: current_course})                
              }
            })
          } else {
            // console.log('here');
            let current_course = group.data.course;
            let current_group = group.data.group;
            let chat_state = (current_group.address) ? 'local' : 'global';
            $this.setState({chat_state: chat_state , user: user, current_group: current_group, current_course: current_course})
          }
        });
      } else {
        $this.setState({no_groups: true});
      }

    })

  }

  componentWillReceiveProps(newProps){
    // if a new course comes in, make current_group the global group for that course
    // if a new group (local or global) comes in, make current_group the local group
    

    if(newProps.account_state === 'logged_in'){
      this.props.accountStateUpdatedAction(null);
      this.callApi()
      return;
    } else {

      if(newProps.chat_page_state){
        this.props.reloadChatPageAction(false);
        window.location.href = '/chat';
        return;
      }

      let $this = this;
      let access_token = window.localStorage.getItem('access_token')

      if($this.state.no_username && newProps.username){
        $this.setState({no_username: false}, ()=>{
          window.localStorage.setItem("username", newProps.username);
          $this.callApi()
          return;
        })
      }

      if(newProps.switch_group && newProps.switch_group._id !== $this.state.current_group._id && newProps.switch_group.location){
        // console.log('new group', newProps.switch_group);
          $this.setState({current_group: null}, ()=>{
            $this.setState({chat_state: ((newProps.switch_group.address) ? 'local' : 'global'), current_group: newProps.switch_group})
          });   
      } 
      if(newProps.switch_group && newProps.switch_group._id !== $this.state.current_group._id && !newProps.switch_group.location){
        // console.log('new group', newProps.switch_group);
        Api.loadCourseGlobalGroup({course_id: newProps.switch_group._id}).then((courseGroup)=>{
          $this.setState({current_group: null, current_course: null}, ()=>{
            $this.setState({chat_state: 'global', current_group: courseGroup.data.group, current_course: courseGroup.data.course});         
          })
        })
      } 

      if(newProps.switch_course && newProps.switch_course._id !== $this.state.current_course._id){
        // console.log('load course global group');
        let course_id = newProps.switch_course._id;
        let data = {access_token: access_token, course_id: course_id};
        Api.loadCourseGlobalGroup(data).then((courseGroup)=>{
          $this.setState({current_group: null, current_course: null}, ()=>{
            $this.setState({chat_state: 'global', user: $this.state.user, current_group: courseGroup.data.group, current_course: courseGroup.data.course});         
          })
        })
      } 
    }
  }

  render() {
    let globalGroupList = (<div></div>)
    let chatView = (<div></div>)
    let localGroupList = (<div></div>)
    let privateConversationsList = (<div></div>)
    let userProfileView = (<div></div>)
    let noGroupsView = (<div></div>)
    //create course groups list (global)
    // if(this.state.user){
      globalGroupList = (<CourseGroupList />)
    // }

    //create chat view
    if(this.state.current_group && this.state.user){
      chatView = (<ChatView 
        course_info={this.state.current_course} 
        current_group={this.state.current_group} 
        user={this.state.user}
        chat_state={this.state.chat_state} />
      )
    } 

    if(this.state.current_group && !this.state.user){
      chatView = (<ChatView 
        course_info={this.state.current_course} 
        current_group={this.state.current_group} 
        user={null}
        chat_state={this.state.chat_state} />
      )
    }

    //create course group list (local)
    if(this.state.current_course ){
      localGroupList = (<LocalGroupsList user={this.state.user} course_info={this.state.current_course} /> )
    }

    //create course group list (local)
    if(this.state.current_course && this.state.user){
      privateConversationsList = (<PrivateConversationsList user={this.state.user} /> )
    }

    //create user profile view
    if(this.state.current_course && this.state.user){
      userProfileView = (<UserProfileView user={this.state.user} /> )
    }

    //if there are no groups 
    if(this.state.no_groups){
      noGroupsView = (
        <div><div style={{height: "70px"}}></div><div style={{textAlign: "center", padding: "10px"}}>You have not joined any course groups yet.</div><div style={{textAlign: "center"}}><button className="general-button color-border" style={{maxWidth: "200px", display: "block", marginLeft: "auto", marginRight: "auto"}} onClick={this.nav.bind(this, 'browse')} >Browse Courses</button></div></div>
      )
    }

    //set up sockets
    let sockets = (<div></div>)
    if(this.state.user && this.state.current_group && this.state.user.username){
      // console.log('user for sockets', this.state.user);
        sockets = (
          <div>
          <Socket 
          user={this.state.user} 
          channel={this.state.current_group}
        />
        <PrivateSocket user={this.state.user} />
        </div>
      )
    }

    var meetForm = (<div></div>)
    if(this.state.user && this.state.current_group){
      meetForm = (
        <MeetForm user={this.state.user} group={this.state.current_group}/>
      )
      // console.log('meet form', meetForm);
    }

    var meetList = (<div></div>)
    if(this.state.user && this.state.current_group){
      meetList = (
        <MeetingsList user={this.state.user} group={this.state.current_group}/>
      )
    }
    
    //set up mainview
    let mainView = (<div></div>)
    if(this.state.no_username){
      mainView = (
        <div>
        <div style={{height: "100px"}}></div>
        <CreateUsername user={this.state.user}/>
        </div>
      )
    } else {
      mainView = (
        <div>
          <div className="container-fluid" style={styles.chatPageContainer}>
            <div className="row">
              {meetForm}
              {meetList}
              {globalGroupList}
              <div className="col-md-12" style={{padding: 0}}>
                {chatView}
              </div>
              {localGroupList}
              {privateConversationsList}
              {userProfileView}
            </div>
          </div>
          {noGroupsView}
          {sockets}
        </div>
      )
    }
    

    return (
      <div className="learnlabs-app">
        <Nav history={this.props.history} />
        {mainView}
      </div>
    );
  }
}

const mapStateToProps = state => ({
    switch_course: state.ChatReducer.course,
    switch_group: state.ChatReducer.group,
    chat_page_state: state.ChatReducer.chat_page_state,
    username: state.AccountReducer.username,
    account_state: state.AccountReducer.account_state,
});

const mapDispatchToProps = {
  reloadChatPageAction,
  accountStateUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatPage);

         
const styles = {
  chatPageContainer: {
    position: "fixed",
    backgroundColor: Constants.BRAND_WHITE,
    padding: "0"
  },

}




