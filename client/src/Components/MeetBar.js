import React, { Component } from 'react';
import { connect } from 'react-redux';
import {toggleCreateMeetingAction, toggleMeetingsListAction,accountStateUpdatedAction } from "../Actions/actionCreator";

class MeetBar extends Component {
  state = {

  };

  componentDidMount() {
  }

  componentWillReceiveProps(newProp){
  }

  callApi = () => {

  };

  createMeeting(){

    if(!window.localStorage.getItem('access_token')){
      //user not logged in, show login modal
      this.props.accountStateUpdatedAction('not_logged_in')
      return;
    }
    this.props.toggleCreateMeetingAction('open');
  }

  showMeetings(){

    if(!window.localStorage.getItem('access_token')){
      //user not logged in, show login modal
      this.props.accountStateUpdatedAction('not_logged_in')
      return;
    }
    this.props.toggleMeetingsListAction('open');

  }

  render() {

    var addMeeting = (<button onClick={this.createMeeting.bind(this)} className="general-button color-border" style={{...styles.addMeetingBtn,...{width: "50px"}}}> + </button>)
    var meetingInfo = (<button  onClick={this.showMeetings.bind(this)}  className="general-button color-border" style={{...styles.addMeetingBtn,...{width: "100px"}}}> Meetings </button>)
    return (
        <div className="container-fluid">
          <div style={{float: "right"}}>
            {addMeeting}
            {meetingInfo}
          </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = {
  toggleCreateMeetingAction, 
  toggleMeetingsListAction,
  accountStateUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetBar);


const styles = {
  addMeetingBtn: {
    marginTop: "10px",
    display: "inline-block",
    marginLeft: "10px"
  }
}