import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
// import CourseList from './CourseList'
import PlaceSearch from './PlaceSearch.js'
import TopicSearch from './TopicSearch.js'
// import PinnedCourse from './PinnedCourse.js'
import CategoriesList from './CategoriesList'
class BrowseCategories extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      pinned_title: false,
      isMounted: false,
    }
  }

  componentDidMount(){
    // this.setState({pinned_title: 'Udemy'})
    this.setState({isMounted: true});
  }

  componentWillUnmount(){
    // this.setState({isMounted: false})
  }

  render(){
    var search;
    var categories = (<React.Fragment></React.Fragment>)
    if(this.state.isMounted){
      search = (
        <div>
        <div style={{padding: "0px 10px 0px 10px"}}>
          <PlaceSearch color={Constants.BRAND_RED}/>    
        </div>     
        <div style={{height: "10px"}}></div>
        <div style={{padding: "0px 10px 0px 10px"}}>
          <TopicSearch />
        </div>
        </div>
        )
      categories = (<CategoriesList history={this.props.history}/>)
    }

    return (
      <div>
        <div className="row" style={styles.descriptionContainer}>
          <div className="col-12">
            <div>Group channels by interests and locations</div>
          </div>
        </div>   
        <div style={{height: "10px"}}></div>
        <div style={{padding: "0px 10px 0px 10px"}}>
          {search}
        </div>
          {categories}
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  
};

export default connect(mapStateToProps, mapDispatchToProps)(BrowseCategories);


const styles = {
  descriptionContainer: {
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    overflow: "hidden",
    marginTop: "10px",
    marginBottom: "10px",
    textAlign: "center",
    fontWeight: "500"
  },
}
