import React, { Component } from 'react';
import { connect } from 'react-redux';

class CoachGroupListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
      group: this.props.group
    };
  }

  nav(){
    this.props.history.push('/chat/?course=' + encodeURIComponent(this.state.group.course.title));      
  }

  render() {
      // console.log('group to render', this.state.group);
      return (
      <div className="row" style={styles.groupRow}>
        <div className="col-9">
          <div style={{paddingTop: "10px"}}>{this.state.group.course.title}</div>          
        </div>
        <div className="col-3">
          <button
          className="general-button color-border-green"
          onClick={this.nav.bind(this)}
          >View</button>          
        </div>
      </div>    
    );
  }
}

         
const mapStateToProps = state => ({

});

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(CoachGroupListItem);

const styles = {
  groupRow: {
    backgroundColor: 'whitesmoke',
    padding: "10px",
    borderBottom: "1px solid #ece7e7"
  }
}