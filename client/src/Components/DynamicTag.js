import React, { Component } from 'react';
import Constants from '../Constants'
import { connect } from 'react-redux';
import { removeTagAction } from "../Actions/actionCreator";

import FA from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';

class DynamicTag extends Component {
  constructor (props) {
    super(props)
    this.state = {
      color: this.props.color || Constants.BRAND_RED,
      tag: this.props.tag,
    }

  }

  removeTag(){
    let $this = this;
    $this.props.removeTagAction($this.state.tag);
  }

  render() {
    let tag = this.state.tag
    return (
        <div onClick={this.removeTag.bind(this)} style={{...styles.tag,...{"backgroundColor": this.state.color}}}>
          <div style={{display: 'inline-block'}}>{tag}</div>
          <FA style={styles.remove} icon={faTimes} />
        </div>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  removeTagAction
};

export default connect(mapStateToProps, mapDispatchToProps)(DynamicTag);

const styles = {
  "tag": {
    display: "inline-block",
    padding: "3px",
    background: Constants.BRAND_RED,
    borderRadius: "5px",
    color: Constants.BRAND_WHITE,
    margin: "3px",
    cursor: 'pointer'
  },
  remove: {
    display: 'inline-block',
    // border: '1px solid',
    // borderRadius: '100%',
    // margin: '10px',
    marginLeft: '5px'
  }
}