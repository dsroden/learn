import React, {Component} from 'react'
import { connect } from 'react-redux';
import PrivateChatViewContents from './PrivateChatViewContents'
import PrivateChatViewInput from './PrivateChatViewInput'

class PrivateChatView extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      sender: this.props.sender,
      receiver: this.props.receiver,
    }
  }

  componentDidMount(){
  }

  componentWillReceiveProps(newProps){

  }

  render(){

    return (
      <div >
        <PrivateChatViewContents sender={this.state.sender} receiver={this.state.receiver} />
        <PrivateChatViewInput sender={this.state.sender} receiver={this.state.receiver} />
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateChatView);


