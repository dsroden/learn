import React, {Component} from 'react'
import Constants from "../Constants"
import { showUserProfileAction, openUserProfileViewAction } from "../Actions/actionCreator";
import { connect } from 'react-redux';
import Api from "../Api";
import Tag from "./Tag"
import ProfilePhoto from "./ProfilePhoto"

class PeopleListItem extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      profile: this.props.profile,
    }
  }

  componentDidMount(){
  }


  showUserProfile(){

    Api.fetchUserProfile(this.state.profile.username).then((response)=>{
      this.props.openUserProfileViewAction(true)
      response.data.details = true;
      this.props.showUserProfileAction(response.data)
    });
  }
  
  render(){
    let height =  '100%';

    let status = (<div></div>)
    if(this.state.profile.status){
      status = (
        <div style={{fontSize: "1.3em", fontWeight: "500", textAlign: "center", padding: "5px", maxWidth: "300px"}}>"{this.state.profile.status}"</div>
      )
    }

    let interests = (<div></div>)
    if(this.state.profile.interests && this.state.profile.interests.length > 0){
      var interestSet = (this.state.profile.interests.length >= 4) ? this.state.profile.interests.splice(0,3) : this.state.profile.interests;
      let tags = interestSet.map((tag, index) =>(
        <Tag color={Constants.BRAND_BLUE} tag={tag} key={index}/>
      ))
      interests = (
        <div style={{marginBottom: "10px", maxWidth: "200px"}}>{tags}</div>
      )
    }

    return (
      <div style={{padding: "5px", minWidth: (window.innerWidth < 500) ? "100%" : 'none'}} >
        <div style={{background: Constants.BRAND_LIGHTGRAY, padding: "15px 5px 5px 5px", boxShadow: "0px 0px 5px " + Constants.BRAND_MIDDLEGRAY, borderRadius: "5px"}}>
          <div style={{width: "100px", display: "block", marginLeft: "auto", marginRight: "auto"}}><ProfilePhoto user={this.state.profile} size="100"/></div>
          <div onClick={this.showUserProfile.bind(this)} style={{cursor: "pointer", height: height, minWidth: "220px", marginLeft: "auto", marginRight: "auto", textAlign: "center", fontWeight: "700", fontSize: "1.2em", marginTop: "10px", marginBottom: "10px"}}>{this.state.profile.username}</div>
          {status}
          {interests}
        </div>
      </div>
    )
  }

}



const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  showUserProfileAction
};

export default connect(mapStateToProps, mapDispatchToProps)(PeopleListItem);

