
import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
// import Api from '../Api';
import FA from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';
import { closeUserProfileViewAction, closePrivateConversationsListAction } from "../Actions/actionCreator";
import PrivateChatView from "./PrivateChatView";
import Api from "../Api";
import UserBlockFlag from "./UserBlockFlag";
import  { o2LoadImage } from '../Utils'
import Tag from "./Tag"

class UserProfileView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listState: 'people-list-close',
      user_profile: null,
      user: this.props.user,
      profile_details: false,
      user_details: false,
      image_valid: false,
      transform: false
    };
  }

  componentWillReceiveProps(newProps){
    this.props.closePrivateConversationsListAction(false);
    // console.log('new props in user profile');
    let listState = '';
    if(newProps.user_profile_view){
      listState =  'people-list-open';
    } else {
       listState = 'people-list-close'
    }
    this.setState({listState: listState});
    if(newProps.user_profile !== this.state.user_profile){
      this.setState({user_profile: null, profile_details: false, user_details: false}, ()=>{
        this.setState({user_profile: newProps.user_profile}, ()=>{
  
          this.loadImage();
          if(newProps.user_profile.details){
            this.showProfileDetails();
          }

        })
      })
    }
  }

  componentDidMount(){
  }

  loadImage(){
    let $this = this;
    let imageUrl = Constants.PROFILE_PHOTO_BASE_URL + $this.state.user_profile.username + '.png?raw=true';
    o2LoadImage(imageUrl).then((result)=>{
      if(result.image_valid){
        let user = $this.state.user_profile;
        user.profile_photo = Constants.PROFILE_PHOTO_BASE_URL + $this.state.user_profile.username + '.png?raw=true';
        $this.setState({user_profile: JSON.parse(JSON.stringify(user)), image_valid: true, transform: result.transform})
      }
    });
  }

  closeList(){
    this.props.closeUserProfileViewAction(false)
  }

  showProfileDetails(){
    let details = this.state.profile_details;
    details = (details) ? false : true;
    if(!this.state.user_details){
      Api.fetchUserProfile(this.state.user_profile.username).then((res)=>{
        if(res.status === 200){
          // console.log('res', res);
          let user_details = res.data;
          this.setState({profile_details: details, user_details: user_details})
        }
      })
      //get user info
    } else {
      this.setState({profile_details: details})
    }
  }

  getAlphabetIndexColor(letter){
    let ALPHABET = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let index = ALPHABET.indexOf(letter);
    let color = Constants.ALPHABET_COLORS[index];
    return color;
  }

  render() {
    let username = (<div></div>)
    if(this.state.user_profile){
      username = (<div style={{display: "inline-block"}}>{this.state.user_profile.username}</div>)
    }
    let user_profile_photo = (<div></div>);
    let username_color = null;
    if(this.state.user_profile && this.state.user_profile.profile_photo){
      username_color = this.getAlphabetIndexColor(this.state.user_profile.username[0].toLowerCase());

      // if(this.state.image_valid){
        user_profile_photo = (
          <div style={{cursor: "pointer", position: "relative", display: "inline-block", width: "40px", height: "40px", textAlign: 'center', lineHeight: "40px", color: "white", fontWeight: "bold",  marginTop: "10px",  borderRadius: "100%", backgroundColor: username_color}}> 
            {this.state.user_profile.username[0].toUpperCase()}
            <div onClick={this.showProfileDetails.bind(this)} style={{cursor: "pointer", position: 'absolute', top: "0px", width: "40px", height: "40px", borderRadius: "100%", backgroundImage: 'url("' + Constants.PROFILE_PHOTO_BASE_URL + this.state.user_profile.username + '.png")', backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat", transform: this.state.transform}}></div> 
          </div>
        )
      // }
    } else if(this.state.user_profile) {
      username_color = this.getAlphabetIndexColor(this.state.user_profile.username[0].toLowerCase());

      user_profile_photo = (
      <div onClick={this.showProfileDetails.bind(this)} style={{cursor: "pointer", position: "relative", display: "inline-block", width: "40px", height: "40px", textAlign: 'center', lineHeight: "40px", color: "white", fontWeight: "bold",  marginTop: "10px",  borderRadius: "100%", backgroundColor: username_color}}> 
        {this.state.user_profile.username[0].toUpperCase()}
      </div>
      )

    }

    let chatView = (<div></div>)
    if(this.state.user_profile && this.state.user){
      // console.log('mounting chat view');
      chatView = (<PrivateChatView sender={this.state.user} receiver={this.state.user_profile}/>)
    }

    let userBlockFlag = (<div></div>);

    if(this.state.user_details && this.state.user.username !== this.state.user_details.username){
      userBlockFlag = (<UserBlockFlag user={this.state.user} details={this.state.user_details} />)
    }

    var interests = (<div></div>)
    if(this.state.user_details.interests && this.state.user_details.interests.length > 0){
      var tags = this.state.user_details.interests.map((tag, index) =>(
        <Tag color={Constants.BRAND_BLUE} tag={tag} key={index}/>
      ))

      interests = (
        <div className="row">
          <div className="col-12" style={{marginBottom: "10px"}}>
            <div style={{padding: "15px"}}><b>Interests: </b></div>
            {tags}
          </div>
        </div>
      )
    }



    return (
      <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{position: 'relative'}}>
          <div className="col-3" style={{cursor: "pointer"}} onClick={this.showProfileDetails.bind(this)}>
          {user_profile_photo}
          </div>
          <div className="col-6" onClick={this.showProfileDetails.bind(this)}>
            <div style={{textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
              {username}
            </div>
          </div>
          <div onClick={this.closeList.bind(this)}  className="col-3" style={{textAlign: 'center', lineHeight: "60px"}}>
            <FA icon={faTimes} style={{ marginLeft: "10px", cursor: "pointer"}} />
          </div>
        </div>
        <div style={{display: (this.state.profile_details) ? 'inherit' : 'none', position: 'absolute', left: "0", right: "0", bottom: "0", top: "60px", zIndex: "2", backgroundColor: "white"}}>
          
          <div className="row">
            <div className="col-12">
              <div style={{padding: "15px"}}><b>Status: </b>{(this.state.user_details.status) ? this.state.user_details.status : this.state.user_details.username + ' has not added a status yet'}</div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div style={{padding: "15px"}}><b>Bio: </b>{(this.state.user_details.bio) ? this.state.user_details.bio : this.state.user_details.username + ' has not added a bio yet'}</div>
            </div>
          </div>
          {interests}
          <div style={{height: "10px"}}></div>
          <div className="row">
            <div className="col-12">
              <button onClick={this.showProfileDetails.bind(this)} className="general-button color-border center-item">Send Message</button>
            </div>
          </div>
          {userBlockFlag}
        </div>
        {chatView}
      </div>
    );
  }
}


const mapStateToProps = state => ({
  user_profile_view: state.ChatReducer.user_profile_view,
  user_profile: state.ChatReducer.user_profile
});

const mapDispatchToProps = {
  closeUserProfileViewAction,
  closePrivateConversationsListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileView);


const styles = {
  listContainer: {
    top: "60px",
    width: "100%",
    maxWidth: "400px",
    position: "fixed",
    height: window.innerHeight - 40 + "px",
    // overflow: "auto",
    right:  "-410px",
    // paddingBottom: "40px",
    background: Constants.BRAND_LIGHTGRAY,
    zIndex: "2147483647",
    boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
  },
  "btn": {width: '200px', marginBottom: "10px", marginTop: "10px", height: '50px', display: 'block', marginLeft: 'auto', marginRight: 'auto'},

}

