import React, {Component} from 'react'
import { connect } from 'react-redux';
import {  } from "../Actions/actionCreator";
// import Constants from "../Constants"
import Nav from "./Nav"

class PrivacyPolicy extends Component  {
  constructor (props) {
    super(props)
    this.state = {
    }
  }


  render(){
    return (
      <div className="learnlabs-app">
        <Nav safe="true" history={this.props.history} />
        <div style={{height: "70px"}} ></div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <h3>
              LearnLabs Privacy Policy
              </h3>
            </div>
          </div>          

          <div className="row">
            <div className="col-12">
              <p>
                Thank you for joining LearnLabs, the place to connect with other learners, learning coaches, and grow as a learner. We at LearnLabs. (“LearnLabs”, “we”, “us”) respect your privacy and want you to understand how we collect, use, and share data about you. This Privacy Policy covers our data collection practices and describes your rights to access, correct, or restrict our use of your personal data.
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
                Unless we link to a different policy or state otherwise, this Privacy Policy applies when you visit or use the LearnLabs website, mobile applications, APIs or related services (the “Services”).
                By using the Services, you agree to the terms of this Privacy Policy. You shouldn’t use the Services if you don’t agree with this Privacy Policy or any other agreement that governs your use of the Services.
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>Table of Contents</p>
              <ul>
                <li>1. What Data We Get</li>
                <li>2. How We Get Data About You</li>
                <li>3. What We Use Your Data For </li>
                <li>4. Who We Share Your Data With</li>
                <li>5. Security</li>
                <li>6. Your Rights</li>
                <li>7. Jurisdiction-Specific Rules</li>
                <li>8. Updates & Contact Info</li>
                <li>Cookie Policy</li>
              </ul>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
                <b> 1. What Data We Get </b>
              </p>
              <p>
                We collect certain data from you directly, like information you enter yourself, data about your participation in groups, and data from third-party platforms you connect with LearnLabs. We also collect some data automatically, like information about your device and what parts of our Services you interact with or spend time using.
              </p>
              <p>
                1.1 Data You Provide to Us
              </p>
              <p>
                We may collect different data from or about you depending on how you use the Services. Below are some examples to help you better understand the data we collect.
                When you create an account and use the Services, including through a third-party platform, we collect any data you provide directly, including:              
              </p>

              <table >
                <tbody>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Account Data
                    </td>
                    <td style={styles.td2}>
                    In order to use certain features (like joining a group or participating in chat), you need to create a user account. When you create or update your account, we collect and store the data you provide, like your email address, password, gender, and date of birth, and assign you a unique identifying number (“Account Data”).
                    </td>
                  </tr>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Profile Data
                    </td>
                    <td style={styles.td2}>
                    You can also choose to provide profile information like a photo, headline, website link, social media profiles, or other data. Your Profile Data will be publicly viewable by others.                  
                    </td>
                  </tr>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Shared Content
                    </td>
                    <td style={styles.td2}>
                    Parts of the Services let you interact with other users or share content publicly, including by posting reviews on a course page, asking or answering questions, sending messages to students or instructors, or posting photos or other work you upload. Such shared content may be publicly viewable by others depending on where it is posted.
                    </td>
                  </tr>


                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Coach Payment Data
                    </td>
                    <td style={styles.td2}>
                    If you are an Learning Coach, you can link your PayPal, Payoneer, or other payment account to the Services to receive payments. When you link a payment account, we collect and use certain information, including your payment account email address, account ID, physical address, or other data necessary for us to send payments to your account. For security, LearnLabs does not collect or store sensitive bank account information. The collection, use, and disclosure of your payment and billing data is subject to the privacy policy and other terms of your payment account provider.
                    </td>
                  </tr>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Data About Your Accounts on Other Services
                    </td>
                    <td style={styles.td2}>
                    We may obtain certain information through your social media or other online accounts if they are connected to your LearnLabs account. If you login to LearnLabs via Facebook or another third-party platform or service, we ask for your permission to access certain information about that other account. For example, depending on the platform or service we may collect your name, profile picture, account ID number, login email address, location, physical location of your access devices, gender, birthday, and list of friends or contacts.
                    Those platforms and services make information available to us through their APIs. The information we receive depends on what information you (via your privacy settings) or the platform or service decide to give us.
                    If you access or use our Services through a third-party platform or service, or click on any third-party links, the collection, use, and sharing of your data will also be subject to the privacy policies and other agreements of that third party.
                    </td>
                  </tr>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Sweepstakes, Promotions, and Surveys
                    </td>
                    <td style={styles.td2}>
                    We may invite you to complete a survey or participate in a promotion (like a contest, sweepstakes, or challenge), either through the Services or a third-party platform. If you participate, we will collect and store the data you provide as part of participating, such as your name, email address, date of birth, or phone number. That data is subject to this Privacy Policy unless otherwise stated in the official rules of the promotion or in another privacy policy. The data collected will be used to administer the promotion or survey, including for notifying winners and distributing rewards. To receive a reward, you may be required to allow us to post some of your information publicly (like on a winner’s page). Where we use a third-party platform to administer a survey or promotion, the third party’s privacy policy will apply.                  
                    </td>
                  </tr>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Communications and Support
                    </td>
                    <td style={styles.td2}>
                    If you contact us for support or to report a problem or concern (regardless of whether you have created an account), we collect and store your contact information, messages, and other data about you like your name, email address, location, operating system, IP address, and any other data you provide or that we collect through automated means (which we cover below). We use this data to respond to you and research your question or concern, in accordance with this Privacy Policy.
                    </td>
                  </tr>
                </tbody>
              </table>

              <p>
              The data listed above is stored by us and associated with your account.
              </p>
              <p>
              1.2 Data We Collect through Automated Means
              </p>
              <p>
              When you access the Services (including browsing courses and groups), we collect certain data by automated means, including:
              </p>

            <table >
              <tbody>

                <tr style={styles.row}>
                  <td style={styles.td1}>
                  System Data
                  </td>
                  <td style={styles.td2}>
                  Technical data about your computer or device, like your IP address, device type, operating system type and version, unique device identifiers, browser, browser language, domain and other systems data, and platform types (“System Data”).
                  </td>
                </tr>

                <tr style={styles.row}>
                  <td style={styles.td1}>
                  Usage Data
                  </td>
                  <td style={styles.td2}>
                  Usage statistics about your interactions with the Services, including groups accessed, time spent on pages or the Service, pages visited, features used, your search queries, click data, date and time, and other data regarding your use of the Services (“Usage Data”).
                  </td>
                </tr>

                <tr style={styles.row}>
                  <td style={styles.td1}>
                  Approximate Geographic Data
                  </td>
                  <td style={styles.td2}>
                  An approximate geographic location, including information like country, city, and geographic coordinates, calculated based on your IP address.
                  </td>
                </tr>

              </tbody>
            </table> 

            <p>
            The data listed above is collected through the use of server log files and tracking technologies, as detailed in the “Cookies and Data Collection Tools” section below. It is stored by us and associated with your account.
            </p>

            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>2. How We Get Data About You</b>
              </p>
              <p>
              We use tools like cookies, web beacons, analytics services, and advertising providers to gather the data listed above. Some of these tools offer you the ability to opt out of data collection.
              </p>

              <p>
              2.1 Cookies and Data Collection Tools
              </p>
              <p>
              As detailed in our Cookie Policy, LearnLabs and service providers acting on our behalf (like Google Analytics and third party advertisers) use server log files and automated data collection tools like cookies, tags, scripts, customized links, device or browser fingerprints, and web beacons (together, “Data Collection Tools”) when you access and use the Services. These Data Collection Tools automatically track and collect certain System Data and Usage Data (as detailed in Section 1) when you use the Services. In some cases, we tie data gathered through those Data Collection Tools to other data that we collect as described in this Privacy Policy.
              We use cookies (small files that websites send to your device to uniquely identify your browser or device or to store data in your browser) for things like analyzing your use of the Services, personalizing your experience, making it easier to log into the Services, and recognizing you when you return. We use web beacons (small objects that allow us to measure the actions of visitors and users using the Services) for things like identifying whether a page was visited, identifying whether an email was opened, and advertising more efficiently by excluding current users from certain promotional messages or identifying the source of a new mobile app download.
              </p>

              <p>
              LearnLabs uses the following types of cookies:
              </p>

              <ul>
                <li>
                Preferences: cookies that remember data about your browser and preferred settings that affect the appearance and behavior of the Services (like your preferred language).
                </li>
                <li> 
                Security: cookies used to enable you to log in and access the Services; protect against fraudulent logins; and help detect and prevent abuse or unauthorized use of your account.
                </li>
                <li>
                Functional: cookies that store functional settings (like the volume level you set for video playback).
                </li>
                <li>
                Session State: cookies that track your interactions with the Services to help us improve the Services and your browsing experience, remember your login details, and enable processing of your course purchases. These are strictly necessary for the Services to work properly, so if you disable them then certain functionalities will break or be unavailable.
                </li>
              </ul>

              <p>
              You can set your web browser to alert you about attempts to place cookies on your computer, limit the types of cookies you allow, or refuse cookies altogether. If you do, you may not be able to use some or all features of the Services, and your experience may be different or less functional.
              </p>

              <p>
              Some of the third-party partners who provide certain features on our site may also use Local Storage Objects (also known as flash cookies or LSOs) to collect and store data.              
              </p>

              <p>
              2.2 Analytics
              </p>
              <p>
              We use third-party browser and mobile analytics services like Google Analytics, Hotjar, and Intercom on the Services. These services use Data Collection Tools to help us analyze your use of the Services, including information like the third-party website you arrive from, how often you visit, events within the Services, usage and performance data, and where the application was downloaded from. We use this data to improve the Services, better understand how the Services perform on different devices, and provide information that may be of interest to you.
              </p>
               <p>
              2.2 Online Advertising
              </p>
              <p>
              We use third-party advertising services like Taboola, Facebook, Google’s ad services, and other ad networks and ad servers to deliver advertising about our Services on other websites and applications you use. The ads may be based on things we know about you, like your Usage Data and System Data (as detailed in Section 1), and things that these ad service providers know about you based on their tracking data. The ads can be based on your recent activity or activity over time and across other sites and services, and may be tailored to your interests.
              </p>
              <p>
              Depending on the types of advertising services we use, they may place cookies or other tracking technologies on your computer, phone, or other device to collect data about your use of our Services, and may access those tracking technologies in order to serve these tailored advertisements to you. To help deliver tailored advertising, we may provide these service providers with a hashed, anonymized version of your email address (in a non-human-readable form) and content that you share publicly on the Services.
              </p>
              <p>
              When using mobile applications you may also receive tailored in-app advertisements. Apple iOS, Android OS, and Microsoft Windows each provide their own instructions on how to control in-app tailored advertising. For other devices and operating systems, you should review your privacy settings or contact your platform operator.
              </p>


            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>3. What We Use Your Data For </b>
              </p>
              <p>
              We use your data to do things like provide our Services, communicate with you, troubleshoot issues, secure against fraud and abuse, improve and update our Services, analyze how people use our Services, serve personalized advertising, and as required by law or necessary for safety and integrity.
              </p>
              <p>
              We use the data we collect through your use of the Services to:
              </p>

              <ul>
                <li>
                  Provide and administer the Services, including to display customized content and facilitate communication with other users.
                </li>
                <li>
                  Communicate with you about your account by:
                  <ul>
                    <li>
                      Responding to your questions and concerns;
                    </li>
                    <li>
                      Sending you administrative messages and information, including messages from instructors and teaching assistants, notifications about changes to our Service, and updates to our agreements;
                    </li>
                    <li>
                      Sending you information and in-app messages about your progress in courses, or participation in groups, rewards programs, new services, new features, promotions, newsletters, and other available courses and groups (which you can opt out of at any time);
                    </li>
                    <li>
                      Sending push notifications to your wireless device to provide updates and other relevant messages (which you can manage from the “options” or “settings” page of the mobile app);                    </li>
                  </ul>
                </li>
                <li>
                  Manage your account preferences;
                </li>
                <li>
                  Facilitate the Services’ technical functioning, including troubleshooting and resolving issues, securing the Services, and preventing fraud and abuse;
                </li>  
                <li>
                  Solicit feedback from users;
                </li>  
                <li>
                  Market and administer surveys and promotions administered or sponsored by LearnLabs;
                </li>  
                <li>
                  Learn more about you by linking your data with additional data through third-party data providers or analyzing the data with the help of analytics service providers;
                </li>  
                <li>
                Identify unique users across devices;
                </li>  
                <li>
                Tailor advertisements across devices;
                </li>  
                <li>
                Improve our Services and develop new products, services, and features;
                </li>    
                <li>
                Analyze trends and traffic, track purchases, and track usage data;
                </li>    
                <li>
                Advertise the Services on third-party websites and applications;
                </li>    
                <li>
                As required or permitted by law; or
                </li>    
                <li>
                As we, in our sole discretion, otherwise determine to be necessary to ensure the safety or integrity of our users, employees, third parties, the public, or our Services.
                </li>    
              </ul>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              4. Who We Share Your Data With
              </b>
              </p>
              <p>
              We share certain data about you with instructors, other students, companies performing services for us, our business partners, analytics and data enrichment providers, your social media providers, companies helping us run promotions and surveys, and advertising companies who help us promote our Services. We may also share your data as needed for security, legal compliance, or as part of a corporate restructuring. Lastly, we can share data in other ways if it is aggregated or de-identified or if we get your consent.
              </p>
              <p>
              We may share your data with third parties under the following circumstances or as otherwise described in this Privacy Policy:
              </p>

              <ul>
                <li>
                With Other Learners and Coaches: Depending on your settings, your shared content and profile data may be publicly viewable, including to other learners and coaches. If you ask a question to another user, group, or coach, your information (including your name) may also be publicly viewable by other users depending on your settings.
                </li>
                <li>
                With Service Providers, Contractors, and Agents: We share your data with third-party companies who perform services on our behalf, like payment processing, data analysis, marketing and advertising services (including retargeted advertising), email and hosting services, and customer services and support. These service providers may access your personal data and are required to use it solely as we direct, to provide our requested service.
                </li>
                <li>
                With Business Partners: We have agreements with other websites and platforms to distribute our Services and drive traffic to LearnLabs. Depending on your location, we may share your data with these partners.
                </li>
                <li>
                With Analytics and Data Enrichment Services: As part of our use of third-party analytics tools like Google Analytics and data enrichment services like Clearbit, we share certain contact information, Account Data, System Data, Usage Data (as detailed in Section 1), or de-identified data as needed. De-identified data means data where we’ve removed things like your name and email address and replaced it with a token ID. This allows these providers to provide analytics services or match your data with publicly-available database information (including contact and social information from other sources). We do this to communicate with you in a more effective and customized manner.
                </li>
                <li>
                To Power Social Media Features: The social media features in the Services (like the Facebook Like button) may allow the third-party social media provider to collect things like your IP address and which page of the Services you’re visiting, and to set a cookie to enable the feature. Your interactions with these features are governed by the third-party company’s privacy policy.
                </li>
                <li>
                To Administer Promotions and Surveys: we may share your data as necessary to administer, market, or sponsor promotions and surveys you choose to participate in, as required by applicable law (like to provide a winners list or make required filings), or in accordance with the rules of the promotion or survey.
                </li>
                <li>
                For Advertising: If we decide to offer advertising in the future, we may use and share certain System Data and Usage Data with third-party advertisers and networks to show general demographic and preference information among our users. We may also allow advertisers to collect System Data through Data Collection Tools (as detailed in Section 2.1), and to use this data to offer you targeted ad delivery to personalize your user experience (through behavioral advertising) and undertake web analytics. Advertisers may also share with us the data they collect about you. To learn more or opt out from participating ad networks’ behavioral advertising, see Section 6.1 (Your Choices About the Use of Your Data) below. Note that if you opt out, you’ll continue to be served generic ads.
                </li>
                <li>
                For Security and Legal Compliance: We may disclose your data to third parties if we (in our sole discretion) have a good faith belief that the disclosure is:
                  <ul>
                    <li>
                    Permitted or required by law;
                    </li>
                    Requested as part of a judicial, governmental, or legal inquiry, order, or proceeding;
                    <li>
                    Reasonably necessary as part of a valid subpoena, warrant, or other legally-valid request;
                    </li>
                    <li>
                    Reasonably necessary to enforce our Terms of Use, Privacy Policy, and other legal agreements;
                    </li>
                    <li>
                    Required to detect, prevent, or address fraud, abuse, misuse, potential violations of law (or rule or regulation), or security or technical issues; or
                    </li>
                    <li>
                    Reasonably necessary in our discretion to protect against imminent harm to the rights, property, or safety of LearnLabs, our users, employees, members of the public, or our Services.
                    </li>
                    <li>
                    We may also disclose data about you to our auditors and legal advisors in order to assess our disclosure obligations and rights under this Privacy Policy.
                    </li>                                                          
                  </ul>
                </li>
                <li>
                During a Change in Control: If LearnLabs undergoes a business transaction like a merger, acquisition, corporate divestiture, or dissolution (including bankruptcy), or a sale of all or some of its assets, we may share, disclose, or transfer all of your data to the successor organization during such transition or in contemplation of a transition (including during due diligence).
                </li>  
                <li>
                After Aggregation/De-identification: we can disclose or use aggregate or de-identified data for any purpose.
                </li>
                <li>
                With Your Permission: with your consent, we may share data to third parties outside the scope of this Privacy Policy.
                </li>          
              </ul>
            </div>
          </div>


          <div className="row">
            <div className="col-12">
              <p>
              <b>
              5. Security
              </b>
              </p>
              <p>
              We use appropriate security based on the type and sensitivity of data being stored. As with any internet-enabled system, there is always a risk of unauthorized access, so it’s important to protect your password and to contact us if you suspect any unauthorized access to your account.
              </p>
              <p>
              LearnLabs takes appropriate security measures to protect against unauthorized access, alteration, disclosure, or destruction of your personal data that we collect and store. These measures vary based on the type and sensitivity of the data. Unfortunately, however, no system can be 100% secured, so we cannot guarantee that communications between you and LearnLabs, the Services, or any information provided to us in connection with the data we collect through the Services will be free from unauthorized access by third parties. Your password is an important part of our security system, and it is your responsibility to protect it. You should not share your password with any third party, and if you believe your password or account has been compromised, you should change it immediately and contact support@learnlabs.city with any concerns.
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              6. Your Rights
              </b>
              </p>
              <p>
              You have certain rights around the use of your data, including the ability to opt out of promotional emails, cookies, and collection of your data by certain analytics providers. You can update or terminate your account from within our Services, and can also contact us for individual rights requests about your personal data. Parents who believe we’ve unintentionally collected personal data about their underage child should contact us for help deleting that information.
              </p>
              <p>
              6.1 Your Choices About the Use of Your Data
              </p>
              <p>
              You can choose not to provide certain data to us, but you may not be able to use certain features of the Services.
              </p>
              <ul>
                <li>
                To stop receiving promotional communications from us, you can opt out by using the unsubscribe mechanism in the promotional communication you receive or by changing the email preferences in your account. Note that regardless of your email preference settings, we will send you transactional and relationship messages regarding the Services, including administrative confirmations, order confirmations, important updates about the Services, and notices about our policies.
                </li>
                <li>
                The browser or device you use may allow you to control cookies and other types of local data storage. Your wireless device may also allow you to control whether location or other data is collected and shared. You can manage Adobe’s LSOs through their Website Storage Settings panel.
                </li>
                <li>
                To get information and control cookies used for tailored advertising from participating companies, see the consumer opt-out pages for the Network Advertising Initiative and Digital Advertising Alliance, or if you’re located in the European Union, visit the Your Online Choices site. To opt out of Google’s display advertising or customize Google Display Network ads, visit the Google Ads Settings page. To opt out of Taboola’s targeted ads, see the Opt-out Link in their Cookie Policy.
                </li>
                <li>
                To opt out of allowing Google Analytics, Hotjar, Mixpanel, ZoomInfo, or Clearbit to use your data for analytics or enrichment, see the Google Analytics Opt-out Browser Add-on, Hotjar Opt-Out Cookie, Mixpanel Opt-Out Cookie, ZoomInfo’s policy, and Clearbit data claiming mechanism.
                </li>
              </ul>
              <p>
              If you have any questions about your data, our use of it, or your rights, contact us at support@learnlabs.city
              </p>
              <p>
              6.2 Accessing, Updating, and Deleting Your Personal Data
              </p>
              <p>
              You can access and update your personal data that LearnLabs collects and maintains as follows:
              </p>
              <ul>
                <li>
                To update data you provide directly, log into your account and update your account profile at any time.
                </li>
                <li>
                To terminate your account:
                  <ul>
                    <li>
                    Visit your LearnLabs profile page and select the option to ‘Delete Account’
                    </li>
                  </ul>
                </li>
              </ul>
              <p>
              6.3 Our Policy Concerning Children
              </p>
              <p>
              We recognize the privacy interests of children and encourage parents and guardians to take an active role in their children’s online activities and interests. Children under 13 (or under 16 in the European Economic Area) should not use the Services. If we learn that we’ve collected personal data from a child under those ages, we will take reasonable steps to delete it.
              </p>
              <p>
              Parents who believe that LearnLabs may have collected personal data from a child under those ages can submit a request that it be removed to support@learnlabs.city
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              7. Jurisdiction-Specific Rules
              </b>
              </p>
              <p>
              If you live in California, you have certain rights to request information. Users outside of the United States should note that we transfer data to the US and other areas outside of the European Economic Area.
              </p>
              <p>
              7.1 Users in California
              </p>
              <p>
              If you are a California resident, you have the right to request certain details about what personal information we share with third parties for those third parties’ direct marketing purposes. To submit your request, send an email to support@learnlabs.city with the phrase “California Shine the Light” and include your mailing address, state of residence, and email address.
              </p>
              <p>
              Since the internet industry is still working on Do Not Track standards, solutions, and implementations, we do not currently recognize or respond to browser-initiated Do Not Track signals.
              </p>
              <p>
              7.2 Users Outside of the U.S.
              </p>
              <p>
              LearnLabs, LLC. is headquartered in New York, and in order to provide the Services to you we must transfer your data to the United States and process it there. By visiting or using our Services, you consent to storage of your data on servers located in the United States. If you are using the Services from outside the United States, you consent to the transfer, storage, and processing of your data in and to the United States or other countries. Specifically, personal data collected in Switzerland and the European Economic Area (“EEA”) is transferred and stored outside those areas.
              </p>
              <p>
              That data is also processed outside of Switzerland and the EEA by our LearnLabs group companies, or our service providers, including to process transactions, facilitate payments, and provide support services as described in Section 4. We have entered into data processing agreements with our service providers that restrict and regulate their processing of your data on our behalf. By submitting your data or using our Services, you consent to this transfer, storage, and processing by LearnLabs and its processors.
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              8. Updates & Contact Info
              </b>
              </p>
              <p>
              When we make a material change to this policy, we’ll notify users via email, in-product notice, or another mechanism required by law. Changes become effective the day they’re posted. Please contact us via email or postal mail with any questions, concerns, or disputes.
              </p>
              <p>
              8.1 Modifications to This Privacy Policy
              </p>
              <p>
              From time to time, we may update this Privacy Policy. If we make any material change to it, we will notify you via email, through a notification posted on the Services, or as required by applicable law. We will also include a summary of the key changes. Unless stated otherwise, modifications will become effective on the day they are posted.
              </p>
              <p>
              As permitted by applicable law, if you continue to use the Services after the effective date of any change, then your access and/or use will be deemed an acceptance of (and agreement to follow and be bound by) the revised Privacy Policy. The revised Privacy Policy supersedes all previous Privacy Policies.
              </p>
              <p>
              8.2 Interpretation
              </p>
              <p>
              Any capitalized terms not defined in this policy are defined as specified in LearnLabs's Terms of Use. Any version of this Privacy Policy in a language other than English is provided for convenience. If there is any conflict with a non-English version, you agree that the English language version will control.
              </p>
              <p>
              8.3 Questions
              </p>
              <p>
              If you have any questions, concerns, or disputes regarding our Privacy Policy, please feel free to contact our privacy team (including our designated personal information protection manager) at support@learnlabs.city
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              Cookie Policy
              </b>
              </p>
              <p>
              What are cookies?
              </p>
              <p>
              Cookies are small text files stored by your browser as you browse the internet. They can be used to collect, store, and share data about your activities across websites, including on LearnLabs. Cookies also allow us to remember things about your visits to LearnLabs, like your preferred language, and to make the site easier to use.
              </p>
              <p>
              We use both session cookies, which expire after a short time or when you close your browser, and persistent cookies, which remain stored in your browser for a set period of time. We use session cookies to identify you during a single browsing session, like when you log into LearnLabs. We use persistent cookies where we need to identify you over a longer period, like when you request that we keep you signed in.
              </p>
              <p>
              Why does LearnLabs use cookies and similar technologies?
              </p>
              <p>
              We use cookies and similar technologies like web beacons, pixel tags, or local shared objects (“flash cookies”), to deliver, measure, and improve our services in various ways. We use these cookies both when you visit our site and services through a browser and through our mobile app. As we adopt additional technologies, we may also gather additional data through other methods.
              </p>
              <p>
              We use cookies for the following purposes:
              </p>
              <table >
                <tbody>
                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Authentication and security
                    </td>
                    <td style={styles.td2}>
                      <ul>
                        <li>
                        To log you into LearnLabs
                        </li>
                        <li>
                        To protect your security
                        </li>
                        <li>
                        To help detect and fight spam, abuse, and other activities that violate LearnLabs’s agreements
                        </li>
                      </ul>
                    </td>
                  </tr>

                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Preferences
                    </td>
                    <td style={styles.td2}>
                      <ul>
                        <li>
                        To remember data about your browser and your preferences
                        </li>
                        <li>
                        To remember your settings and other choices you’ve made                        
                        </li>
                        <li>
                        To help detect and fight spam, abuse, and other activities that violate LearnLabs’s agreements
                        </li>
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p>
              For example, cookies help us remember your preferred language or the country you’re in, so we can provide content in your preferred language without asking each time you visit.
              </p>
              <table >
                <tbody>
                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Analytics and research
                    </td>
                    <td style={styles.td2}>
                      <ul>
                        <li>
                        To help us improve and understand how people use LearnLabs
                        </li>
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p>
              For example, cookies help us test different versions of LearnLabs to see which features or content users prefer, web beacons help us determine which email messages are opened, and cookies help us see how you interact with LearnLabs, like the links you click on.
              </p>
              <p>
              We also work with a number of analytics partners, including Google Analytics and Mixpanel, who use cookies and similar technologies to help us analyze how users use the Services, including by noting the sites from which you arrive. Those service providers may either collect that data themselves or we may disclose it to them.
              </p>
              <p>
              You can opt out of some of these services through tools like the Google Analytics Opt-out Browser Add-on and Hotjar Opt-Out Cookie.
              </p>
              <table >
                <tbody>
                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Personalized content
                    </td>
                    <td style={styles.td2}>
                      <ul>
                        <li>
                        To customize LearnLabs with more relevant content
                        </li>
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p>
              For example, cookies help us show a personalized list of recommended courses or groups on the homepage.
              </p>
              <table >
                <tbody>
                  <tr style={styles.row}>
                    <td style={styles.td1}>
                    Advertising
                    </td>
                    <td style={styles.td2}>
                      <ul>
                        <li>
                        To provide you with more relevant advertising
                        </li>
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table>
              <p>
              To learn more about targeting and advertising cookies and how you can opt out, visitwww.allaboutcookies.org/manage-cookies/index.html, or if you’re located in the European Union, visit the Your Online Choices site.
              </p>
              <p>
              Please note that where advertising technology is integrated into the Services, you may still receive advertising on other websites and applications, but it will not be tailored to your interests.
              </p>
              <p>
              When using mobile applications you may also receive tailored in-app advertisements. Apple iOS, Android OS, and Microsoft Windows each provide its own instructions on how to control in-app tailored advertising. For other devices and operating systems, you should review your privacy settings or contact your platform operator.
              </p>
              <p>
              What are my privacy options?
              </p>
              <p>
              You have a number of options to control or limit how we and our partners use cookies:
              </p>
              <ul>
                <li>
                Most browsers automatically accept cookies, but you can change your browser settings to decline cookies by consulting your browser’s support articles. If you decide to decline cookies, please note that you may not be able to sign in, customize, or use some interactive features in the Services.
                </li>
                <li>
                Flash cookies operate differently than browser cookies, so your browser’s cookie-management tools may not remove them. To learn more about how to manage Flash cookies, see Adobe’s article on managing flash cookiesand Website Storage Settings panel.
                </li>
                <li>
                To get information and control cookies used for tailored advertising from participating companies, see the consumer opt-out pages for the Network Advertising Initiative and Digital Advertising Alliance, or if you’re located in the European Union, visit the Your Online Choices site. To opt out of Google Analytics’ display advertising or customize Google Display Network ads, visit the Google Ads Settings page.
                </li>
                <li>
                For general information about targeting cookies and how to disable them, visit www.allaboutcookies.org.
                </li>
              </ul>

              <p>
              Updates & Contact Info
              </p>
              <p>
              From time to time, we may update this Cookie Policy. If we do, we’ll notify you by posting the policy on our site with a new effective date. If we make any material changes, we’ll take reasonable steps to notify you in advance of the planned change.
              </p>
              <p>
              If you have any questions about our use of cookies, please email us at support@learnlabs.city
              </p>
            </div>
          </div>



        </div>
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy);


const styles = {
  row: {padding: "20px", border: "1px solid #ccc"},
  td1: {padding: "20px", fontWeight: "700", borderRight: "1px solid #ccc"},
  td2: {padding: "20px"}
}

