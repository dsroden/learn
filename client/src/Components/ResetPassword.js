/*eslint no-useless-escape: "off"*/

import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import ReactPasswordStrength from 'react-password-strength';
import Api from "../Api";
import logo from '../assets/learnlabs_logo-01.png';
const FA = require('react-fontawesome')

// const queryString = require('query-string');

class ResetPassword extends Component  {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handlePasswordReset = this.handlePasswordReset.bind(this);
    this.state = {
      verified: false,
      feedback: false,
      email: '',
      emailValid: false,
      loading: true,
      token: false,
      password: '', 
      buttonIsHovered: false,
      passwordsMatch: '',
      errorMsg: false,
      successMsg: false,
    }
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  componentDidMount(){
    let $this = this;
    let currentUrl = $this.props.history.location
    let parsedUrl = new URLSearchParams(currentUrl.search);
    let parsed = {};
    parsed.token = parsedUrl.get("token");

    if(parsed.token){
      this.setState({token: parsed.token})
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });

    if(name === 'email'){
      let regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      let emailValid = (regex.test(value)) ? true : false;
      this.setState({emailValid: emailValid})
    }


    if(name === 'confirmPassword'){
      if(value === this.state.password && this.state.passwordValid){
        this.setState({passwordsMatch: true})
      } else {
        this.setState({passwordsMatch: false})
      }
    }
  }

  changeCallback = (event) =>{
    this.setState({password: event.password, passwordValid: event.isValid})
  }

  signUpButtonHovered = (val) =>{
    this.setState({buttonIsHovered: val});
  }

  handlePasswordReset = (event) =>{
    event.preventDefault();

    let data = {};
    if(!this.state.passwordsMatch || !this.state.emailValid){
      return;
    }
    data.email = this.state.email;
    data.password = this.state.password;
    data.token = this.state.token;
    Api.resetPassword(data).then((res)=>{
      if(res.status === 200){
        this.setState({successMsg: 'Password reset successfuly. Redirecting to login.'})
        this.nav('');
      } else {
        this.setState({errorMsg: 'There was an error resetting the password, please check for the correct email and that the passwords match.'})
        setTimeout(()=>{
          this.setState({errorMsg: false})
        }, 6000)
      }
    });

  }
  render(){
    const inputProps = {
      placeholder: "Create password",
      className: 'another-input-prop-class-name',
    };

    return (
      <div className="learnlabs-app">
        <div style={{height: "50px"}}></div>
        <img alt="logo" src={logo} style={{display: "block", marginLeft: "auto", marginRight: "auto"}} width="100" height="100" />
        <div style={{height: "20px"}}></div>
        <h1 style={{textAlign: "center"}}>LearnLabs</h1>
        <div style={{height: "10px"}}></div>

        <form style={styles.formContainer} onSubmit={this.handlePasswordReset}>
          <div className="row">
            <div className="col-12">
              <div style={{textAlign: 'center', padding: "15px"}}><b> Please enter your email and a new password</b></div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <input style={styles.input} type="email" placeholder="Email address" ref="email" name="email" value={this.state.email} onChange={this.handleInputChange} autoComplete="email"/>
              <div style={{display: ((this.state.emailValid) ? 'inherit' : 'none'), position: 'absolute', right: '20px', top: '0', lineHeight: '40px', color: Constants.BRAND_GREEN, fontSize: '1.2em' }}>
                <FA name="check" />
              </div>
            </div>
          </div>
       
          <div className="row">
            <div className="col-12 ">
              <ReactPasswordStrength
                  style={styles.passwordInput}
                  ref={ref => this.ReactPasswordStrength = ref}
                  minLength={6}
                  inputProps={{ ...inputProps, id: 'inputPassword1' }}
                  changeCallback={this.changeCallback}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12 ">
              <input style={styles.input} type="password" placeholder="Confirm password" ref="title" name="confirmPassword" value={this.state.confirmPassowrd} onChange={this.handleInputChange} />
              <div style={{display: ((this.state.passwordsMatch) ? 'inherit' : 'none'), position: 'absolute', right: '20px', top: '0', lineHeight: '40px', color: Constants.BRAND_GREEN, fontSize: '1.2em' }}>
                <FA name="check" />
              </div>
            </div>
          </div>  

          <div className="row">
            <div className="col-12 ">    
              <button 
                      // onMouseEnter={this.signUpButtonHovered.bind(this, true)} 
                      // onMouseLeave={this.signUpButtonHovered.bind(this, false)}
                      onClick={this.handlePasswordReset}
                      className="general-button center-item color-border"
                      // style={((this.state.buttonIsHovered) ? styles.signUpBtnHover : styles.signUpBtn)} 
                      type="submit"
                      >Reset</button>
            </div>
          </div>
          
        </form>
        <div style={{height: "15px"}}></div>
        <div style={{maxWidth: "400px", display: "block", marginLeft: "auto", marginRight: "auto"}}>
          <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%', height: '100%', backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
          <div style={{display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%', height: '100%', backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             
        </div>
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);


const styles = {
    browsePageContainer: {
    "height": "100%",
    "minHeight": "100vh",
    "width:": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "boxShadow": "0px 0px 3px " + Constants.BRAND_BLACK,
    fontFamily: "Montserrat, sans-serif"
  },
  centerBlock: {
    width: "100%",
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
  },
  formContainer: {
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },

  input: {
    "height": "50px",
    width: "100%",
    maxWidth: "400px",
    border: '1px solid #c6c6c6',
    padding: '15px',
    marginBottom: '10px',
  },
  passwordInput: {
    marginBottom: '10px',
    "height": "50px",
    width: "100%",
    maxWidth: "400px",
    backgroundColor: "white"
  },
  signUpBtn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  signUpBtnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_BLACK,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
}

