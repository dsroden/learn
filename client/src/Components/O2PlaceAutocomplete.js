import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import { updateSearchPlaceAction, placeUpdatedAction } from "../Actions/actionCreator";
import Api from "../Api"

const FA = require('react-fontawesome')


class O2PlaceAutocomplete extends Component {
  constructor (props) {
    super(props)
    this.state = {ip_data: null, color: this.props.color || Constants.BRAND_RED, address: this.props.address || '' , location: null};
  }


  renderFooter = () => (
    <div className="dropdown-footer">
      <div>
        <img style={{width: '150px', float: 'right'}} alt="google-logo" src={require('../assets/powered_by_google_on_white_hdpi.png')} />
      </div>
    </div>
  )

  // componentDidMount(){
  //  let $this = this;
  //   getLocalIPs(function(ips) { // <!-- ips is an array of local IP addresses.
  //     // document.body.textContent = 'Local IP addresses:\n ' + ips.join('\n ');
  //       // freegeoip.net/{format}/{IP_or_hostname}
  //     // http://api.ipstack.com/95.91.215.97?access_key=c26d64adfebba0896785c57f0bcd5c58
  //     console.log('ips', ips);
  //     if(ips.length < 1){
  //       return;
  //     }
  //     // http://api.hostip.info/?ip=
  //     // http://ip-api.com/json/208.80.152.201
  //     axios.get(`http://ip-api.com/json/${ips[0]}`).then((res)=>{

  //     // axios.get(`http://api.ipstack.com/${ips[1]}?access_key=${Constants.IPSTACK_KEY}`).then((res)=>{
  //     // axios.get(`https://freegeoip.net/json/${ips[1]}`).then((res)=>{
  //       $this.setState({ip_data: res})
  //       console.log('ip locaiton', res);
  //       if(res.data.city){
  //         let address = res.data.city + ", " + res.data.country_name;
  //         $this.setState({address: address, location: {lat: res.data.latitude, lng: res.data.longitude}}, ()=>{
  //           $this.props.placeUpdatedAction({address: address, location: {lat: res.data.latitude, lng: res.data.longitude}})
  //         })
  //       }
  //     }).catch((err)=>{
  //        console.log('error creating example', err);
  //     })
  //   });

  // }
 
  componentDidMount(){
    let $this = this;
    if($this.state.address){
      return;
    }
    Api.lookupIp().then((res)=>{
      // console.log('res', res);
      if(res.status === 200){
        // let address = res.data.address
        // $this.setState({address: address, location: {lat: res.data.latitude, lng: res.data.longitude}}, ()=>{
          // setTimeout(()=>{
            // $this.props.placeUpdatedAction({address: address, location: {lat: res.data.location.lat, lng: res.data.location.long}})
          // }, 1000);
        // })
      } else {
        return;
      }
    });
  }

  locationSelection = (address) =>{
    let $this = this;
    $this.setState({address}, ()=>{
     geocodeByAddress($this.state.address)
      .then(results => getLatLng(results[0]))
      .then(latLng => this.setState({location: latLng}, ()=>{this.props.updateSearchPlaceAction({address: this.state.address, location: this.state.location})}))
      .catch(error => this.setState({location: null}, ()=>{this.props.updateSearchPlaceAction(null); this.props.placeUpdatedAction(null)}))
    })
  }

  onError = (status, clearSuggestions) => {
    // console.log('Google Maps API returned error with status: ', status)
    clearSuggestions()
  }

  selectPlace = (address) =>{
     let $this = this;
    $this.setState({address}, ()=>{
     geocodeByAddress($this.state.address)
      .then(results => getLatLng(results[0]))
      .then(latLng => this.setState({location: latLng}, ()=>{this.props.placeUpdatedAction({address: this.state.address, location: this.state.location})}))
      .catch(error => this.setState({location: null}, ()=>{this.props.updateSearchPlaceAction(null); this.props.placeUpdatedAction(null)}))
    })   
  }

  render() {
    const inputProps = {
      value: this.state.address,
      onChange: this.locationSelection,
      placeholder: 'Search by city...',
    }
    const autocompletePlacesStyle = {
      root: {
            display: 'inline-block',
            "width": "90%",
            "height": "50px",
            marginTop: "10px",
            marginBottom: "10px",
            border: 'none',
            // borderBottom: '2px solid ' + Constants.BRAND_RED,
            zIndex: '2',
            lineHeight: "35px"

      },
      input: { width: '100%', border: 'none' },
      autocompleteContainer: { backgroundColor: Constants.BRAND_LIGHTGRAY },
      autocompleteItem: { color: 'black' },
      autocompleteItemActive: { color: Constants.BRAND_GREEN },
    }
  

    let autocompletePlaces = (
                  <div>
                  <div>
                    <FA style={{...styles.inputIcon,...{color: this.state.color}}} name="map-marker" />
                    <PlacesAutocomplete    
                    styles={autocompletePlacesStyle} 
                    inputProps={inputProps} 
                    renderFooter={this.renderFooter} 
                    onError={this.onError}
                    onSelect={this.selectPlace} 
                    />
                  </div>
                  </div>
    )


    return (
        <div>
          {autocompletePlaces}    
        </div>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  placeUpdatedAction,
  updateSearchPlaceAction
};

export default connect(mapStateToProps, mapDispatchToProps)(O2PlaceAutocomplete);




const styles = {
  inputIcon: {
    width: "10%",
    textAlign: 'center',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_RED,
    fontSize: "1.8em"
  }
}


