import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import  { o2LoadImage } from '../Utils'
import Api from "../Api";
import { showUserProfileAction, openUserProfileViewAction } from "../Actions/actionCreator";

class ProfilePhoto extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      image_valid: false,
      transform: false,
      size: this.props.size + 'px'
    }
  }

  componentDidMount(){
    let $this = this;
    let imageUrl = Constants.PROFILE_PHOTO_BASE_URL + $this.state.user.username + '.png?raw=true';
    
    o2LoadImage(imageUrl).then((result)=>{
      if(result.image_valid){
        $this.setState({image_valid: true, transform: result.transform})
      }
    });
  }

  getAlphabetIndexColor(letter){
    let ALPHABET = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let index = ALPHABET.indexOf(letter);
    let color = Constants.ALPHABET_COLORS[index];
    if(color === undefined || color === null || color === ''){
      color = 'black';
    }
    return color;
  }

  showUserProfile(){
    if(!window.localStorage.getItem('access_token')){
      return;
    }
    Api.fetchUserProfile(this.state.user.username).then((response)=>{
      this.props.openUserProfileViewAction(true)
      response.data.details = true;
      this.props.showUserProfileAction(response.data)
    });
  }

  render(){
    let username_color = (this.state.user && this.state.user.username) ? this.getAlphabetIndexColor(this.state.user.username[0].toLowerCase()) : '';
    let profile_photo = (<div></div>)
    if(this.state.user.username){
    if(!this.state.user.profile_photo && !this.state.image_valid ){
      profile_photo = (
          <div onClick={this.showUserProfile.bind(this)} style={{position: "relative", display: "inline-block", width: this.state.size, height: this.state.size, textAlign: 'center', lineHeight: this.state.size, color: "white", fontWeight: "bold",  marginTop: "5px", borderRadius: "100%", cursor: "pointer", backgroundColor: username_color}}> 
            {this.state.user.username[0].toUpperCase()}
          </div>
      ) 
    } else {
      profile_photo = (
        <div onClick={this.showUserProfile.bind(this)} style={{position: "relative", display: "inline-block", width: this.state.size, height: this.state.size, textAlign: 'center', lineHeight: this.state.size, color: "white", fontWeight: "bold",  marginTop: "5px", borderRadius: "100%", cursor: "pointer", backgroundColor: username_color}}> 
            {this.state.user.username[0].toUpperCase()}
            <div style={{position: 'absolute', top: "0px", width: this.state.size, height: this.state.size, borderRadius: "100%", backgroundImage: 'url("' + Constants.PROFILE_PHOTO_BASE_URL + this.state.user.username + '.png")', backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat", transform: this.state.transform}}> 
            </div>
          </div>
      )      
    }
    }
    return (
      <div >
        {profile_photo}
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  showUserProfileAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePhoto);



