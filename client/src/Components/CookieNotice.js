
import React, { Component } from 'react';

import Constants from '../Constants'

class CookieNotice extends Component {
constructor(props) {
    super(props);
    this.state = {
    	agreed: false
    }
  }

  componentDidMount(){
  	var agreed = window.localStorage.getItem('cookie_notice');
  	if(agreed){
  		this.setState({agreed: true})
  	}
  }

  agree(){
  	this.setState({agreed: true});
  	window.localStorage.setItem('cookie_notice', true);
  }

  render() {
  	var view = (
  		<React.Fragment></React.Fragment>
  	)

  	if(!this.state.agreed){
  		view = (
        <div className="container-fluid" style={styles.noticeContainer}>
            <div>LearnLabs uses browser cookies to give you the best possible experience.
            To make LearnLabs work, we log user data and share it with processors.
            To use LearnLabs, you must agree to our Privacy Policy, including cookie policy.
			</div>
            <a style={styles.link} href="/privacy">Privacy Policy</a>
            <span> & </span>
            <a style={styles.link}  href="/terms">Terms of Use</a>
            <div></div>
            <div style={{height: "5px"}}></div>
            <button onClick={this.agree.bind(this)} className="general-button" style={{height: "40px", maxWidth: "100px"}}>I agree</button>
        </div>
  		)
  	}
    return (<div>
    	{view}
    	</div>
    );
  }
}

export default CookieNotice;

const styles = {
  "noticeContainer": {
  	position: "fixed",
  	zIndex: '9',
  	bottom: "0",
  	right: "0",
  	left: "0",
    width: "100%",
    backgroundColor: Constants.BRAND_GRAY,
    color: Constants.BRAND_WHITE,
    textAlign: 'center',
    padding: "10px",
    fontSize: "12px"
  },
  link: {
    display: "inline",
    color: "white"
  }
}