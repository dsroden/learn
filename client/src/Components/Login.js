/*eslint no-useless-escape: "off"*/

import React, { Component } from 'react';
import Constants from '../Constants';
import SocialButton from './SocialButton';
import ReactPasswordStrength from 'react-password-strength';
import { connect } from 'react-redux';
import { accountStateUpdatedAction } from "../Actions/actionCreator";
// import logo from '../assets/learnlabs_logo-01.png';
import logo from '../assets/circle_logo-01.png';
import Api from "../Api";

const FA = require('react-fontawesome')
// const Months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
// const Days = [];
// const Years = [];
// for(var i = 1; i < 32; i++){
//   Days.push(i);
// }

// for(var j = 2018; j > 1900; j--){
//   Years.push(j);
// }

class Login extends Component {
  constructor (props) {
    super(props)
    this.handleLogin = this.handleLogin.bind(this);
    this.handleRegistration = this.handleRegistration.bind(this);
    this.handleForgotPassword = this.handleForgotPassword.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      logoHover: false,
      signupHover: false,
      loginHover: false,
      open: false,
      action: this.props.action, 
      email: '', 
      password: '', 
      username: '',
      buttonIsHovered: false,
      passwordsMatch: false,
      emailValid: false,
      first_name: '',
      last_name: '',
      monthValue: '',
      dayValue: '',
      yearValue: '',
      forgotPassword: false,
      termsAgree: false,
      errorMsg: false
    };
  }


  setButtonHovered = (type, val) =>{
    let state = type + 'Hover';
    this.setState({[state]: val});
  }

  signUpButtonHovered = (val) =>{
    this.setState({buttonIsHovered: val});
  }
  onOpenModal = (action) => {
    this.setState({ open: true, action: action});
  };

  onCloseModal = () => {
    this.setState({ logoHover: false,
      signupHover: false,
      loginHover: false,
      open: false,
      action: null, 
      email: '', 
      password: '', 
      buttonIsHovered: false,
      passwordsMatch: '',
      emailValid: false,
      errorMsg: false,
      successMsg: false,
      emailSignup: false,
      username: '',
      termsAgree: false
    });
  };

  handleSocialLogin = (user) => {
    let $this = this;
    let data = new FormData();
    data.append('_profile', JSON.stringify(user._profile));
    data.append('_token', JSON.stringify(user._token));
    data.append('_provider', user._provider);
    Api.socialLogin(data).then(res=>{
      if(res.status === 200){
        $this.storeToken(res.data.access_token);
        $this.props.accountStateUpdatedAction('logged_in');   
      } else {
        $this.setState({errorMsg: 'Registeration attempt failed, please try again.'}, ()=>{
          setTimeout(()=>{
            $this.setState({errorMsg: false})
          }, 2000)
        })          
      }
    });
  }

  handleSocialLoginFailure = (err) => {
    // console.error(err)
  }

  handleRegistration(event) {
    //prevent from reload
    event.preventDefault();
    //create form data 
    // console.log('terms', this.state.termsAgree);
    // if(!this.state.passwordsMatch || !this.state.emailValid || !this.state.passwordValid || !this.state.username || !this.state.yearValue || !this.state.dayValue || !this.state.monthValue){
    if(!this.state.emailValid || !this.state.passwordValid || !this.state.username || !this.state.termsAgree){
      this.setState({errorMsg: 'Please check the registeration fields and terms.'}, ()=>{
                  setTimeout(()=>{
                    this.setState({errorMsg: false})
                  }, 4000)
      })
    } else {
      // if((new Date().getFullYear()) - (parseInt(this.state.yearValue, 10)) < 18){
      //   this.setState({errorMsg: 'Please check the age field'})
      //   return;
      // }
      let $this = this;
      let data = new FormData();
      data.append('email', $this.state.email);
      data.append('password', $this.state.password);
      data.append('username', $this.state.username);
       var t = /^[a-zA-Z_\-]+$/
      var testReg = t.test($this.state.username)
      console.log('testreg', testReg);
    // return;
      if($this.state.username === '' || $this.state.username.length < 6  || !testReg){
        $this.setState({errorMsg: 'Username must be six or more characters and contain no spaces'});
        setTimeout(()=>{
          $this.setState({errorMsg: false});
        }, 2000)
        return;
      } else {
      // if(!t.test($this.state.username)){
        
        Api.checkUsername(data).then((res)=>{
          // console.log('res form checking username', res);
          if(res.status === 200){
          Api.signup(data).then((signup_res)=>{
              // console.log('singup res', signup_res);
              if(signup_res.status === 200){
                $this.setState({successMsg: 'Welcome to LearnLabs!'})

                // setTimeout(()=>{
                  $this.storeUsername(signup_res.data.username);
                  $this.storeToken(signup_res.data.access_token);
                  $this.props.accountStateUpdatedAction('logged_in');
                // }, 200);
              } else {
                // console.log(signup_res)
                $this.setState({errorMsg: signup_res.data.message}, ()=>{
                  setTimeout(()=>{
                    $this.setState({errorMsg: false})
                  }, 4000)
                })              
              }
            })
          } else {
            $this.setState({errorMsg: res.data.message}, ()=>{
              setTimeout(()=>{
              $this.setState({errorMsg: false})
              }, 4000)
            });
          }
        });
      }
      // } else {
      //            $this.setState({errorMsg: 'Usernames can\'t have special cases or spaces.'}, ()=>{
      //         setTimeout(()=>{
      //           $this.setState({errorMsg: false})
      //         }, 4000)
      //       }) 
      //     return;
      // }
    }
    

  }

  storeToken(token){
    window.localStorage.setItem('access_token', token);
  }

  storeUsername(username){
    window.localStorage.setItem('username', username);
  }

  handleLogin(event) {
    //prevent from reload
    event.preventDefault();
    //create form data 
    let $this = this;
    let data = new FormData();
    data.append('email', $this.state.email);
    data.append('password', $this.state.password);
    Api.login(data).then((res)=>{
      if(res.status === 200){
        $this.storeToken(res.data.access_token);
        $this.storeUsername(res.data.username);
        $this.props.accountStateUpdatedAction('logged_in');
      } else {
        // console.log('failed login');
        $this.setState({errorMsg: 'Login attempt failed, please check email and password.'}, ()=>{
          setTimeout(()=>{
            $this.setState({errorMsg: false})
          }, 2000)
        })
      }
    });
  }

  handleForgotPassword(event) {
    //prevent from reload
    event.preventDefault();
    Api.resetPasswordRequest({email: this.state.email}).then(res=>{
      // console.log('res', res);
      if(res.status === 200){
        this.setState({successMsg: 'Success! Check your email.'});
      } else {
        this.setState({errorMsg: 'There was an error processing your request'});
      }
    });

  }


  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    // console.log('value', name, value);
    this.setState({
      [name]: value
    });

    if(target.type === 'checkbox'){
      this.setState({termsAgree: value});
    }

    if(name === 'email'){
      let regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      let emailValid = (regex.test(value)) ? true : false;
      this.setState({emailValid: emailValid})
    }

    if(name === 'confirmPassword'){
      if(value === this.state.password && this.state.passwordValid){
        this.setState({passwordsMatch: true})
      } else {
        this.setState({passwordsMatch: false})
      }
    }
  }

  changeCallback = (event) =>{
    this.setState({password: event.password, passwordValid: event.isValid})
  }


  displayEmailSignup(event) {
    let val = this.state.emailSignup;
    let toggle = (val) ? false : true;
    this.setState({emailSignup: toggle});
  }

  toggleRegLogin(event){
    let val = this.state.action;
    if(val === 'login'){
      this.setState({emailSignup: false, action: 'signup'})
    } else {
      this.setState({emailSignup: false, action: 'login'})
    }
  }

  toggleForgotPassword(event){
    let toggle = this.state.forgotPassword;
    toggle = (toggle) ? false : true;
    this.setState({forgotPassword: toggle, })
  }

  render() {
    let login = (<div></div>)
    let signup = (<div></div>)
    const inputProps = {
      placeholder: "Create password",
      className: 'another-input-prop-class-name',
    };

    let forgot = (<div></div>)
    if(this.state.forgotPassword){
      forgot = (
        <div>
          <div style={{height: "20px"}}></div>
          <form style={styles.formContainer} onSubmit={this.handleForgotPassword}>
            <div className="row">
              <div className="col-12">
                <input style={styles.input} type="email" placeholder="Email address" ref="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
                <div style={{display: ((this.state.emailValid) ? 'inherit' : 'none'), position: 'absolute', right: '20px', top: '0', lineHeight: '40px', color: Constants.BRAND_GREEN, fontSize: '1.2em' }}>
                  <FA name="check" />
                </div>
              </div>
            </div> 
            <div className="row">
              <div className="col-12 ">    
                <button  
                  className="general-button color-border"
                  type="submit"
                >Reset password</button>
              </div>
            </div>
         </form>
          <div className="row" style={{marginTop: "15px"}}>
            <div className="col-12 ">    
              <div onClick={this.toggleForgotPassword.bind(this)} style={styles.or}> Back to Login </div>
            </div>
          </div>        
        </div>      
      )
    }


    if(this.state.action === 'signup' && this.state.emailSignup){
      // let months = Months.map((m) =>(
      //   <option key={m} value={m}>{m}</option>
      // ))
      // let days = Days.map((d) =>(
      //   <option key={d} value={d}>{d}</option>
      // ))
      // let years = Years.map((y) =>(
      //   <option key={y} value={y}>{y}</option>
      // ))
      // let birthdate = (
      //   <div>
      //     <div className="row">
      //       <div className="col-12">
      //         <div style={styles.birthdateDesc}>To sign up you must be 18 years or older. Other people won't see your birthday.</div>
      //       </div>
      //     </div>          
      //     <div className="row">
      //       <div className="col-5">
      //           <select style={styles.select} name="monthValue" value={this.state.monthValue} onChange={this.handleInputChange}>
      //             <option value="Month">Month</option>
      //             {months}
      //           </select>
      //       </div>
      //       <div className="col-3">
      //           <select style={styles.select} name="dayValue" value={this.state.dayValue} onChange={this.handleInputChange}>
      //             <option value="Day">Day</option>

      //             {days}
      //           </select>
      //       </div>
      //       <div className="col-4">
      //           <select style={styles.select} name="yearValue" value={this.state.yearValue} onChange={this.handleInputChange}>
      //             <option value="Year">Year</option>
      //             {years}
      //           </select>
      //       </div>
      //     </div>
      //     </div>
      // )

      // let passwordConfirm = (
      //     <div className="row">
      //       <div className="col-12 ">
      //         <input style={styles.input} type="password" placeholder="Confirm password" ref="title" name="confirmPassword" value={this.state.confirmPassowrd} onChange={this.handleInputChange} />
      //         <div style={{display: ((this.state.passwordsMatch) ? 'inherit' : 'none'), position: 'absolute', right: '20px', top: '0', lineHeight: '40px', color: Constants.BRAND_GREEN, fontSize: '1.2em' }}>
      //           <FA name="check" />
      //         </div>
      //       </div>
      //     </div>
      // )
      signup = (
        <div>
         <form style={styles.formContainer} onSubmit={this.handleRegistration}>
          <div className="row">
            <div className="col-12">
              <label style={styles.label}>Enter your email</label>
              <input style={styles.input} type="email" placeholder="Email address" ref="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
              <div style={{display: ((this.state.emailValid) ? 'inherit' : 'none'), position: 'absolute', right: '20px', top: '0', lineHeight: '40px', color: Constants.BRAND_GREEN, fontSize: '1.2em' }}>
                <FA name="check" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <label style={styles.label}>Create a username <span style={{fontSize: "10px"}}>Minimum 6 letters</span></label>
              <input style={styles.input} type="text" placeholder="Create a user name" ref="username" name="username" value={this.state.username} onChange={this.handleInputChange} />
            </div>
          </div>
       
          <div className="row">
            <div className="col-12 ">
              <label style={styles.label}>Create password</label>

              <ReactPasswordStrength
                  style={styles.passwordInput}
                  ref={ref => this.ReactPasswordStrength = ref}
                  minLength={6}
                  inputProps={{ ...inputProps, id: 'inputPassword1' }}
                  changeCallback={this.changeCallback}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <label>
                <input
                  className={this.state.termsAgreed ? 'checkbox-check' : 'checkbox-blank'}
                  name="terms"
                  type="checkbox"
                  checked={this.state.termsAgreed}
                  onChange={this.handleInputChange} />
                <div style={{display: "inline", cursor: "pointer"}}>I agree to the <a href="/terms" target="_blank"> Terms of Use </a> and <a href="/privacy" target="_blank">Privacy Policy</a></div>
              </label>
            </div>
          </div>

          <div className="row">
            <div className="col-12 ">    
              <button 
                      onClick={this.handleRegistration}
                      className="general-button color-border"
                      type="submit">Sign up</button>
            </div>
          </div>
        </form>
          <div className="row" style={{marginTop: "15px"}}>
            <div className="col-12 ">    
              <div onClick={this.toggleRegLogin.bind(this)} style={styles.or}> Signed up already? Login </div>
            </div>
          </div>        
        </div>
      )
    }

    let forgotPassword = (
      <div style={{marginTop: "15px"}}>
          <div className="row">
            <div className="col-12">
            <div onClick={this.toggleForgotPassword.bind(this)} style={styles.or}> Forgot password? </div>
            </div>
          </div>    
          <br/>     
          <div className="row">
            <div className="col-12">
            <div style={styles.or} onClick={this.toggleRegLogin.bind(this)} > Don't have an account? Signup </div>
            </div>
          </div>  
      </div>
    )

    if(this.state.action === 'login' && !this.state.forgotPassword){
    login = (
          <div>
          <div className="row">
            <div className="col-5">
            <div style={styles.seperation}></div>
            </div>
            <div className="col-2">
            <div style={styles.or}> or </div>
            </div>
            <div className="col-5">
            <div style={styles.seperation}></div>
            </div>
          </div> 
         <form style={styles.formContainer} onSubmit={this.handleLogin}>
          <div className="row">
            <div className="col-12">
              <input style={styles.input} type="email" placeholder="Email address" ref="title" name="email" value={this.state.email} onChange={this.handleInputChange} />
            </div>
          </div>
          <div className="row">
            <div className="col-12 ">
              <input style={styles.input} type="password" placeholder="Password" ref="title" name="password" value={this.state.password} onChange={this.handleInputChange} />
            </div>
          </div>
          <div className="row">
            <div className="col-12 ">    
              <button 
                  onClick={this.handleLogin}
                  className="general-button color-border"
                  type="submit"
              >Sign In</button>
            </div>
          </div>
        </form>
        {forgotPassword}
        </div>
      )
    }

    let emailSingleton = (<div></div>)

    if(this.state.action === 'signup'){
      emailSingleton = (
             <div>
              <div className="row">
                <div className="col-5">
                <div style={styles.seperation}></div>
                </div>
                <div className="col-2">
                <div style={styles.or}> or </div>
                </div>
                <div className="col-5">
                <div style={styles.seperation}></div>
                </div>
              </div>            
             <div className="row">
                <div className="col-12">
                  <button
                    onClick={this.displayEmailSignup.bind(this)}
                    className="general-button color-border"
                  >
                  <FA  style={styles.socialIcon}  name="envelop" />
                  Sign up with email
                  </button>
                </div>
              </div>
               <div className="row" style={{marginTop: "15px"}}>
                  <div className="col-12 ">    
                    <div onClick={this.toggleRegLogin.bind(this)} style={styles.or}> Signed up already? Login </div>
                  </div>
                </div>   
            </div>
        )
    }

    let registrationHeader = (
              <div className="row">
                <div className="col-12">
                <img alt="logo" src={logo} width="70" height="70" style={{display: "block", marginLeft: "auto", marginRight: "auto"}}/>
                <div style={styles.registrationHeader}> {Constants.LOGIN_HEADER} </div>
                </div>
              </div> 
    )

    let socialLogins = (<div></div>)

    if(!this.state.forgotPassword){
      socialLogins = (
        <div>
        <div style={{height: "20px"}}></div>
        <div className="row">
          <div className="col-12">
            <div onClick={this.displayEmailSignup.bind(this)} style={styles.socialSignup}>Sign up with Facebook, LinkedIn, or Google</div>
          </div>
        </div>
        <br/>
        <div className="row">
          <div className="col-5">
          <div style={styles.seperation}></div>
          </div>
          <div className="col-2">
          <div style={styles.or}> or </div>
          </div>
          <div className="col-5">
          <div style={styles.seperation}></div>
          </div>
        </div>
        </div> 
      )
    }

    if(!this.state.emailSignup && !this.state.forgotPassword){
      socialLogins = (
        <div>
          <br/>
          {registrationHeader}
          <br/>
          <div className="row">
            <div className="col-5">
            <div style={styles.seperation}></div>
            </div>
            <div className="col-2">
            <div style={styles.or}> Social </div>
            </div>
            <div className="col-5">
            <div style={styles.seperation}></div>
            </div>
          </div> 
          <div className="row">
            <div className="col-12">
              <SocialButton
                style={{...styles.socialBtn, ...styles.fbBtn}}
                provider='facebook'
                appId='208489999901664'
                onLoginSuccess={this.handleSocialLogin}
                onLoginFailure={this.handleSocialLoginFailure}
              >
                  <FA style={styles.socialIcon} name="facebook" />
                  Login with Facebook
              </SocialButton>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <SocialButton
                style={{...styles.socialBtn, ...styles.linkedinBtn}}
                provider='linkedin'
                appId='8690quvr68i4xq'
                onLoginSuccess={this.handleSocialLogin}
                onLoginFailure={this.handleSocialLoginFailure}
              >
                <FA  style={styles.socialIcon} name="linkedin" />
                Login with LinkedIn
              </SocialButton>
            </div>
          </div>             
          <div className="row">
            <div className="col-12">
              <SocialButton
                style={{...styles.socialBtn, ...styles.googleBtn}}
                provider='google'
                appId='59277921293-2nq8af5e70f3ujt42qdrhghb0ui2so16.apps.googleusercontent.com'
                onLoginSuccess={this.handleSocialLogin}
                onLoginFailure={this.handleSocialLoginFailure}
              >
              <FA  style={styles.socialIcon}  name="google" />
                Login with Google
              </SocialButton>
            </div>
          </div> 
          {emailSingleton} 
        </div>
      )
    }

    return (
        <div className="learnlabs-app">
          <div className="container-fluid" style={styles.modalContents}>
            {socialLogins}
            {signup}
            {login}
            {forgot}
            <div style={{borderRadius: "15px", display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
            <div style={{borderRadius: "15px", display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             

          </div>
        </div>
    );
  }
}


const mapStateToProps = state => ({
  // account_state: state.AccountReducer.account_state
});

const mapDispatchToProps = {
  accountStateUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);





const styles = {
  navContainer: {
    "width": "100%",
    "height": "60px",
    "backgroundColor": Constants.BRAND_WHITE,
    "boxShadow": "0px 0px 1px " + Constants.BRAND_YELLOW,
    marginBottom: "1px",
    position: 'fixed',
    left: '0',
    right: '0',
    zIndex: '99'
  },
  logo: {
    marginLeft: '10px',
    cursor: 'pointer',
    float: 'left',
    background: 'transparent',
    height: "60px",
    color: Constants.BRAND_GREEN,
    border: 'none',
    borderBottom: 'none',
    fontSize: '2em'
  },
  logoHover: {
    marginLeft: '10px',
    cursor: 'pointer',
    float: 'left',
    background: 'transparent',
    height: "60px",
    color: Constants.BRAND_GREEN,
    border: 'none',
    borderBottom: '2px solid ' + Constants.BRAND_YELLOW,
    fontSize: '2em'
  },
  btn: {
    marginLeft: '10px',
    cursor: 'pointer',
    float: 'right',
    background: 'transparent',
    height: "60px",
    color: Constants.BRAND_GREEN,
    border: 'none',
    borderBottom: 'none'
  },
  btnHover: {
    marginLeft: '10px',
    cursor: 'pointer',
    float: 'right',
    background: 'transparent',
    height: "60px",
    color: Constants.BRAND_GREEN,
    border: 'none',
    borderBottom: '2px solid ' + Constants.BRAND_YELLOW
  },
  modalContents: {
    "marginTop": "15px",
    width: "100vw",
    maxWidth: "400px"
  },
  socialBtn: {
    cursor: "pointer",
    width: "100%",
    maxWidth: "400px",
    color: Constants.BRAND_WHITE,
    margin: "10px",
    height: "50px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    borderRadius: "5px"
  },
  socialIcon: {
    padding: "10px"
  },
  fbBtn: {
    backgroundColor: Constants.BRAND_FACEBOOK
  },
  linkedinBtn: {
    backgroundColor: Constants.BRAND_LINKEDIN
  },
  googleBtn: {
    backgroundColor: Constants.BRAND_GOOGLE
  },
  emailBtn: {
    backgroundColor: Constants.BRAND_GREEN
  },
  formContainer: {
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  inputIcon: {
    width: "10%",
    textAlign: 'center',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_YELLOW,
    fontSize: "1.8em"

  },
  label: {
    width: "100%",
    fontWeight: "600"
  },
  input: {
    "height": "50px",
    width: "100%",
    maxWidth: "400px",
    // border: 'none',
    // border: '1px solid ' + Constants.BRAND_RED,
    border: '1px solid #c6c6c6',
    padding: '15px',
    marginBottom: '10px',
  },
  passwordInput: {
    marginBottom: '10px',
    "height": "50px",
    width: "100%",
    maxWidth: "400px",
    backgroundColor: "white"
    // border: 'none',
    // border: '1px solid ' + Constants.BRAND_RED,
  },
  signUpBtn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  signUpBtnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_BLACK,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  seperation: {
    height: "2px",
    margin: '10px',
    width: '100%',
    backgroundColor: Constants.BRAND_MEDIUMGRAY
  },
  or: {
    textAlign: 'center',
    color: Constants.BRAND_BLACK,
    cursor: 'pointer'
  },
  registrationHeader: {
    textAlign: 'center',
    fontSize: '1.3em',
    fontWeight: '700'
  },
  socialSignup: {
    cursor: 'pointer',
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    border: '1px solid ' + Constants.BRAND_MEDIUMGRAY,
    borderRadius: '5px',
    padding: '10px',
    textAlign: 'center'
  },
  select: {
    width: '100%',
    borderRadius: '5px',
    height: '50px',
    marginTop: "20px",
    marginBottom: "20px"
  },
  selectArrow: {
    position: 'absolute',
    top: '0',
    right: '0'
  },
  birthdateDesc: {
    textAlign: 'left',
    color: Constants.BRAND_DARKGRAY,
    fontSize: '1em',
    marginTop: "15px"
  }
}

