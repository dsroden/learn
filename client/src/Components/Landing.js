import React, { Component } from 'react';
import { connect } from 'react-redux';
import Constants from '../Constants'
import image from '../assets/circle_logo.png'
// import blue_circle from  '../assets/circle_blue.png'
// import yellow_circle from  '../assets/circle_yellow.png'
// import green_circle from  '../assets/circle_green.png'
// import Api from '../Api'
import CookieNotice from './CookieNotice';

const lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'

const DATA = {
  "sections": [
    {
      "_id": "learners",
      "placement": "left",
      "details": {
        "title": "Chat channels for learners",
        "description": "Talk with people in your city or around the world who are interested in the same topics and online classes as you.",
        "image_url": image,
        "color": "#4a4a54",
      }
    }
    // {
    //   "_id": "employers",
    //   "placement": "right",
    //   "details": {
    //     "title": "Employers",
    //     "description": "The best hires are the ones who always keep learning. LearnLabs offers you an opportunity to find developing talent for positions or projects. Post a job position or a project today.",
    //     "image_url": yellow_circle,
    //     "color": "#33343c",

    //   }
    // },
    // {
    //   "_id": "coaches",
    //   "placement": "left",
    //   "details": {
    //     "title": "Coaches",
    //     "description": "Whether you're a learner or an employer, if you feel that you could use help from a subject matter expert, start a conversation with a LearnLabs coach. If you think you have the experience needed to help someone get to the next stage in their learning build your LearnLabs coach profile.",
    //     "image_url": green_circle,
    //     "color": "#44444c",

    //   }
    // },
  ]
}


class Landing extends Component {
  state = {
    width: window.innerWidth
  };

  componentDidMount(){
    // Api.callLanding().then(res=>{

    // });
  }

  scrollTo(value){
  // var elem = document.getElementById(value); 
  // elem.scrollTop = elem.scrollHeight;
    this.refs[value].scrollIntoView({behavior: "smooth", block: "start", display: "inline"});
  }


  render() {
    let sections = (<div></div>)
    if(DATA.sections.length > 0){
    sections = DATA.sections.map((section) =>(
            <div ref={section._id} key={section._id}>
            <Section details={section.details} placement={section.placement}/>
            </div>
    ))
    }
    return (
        <div className="landing-page">
          <div className="landing-page-transparency"></div>
          <div className="landing-page-background"></div>
          <CookieNotice />
          <FixedCTA history={this.props.history}/>
          <div className="container-fluid landing-contents">
            <div className="row intro">
              <div className="col-12">
                <h1>CONNECTING LEARNERS</h1>
                <div>
                <div style={{height: "100px"}}></div>
                <button onClick={this.scrollTo.bind(this, 'learners')} className="intro-button color-border-blue">Learn more</button>
                </div>
              </div>
            </div>
            <br/>
            {sections}
            <CallToAction history={this.props.history} />
            <Footer />
          </div>
        </div>
    );
  }
}



class Section extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.details.title || 'Section Title', 
      description: this.props.details.description || lorem,
      image_url: this.props.details.image_url || image,
      width: window.innerWidth,
      image_placement: this.props.placement,
      color: this.props.details.color || "#2d2d2d"
    };
  }
  componentDidMount(){
    var $this = this;
    window.addEventListener('resize', $this.handleResize.bind($this))
  }

  componentWillUnmount(){
    window.removeEventListener('resize', this.handleResize);
  }


  handleResize(){
    // console.log('handle resize', window.innerWidth);
    this.setState({width: window.innerWidth});
  }
  render(){

    var position = (this.state.width <= 764) ? 'left' : this.state.image_placement;
    // console.log('width position', this.state.width, position);
    var image_column = (
        <div className="col-12 col-md-4">
          <img alt="section" src={this.state.image_url} width="100%" />
        </div>
    )

    var description_column = (
        <div className="col-12 col-md-8" >
          <div style={{...((this.state.width <= 764) ? styles.section_mobile : styles.section_desktop), ...{textAlign: position}}}>
            <h3>{this.state.title}</h3>
            <div>{this.state.description}</div>
          </div>
        </div>
    )

    var left, right;
    if(position === 'left'){
      left = image_column;
      right = description_column;
    } else {
      left = description_column;
      right = image_column
    }
    return (
      <div className="row section" style={{backgroundColor: this.state.color}}>
        <div className="col-12" style={{padding: (this.state.width <= 764) ? "30px" : "100px", display: "block", margin: "0 auto", maxWidth: "900px", width: "100%"}}>
        <div className="row">
          {left}
        <div></div>
          {right}
        </div>
        </div>
      </div>
    )
  }
}

// class Subscribe extends Component {
//   constructor(props) {
//     super(props);
//     this.handleInputChange = this.handleInputChange.bind(this);

//     this.state = {
//       subscription_type: null,
//       email: '',
//       options: {learner: false, coach: false, employer: false}
//     };
//   }

//  handleInputChange(event) {
//     const target = event.target;
//     const name = target.name;
//     const value = target.type === 'checkbox' ? target.checked : target.value;

//     // console.log('value', name, value);
//     this.setState({
//       [name]: value
//     });

//   }

//   render(){

//     var prompt = (
//       <div className="row">
//         <div className="col-12" style={{textAlign: 'center', padding: "20px", color: "white"}}>
//           <h3>Join the LearnLabs network.</h3>
//           <h5>Are you a </h5>
//         </div>
//       </div>
//     )
//     var options = (
//       <div className="row">
//         <div className="col-12">
//           <div className="row" style={{width: "100%", maxWidth: "600px", margin: '0 auto'}}>
//           <div className="col-12 col-md-4">
//           <button style={{...styles.optionBtn, ...{backgroundColor: (this.state.options.learner) ? 'green' : Constants.BRAND_LIGHTGRAY }}}>Learner</button>
//           </div>
//           <div className=" col-12 col-md-4">
//           <button style={{...styles.optionBtn, ...{backgroundColor: (this.state.options.coach) ? 'green' : Constants.BRAND_LIGHTGRAY }}}>Coach</button>
//           </div>
//           <div className="col-12 col-md-4">
//           <button style={{...styles.optionBtn, ...{backgroundColor: (this.state.options.employer) ? 'green' : Constants.BRAND_LIGHTGRAY }}}>Employer</button>
//           </div>
//           </div>
//         </div>
//       </div>
//     )

//     var input = (
//       <div style={{padding: "20px", maxWidth: "600px", display: "block", margin: '0 auto'}}>
//         <input style={styles.input} type="email" placeholder="Email address" ref="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
//         <button style={styles.subscribeBtn}>Join</button>
//       </div>
//     )

//     return (
//       <div style={{padding: "30px", backgroundColor: "#ed184fe3"}}>
//       {prompt}
//       {options}
//       {input}
//       </div>
//     )
//   }
// }

class CallToAction extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      subscription_type: null,
      email: '',
      options: {learner: false, coach: false, employer: false}
    };
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

 handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    // console.log('value', name, value);
    this.setState({
      [name]: value
    });

  }


  render(){

    // var fixed = (
    //   <div style={{position: "fixed", top: "15x", right: "15px"}}>
    //     <button className="general-button">Explore</button>
    //   </div>
    // )

    var prompt = (
      <div className="row">
        <div className="col-12" style={{textAlign: 'center', padding: "20px", color: "white"}}>
          <h3 style={{maxWidth: "600px", display: "block", margin: "0 auto"}}>The future of learning is flexible, personalized, social, and full of opportunities.</h3>
        </div>
      </div>
    )

    var cta = (
      <div style={{padding: "20px", maxWidth: "600px", display: "block", margin: '0 auto'}}>
        <button onClick={this.nav.bind(this, 'browse')} className="general-button" style={styles.subscribeBtn}>Explore Channels</button>
      </div>
    )

    return (
      <div className="learnlabs-app" style={{padding: "30px", backgroundColor: "#ed184fe3"}}>
        {prompt}
        {cta}
      </div>
    )
  }
}


class FixedCTA extends Component {
    nav = (location) =>{
      this.props.history.push('/' + location);
    }
    render(){

    var fixed = (
      <div style={{position: "fixed", top: "15px", right: "15px", zIndex: "2"}}>
        <button onClick={this.nav.bind(this, 'browse')} className="general-button color-border">Explore</button>
      </div>
    )

    return (
      <div>
        {fixed}
      </div>
    )
  }
}

class Footer extends Component{
  render(){
    return (
      <div className="row" style={styles.footer}>
        <div className="col-12">
          <div style={{padding: "30px"}}>© Copyright LearnLabs 2018 </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Landing);


const styles = {
  section_desktop: {position: "absolute", top: "50%", transform: "translateY(-50%)", padding: "20px"},
  section_mobile: {position: "relative", padding: "20px"},
  optionBtn: {
    marginTop: "10px",
    marginBottom: "10px",
    cursor: 'pointer', 
    width: "100%", 
    height: "50px", 
    borderRadius: "30px", 
    border: '1px solid ' + Constants.BRAND_BLUE
  },
    input: {
    "height": "50px",
    width: "100%",
    padding: '15px',
    borderRadius: "20px",
    border: "none",
  },
  subscribeBtn: {
    width: "100%",
    maxWidth: "300px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    height: "50px",
    // marginTop: "15px",
    marginBottom: "15px",
    // backgroundColor: Constants.BRAND_BLUE,
    // color: "white",
    borderRadius: "30px"
  },
  footer: {
    textAlign: 'center',
    color: "white",
    backgroundColor: "#33343c",
  }
}