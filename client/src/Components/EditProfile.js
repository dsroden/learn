import React, {Component} from 'react'
import { connect } from 'react-redux';
import LearnerProfile from "./LearnerProfile"
import CoachProfile from "./CoachProfile"
import {updateUserAction} from "../Actions/actionCreator";

import Api from "../Api"

class EditProfile extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
      current_profile: (this.props.user.coach) ? 'coach' : 'learner'
    }
  }


switchProfiles(val){
    this.callApi(val);
}

  callApi(val){
    console.log('calling api in edit profile');
    Api.fetchUserSelf({}).then((res)=>{
      if(res.status === 200){
        this.setState({user: JSON.parse(JSON.stringify(res.data)), current_profile: val});
      }
    });
  }

render() {
    let profileSwitchButton = (<div></div>)
    // if(!this.state.user.coach && this.state.current_profile === 'learner');
    //   profileSwitchButton = (
    //     <div className="container-fluid center-item" style={{maxWidth: "600px"}}>
    //       <div className="row" style={styles.formRow}>
    //         <div className="col-12">    
    //           <button className="general-button color-border-green"  onClick={this.switchProfiles.bind(this, 'coach')}>Become a coach!</button>
    //         </div>
    //       </div>
    //     </div>
    // )
    // if(this.state.user.coach && this.state.current_profile === 'learner'){
    //   profileSwitchButton = (
    //     <div className="container-fluid center-item" style={{maxWidth: "600px"}}>
    //       <div className="row" style={styles.formRow}>
    //         <div className="col-9">    
    //           <div style={{lineHeight: "60px"}}>Your coaching profile</div>
    //         </div>
    //         <div className="col-3">    
    //           <button className="general-button color-border-green"  onClick={this.switchProfiles.bind(this, 'coach')} style={{width: "80px", float: "right"}}>Edit</button>
    //         </div>
    //       </div>
    //     </div>
    //   )
    // }

    // if(this.state.current_profile === 'coach'){
    //    profileSwitchButton = (
    //     <div className="container-fluid center-item" style={{maxWidth: "600px"}}>
    //       <div className="row" style={styles.formRow}>
    //         <div className="col-9">    
    //           <div style={{lineHeight: "60px"}}>Your learner profile</div>
    //         </div>
    //         <div className="col-3">    
    //           <button className="general-button color-border-blue"   onClick={this.switchProfiles.bind(this, 'learner')} style={{width: "80px", float: "right"}}>Edit</button>
    //         </div>
    //       </div>
    //     </div>
    //   )     
    // }

    let profile = (<div></div>)
    if(this.state.current_profile === 'coach'){
      profile = (<CoachProfile history={this.props.history} user={this.state.user}/>)
    } else if(this.state.current_profile === 'learner'){
      profile = (<LearnerProfile history={this.props.history} user={this.state.user}/>)
    } 


    return (
        <div>
        {profileSwitchButton}
        {profile}
      </div>
    );
  }
}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  updateUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

