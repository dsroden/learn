import React, { Component } from 'react';
import Constants from '../Constants'


class Tag extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tag: this.props.tag,
      color: this.props.color || Constants.BRAND_RED
    }
  }

  render() {
    let tag = this.state.tag
    return (
        <div style={{...styles.tag,...{backgroundColor: this.state.color}}}>
          <div>{tag}</div>
        </div>
    );
  }
}

export default Tag;

const styles = {
  "tag": {
    display: "inline-block",
    padding: "3px",
    borderRadius: "5px",
    color: Constants.BRAND_WHITE,
    margin: "3px",
  }
}