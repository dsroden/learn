import React, {Component} from 'react'
import { connect } from 'react-redux';
import { createdUsernameAction } from "../Actions/actionCreator";
// import Constants from "../Constants"
import Api from "../Api"

class CreateUsername extends Component  {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this);
    this.createUsername = this.createUsername.bind(this);
    this.state = {
      user: this.props.user,
      username: '',
      buttonIsHovered: false,
      errorMsg: false,
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  }

  signUpButtonHovered = (val) =>{
    this.setState({buttonIsHovered: val});
  }

  createUsername = e =>{
   e.stopPropagation();
    let $this = this;
    var t = /^[a-zA-Z_\-]+$/
    var testReg = t.test($this.state.username)
    // console.log('testreg', testReg);
    // return;
    if($this.state.username === '' || $this.state.username.length < 6 || testReg === false){
      $this.setState({errorMsg: 'Username must be six or more characters and contain no spaces'});
      setTimeout(()=>{
        $this.setState({errorMsg: false});
      }, 2000)
      return;
    } 
    // else {
      let data = {user_id: this.state.user._id, username: this.state.username};
      Api.checkUsername({username: this.state.username}).then(res=>{
        if(res.status !== 200){
          $this.setState({errorMsg: 'Sorry, that username is not available'})
          return;
        } else {
          Api.createUsername(data).then((res)=>{
            if(res.status === 200){
              this.props.createdUsernameAction(this.state.username);
            }
          });
        }
      })
    // } 

  }


  render(){
    // console.warn('card image url', this.state.card.image_url)
    let errorMsg = (<div></div>)
    if(this.state.errorMsg){
      errorMsg = (
           <div className="row">
                <div className="col-12">
                  <div style={{textAlign: 'center', padding: "10px"}}>{this.state.errorMsg}</div>
                </div>
            </div> 
      )
    }
    return (
        <div className="container-fluid">
           <div className="row">
                <div className="col-12">
                  <div style={{textAlign: 'centter'}}>It looks like you don't have a username yet. You must create a username to participate in LearnLabs chats.</div>
                </div>
            </div>        
            <div className="row">
                <div className="col-12">
                  <input style={styles.input} type="text" placeholder="Create a user name" ref="username" name="username" value={this.state.username} onChange={this.handleInputChange} />
                </div>
            </div>
            <div className="row">
              <div className="col-12 ">    
                <button 
                        
                        onClick={this.createUsername}
                        className="general-button color-border"
                        style={((this.state.buttonIsHovered) ? styles.signUpBtnHover : styles.signUpBtn)}>Create username</button>
              </div>
            </div>
          {errorMsg}
        </div>
       
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  createdUsernameAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUsername);


const styles = {
  formContainer: {
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  input: {
    "height": "50px",
    width: "100%",
    maxWidth: "400px",
    // border: 'none',
    // border: '1px solid ' + Constants.BRAND_RED,
    border: '1px solid #c6c6c6',
    padding: '15px',
    marginBottom: '10px',
  }
}

