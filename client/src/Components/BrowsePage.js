import React, { Component } from 'react';
import Constants from '../Constants';
import image from '../assets/landing.png';
import image2 from '../assets/landing.png';

import { connect } from 'react-redux';
import { accountStateUpdatedAction } from "../Actions/actionCreator";

import Nav from './Nav';
// import CourseList from './CourseList'
import Footer from './Footer'
// import PlaceSearch from './PlaceSearch.js'
// import TopicSearch from './TopicSearch.js'
import BrowseLabs from './BrowseLabs'
import BrowsePeople from './BrowsePeople'
import BrowseCategories from './BrowseCategories'
import CourseSuggestionBox from './CourseSuggestionBox'
import Api from '../Api'

class BrowsePage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      logged_in: false,
      locationFilter: true,
      browse_type: 'categories',
      group_type: 'categories',
      user: null
    };
  }

  componentDidMount(){
    window.scrollTo(0, 0);
    this.callApi()

  }

  callApi(){
    Api.fetchUserSelf({}).then((res)=>{
        if(res.status === 200){
          let temp_type = this.state.browse_type;
            this.setState({browse_type: temp_type, user: JSON.parse(JSON.stringify(res.data)), logged_in: true})
        }
    });
  }
  componentWillReceiveProps(newProps){
    if(newProps.account_state === 'logged_in'){
      this.callApi();
      // this.props.accountStateUpdatedAction(null);
    }
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  showLocationFilter(){
    this.setState({locationFilter: true})
  }

  switchBrowse(val){
    this.setState({browse_type: val})
  }


  render() {
    


    let browseContents = (<div></div>)
    switch(this.state.browse_type){
      case 'classes':
      browseContents = (<BrowseLabs history={this.props.history} />);
      break;
      case 'categories': 
        if(this.state.user){
        browseContents = (<BrowseCategories user={this.state.user} history={this.props.history} />);
        } else {
        browseContents = (<BrowseCategories user={null} history={this.props.history} />);
        }
      break;
      case 'people': 
      if(this.state.user){
          browseContents = (<BrowsePeople user={this.state.user} history={this.props.history} />);
      } else {
        browseContents = (<BrowsePeople user={null} history={this.props.history} />);
      }
      break;
      default:
      browseContents = (<BrowseLabs history={this.props.history} /> );      
    }

    // let browseBar = (
    //         <div className="row" style={{padding: "10px"}}>
    //           <div className="col-12 color-border center-item " style={{border: "1px solid #ccc", overflow: "hidden", backgroundColor: "white", maxWidth: "600px", marginLeft: "auto", marginRight: "auto", display: "block", padding: "0"}}>
    //             <div className="row">
    //               <div className="col-6" style={{padding: "0px", backgroundColor: (this.state.browse_type === 'labs') ? Constants.BRAND_RED : 'white', fontWeight: (this.state.browse_type === 'labs') ? '700' : '400', color: (this.state.browse_type === 'labs') ? 'white' : ''  }}>
    //                 <div onClick={this.switchBrowse.bind(this, 'labs')} className="center-item general-hover" style={{padding: "15px", textAlign: "center", borderRight: "1px solid #ccc" }}>Groups</div>
    //               </div>
    //               <div className="col-6" style={{padding: "0px", backgroundColor: (this.state.browse_type === 'people') ? Constants.BRAND_BLUE : 'white', fontWeight: (this.state.browse_type === 'people') ? '700' : '400', color: (this.state.browse_type === 'people') ? 'white' : '' }}>
    //                 <div onClick={this.switchBrowse.bind(this, 'people')} className="center-item general-hover" style={{padding: "15px", textAlign: "center" }}>People</div>     
    //               </div>
    //             </div>
    //           </div>
    //         </div>
    // )
    let suggestionBox = (<div></div>)
    if(this.state.browse_type === 'labs'){
      suggestionBox = (<CourseSuggestionBox history={this.props.history}/>)
    }

    // var intro = (
    //         <div style={styles.searchContainer}>
    //           <div style={styles.imageTransparency}></div>
    //           <h1 style={styles.browseTitle}> {Constants.SLOGAN}</h1>
    //           <h2 style={styles.browseDescription}> {Constants.SLOGAN_DESCRIPTION} </h2>
    //           <div style={styles.searchBar}>
    //           </div>
    //         </div>
    // )

    var browseBar = (
            <div className="row" style={{padding: "10px", borderRadius: "15px", maxWidth: "900px", margin: "0 auto"}}>
              <div className="col-12 center-item " style={{overflow: "hidden", backgroundColor: "white", maxWidth: "900px", marginLeft: "auto", marginRight: "auto", display: "block", padding: "0"}}>
                <div style={{ display: "inline-block", padding: "0px", borderBottom: (this.state.browse_type === 'categories') ? "2px solid " + Constants.BRAND_GRAY : 'white', fontWeight: (this.state.browse_type === 'categories') ? '700' : '400', color: (this.state.browse_type === 'categories') ? '' : ''  }}>
                  <div onClick={this.switchBrowse.bind(this, 'categories')} className="center-item general-hover" style={{padding: "15px", textAlign: "center"}}>Interests</div>
                </div>
                <div style={{ display: "inline-block", padding: "0px", borderBottom: (this.state.browse_type === 'classes') ? "2px solid " + Constants.BRAND_GRAY : 'white', fontWeight: (this.state.browse_type === 'classes') ? '700' : '400', color: (this.state.browse_type === 'classes') ? '' : '' }}>
                  <div onClick={this.switchBrowse.bind(this, 'classes')} className="center-item general-hover" style={{padding: "15px", textAlign: "center" }}>Classes</div>     
                </div>
                <div style={{ display: "inline-block", padding: "0px", borderBottom: (this.state.browse_type === 'people') ? "2px solid " + Constants.BRAND_GRAY : 'white', fontWeight: (this.state.browse_type === 'people') ? '700' : '400', color: (this.state.browse_type === 'people') ? '' : '' }}>
                  <div onClick={this.switchBrowse.bind(this, 'people')} className="center-item general-hover" style={{padding: "15px", textAlign: "center" }}>People</div>     
                </div>
              </div>
            </div>
    )
    return (
       <div className="learnlabs-app">

        <div style={styles.browsePageContainer} >
            <Nav history={this.props.history} />
            <div style={{height: "70px"}}></div>
            {suggestionBox}
            {browseBar}
            {browseContents}
            <br/>
        </div>
        <Footer />
        </div>
    );
  }
}

const mapStateToProps = state => ({
  account_state: state.AccountReducer.account_state
});

const mapDispatchToProps = {
  accountStateUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(BrowsePage);



const styles = {
  browsePageContainer: {
    "height": "100%",
    "minHeight": "100vh",
    "width:": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "boxShadow": "0px 0px 3px " + Constants.BRAND_BLACK,
    fontFamily: "Montserrat, sans-serif"
  },
  searchContainer: {
    position: 'relative',
    backgroundImage: 'url(' + ((window.innerWidth < 700) ? image2 : image) + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    backgroundRepeat: 'no repeat',
    // height: (window.innerHeight < 700) ? "90vh" : "85vh"
    height: "70vh"
  },
  searchBar: {
    marginTop: '30px',
    marginLeft: "10px",
    marginRight: '10px'
  },
  imageTransparency: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%',
    height: '100%',
    backgroundColor: "rgba(0, 0, 0, 0.15)"
  },
  browseTitle: {
    paddingTop: "30vh",
    position: "relative",
    textAlign: "center",
    color: Constants.BRAND_WHITE,
    textShadow: '1px 1px 40px rgba(22, 23, 31, 0.8)',
    fontWeight: '700',
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    fontSize: "2.5em",
  },
  browseDescription: {
    position: "relative",
    textAlign: "center",
    color: Constants.BRAND_WHITE,
    padding: "10px",
    textShadow: '1px 1px 40px rgba(22, 23, 31, 0.8)',
    fontWeight: '700',
    maxWidth: "800px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    fontSize: "1.3em",

  },
}

           // <div style={{padding: "10px", display: (this.state.locationFilter) ? 'inherit' : 'none'}}>
           //  <PlaceSearch />    
           //  </div>     
           //  <div style={{height: "10px"}}></div>
           //  <div style={{padding: "10px"}}>
           //  <TopicSearch />
           //  </div>
           //  <div style={{height: "10px"}}></div>
           //  <CourseList history={this.props.history}/>