import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import { placeUpdatedAction} from "../Actions/actionCreator";
import O2PlaceAutocomplete from "./O2PlaceAutocomplete";


class PlaceSearch extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      color: this.props.color,
      address: this.props.address, 
      place: null, 
      selectedPlace: null, 
      buttonIsHovered: false, 
      suggestions: [], 
      isLoading: false
    };
  }

  componentWillReceiveProps(newProps){
    this.setState({place: newProps.place});
  }

  handleSubmit(event) {
    //prevent from reload
    event.preventDefault();
    this.props.placeUpdatedAction(this.state.place);
  }

  setButtonHovered = (val) => {
    this.setState({buttonIsHovered: val});
  }


  render() {

    let autocompletePlaces = (
      <div className="col-12 col-md-12" style={{padding: "0"}}>
        <O2PlaceAutocomplete color={this.state.color} address={this.state.address}/>
      </div>
      )


    return (
        <div className="container-fluid " style={{...styles.searchContainer, ...{border: "1px solid #ccc"}}}>
            <div>
              <form onSubmit={this.handleSubmit}>
                <div className="row" style={{padding: "0"}}>
                  {autocompletePlaces}
                </div>
              </form>
              <div className="row">
                
              </div>
            </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  place: state.PlacesReducer.place,
  selectedPlace: state.SearchReducer.place

});

const mapDispatchToProps = {
  placeUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaceSearch);




const styles = {
  searchContainer: {
    "position": "relative",
    "width": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "borderRadius": "5px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  btn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}

// <div className="col-4 col-md-3">    
//                     <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
//                             onMouseLeave={this.setButtonHovered.bind(this, false)}
//                             className="place-search-button color-border"
//                             >Search</button>
//                   </div>

                            // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} type="submit">Search</button>


