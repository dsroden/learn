import React, {Component} from 'react'
import Constants from "../Constants"
import { showUserProfileAction, openUserProfileViewAction, coachGroupAddedAction, accountStateUpdatedAction} from "../Actions/actionCreator";
import { connect } from 'react-redux';
import Api from "../Api";


class CategoriesListItem extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      category: this.props.category,
      place: this.props.place,

    }
  }

  componentDidMount(){
    // console.log('mounted', this.state.place);
  }

  joinCourse = (e) =>{
    let accessToken = window.localStorage.getItem('access_token');

    // if(!accessToken){
    //   //user not logged in, show login modal
    //   this.props.accountStateUpdatedAction('not_logged_in')
    //   return;
    // }

    // e.stopPropagation();

    // if user logged in:
    // add the user as member to the local group if one exists
    // or create a local group around the user's location
    // same for global group - add the user as a member or create the group. 
    var category = null;
    category = this.state.category;
    category.type = 'category';

    if(this.state.place){
      // this.state.course.place = this.state.place;
      // console.log('place', this.state.place);
      category = this.state.category;
      category.place = this.state.place
      this.setState({"category": category}, ()=>{
        this.join(accessToken, this.state.category);
      });
    } else {
      this.join(accessToken, this.state.category);
    }

  }

  nav = (category) =>{
    // console.log(category, this.state.place);
    if(this.state.place){
      this.props.history.push('/chat/?course=' + encodeURIComponent(category.title) + '&place=' + this.state.place.address + '&lng=' + this.state.place.location.lng + '&lat=' + this.state.place.location.lat);
    } else {
      this.props.history.push('/chat/?course=' + encodeURIComponent(category.title));      
    }
  }

  join = (access_token, course) => {
    // console.log('course', course);
    let $this = this;
    let data = {course: course};
    Api.viewGroup(data).then((results)=>{
      // console.log('view group', results);
      if(results.status === 200){
        if($this.state.coach){
          // console.log('res data', results.data);
          Api.coachJoinGroup(data).then(coachResults =>{
            if(coachResults.status === 200){
              $this.setState({coach_confirmed: true}, ()=>{
                $this.props.coachGroupAddedAction(coachResults.data)
              });
            }
          })
        } else {
          $this.nav(course);
        }
      } else {
        window.localStorage.removeItem('access_token');
        $this.joinCourse()       
      }
    });


  };


  render(){
    var width = (window.innerWidth <= 764) ? (window.innerWidth - 30)/2: 900/4;
    var global_members = (<React.Fragment></React.Fragment>);
    var local_members = (<React.Fragment></React.Fragment>);
    // console.log('category', this.state.category);
    if(this.state.category.global_group_views){
      global_members = (<div style={styles.description}>{this.state.category.global_group_views} Total Views</div>)
    } else {
      global_members = (<div style={styles.description}></div>)
    }
    if(this.state.category.local_group_views){
      global_members = (<div style={styles.description}>{this.state.category.global_group_views} Total Views</div>)
      if(this.state.place){
        local_members = (<div style={styles.description}> {(this.state.category.local_group_views) ? this.state.category.local_group_views : 0} Views in {this.state.place.address}</div>)
      }
    } else {
      if(this.state.place){
        local_members = (<div style={styles.description}> View for {this.state.place.address}</div>)
      }
    }

    var members = (
      <div>
        {global_members}
        {local_members}
      </div>
    )
    return (
      <div  style={{padding: "5px", width:  width + "px", height: width + "px", display: "inline-block"}}>
        <div className="category-box" onClick={this.joinCourse.bind(this)} style={styles.container}>
          <h5 style={{color: "white", padding: "10px", wordBreak: "break-all"}}>{this.state.category.title}</h5>
          {members}
        </div>
      </div>
    )
  }

}



const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  showUserProfileAction,
  accountStateUpdatedAction,
  coachGroupAddedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesListItem);

const styles = {
  container: {
    width: "100%",
    height: "100%",
    minHeight: "100px",
    backgroundColor: Constants.BRAND_RED,
    borderRadius: "5px",
    boxShadow: "0px 0px 5px " + Constants.BRAND_LIGHTGRAY,
    cursor: 'pointer'
  },
  description: {
    padding: "10px",
    color: "white"
  }
}

          // <p style={{color: "white", padding: "10px", wordBreak: "break-all"}}>{this.state.category.description}</p>

