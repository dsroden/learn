import React, {Component} from 'react'
import { connect } from 'react-redux';
import { receiveMessageAction} from "../Actions/actionCreator";
import socketCluster from 'socketcluster-client';
import debug from '../Constants/debug.js'
import Constants from '../Constants/index.js'
class Socket extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      channel: this.props.channel,
      channel_id: this.props.channel._id,
      message: null,
      private_message: null
    }
  } 

  componentWillReceiveProps(newProps){
    // console.log('props', newProps);
    if(newProps.message !== this.state.message){
      this.setState({"message": newProps.message}, ()=>{
        this.publish(newProps.message);
      });
    }

  }

  componentDidMount(){
    let $this = this;  

    var options;

    if(!debug.debug){
      options = {
        hostname: Constants.SOCKET_HOST,
        secure: true,
        // port: Constants.SOCKET_PORT,
        rejectUnauthorized: false
      };      
    } else {
      options = {
        port: 5000
      };
    }


    // Initiate the connection to the server
    $this.socket = socketCluster.create(options);
    
    $this.socket.on('connect', function (status) {
      //check authentication
      if(!status.isAuthenticated){
        $this.authenticate()
      } else { 
        $this.registerEvents();
      }
    });
     
  }

  authenticate(){
    let $this = this;
    var credentials = {
      username: $this.props.user.username,
      access_token: $this.props.user.access_token
    };
    $this.socket.emit('setAuth', credentials, function (err) {
      // This callback handles the response from the server
      if (err) {
        //err would be caused if login failed
        console.log('err', err)
      } else {
        $this.registerEvents();
        // goToMainScreen();
      }
    });
  }

  registerEvents(){
    let $this = this;
    //subscribe to the given channel
    $this.socket.subscribe(this.state.channel_id).watch(function (data) {
      //data received here should be passed along to chatcontent component
      $this.props.receiveMessageAction(data);
    });

  }

  publish(messageData){
    // Client code
    messageData.channelId = this.state.channel_id;
    messageData.username = this.state.user.username;
    messageData.created_at = new Date();
    this.socket.publish(this.state.channel_id, messageData, function (err) {
      if (err) {
        console.log('failed', err);
        // Failed to publish event, retry or let the user know and keep going?
      } else {
        // console.log('success');
        // Event was published successfully
      }
    });
  }


  destroy(){
    // destroying the socket will destory the connection and authentication will need to happen again
    this.socket.destroy();    
  }

  componentWillUnmount(){
    //destroying the socket will destory the connection and authentication will need to happen again
    this.socket.destroy();
  }



  render(){
    return (
      <div>
      </div>
    )
  } 
}


const mapStateToProps = state => ({
    message: state.ChatReducer.message,
});

const mapDispatchToProps = {
    receiveMessageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Socket);



