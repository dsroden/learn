
import React, { Component } from 'react';
import CoachGroupListItem from './CoachGroupListItem';
import { connect } from 'react-redux';
import Api from '../Api';

class CoachGroupList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
      groups: null,
      group_added: null
    };
  }

  componentDidMount(){
    this.callApi();
  }

  componentWillReceiveProps(newProps){
    // console.log('new props in coach group list', newProps);
    if(newProps.new_group){
      this.callApi();
    }
  }

  callApi(){
    let $this = this;
    Api.fetchCoachGroups({coach_id: this.state.user._id}).then(response =>{
      // console.log(response);
      if(response.status === 200){
        $this.setState({groups: response.data});
      }
    }).catch((err)=>{
      // console.log('ERROR, unable to load groups', err);
    });
  }


  render() {
      let groups = (<div></div>)
      if(this.state.groups && this.state.groups.length > 0){
        groups = this.state.groups.map((group) =>(
          <CoachGroupListItem user={this.state.user} history={this.props.history} group={group} key={group._id}/>
        ));
      } else {
        groups = (
          <div className="row" style={styles.groupRow}>
            <div className="col-12">
              <div style={{paddingTop: "10px"}}>Groups you join as a coach will appear here</div>          
            </div>
          </div>   
        )
      }
      return (
      <div >
        {groups}
      </div>    
    );
  }
}

         
const mapStateToProps = state => ({
  new_group: state.CoachReducer.new_group
});

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(CoachGroupList);

const styles = {
  groupRow: {
    backgroundColor: 'whitesmoke',
    padding: "10px",
    borderBottom: "1px solid #ece7e7"
  }
}