import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import { courseSearchDoneAction } from "../Actions/actionCreator";
import O2PlaceAutocomplete from "./O2PlaceAutocomplete";
import O2TopicAutocomplete from "./O2TopicAutocomplete";
import Api from "../Api";

class Search extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      topic: null, 
      place: {address: '', location: ''},
      buttonIsHovered: false,
      suggestions: [], 
      isLoading: false
    }
  }

  componentWillReceiveProps(newProps){
    if(newProps.topic !== this.state.topic){
      this.setState({topic: newProps.topic});
    }
    if(!this.state.place || newProps.place.address !== this.state.place.address){
      this.setState({place: newProps.place});
    }
  }

  handleSubmit(event) {
    //prevent from reload
    event.preventDefault();
    //create form data 
    let $this = this;
    let data = new FormData();
    // console.log($this.state.place.address.length);
    let place = ($this.state.place.address.length <= 3) ? null : JSON.stringify($this.state.place);
    data.append('topic', $this.state.topic);
    data.append('place', place);
    Api.search(data).then((res=>{
      if(res.status === 200){
        $this.props.courseSearchDoneAction(res.data.courses);
      }
    }));
  }

  setButtonHovered = (val) => {
    this.setState({buttonIsHovered: val});
  }


  render() {

    let autocompletePlaces = (
      <div className="col-12 col-md-5">
        <O2PlaceAutocomplete />
      </div>
      )

    let autocompleteTopics = (
      <div className="col-12 col-md-5">
        <O2TopicAutocomplete />
      </div>
    )

    return (
        <div className="container-fluid" style={styles.searchContainer}>
            <div>
              <form onSubmit={this.handleSubmit}>
                <div className="row">
                  {autocompleteTopics}
                  {autocompletePlaces}
                  <div className="col-12 col-md-2">    
                    <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
                            onMouseLeave={this.setButtonHovered.bind(this, false)}
                            style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} type="submit">Search</button>
                  </div>
                </div>
              </form>
              <div className="row">
                
              </div>
            </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  topic: state.TopicsReducer.topic,
  place: state.PlacesReducer.place,

});

const mapDispatchToProps = {
  courseSearchDoneAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);




const styles = {
  searchContainer: {
    "position": "relative",
    "width": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "borderRadius": "5px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  btn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_BLACK,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}


