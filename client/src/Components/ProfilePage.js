import React, { Component } from 'react';
import { connect } from 'react-redux';
import {updateUserAction} from "../Actions/actionCreator";

import Constants from '../Constants';
import Nav from './Nav.js'
import EditProfile from './EditProfile';
import Api from '../Api'

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: null,
      user: null,
      delete_account: false,
    };
  }

  componentDidMount(){
    this.callApi()
  }

  callApi(){
    Api.fetchUserSelf({}).then((res)=>{
      if(res.status === 200){
        this.setState({user: res.data});
      }
    });
  }
  // componentWillReceiveProps(newProps){
  //   console.log('new props', newProps.user_updated);
  //   if(newProps.user_updated){
  //     this.callApi();
  //   }

  // }


  logout(){
    window.localStorage.clear()
    this.nav('');
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }
  deleteAccount(){
    let deleteView = (this.state.delete_account) ? false : true;
    this.setState({delete_account: deleteView});
  }

  confirmDeleteAccount(){

  }

  render() {
    let editProfile = (<div></div>);
    let deleteAccount = (<div></div>)
    let profileView = (<div></div>);
    if(this.state.user && !this.state.delete_account){
      editProfile = (<EditProfile history={this.props.history} user={this.state.user}/>)
      deleteAccount = (
        <div className="row">
          <div className="col-12">    
            <button className="general-button color-border" style={{maxWidth: "200px", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "10px"}} onClick={this.deleteAccount.bind(this)}>Delete Account</button>
          </div>
        </div>
        )

      profileView = (
        <div>
        <div style={{height: "60px"}}></div>
        {editProfile}
        <div className="container-fluid">
        {deleteAccount}
        </div>
        <div style={{height: "100px"}}></div>
        </div>)
    }

    if(this.state.delete_account){
      profileView = (
          <div>
          <div style={{height: "60px"}}></div>
          <div className="container-fluid" >
            <div  style={{ textAlign: 'center', maxWidth: "400px", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "15px"}}>
              <div className="row">
                <div className="col-12">
                  Are you sure you want to delete your account? Deleting your account will remove you from any of your courses or groups and you will lose access to your contacts. This action cannot be undone.
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                  style={{marginTop: "10px"}}
                  className="general-button color-border"
                  onClick={this.confirmDeleteAccount.bind(this)}> Yes
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                  className="general-button"
                  style={{marginTop: "10px", backgroundColor: Constants.BRAND_RED, color: "white"}}
                  onClick={this.deleteAccount.bind(this)}> Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )

    }
    return (
      <div className="learnlabs-app">
        <Nav history={this.props.history} />
        {profileView}
      </div>
    );
  }
}


const mapStateToProps = state => ({
  user_updated: state.AccountReducer.user_updated
});

const mapDispatchToProps = {
  updateUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);


         
// const styles = {
// }


  // user_updated: state.AccountReducer.user_updated


