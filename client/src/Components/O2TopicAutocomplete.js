import React, { Component } from 'react';
import Constants from '../Constants';
import axios from 'axios';
import { connect } from 'react-redux';
import Autosuggest from 'react-autosuggest';
import { updateSearchTopicAction } from "../Actions/actionCreator";
import FA from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/fontawesome-free-solid'

// const FA = require('react-fontawesome')



let topics = [
];

function getMatchingTopics(value) {
  const escapedValue = escapeRegexCharacters(value.trim());
  
  if (escapedValue === '') {
    return [];
  }
  
  const regex = new RegExp('^' + escapedValue, 'i');

  return topics.filter(topic => regex.test(topic.name));
}

/* ----------- */
/*    Utils    */
/* ----------- */

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

/* --------------- */
/*    Component    */
/* --------------- */

function getSuggestionValue(suggestion) {
  return suggestion.name;
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion.name}</span>
  );
}


class O2TopicAutocomplete extends Component {
  constructor (props) {
    super(props)
    this.state = {color: this.props.color || Constants.BRAND_RED, topic: '', value: '', suggestions: [], isLoading: false};
  }


  loadSuggestions(value) {
    // Cancel the previous request
    if (this.lastRequestId !== null) {
      clearTimeout(this.lastRequestId);
    }
    
    this.setState({
      isLoading: true
    });
    
    // Fake request
    axios.get(Constants.API_URL + '/search/topics/?q=' + this.state.value ).then((res)=>{
      // console.log('res', res);
      topics = res.data;
      this.setState({
        isLoading: false,
        suggestions: getMatchingTopics(value)
      });
    }).catch((err)=>{
      // console.log('error creating example', err);
    })
  }

  onTopicChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    }, ()=>{
      // this.props.updateSearchTopicAction(newValue)
    });
  };
    
  onSuggestionsFetchRequested = ({ value }) => {
    this.loadSuggestions(value);
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  suggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) =>{
    this.props.updateSearchTopicAction(suggestionValue)
    this.setState({value: ''})

  }


  render() {
    const { value, suggestions } = this.state;
    const topicsProps = {
      placeholder: "Search by keywords",
      value,
      onChange: this.onTopicChange
    };

    let autocompleteTopic = (
      <div >
        <FA style={{...styles.inputIcon,...{color: this.state.color}}} icon={faSearch} />
        <Autosuggest 
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          onSuggestionSelected={this.suggestionSelected}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={topicsProps} />
      </div>
    )



    return (
        <div>
          {autocompleteTopic}
        </div>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  updateSearchTopicAction
};

export default connect(mapStateToProps, mapDispatchToProps)(O2TopicAutocomplete);




const styles = {
  searchContainer: {
    "position": "relative",
    "width": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "borderRadius": "5px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    lineHeight: "35px"
  },
  inputIcon: {
    width: "10%",
    textAlign: 'center',
    display: 'inline-block',
    marginTop: "20px",
    color: Constants.BRAND_RED,
    fontSize: "1.5em"

  },
  input: {
    "width": "100%",
    "height": "50px",
    marginTop: "10px",
    marginBottom: "10px",
    border: 'none',
    borderBottom: '2px solid ' + Constants.BRAND_RED
  },
  btn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_BLACK,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}


