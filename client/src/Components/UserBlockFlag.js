import React, {Component} from 'react'
import { connect } from 'react-redux';
import { blockUserAction, unblockUserAction } from "../Actions/actionCreator";
import Constants from "../Constants"
import Api from "../Api"
import _ from "lodash"

class UserBlockFlag extends Component  {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      user: this.props.user,
      user_details: this.props.details,
      action: null,
      flagValue: '',
      is_blocked: null,
      response_feedback: null
    }
  }

  componentDidMount(){
    let $this = this;
    let user = $this.state.user;
    let blocked = null;
    if(user.blocked){
      blocked = _.find(user.blocked, (u)=>{
        return u === $this.state.user_details.username;
      });
    }
    $this.setState({is_blocked:  blocked})
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  }

  flag(){
    console.log('flag');
    this.setState({action: 'flag'});
  }

  block(){
    //this will prevent the user from showing up in the private messages list
    //if the blocked user sends a message catch it, don't pass it through, then delete it from db
    //the blcoked user should not be able to see the user that blocked them in the list of users, but will be able to see them in the group chat
    let $this = this;
    let user = $this.state.user;
    let blocked = null;
    if(user.blocked){
      blocked = _.find(user.blocked, (u)=>{
        return u === $this.state.user_details.username;
      });
    }
    console.log('blocked before api call', blocked);
    Api.blockUser({blocker: $this.state.user, blocked: $this.state.user_details, block: blocked}).then((res)=>{
      if(!blocked){
        if(user.blocked){
          user.blocked.push($this.state.user_details.username)
          $this.props.blockUserAction($this.state.user_details.username);
        } else {
          user.blocked = [$this.state.user_details.username];
        }
        blocked = true;
      } else {
        let index = user.blocked.indexOf($this.state.user_details.username);
        user.blocked.splice(index, 1);
        blocked = false;
        $this.props.unblockUserAction($this.state.user_details.username);
      }
      $this.setState({is_blocked: null, user: null}, ()=>{ $this.setState({is_blocked: blocked, user: user})});
    });
  }

  submitFlag(){
    let $this = this;
    if($this.state.flagValue === ''){
      return;
    }
    let data = {flagged_user: $this.state.user_details.username, flagged_by: $this.state.user.username, text: $this.state.flagValue}
    Api.flagUser(data).then((res)=>{

      $this.setState({response_feedback: 'We review all flagged users. Thank you for helping us build a healthy community.', flagValue: '', action: null}, ()=>{
        setTimeout(()=>{
          $this.setState({response_feedback: null})
        }, 3000);
      });
    });
  }

  render(){
    let userFeedback = (<div></div>);
    if(this.state.action === 'flag' && this.state.user_details){
      userFeedback = (
        <div>
          <div className="row" style={styles.formRow}>
            <div className="col-12">        
              <label style={styles.label}>
                Please share why you are flagging <b>{this.state.user_details.username}</b>
                <textarea  style={styles.textarea} name="flagValue" value={this.state.flagValue} onChange={this.handleInputChange} />
              </label>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button
              onClick={this.submitFlag.bind(this)}
              style={styles.btn}
              className="general-button color-border"
              > Submit
              </button>
            </div>
          </div>
        </div>
      )
    }

    let responseFeedback = (<div></div>)
    if(this.state.response_feedback){
      responseFeedback = (
        <div className="row" >
          <div className="col-12">
            <div style={{textAlign: 'center'}}><b>{this.state.response_feedback}</b></div>
          </div>
        </div>
      )
    }
    return (
      <div style={{marginTop: "50px"}}>
         <div className="row">
            <div className="col-12">
              <button
              onClick={this.block.bind(this)}
              style={styles.btn}
              className="general-button color-border"
              > {this.state.is_blocked ? 'Unblock' : 'Block'}
              </button>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button
              style={styles.btn}
              className="general-button color-border"
              onClick={this.flag.bind(this)}
              > Flag
              </button>
            </div>
          </div>
          {userFeedback}
          {responseFeedback}
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  blockUserAction,
  unblockUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(UserBlockFlag);


const styles = {
  "textarea": {width: '100%', height: '70px', border: "1px solid " + Constants.BRAND_LIGHTGRAY},
  "formRow": {padding: '10px', fontWeight: 'bold', fontSize: '1.1em'},
  "btn": {marginTop: "10px", maxWidth: "200px", display: 'block', marginLeft: 'auto', marginRight: 'auto'},
}

