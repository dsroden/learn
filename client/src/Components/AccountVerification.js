/*eslint no-useless-escape: "off"*/

import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import Nav from "./Nav";
import Api from "../Api";


class AccountVerification extends Component  {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      verified: false,
      feedback: false,
      email: '',
      emailValid: false,
      loading: true,
      errorMsg: false
    }
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  componentDidMount(){
    let $this = this;
    let currentUrl = $this.props.history.location
    let parsed = new URLSearchParams(currentUrl.search);
    if(parsed.token){
      this.verify(parsed.token);
    }
  }

  verify(token){
    let $this = this;
    if(!token){
      $this.setState({loading: false, errorMsg: 'Failed to verify. Please check the email field'}, ()=>{

        setTimeout(()=>{
          $this.setState({errorMsg: false})
        }, 2000)          
      });
    } else {
      Api.verifyUser({verification_token: token}).then((res)=>{
        if(res.status === 200){
          $this.setState({verified: true, loading: false});
        } else {
          $this.setState({loading: false, errorMsg: 'Failed to verify. Please check the email field'}, ()=>{
            setTimeout(()=>{
              $this.setState({errorMsg: false})
            }, 2000)          
          });
        }
      });
    }
  }

  sendVerificationEmail(){
    //send
    let $this = this;
    Api.sendVerificationEmail({email: $this.state.email}).then((res)=>{
      if(res.status === 200){
        $this.setState({feedback: true});
      } else {
        $this.setState({errorMsg: 'Failed to send verification email. Please try again'}, ()=>{
          setTimeout(()=>{
            $this.setState({errorMsg: false})
          }, 2000)
        })
      }
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });

    if(name === 'email'){
      let regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      let emailValid = (regex.test(value)) ? true : false;
      this.setState({emailValid: emailValid})
    }
  }

  render(){
    let verificationView = (<div></div>)
    if(this.state.verified && !this.state.loading){
      verificationView = (
        <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div>Verified!</div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <a href="/">Join course groups</a>
          </div>
        </div>
        </div>
      )
    } else if(!this.state.verified && !this.state.feedback) { 
       verificationView = (
        <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div style={{padding: "15px"}}><b>Please verify your email address.</b></div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
                <input style={styles.input} type="email" placeholder="Email address" ref="email" name="email" value={this.state.email} onChange={this.handleInputChange} />
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <button className="general-button color-border" onClick={this.sendVerificationEmail.bind(this)}>Verfiy</button>
          </div>
        </div>
        </div>
      )     
    } else if(!this.state.verified && this.state.feedback){
        verificationView = (
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <div>Please check your email account for a verification email from LearnLabs.</div>
            </div>
          </div>
        </div>
        )
    }
    return (
      <div className="learnlabs-app">
      <div style={styles.browsePageContainer}>
        <Nav history={this.props.history} />
        <div style={{height: "100px"}}></div>
        <div style={styles.centerBlock}>
        {verificationView}
        <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
        </div>
        </div>
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountVerification);


const styles = {
    browsePageContainer: {
    "height": "100%",
    "minHeight": "100vh",
    "width:": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "boxShadow": "0px 0px 3px " + Constants.BRAND_BLACK,
    fontFamily: "Montserrat, sans-serif"
  },
  centerBlock: {
    width: "100%",
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
  },
  input: {
    "height": "50px",
    width: "100%",
    maxWidth: "400px",
    border: '1px solid #c6c6c6',
    padding: '15px',
    marginBottom: '10px',
  },
}

