import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import FA from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/fontawesome-free-solid';
import Api from '../Api'
import {toggleCreateMeetingAction} from "../Actions/actionCreator";
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet
import 'rc-time-picker/assets/index.css';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import PlaceSearch from './PlaceSearch';

const format = 'h:mm a';

const now = moment().hour(0).minute(0);


class MeetForm extends Component  {
  constructor (props) {
    super(props)
    this.handleReview = this.handleReview.bind(this)
    this.handleSubmission = this.handleSubmission.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.state = {
      errorMsg: false,
      successMsg: false,
      listState: 'local-list-close',
      showForm: true,
      date: new Date(),
      time: '',
      place: null,
      goal: '',
      review: false,
      meeting_data: false,
      group: this.props.group,
      user: this.props.user,
    }
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  componentDidMount(){
  }

  componentWillReceiveProps(newProps){
    this.setState({place: newProps.place});
    if(newProps.box_state === 'open'){
      this.setState({listState: 'local-list-open'})
      this.props.toggleCreateMeetingAction('reset');
    }
  }

  onDateChange(value){
    this.setState({date: value});
  }

  onTimeChange(value) {
    // console.log('time changed', value && value.format(format));
    this.setState({time: value.format(format)})
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    // console.log('value', name, value);
    this.setState({
      [name]: value
    });

  }

  handleReview(event){
    event.preventDefault();
    let data = {};
    data.date = this.state.date;
    data.time = this.state.time;
    data.place = this.state.place;
    data.goal = this.state.goal;
    data.group_id = this.state.group._id;
    data.creator_id = this.state.user._id;
    let errors = []

    if(data.date === ''){
      errors.push(' please select a meeting date');
    } 
    if(data.time === ''){
      errors.push(' please select a meeting time')
    }
    if(data.goal.length < 10){
      errors.push(' please describe the meeting goals in more detail')
    }

    if(errors.length > 0){
      this.setState({errorMsg: 'Check the required fields: ' + errors.join(',') }, ()=>{
        var $this = this;
        setTimeout(()=>{
          $this.setState({errorMsg: false})
        }, 5000)
      })
      return;
    } 

    this.setState({review: 'review', meeting_data: data});

  }

  handleSubmission(event){
    var $this = this;
    event.preventDefault();
    Api.submitMeeting($this.state.meeting_data).then((res)=>{
      if(res.status === 200){
        $this.setState({review: 'complete'});
      }
    });
  }

  toggleForm(){
    let show = (this.state.showForm) ? false : true;
    // this.setState({showForm: show});
    let listState = show ? 'local-list-open' : 'local-list-close';
    this.setState({listState: listState});
  }

  editMeeting(){
    this.setState({review: false});
  }
  render(){
    // Render the Calendar
      var today = new Date();
      var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);

      let suggestionForm = (<div></div>);
      if(this.state.showForm && !this.state.review){
        suggestionForm = (
        <div>
          <div style={styles.suggestionBox}>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Suggest a meeting time and point!</b></div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Date (required):</b></div>
              </div>
            </div>
            <div className="row">
              <div className="col-12" style={{padding: "0"}}>
                <div style={{width: "300px", display: "block", marginLeft: "auto", marginRight: "auto"}}>
                <InfiniteCalendar
                  width={300}
                  height={200}
                  selected={this.state.date}
                  // disabledDays={[0,6]}
                  onSelect={this.onDateChange.bind(this)}
                  minDate={lastWeek}
                />
                </div>
              </div>
            </div>
            <div style={{height: "10px"}}></div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px", display: "inline-block"}}><b>Time (required):</b></div>
                  <TimePicker
                  showSecond={false}
                  defaultValue={now}
                  className="xxx"
                  onChange={this.onTimeChange.bind(this)}
                  format={format}
                  use12Hours
                  inputReadOnly
                />
            </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Location (recommended):</b></div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
            <PlaceSearch />
            </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Set goal and include any important information (required):</b></div>
              </div>
            </div>
           <div className="row">
              <div className="col-12">
                <textarea style={styles.textarea} type="text" placeholder="Tell others why you want to meet." ref="goal" name="goal" value={this.state.goal} onChange={this.handleInputChange} />
              </div>
            </div>
      
            <div className="row">
              <div className="col-12 ">    
                <button 
                        onClick={this.handleReview}
                        // style={((this.state.buttonIsHovered) ? styles.signUpBtnHover : styles.signUpBtn)} 
                        className="general-button color-border"
                        type="submit">Submit</button>
              </div>
            </div>
          </div>   
          <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
          <div style={{display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             
          </div>
        )
      }

      // console.log('date format', new Date(this.state.date));
      var reviewForm = (<div></div>)
      if(this.state.review === 'review'){
        reviewForm = (
       <div>
          <div style={styles.suggestionBox}>
            <div className="row">
              <div className="col-12 ">    
                <button 
                        onClick={this.editMeeting.bind(this)}
                        // style={((this.state.buttonIsHovered) ? styles.signUpBtnHover : styles.signUpBtn)} 
                        className="general-button color-border"
                        type="submit">Edit details</button>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Please review the date, time, location, and meeting goals and then share it with the group!</b></div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Date:</b> {moment(this.state.date).format("MMM Do YY")}</div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Time:</b> {this.state.time} </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Location:</b> {(this.state.place) ? this.state.place.address : 'No location specified'}</div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Goals and info:</b> {this.state.goal} </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 ">    
                <button 
                    onClick={this.handleSubmission}
                    // style={((this.state.buttonIsHovered) ? styles.signUpBtnHover : styles.signUpBtn)} 
                    className="general-button color-border"
                    type="submit">Share to group</button>
              </div>
            </div>
          </div>   
          <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
          <div style={{display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             
          </div>
      )
    } else if(this.state.review === 'complete'){
       reviewForm = (
        <div>
          <div style={styles.suggestionBox}>
            <div className="row">
              <div className="col-12">
                <div style={{padding: "10px"}}><b>Success! Your meeting suggestion will be shared with the group</b></div>
              </div>
            </div>            
          </div>
        </div>
        )
    }

    return (
      <div >
        <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{color: "white",borderTop: '1px solid #ffffff42', backgroundColor: Constants.BRAND_RED,}}>
          <div className="col-12">
          <div onClick={this.toggleForm.bind(this)} style={{ textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
            <FA icon={faTimes} style={{float: "left", marginLeft: "10px"}} />
            Meet
          </div>
          </div>
        </div>
        <div style={styles.innerSideMenuContents}>
        {suggestionForm}
        {reviewForm}
        </div>
        </div>

      </div>
    )
  }

}


const mapStateToProps = state => ({
  place: state.SearchReducer.place,
  box_state: state.MeetingsReducer.create_meeting
});

const mapDispatchToProps = {
  toggleCreateMeetingAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetForm);


const styles = {
  suggestionBox: {
    zIndex: "99",
    bottom: "60px",
    right: "0",
    padding: "20px",
  },
  suggestionButton: {
    position: "fixed",
    bottom: "10px",
    right: "10px",
    width: "50px",
    height: "50px",
    borderRadius: "100%",
    boxShadow: "0px 0px 5px " + Constants.BRAND_DARKGRAY,
    textAlign: "center",
    fontSize: "1.5em",
    fontWeight: "700",
    lineHeight: "45px",
    border: "1px solid " + Constants.BRAND_RED,
    cursor: "pointer",
    backgroundColor: "white",
    zIndex: "99"
  },
  suggestionButtonDescription: {
    position: "fixed",
    bottom: "10px",
    right: "10px",
    boxShadow: "0px 0px 5px " + Constants.BRAND_MIDDLEGRAY,
    textAlign: "center",
    fontSize: "1.1em",
    cursor: "pointer" ,
    backgroundColor: "white",
    padding: "10px",
    zIndex: "99",
    maxWidth: "150px",
  },
  formContainer: {
    maxWidth: "400px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  // input: {
  //   "height": "50px",
  //   width: "100%",
  //   maxWidth: "400px",
  //   border: '1px solid #c6c6c6',
  //   padding: '15px',
  //   marginBottom: '10px',
  // },
    listContainer: {
     top: "60px",
      width: "100%",
      maxWidth: "400px",
      position: "fixed",
      // height: window.innerHeight - 40 + "px",
      // overflow: "auto",
      right:  "-415px",
      // paddingBottom: "40px",
      background: Constants.BRAND_LIGHTGRAY,
      zIndex: "2147483647",
      boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
    },
      innerSideMenuContents: {
    height: window.innerHeight - 60 + "px",
    overflowY: "scroll",
    paddingBottom: "100px"
  },
    "input": {border: "1px solid #ccc",  borderRadius: "5px", padding: "15px", width: '100%', height: '40px', marginBottom: "10px"},
  "textarea": {marginBottom: "15px", border: "1px solid #ccc", width: '100%', borderRadius: "5px", padding: "15px", minHeight: "150px"},
 
}

