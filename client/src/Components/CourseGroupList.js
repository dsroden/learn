
import React, { Component } from 'react';
import Constants from '../Constants';
import CourseGroupListItem from './CourseGroupListItem';
import { connect } from 'react-redux';
import FA from '@fortawesome/react-fontawesome';
import {faTimes } from '@fortawesome/fontawesome-free-solid';
import { coursesToggleAction, closeCourseListAction } from "../Actions/actionCreator";
import Api from '../Api';

class CourseGroupList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      global_groups: this.props.global_groups,
      listState: 'global-list-close'
    };
  }

  componentDidMount(){
    if(window.localStorage.getItem('access_token')){
      this.callApi();
    }
  }

  callApi(){
    let $this = this;
    Api.loadCoursesGlobalGroupsMemberAll().then(globalres =>{
        $this.setState({chat_state: 'global', user: globalres.data.user, global_groups: globalres.data.global_groups})
    }).catch((err)=>{
      // console.log('ERROR, unable to load groups', err);
    });
  }

  componentWillReceiveProps(newProps){
    let listState = '';
    if(newProps.course_list_state){
      listState =  'global-list-open';
    } else {
       listState = 'global-list-close'
    }
    this.setState({listState: listState});
    // if(newProps.account_state){
      // console.log('call api course group list');
      this.callApi()
    // }
  }

  setDivHovered = (val) =>{
    this.setState({courseIsHovered: val});
  }

  toggleCourseSelected = () =>{
    let courseState = this.state.courseIsSelected;
    let newState = (courseState) ? false : true;
    this.setState({courseIsSelected: newState}, ()=>{
      this.props.coursesToggleAction(null);
    })
  }

  closeList(){
    this.props.closeCourseListAction(false);
  }

  render() {
      let groups = (<div></div>)
      if(this.state.global_groups && this.state.global_groups.length > 0){
        groups = this.state.global_groups.map((group) =>(
          <CourseGroupListItem type="global" course_info={group.course_info} group={group} key={group._id}/>
        ));
        groups = groups.reverse();
      }
      if(!window.localStorage.getItem("access_token")){
        groups = (<div className="row"><div className="col-12">You must register to bookmark groups</div></div>)
      }
      return (
      <div  style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{margin: "0", borderTop: '1px solid #ffffff42', color: "white", backgroundColor: Constants.BRAND_RED, }}>
          <div className="col-12">
            <div onClick={this.closeList.bind(this)} style={{textAlign: "center", padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
              <FA icon={faTimes} style={{float: "right", marginRight: "10px"}} />
              Groups
            </div>
          </div>
        </div>
        <div style={styles.innerSideMenuContents}>
        {groups}
        </div>
      </div>    
    );
  }
}

         
const mapStateToProps = state => ({
    courses_toggle: state.ChatReducer.courses_toggle,
    course_list_state: state.ChatReducer.course_list_state,
    account_state: state.AccountReducer.account_state
});

const mapDispatchToProps = {
  coursesToggleAction,
  closeCourseListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseGroupList);

const styles = {
  listContainer: {
        top: "60px",
        width: "100%",
        maxWidth: "400px",
        position: "fixed",
        // height: window.innerHeight - 40 + "px",
        left:  "-415px",
        paddingBottom: "40px",
        background: Constants.BRAND_LIGHTGRAY,
        zIndex: "2147483647",
        boxShadow: '-2px 2px 4px ' + Constants.BRAND_DARKGRAY
  },
    innerSideMenuContents: {
    height: window.innerHeight - 100 + "px",
    overflowY: "scroll",
    paddingBottom: "40px"
  }
}

