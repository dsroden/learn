import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import _ from 'underscore';
import { updateSearchTopicAction, removeTagAction, topicsUpdatedAction } from "../Actions/actionCreator";

import O2TopicAutocomplete from "./O2TopicAutocomplete";
import DynamicTag from "./DynamicTag";
import update from 'immutability-helper';


class TopicSearch extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      color: this.props.color,
      topic: null,
      buttonIsHovered: false,
      suggestions: [],
      isLoading: false,
      tags: this.props.tags || [], 
      origin: this.props.origin
    };
  }

  componentWillUnmount(){
    this.setState({
      tags: []
    })
  }


  componentWillReceiveProps(newProps){
    console.log('newProps', newProps);

    if(newProps.topic){
      let currentTags = this.state.tags;
      let exists = _.find(currentTags, (t)=>{
        return (t[0] === newProps.topic || t === newProps.topic);
      }); 
      if(!exists){
        // console.log('adding');
        this.addTag(newProps.topic);
      }
      this.props.updateSearchTopicAction(null)
    }
    if(newProps.tag){
      this.deleteTag(newProps.tag);
    }

  }

  deleteTag(tag){
   var tags = this.state.tags;
    var tagIndex = tags.findIndex(function(c) { 
        return c === tag; 
    });
    if(tagIndex < 0){
      return;
    }
    var updatedTags = update(tags, {$splice: [[tagIndex, 1]]}); 
    this.setState({tags: updatedTags}, ()=>{
        this.props.topicsUpdatedAction(this.state.tags);
    });
  }

  addTag(tag){
    var tags = this.state.tags;
    var updatedTags = update(tags, {$push: [[tag]]}); 
    this.setState({tags: updatedTags}, ()=>{
      this.props.topicsUpdatedAction(this.state.tags);
    })
  }

  handleSubmit(event) {
    //prevent from reload
    event.preventDefault();

  }

  setButtonHovered = (val) => {
    this.setState({buttonIsHovered: val});
  }


  render() {


    let autocompleteTopics = (
      <div className="col-12 col-md-12" style={{padding: "0"}}>
          <O2TopicAutocomplete color={this.props.color} />
      </div>

    )

    let tagsGroup = (<div></div>)
    if(this.state.tags.length > 0){
      let tags = this.state.tags.map((tag, index) =>(
        <DynamicTag color={this.props.color} tag={tag} key={tag}/>
      ));
      tagsGroup = (
        <div style={styles.tagsContainer} className="row">
          {tags}
        </div>
      )
    }

    return (
        <div>
        <div className="container-fluid " style={{...styles.searchContainer, ...{border: "1px solid #ccc"}}}>
            <div>
              <form onSubmit={this.handleSubmit}>
                <div className="row" style={{padding: "0"}}>
                  {autocompleteTopics}
                </div>
              </form>
            </div>
        </div>
        <div className="container-fluid " style={{...styles.searchContainer, ...{backgroundColor: "transparent"}}}>
        {tagsGroup}
        </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
  topic: state.TopicsReducer.topic,
  tag: state.TagReducer.tag

});

const mapDispatchToProps = {
    updateSearchTopicAction,
    removeTagAction,
    topicsUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(TopicSearch);




const styles = {
  searchContainer: {
    "position": "relative",
    "width": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "borderRadius": "5px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
  },
  btn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  tagsContainer: {
    padding: "10px"
  }
}
