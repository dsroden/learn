import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import {removeTagAction} from "../Actions/actionCreator";
import PlaceSearch from "./PlaceSearch"
import TopicSearch from "./TopicSearch"
import  { o2LoadImage } from '../Utils'
import CreateUsername from './CreateUsername'
import Api from "../Api"

class LearnerProfile extends Component  {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      user: this.props.user,
      bioValue: this.props.user.bio || '',
      statusValue: this.props.user.status || '' ,
      fileName: 'No file uploaded',
      socialValue: '',
      place: this.props.user.place || {address: null },
      loading: false,
      errorMsg: false,
      successMsg: false,
      interests: this.props.user.interests || [],
      isMounted: false,
      image_valid: false,
      transform: false
    }
  }

  componentDidMount(){
    this.setState({isMounted: true});
    var $this = this;
    let imageUrl = Constants.PROFILE_PHOTO_BASE_URL + $this.state.user.username + '.png?raw=true';

    o2LoadImage(imageUrl).then((result)=>{
      // console.log('result', result);
      if(result.image_valid){
        $this.setState({image_valid: true, transform: result.transform})
      }
    });
  }
  componentWillUnmount(){
    // console.log('unmounting learner profile');
    this.setState({isMounted: false})
  }

  // componentWillReceiveProps(newProps){
  //   if(!this.state.isMounted){
  //     console.log('blocked props on learner profile');
  //     return;
  //   }
    // console.log('new props on learner profile');
    // if(!this.state.place || (newProps.place && newProps.place.address !== this.state.place.address)){
    //   this.setState({place: newProps.place});
    // }
    // let user = this.state.user;

    // if(newProps.topic){
    //   let interests = _.uniq(this.state.interests.concat(newProps.topic));
    //   user.interests = interests;
    //   this.setState({user: JSON.parse(JSON.stringify(user))}, ()=>{
    //     this.handleSubmit();
    //   });
    // }
    // if(newProps.tag && !newProps.topic){
    //   let type = typeof(newProps.tag)
    //   let tag = (type === 'string') ? newProps.tag : newProps.tag[0];
    //   let arr = _.without(this.state.interests, _.find(this.state.interests, (i)=>{
    //     return i === tag;
    //   }));
    //   user.interests = arr
    //   this.setState({user: JSON.parse(JSON.stringify(user))}, ()=>{
    //     console.log(this.state.interests);
    //     this.handleSubmit();
    //   });

    // }
  // }

   componentWillReceiveProps(newProps){
    console.log('new props in course list', newProps);

    let $this = this;
    let user = $this.state.user;
    if(newProps.username){
      user.username = newProps.username;
    }
    console.log('user', user);
    if(!$this.state.place || (newProps.place && newProps.place.address !== $this.state.place.address)){
      user.place = newProps.place
      $this.setState({user: JSON.parse(JSON.stringify(user)), place: newProps.place});
    }
    let searchData = {topics: [].concat.apply([], newProps.topics)};
    user.interests = searchData.topics;

    $this.setState({user: JSON.parse(JSON.stringify(user))}, ()=>{
      $this.handleSubmit();
    })
 }

  handleSubmit(event) {
    //prevent from reload
    if(!this.state.isMounted){
      return;
    }
    if(event){
      event.preventDefault();
    }
    let $this = this;
    //create form data 
    let data = {};
    data.status = $this.state.statusValue;
    data.bio = $this.state.bioValue;
    data.place = $this.state.place;
    data.interests = $this.state.user.interests;

    Api.updateUserProfile(data).then((res)=>{
      // console.log('learner updated ', res);
      //user profile updated
      if(res.status === 200){
        $this.setState({successMsg: 'Profile saved'}, ()=>{
          setTimeout(()=>{
            $this.setState({successMsg: false});
          }, 3000)
        })
      } else {
        $this.setState({errorMsg: 'There was an error. Please try again'}, ()=>{
          setTimeout(()=>{
            $this.setState({errorMsg: false})
          }, 3000)
        })
      }
    });

  }


  handleInputChange(event) {
      event.preventDefault();

    const target = event.target;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if(name === 'fileName'){
      // console.log('this.fileinput', this.fileInput);
      // value = this.fileInput.files[0].name;
      if(this.fileInput.files[0]){
        value = this.fileInput.files[0].name;
      }
    }

    this.setState({
      [name]: value
    });

    if(this.fileInput.files[0]){
      let data = new FormData()
      data.set('file', this.fileInput.files[0]);
      data.set('filename', this.fileInput.files[0].name);
      this.setState({loading: true},()=>{
        Api.updateUserProfilePhoto(data).then((res)=>{
          // console.log('res from uplaoding user profile photo', res);
          if(res.status === 200){
            let user = res.data;
            let temp_photo = user.profile_photo;
            let edited_user = JSON.parse(JSON.stringify(user));
            edited_user.profile_photo = null
            this.setState({user: edited_user, [name]: null}, ()=>{
              edited_user = JSON.parse(JSON.stringify(edited_user));
              edited_user.profile_photo = temp_photo;
              this.setState({user: edited_user, [name]: null, loading: false})
            });
          }
        });
      });
    }
  }

render() {
    let profilePhoto = (<div className="color-border" style={styles.profilePhoto}>Add photo</div>)
    if(this.state.user && this.state.user.profile_photo && this.state.image_valid){
      profilePhoto = (<div style={{...styles.profilePhoto, ...{backgroundImage: 'url(' + this.state.user.profile_photo + ')', backgroundSize: 'cover', backgroundPosition: "center center", backgroundRepeat: 'no-repeat', transform: this.state.transform}}}> </div>)
      // profilePhoto = (<img src={this.state.user.profile_photo} />)
    }
    let loading = (<div></div>)
    if(this.state.loading){
      loading = (<div style={{padding: "10px", display: 'inline-block'}}>Loading...</div>)
    }


    var username = (<div></div>);
    var username_display = (<div></div>)
    if(this.state.user.username){
      username_display = (
        <div className="row" style={styles.formRow}>
          <div className="col-12">  
          <label style={styles.label}> Username:
          </label>  
          <div>{this.state.user.username}</div>
          </div>
        </div>
        )
    } else {
      username = (<div style={{padding: "10px"}}><CreateUsername user={this.state.user}/></div>)
    }
    return (
        <div>
        {username}
        <div className="container-fluid" style={styles.editProfileContainer}>
        <div className="row">
        <div className="col-12">
        <form style={styles.formContainer}>
         <div className="row" style={styles.formRow}>
          <div className="col-9">    
            <div style={{lineHeight: "60px"}}>Your learner profile</div>
          </div>
          <div className="col-3">    
            <button className="general-button color-border-blue"  style={{width: "80px", float: "right"}} onClick={this.handleSubmit} >Save</button>
          </div>
        </div>
        {username_display}
        <div className="row" style={styles.formRow}>
          <div className="col-12">           
            <label>
              <div style={{padding: "10px 10px 10px 0px"}}>Upload a profile image:</div>
              <input 
                style={{opacity: "0", position: "absolute", width: "300px"}}
                type="file"
                ref={input => {
                  this.fileInput = input;
                }}
                onChange={this.handleInputChange}
                name="fileName"
              />
              {profilePhoto}
              {loading}
            </label>
          </div>
        </div>
        </form>
        <div className="row" style={styles.formContainer}>
          <div className="col-12">
            <label style={styles.label}>
              <div style={{fontWeight: "bold"}}>Add location:</div>
              <PlaceSearch color={Constants.BRAND_BLUE} address={(this.state.user.place) ? this.state.user.place.address : null}/>
            </label>
          </div>
        </div>
        <div className="row" style={styles.formContainer}>
          <div className="col-12">
            <label style={styles.label}>
              <div style={{fontWeight: "bold"}}>Add interests:</div>
              <TopicSearch color={Constants.BRAND_BLUE} tags={this.state.user.interests}/>
            </label>
          </div>
        </div>
        <form onSubmit={this.handleSubmit} style={styles.formContainer}>
        <div className="row" style={styles.formRow}>
          <div className="col-12">        
            <label style={styles.label}>
              Bio:
              <textarea placeholder="Tell others a little bit about yourself and your background" style={styles.textarea} name="bioValue" value={this.state.bioValue} onChange={this.handleInputChange} />
            </label>
          </div>
        </div>

        <div className="row" style={styles.formRow}>
          <div className="col-12">        
            <label style={styles.label}>
              Goals:
              <textarea  placeholder="Share your current learning goals" style={styles.textarea} name="statusValue" value={this.state.statusValue} onChange={this.handleInputChange} />
            </label>
          </div>
        </div>
        <div className="row" style={styles.formRow}>
          <div className="col-12">        
            <label style={styles.label}>
              Add social link:
              <input  placeholder="Add a link to another social profile" style={styles.input} name="socialValue" value={this.state.socialValue} onChange={this.handleInputChange} />
            </label>
          </div>
        </div>
        <br/>

        <div className="row" style={styles.formRow}>
          <div className="col-12">    
            <button className="general-button color-border-blue"  type="submit">Save</button>
          </div>
        </div>
      </form>
      <div style={{maxWidth: "600px", marginLeft: "auto", marginRight: "auto", display: "block"}} >
        <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
        <div style={{display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             
      </div>
      </div>
      </div>
      </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
    // place: state.PlacesReducer.place,
    place: state.SearchReducer.place,
    // topic: state.TopicsReducer.topic,
    // tag: state.TagReducer.tag,
    topics: state.SearchReducer.topics,
    username: state.AccountReducer.username

});

const mapDispatchToProps = {
  removeTagAction
};

export default connect(mapStateToProps, mapDispatchToProps)(LearnerProfile);



const styles = {
  editProfileContainer: {   
    backgroundColor: Constants.BRAND_LIGHTESTGRAY,
    boxShadow: "0 -1px 10px rgb(175, 173, 173)", //+ Constants.BRAND_DARKGRAY,
    borderTop: "1px solid " + Constants.BRAND_LIGHTGRAY,
    maxWidth: "600px",
    padding: "10px 0 20px 0",
    marginBottom: "20px",
    marginTop: "10px"
  },
  formContainer: {maxWidth: "600px", marginLeft: "auto", marginRight: "auto"},
  "profilePhoto": {width: "100px", height: "100px", borderRadius: "100%", backgroundColor: Constants.BRAND_LIGHTGRAY, textAlign: "center",
  lineHeight: "100px",
  fontSize: "12px", cursor: "pointer"},
  "header": {padding: '10px', backgroundColor: 'black', fontWeight: 'bold', fontSize: '1.1em'},
  "formRow": {padding: '0px', fontWeight: 'bold', fontSize: '1em'},
  "label": {width: '100%'},
  "select": {width: '200px', height: '40px', marginLeft: '10px'},
  "input": {border: "1px solid #ccc", width: '100%', borderRadius: "5px", padding: "15px", height: '40px'},
  "textarea": {border: "1px solid #ccc", width: '100%', borderRadius: "5px", padding: "15px", minHeight: "200px"},
  "submitBtn": {width: '200px', display: 'block', marginLeft: 'auto', marginRight: 'auto'}
}