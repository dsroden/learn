import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import PlaceSearch from './PlaceSearch.js'
import TopicSearch from './TopicSearch.js'
import PeopleList from './PeopleList';
import PrivateSocket from './PrivateSocket'
// import PrivateConversationsList from './PrivateConversationsList';
import UserProfileView from './UserProfileView'

class BrowsePeople extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user
    }
  }

  componentDidMount(){

  }

  render(){
    // console.log('browse people');
    let view = (<div style={{marginTop: "40px", display: "block", marginLeft: "auto", marginRight: "auto", maxWidth: "600px", width: "100%", textAlign: "center", borderRadius: "30px", padding: "30px", backgroundColor: Constants.BRAND_BLUE, color: "white", boxShadow: "0px 0px 5px #ccc"}}> Please log in to connect with other learners</div>)
    if(this.state.user){
      view = (
        <div >
        <div className="row" style={styles.descriptionContainer}>
          <div className="col-12">
            <div>Find people with similar interests by location</div>
          </div>
        </div>

        <div style={{padding: "10px"}}>
          <PlaceSearch color={Constants.BRAND_BLUE}/>    
        </div>     
        <div style={{padding: "10px"}}>
          <TopicSearch color={Constants.BRAND_BLUE} />
        </div>
        <div style={{maxWidth: "1000px", display: "block", marginLeft: "auto", marginRight: "auto"}}>
          <PeopleList history={this.props.history}/>
        </div>
        <div >
        <PrivateSocket user={this.state.user} />
        <UserProfileView user={this.state.user} />
        </div>
        </div>
      )
    }
    return (
      <div>
        {view}

      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  
};

export default connect(mapStateToProps, mapDispatchToProps)(BrowsePeople);


const styles = {
  descriptionContainer: {
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    overflow: "hidden",
    marginTop: "10px",
    marginBottom: "10px",
    textAlign: "center",
    fontWeight: "500"
  },
}
