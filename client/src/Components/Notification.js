import React, {Component} from 'react'
import { connect } from 'react-redux';
import {  openUserProfileViewAction, showUserProfileAction } from '../Actions/actionCreator.js'
import Constants from "../Constants"
import Timestamp from 'react-timestamp';
import Api from "../Api";

class Notification extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      notification: null,
      notificationClass: 'notification-close',
      notificationType: null,
    }
  }

  componentWillReceiveProps(newProps){
    if(newProps.received_private_message && newProps.received_private_message !== this.state.notification){
      this.renderNotification(newProps.received_private_message, 'private_message');
    }
  }

  renderNotification(notification, notificationType){
    let notificationClass = 'notification-open';
    if(notificationType === 'private_message' && notification.username !== this.state.user.username){
      // this.setState({notification: null}, ()=>{
        this.setState({notificationClass: notificationClass, notification: notification, notification_type: notificationType})
      // });
      let $this  = this;
      setTimeout(()=>{
        $this.setState({notificationClass: 'notification-close'})
      }, 2000)
    }
  }

  showUserProfile(){
    Api.fetchUserProfile(this.state.notification.username).then((user)=>{
      this.setState({notificationClass: 'notification-close'})
      this.props.openUserProfileViewAction(true)
      this.props.showUserProfileAction(user.data)
    });
  }


  render(){
    let notification = (<div></div>)
    if(this.state.notification_type === 'private_message' && this.state.notification){
      notification = (
        <div onClick={this.showUserProfile.bind(this)}>
            <div className="username" style={styles.username}>{this.state.notification.username}</div>
            <div style={styles.text}>{this.state.notification.text}</div>
            <Timestamp time={this.state.notification.created_at} style={styles.timestamp} format="time" />
          </div>
      )
    }
    return (
      <div style={styles.notification} className={this.state.notificationClass} >
        <div className="row">
        <div className="col-12">
          {notification}
        </div>
        </div>
      </div>
    )
  }

}


const mapStateToProps = state => ({
  received_private_message: state.ChatReducer.received_private_message
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  showUserProfileAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Notification);


const styles = {
  notification: {
    backgroundColor: Constants.BRAND_GREEN,
    color: "white",
    padding: "20px",
    cursor: "pointer",
    top: "70px",
    width: "100%",
    maxWidth: "400px",
    position: "fixed",
    height: 100 + "px",
    right:  "-410px",
    paddingBottom: "40px",
    zIndex: "2147483647",
    boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",

  },
    messageContainer: {
    padding: "15px 15px 20px 15px",
    margin: "10px",
    borderRadius: "5px",
    boxShadow: "0px 0px 2px black"
  },
  username: {
    fontSize: "14px",
    fontWeight: "bold",
    cursor: "pointer"
  },
  timestamp: {
    fontSize: "14px",
    float: "right",
  }
}

