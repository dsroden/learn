/*eslint no-useless-escape: "off"*/

import React, { Component } from 'react';
import Constants from '../Constants';
import Modal from 'react-responsive-modal';
import { connect } from 'react-redux';
import { accountStateUpdatedAction, toggleMenuAction } from "../Actions/actionCreator";
import Login from "./Login";
import FA from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';
import Notification from "./Notification";
import Api from "../Api";
// import logo from '../assets/learnlabs_logo-01.png';
import logo from '../assets/circle_logo.png';
import CookieNotice from './CookieNotice';

import SideMenu from "./SideMenu"


class Nav extends Component {
  constructor (props) {
    super(props)
    this.state = {
      logoHover: false,
      signupHover: false,
      loginHover: false,
      browseHover: false,
      chatHover: false,
      profileHover: false,
      open: false,
      action: null, 
      buttonIsHovered: false,
      user: null,
      scrolling: false,
      showMenu: false,
      listState: 'local-list-close',
      safe_page: this.props.safe
    };
  }

  componentDidMount(){
    window.addEventListener('scroll', this.onScroll, false);
    this.getSelfUser();
    this.setState({mounted: true});
  }


  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }


  componentWillReceiveProps(newProps){
    // console.log('new props in nav', newProps);
    if(newProps.menu_state === 'close'){
      this.closeMenu()
      this.props.toggleMenuAction('reset');
    }
    if(newProps.menu_state === 'open'){
      this.openMenu()
      this.props.toggleMenuAction('reset');
    }
    if(newProps.account_state === 'not_logged_in'){
      this.setState({open: true, action: 'signup'})
    } else if (newProps.account_state === 'logged_in'){
      this.setState({open: false, action: 'signup'})
    } else {
      this.setState({open: false, action: null})
    } 

  }

  getSelfUser(){
    Api.fetchUserSelf({}).then((res)=>{
      if(res.status === 200){
        this.setState({user: res.data});
      } else {
        // if(!this.state.safe_page){
        // this.nav('browse');
        // }
      }
    })
  }

  onScroll = () => {
    if (window.scrollY > 0){
      this.setState({scrolling: true});
    } else {
      this.setState({scrolling: false});
    }
  }


  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  setButtonHovered = (type, val) =>{
    let state = type + 'Hover';
    this.setState({[state]: val});
  }

  signUpButtonHovered = (val) =>{
    this.setState({buttonIsHovered: val});
  }

  onOpenModal = (action) => {
    this.setState({ open: true, action: action});
  };

  onCloseModal = () => {
    this.setState({ logoHover: false,
      signupHover: false,
      loginHover: false,
      open: false,
    }, ()=>{
      this.props.accountStateUpdatedAction(null);
    });
  };


  closeMenu = () =>{
    let listState = 'local-list-close';
    this.setState({showMenu: false, listState: listState});
  }

  openMenu = () =>{
    let listState = 'local-list-open';
    this.setState({showMenu: true, listState: listState});
  }

  toggleMenu = ()=>{
    // console.log('toggle');
    let show = (this.state.showMenu) ? false : true;
    if(show){
      this.openMenu();
    } else {
      this.closeMenu();
    }
  }


  render() {

    let nav = (<div></div>)
    let token = window.localStorage.getItem("access_token");
    // let navBackground = (this.state.scrolling || token) ? Constants.BRAND_GRAY : 'transparent';
    var navBackground = Constants.BRAND_GRAY;
    let btnBackground = (this.state.scrolling) ? 'white' : Constants.BRAND_RED;
    
    if(!token){
      nav = (
        <div className="learnlabs-app">
                  <CookieNotice />

        <div className="container-fluid " style={{...styles.navContainer, ...{"backgroundColor": navBackground}}}>
            <div className="row" style={{padding: 0}}>
              <div className="col-1" style={{padding: "0"}}>
                <div onClick={this.nav.bind(this, 'browse')} style={((this.state.logoHover) ? styles.logoHover : styles.logo)}>
                  <img alt="logo" src={logo} style={{marginLeft: "5px", marginTop: "5px", width: "50px", height: "50px"}}/>
                </div>
              </div>
              <div className="col-11">
                <button 
                        onClick={this.onOpenModal.bind(this, 'signup')}
                        className="general-button color-border"
                        style={{...(styles.btn),...{backgroundColor: btnBackground, color: (!this.state.scrolling) ? 'white' : ''}}}>Sign Up</button>
                <button 
                        onClick={this.onOpenModal.bind(this, 'login')}
                        className="general-button color-border"
                        style={{...(styles.btn),...{backgroundColor: btnBackground, color: (!this.state.scrolling) ? 'white' : ''}}}>Login</button>
              </div>
            </div>
        </div>
         <Modal style={{backgroundColor: "#dad7d7"}} open={this.state.open} onClose={this.onCloseModal} little>
          <Login action={this.state.action} />
        </Modal>
        </div>
      )
    } else if(token ){
      nav = (
        <div className="learnlabs-app">
                          <CookieNotice />

        <div className="container-fluid" style={{...styles.navContainer, ...{"backgroundColor": navBackground}}}>
            <div className="row">
              <div className="col-12">
                <div onClick={this.nav.bind(this, 'browse')} style={((this.state.logoHover) ? styles.logoHover : styles.logo)}>
                  <img alt="logo" src={logo} style={{marginLeft: "-10px", marginTop: "5px", width: "50px", height: "50px"}}/>
                </div>
                <div onClick={this.toggleMenu.bind(this)} className={'menu-toggle ' + ((this.state.showMenu) ? 'on' : '')}>
                  <div className="one"></div>
                  <div className="two"></div>
                  <div className="three"></div>
                </div>
              </div>
            </div>
        </div>
        </div>
      )
    }

    let notification = (<div></div>)
    if(this.state.user){
      notification = (<Notification user={this.state.user}/>)
    }

    let sideMenu = (
        <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{borderTop: '1px solid #ffffff42', color: "white", backgroundColor: Constants.BRAND_RED}}>
          <div className="col-12">
          <div onClick={this.toggleMenu.bind(this)} style={{textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
            Menu
            <FA icon={faTimes} style={{float: "left", marginLeft: "10px"}} />
          </div>
          </div>
        </div>
        <div style={styles.innerSideMenuContents}>
        <SideMenu history={this.props.history}/>
        </div>
        </div>

    )
    
    return (
        <div>
        {nav}
        {notification}
        {sideMenu}
        </div>
    );
  }
}


const mapStateToProps = state => ({
  account_state: state.AccountReducer.account_state,
  menu_state: state.MenuReducer.menu_state,
});

const mapDispatchToProps = {
  accountStateUpdatedAction,
  toggleMenuAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Nav);





const styles = {
  navContainer: {
    "width": "100%",
    "height": "60px",
    marginBottom: "1px",
    position: 'fixed',
    left: '0',
    right: '0',
    zIndex: '99',
    padding: "0"
  },
  logo: {
    cursor: 'pointer',
    float: 'left',
    background: 'transparent',
    height: "60px",
    border: 'none',
    fontSize: '2em'
  },
  logoHover: {
    cursor: 'pointer',
    float: 'left',
    background: 'transparent',
    height: "60px",
    border: 'none',
    fontSize: '2em'
  },
  btn: {
    marginTop: "5px",
    marginLeft: '10px',
    cursor: 'pointer',
    float: 'right',
    width: "100px"
  },
  listContainer: {
    top: "60px",
    width: "100%",
    maxWidth: "400px",
    position: "fixed",
    // height: window.innerHeight - 60  + "px",
    right:  "-415px",
    paddingBottom: "40px",
    background: Constants.BRAND_LIGHTGRAY,
    zIndex: "2147483647",
    boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
  },
  innerSideMenuContents: {
    height: window.innerHeight - 100 + "px",
    overflowY: "scroll",
    paddingBottom: "40px"
  }
}


