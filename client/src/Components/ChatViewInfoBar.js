import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import FA from '@fortawesome/react-fontawesome';
import { faUsers, faTable, faBookmark, faInfo, faGlobe, faMapMarker, faTimes} from '@fortawesome/fontawesome-free-solid';
import Api from '../Api'
import {accountStateUpdatedAction, reloadChatPageAction, switchGroupAction, openLocalListAction, closeLocalListAction, openCourseListAction, openPrivateConversationsListAction } from "../Actions/actionCreator";
import UsersAvatarList from "./UsersAvatarList"
import MeetBar from "./MeetBar"

// import ShareButtons from "./ShareButtons"

class ChatViewInfoBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      course_info: this.props.courseinfo,
      current_group: this.props.current_group,
      toggle_state: this.props.chat_state,
      chat_state: this.props.chat_state,
      course_details: false,
      buttonIsHovered: false,
      leaveIsHovered: false,
      cancelIsHovered: false,
      leave_group: false,
      leave_course: false,
      is_member: false,
    }
  }

  componentDidMount(){
    // console.log('current gorup', this.state.current_group);
    if(window.localStorage.getItem('access_token')){
      Api.isGroupMember({user: this.state.user, group: this.state.current_group}).then((response)=>{
        // console.log('is member', response);
        if(response.status === 200){
        this.setState({is_member: true})
        } else {
        this.setState({is_member: false})

        }
      });
    }
  }

  componentWillReceiveProps(){
    if(window.localStorage.getItem('access_token')){
      Api.isGroupMember({user: this.state.user, group: this.state.current_group}).then((response)=>{
        // console.log('is member', response);
        if(response.status === 200){
        this.setState({is_member: true})
        } else {
        this.setState({is_member: false})

        }
      });
    }
  }

  openCourseList(){
    this.props.openCourseListAction(true)
  }

  openLocalList(){
    this.props.openLocalListAction(true);    
  }

  openPrivateConversationsList(){
    this.props.openPrivateConversationsListAction(true)
  }

  switchToGlobal(){
    //get the global group for this course in which the user is a member
    // if(this.state.user){
      // Api.fetchClobalGroupByCourseIdAndMemberId({course_id: this.state.course_info._id, user_id: this.state.user._id}).then((group)=>{
      //   if(group.status === 200){
      //     this.props.switchGroupAction(group.data);
      //     this.props.closeLocalListAction(false);    
      //   }
      // });
      Api.viewGroup({course: this.state.course_info}).then((res)=>{
        // console.log('response from view group', res);
        if(res.status === 200){
          this.props.switchGroupAction(res.data);
          this.props.closeLocalListAction(false);    
        }
      })
    // } else {
    //   let data = {title: this.state.course_info.title};

    //   Api.fetchGroupByCourseTitleAndLocation(data).then((res)=>{
    //     if(res.status === 200){
    //       this.props.switchGroupAction(res.data.group);
    //       // this.props.closeLocalListAction(false);    
    //     }        
    //   });

    // }
  }

  showCourseDetails(){
    let detailsState = this.state.course_details;
    detailsState = (detailsState) ? false : true;
    this.setState({course_details: detailsState});
  }

  setButtonHovered(val){
    this.setState({buttonIsHovered: val});
  }

  exitChatGroup(){
    let exitGroup = this.state.leave_group;
    exitGroup = (exitGroup) ? false : true;
    this.setState({leave_group: exitGroup});

  }

  exitCourse(){
    let leaveCourse = this.state.leave_course;
    leaveCourse = (leaveCourse) ? false : true;
    this.setState({leave_group: leaveCourse})
  }

  optionButtonHovered(option){
    let hovered = false;
    if(option === 'leave'){
      hovered = this.state.leaveIsHovered;
      hovered = (hovered) ? false : true;
      this.setState({leaveIsHovered: hovered})
    }
    if(option === 'cancel'){
      hovered = this.state.cancelIsHovered;
      hovered = (hovered) ? false : true;
      this.setState({cancelIsHovered: hovered})      
    }
  }

  confirmedLeaveGroup(){
    let $this = this;
    let data = {user_id: $this.state.user._id, group_id: $this.state.current_group._id, group_type: (($this.state.current_group.address) ? 'local' : 'global')}
    Api.leaveGroup(data).then((result)=>{
      if(result.status !== 200){
        return;
      }
      // if the user has left the current group some reload state needs to take place at the top of the chat page
      $this.props.reloadChatPageAction(true);
    });
  }

  confirmedLeaveCourse(){
    let $this = this;
    let data = {user_id: $this.state.user._id, course_id: $this.state.course_info._id};
    Api.leaveCourse(data).then((result)=>{
      if(result.status !== 200){
        return;
      }
      // if the user has left the current group some reload state needs to take place at the top of the chat page
      $this.props.reloadChatPageAction(true);
    });
  }


  join = () => {
    // console.log('course', course);

    if(!window.localStorage.getItem('access_token')){
      //user not logged in, show login modal
      this.props.accountStateUpdatedAction('not_logged_in')
      return;
    }
    let $this = this;
    let data = {course: $this.state.current_group};
    // data.current_
    if($this.state.current_group.location) {
      data.course.place = $this.state.current_group.location;
    }

    // console.log(this.state.current_course, this.state.current_group);
    // console.log('data to join', data);
    Api.joinGroup(data).then((results)=>{
      // console.log('view group', results);
      if(results.status === 200){
        // console.log('joined', results)
        $this.setState({is_member: true})
      } else {
        // window.localStorage.removeItem('access_token');
        // $this.joinCourse()       
      }
    });


  };

  render(){
    // console.log('current_group', this.state.current_group);

    // console.log('render info bar');
    let course_info = this.state.course_info;
    let title = course_info.title;
    
    let coursesOption = (<div></div>);
    coursesOption = (
      <div className="col-3 col-md-3">
        <div onClick={this.openCourseList.bind(this)} style={{cursor: "pointer"}}>
          <FA style={{...styles.icon, ...{marginRight: "10px"}}} icon={faTable}/>
          <div style={{display: (window.innerWidth > 768) ? 'inline-block' : 'none'}}>Groups</div>
        </div>
      </div>
    )

    let chatOptions = (<div></div>);
    chatOptions = (
      <div className="col-6 col-md-6" >
        <div className="row">
          <div className="col-6" style={{...{cursor: "pointer"}, ...{color: (this.state.toggle_state === 'global') ? Constants.BRAND_BLUE : "rgb(136, 136, 136)"}}} onClick={this.switchToGlobal.bind(this)}>
            <FA  icon={faGlobe} style={{...styles.icon, ...{float: "right", color: (this.state.toggle_state === 'global') ? Constants.BRAND_BLUE: "rgb(136, 136, 136)"}}}/>
            <div style={{...styles.chatOption, ...{float: "right", display: (window.innerWidth > 768) ? 'inherit' : 'none',}}}>Global Chat</div>
          </div>
          <div className="col-6" style={{...{cursor: "pointer"}, ...{color: (this.state.toggle_state === 'local') ? Constants.BRAND_BLUE : "rgb(136, 136, 136)"}}} onClick={this.openLocalList.bind(this)}>
            <div style={{...styles.chatOption, ...{float: "left", display: (window.innerWidth > 768) ? 'inherit' : 'none'}}}>Local Chat</div>
            <FA  icon={faMapMarker} style={{...styles.icon, ...{color: (this.state.toggle_state === 'local') ? Constants.BRAND_BLUE : "rgb(136, 136, 136)"}}}/>
          </div>
        </div>
      </div>
    )


    let privateMessageOption = (<div></div>)
    privateMessageOption = (
      <div className="col-3 col-md-3" onClick={this.openPrivateConversationsList.bind(this)}>
        <div style={{float: 'right'}}>
        <div style={{display: (window.innerWidth > 768) ? 'inline-block' : 'none', cursor: 'pointer'}}>Contacts</div>
        <FA style={{...styles.icon, ...{marginLeft: "10px"}}} icon={faUsers}/>
        </div>
      </div>
    )

    let mobileTitles = (<div></div>)
    if(window.innerWidth <= 768){
      mobileTitles = (
        <div className="row" style={{fontSize: "10px", marginTop: "-3px", backgroundColor: "#fbfbfb"}}>
          <div className="col-3" style={{textAlign: "left"}}>
            <div style={{marginLeft: "5px"}}>Groups</div>
          </div>
          <div className="col-6">
            <div className="row">
            <div className="col-12" style={{textAlign: "center"}}>
            <div>Global/Local Chat</div>
            </div>
            </div>
          </div>
          <div className="col-3" style={{textAlign: "right"}}>
            <div style={{marginRight: "5px"}}>Contacts</div>
          </div>
        </div>
      )
    }

    let groupType = (this.state.current_group.address) ? this.state.current_group.address + " Chat" :'Global Chat';

    let courseInfo = (<div></div>)
    var join = (<div></div>)
    // if(!this.state.is_member){
        join = (<div style={{...styles.infoBookmark, ...{cursor: "pointer", color: ((this.state.is_member) ? "#b8b8b8" : Constants.BRAND_RED)}}} onClick={this.join.bind(this)} >
          {this.state.is_member ?  'Member' : 'Join' } 
          <FA style={{...styles.icon, ...{marginTop: "5px", marginLeft: "10px", marginRight: "10px", color: ((this.state.is_member) ? "#b8b8b8" : Constants.BRAND_RED)}}} icon={faBookmark}/>
        </div>
        )
    // }
    courseInfo = (
      <div className="row" style={{backgroundColor: "#fbfbfb"}}>
        <div style={styles.infoText} className="col-12">
          <div style={{...styles.infoTitle, ...{cursor: "pointer"}}} onClick={this.showCourseDetails.bind(this)} >
            <FA style={{...styles.icon, ...{marginTop: "5px", marginRight: "10px", color: "#b8b8b8"}}} icon={(this.state.course_details) ? faTimes : faInfo}/>
            {title}
          </div>
          {join}
        </div>

      </div>
    )

    let leaveBtn = (<div></div>)
    if(this.state.is_member){
      if(this.state.current_group.address){
      leaveBtn = (<div className="row">
              <div className="col-12">
                <button
               onMouseEnter={this.setButtonHovered.bind(this, true)} 
                onMouseLeave={this.setButtonHovered.bind(this, false)}
                className="general-button color-border"
                // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
                onClick={this.exitChatGroup.bind(this)}> Leave this local chat
                </button>
              </div>
            </div>)
    } else {
      leaveBtn = (<div className="row">
        <div className="col-12">
          <button
         onMouseEnter={this.setButtonHovered.bind(this, true)} 
          onMouseLeave={this.setButtonHovered.bind(this, false)}
          className="general-button color-border"
          // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
          onClick={this.exitCourse.bind(this)}> Leave this group
          </button>
        </div>
      </div>)
    }
    }
    
    let fullCourseInfo = (<div></div>)
    if(this.state.course_details && !this.state.leave_group){
      let usersAvatarList = (<UsersAvatarList group={this.state.current_group}/>)
      fullCourseInfo = (
        <div style={{position: "absolute", zIndex: "2", top: "140px", bottom: "0", left: "0", right: "0", backgroundColor: "white"}}>
          <div className="container-fluid" >
            <div  style={{maxWidth: "400px", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "15px", textAlign: "center"}}>

            <div className="row">
              <div className="col-12">
                <b> {this.state.course_info.title} </b>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <a href={this.state.course_info.url} target="_blank" >{this.state.course_info.url}</a>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                {this.state.course_info.description}
              </div>
            </div>
            <div style={{height: "20px"}}></div>
            <div className="row">
              <div className="col-12">
                <b>{groupType}, {this.state.current_group.members.length} Group Members </b>
              </div>
            </div>
            {usersAvatarList}
            <div style={{height: "20px"}}></div>
            {leaveBtn}
            </div>
          </div>
        </div>
      )
    }
    if(this.state.course_details && this.state.leave_group){
      fullCourseInfo = (
        <div style={{position: "absolute", zIndex: "2", top: "140px", bottom: "0", left: "0", right: "0", backgroundColor: "white"}}>
          <div className="container-fluid" >
            <div  style={{ textAlign: 'center', maxWidth: "400px", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "15px"}}>
              <div className="row">
                <div className="col-12">
                  Are you sure you want to leave the {groupType} chat group for <b>{this.state.course_info.title}</b>?
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                 onMouseEnter={this.optionButtonHovered.bind(this, 'leave')} 
                  onMouseLeave={this.optionButtonHovered.bind(this, 'leave')}
                  style={((this.state.leaveIsHovered) ? styles.btnHover : styles.btn)} 
                  onClick={this.confirmedLeaveGroup.bind(this)}> Yes
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                 onMouseEnter={this.optionButtonHovered.bind(this, 'cancel')} 
                  onMouseLeave={this.optionButtonHovered.bind(this, 'cancel')}
                  style={((this.state.cancelIsHovered) ? styles.cancelBtnHover : styles.cancelBtn)} 
                  onClick={this.exitChatGroup.bind(this)}> Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
    if(this.state.course_details && this.state.leave_course){
      fullCourseInfo = (
        <div style={{position: "absolute", zIndex: "2", top: "150px", bottom: "0", left: "0", right: "0", backgroundColor: "white"}}>
          <div className="container-fluid" >
            <div  style={{ textAlign: 'center', maxWidth: "400px", display: "block", marginLeft: "auto", marginRight: "auto", marginTop: "15px"}}>
              <div className="row">
                <div className="col-12">
                  Are you sure you want to leave <b>{this.state.course_info.title}</b>? If you leave the course you will be removed from the global chat group and any local chat groups related to this course in which you are a member. This action cannot be undone.
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                 onMouseEnter={this.optionButtonHovered.bind(this, 'leave')} 
                  onMouseLeave={this.optionButtonHovered.bind(this, 'leave')}
                  style={((this.state.leaveIsHovered) ? styles.btnHover : styles.btn)} 
                  onClick={this.confirmedLeaveCourse.bind(this)}> Yes
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                 onMouseEnter={this.optionButtonHovered.bind(this, 'cancel')} 
                  onMouseLeave={this.optionButtonHovered.bind(this, 'cancel')}
                  style={((this.state.cancelIsHovered) ? styles.cancelBtnHover : styles.cancelBtn)} 
                  onClick={this.exitCourse.bind(this)}> Cancel
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }

    var meetBar = (<div></div>)
    if(this.state.toggle_state === 'local' ){
    meetBar = (
      <div style={{zIndex: "1", position: "absolute", left: "0", right: "0"}}>
      <MeetBar />
      </div>
    )
    }

    return (
      <div>
        <div style={{height: "60px"}}></div>
        <div className="row" style={styles.infoBarContainer} >
          {coursesOption}
          {chatOptions}
          {privateMessageOption}
        </div>
        {mobileTitles}

        {courseInfo}
        {meetBar}
        {fullCourseInfo}
      </div>
    )
  }
}



const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  switchGroupAction,
  openLocalListAction,
  closeLocalListAction,
  openCourseListAction,
  openPrivateConversationsListAction,
  reloadChatPageAction,
  accountStateUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatViewInfoBar);


const styles = {
  infoBarContainer: {
    // backgroundColor: Constants.BRAND_WHITE,
    padding: "2px 10px 2px 10px",
    minHeight: "40px",
    backgroundColor: "#fbfbfb"
  },
  info: {
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    lineHeight: "40px"
  },
  infoText:{
    color: '#b8b8b8',
    boxShadow: "0 4px 2px -2px rgb(236, 234, 234)",
    zIndex: "99",
    backgroundColor: "#fbfbfb"
  },
  infoTitle: {
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    maxWidth: "200px",
    padding: "3px",
    textAlign: "center",
    float: "left"
    // display: "block",
    // marginLeft: "auto",
    // marginRight: "auto"
  },
  infoBookmark: {
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    maxWidth: "300px",
    padding: "3px",
    textAlign: "center",
    float: "right"
    // display: "block",
    // marginLeft: "auto",
    // marginRight: "auto"
  },
  chatOption: {
    padding: "5px",
    paddingTop: "10px"
  },
  icon: {
    fontSize: '1.5em',
    marginTop: '5px',
    color: "rgb(136, 136, 136)"
  },
  btn: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  cancelBtn: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_RED,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  cancelBtnHover: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: 'rgb(153, 33, 24)',
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}


// <div style={{zIndex: "2", height: "30px", right: "0px", float: "right", position: "absolute", top: "180px", width: "100%"}} >
//     <ShareButtons  share_url={Constants.SHARE_URL} />
// </div>


// <div style={styles.infoText} className="col-6">
//   <div style={{...styles.infoTitle, ...{marginTop: "12px"}}} >
//   <div style={{display: "inline-block"}}>{groupType} Chat</div>
//   </div>
// </div>
