import React, {Component} from 'react'
import { connect } from 'react-redux';
// import Constants from "../Constants";
import Api from "../Api";
import PreviewChatContents from "./PreviewChatContents"

class RecentActivityPreview extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      course: this.props.course,
      place: this.props.place,
      messages: null
    }
  }

  componentDidMount(){
    let $this = this;
    let data = {course_id: $this.state.course._id, place: $this.state.place}
    Api.fetchRecentActivityForCourse(data).then((res)=>{
      if(res.status === 200){
        // console.log('success', res);
        let messages = res.data;
        if(messages.length < 1){
          return;
        }
        $this.setState({messages: messages});
      } else {
        // console.log('error', res);
      }
    });
  }


  render(){
    let chat = (<div></div>)
    if(this.state.messages){
      chat = (
          <PreviewChatContents messages={this.state.messages}/>
        )
    } else {
      chat = (
        <div className="row">
          <div className="col-12">
            <div style={{padding: "20px"}}>No recent activity. Start the conversation!</div>
          </div>
        </div>
      )
    }
    return (
      <div className="container-fluid " style={{padding: 0}} >
        {chat}
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(RecentActivityPreview);


// const styles = {

// }

