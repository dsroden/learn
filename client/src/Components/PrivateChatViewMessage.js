import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import Timestamp from 'react-timestamp';
// import Api from '../Api'
import {openUserProfileViewAction } from "../Actions/actionCreator";
import  { o2LoadImage } from '../Utils'

class PrivateChatViewMessage extends Component { 
  constructor(props){
    super(props)
    this.state ={
      message: this.props.message,
      isself: this.props.isself,
      previoustime: this.props.previoustime,
      image_valid: false,
      transform: false
    } 
  }

  componentDidMount(){
    let $this = this;
    let imageUrl = Constants.PROFILE_PHOTO_BASE_URL + $this.state.message.username + '.png?raw=true';

    o2LoadImage(imageUrl).then((result)=>{
      if(result.image_valid){
        $this.setState({image_valid: true, transform: result.transform})
      }
    });
  }

  getAlphabetIndexColor(letter){
    let ALPHABET = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let index = ALPHABET.indexOf(letter);
    let color = Constants.ALPHABET_COLORS[index];
    return color;
  }

  compareDates(d1, d2){
   return d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate();
  }

  render() {
    let message = this.state.message;
    let user_profile_photo = (<div></div>);
    // if(this.state.message.username){
    //   let username_color = this.getAlphabetIndexColor(this.state.message.username[0].toLowerCase());
    //   user_profile_photo = (
    //     <div   style={{cursor: "pointer", position: "relative", display: "inline-block", width: "40px", height: "40px", textAlign: 'center', lineHeight: "40px", color: "white", fontWeight: "bold",  marginTop: "5px",  borderRadius: "100%", backgroundColor: username_color}}> 
    //       {this.state.message.username[0].toUpperCase()}
    //       <div  style={{cursor: "pointer", position: 'absolute', top: "0px", width: "40px", height: "40px", borderRadius: "100%", backgroundImage: 'url("' + Constants.PROFILE_PHOTO_BASE_URL + this.state.message.username + '.png")', backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat"}}> 
    //       </div>
    //     </div>
    //     )
    // }

    if(this.state.message.username){
      let username_color = this.getAlphabetIndexColor(this.state.message.username[0].toLowerCase());

      user_profile_photo = (
          <div style={{cursor: "pointer", position: "relative", display: "inline-block", width: "40px", height: "40px", textAlign: 'center', lineHeight: "40px", color: "white", fontWeight: "bold",  marginTop: "5px",  borderRadius: "100%", backgroundColor: username_color}}> 
            {this.state.message.username[0].toUpperCase()}
          </div>)

      if(this.state.image_valid){
        user_profile_photo = (
          <div style={{cursor: "pointer", position: "relative", display: "inline-block", width: "40px", height: "40px", textAlign: 'center', lineHeight: "40px", color: "white", fontWeight: "bold",  marginTop: "5px",  borderRadius: "100%", backgroundColor: username_color}}> 
            {this.state.message.username[0].toUpperCase()}
            <div style={{cursor: "pointer", position: 'absolute', top: "0px", width: "40px", height: "40px", borderRadius: "100%", backgroundImage: 'url("' + Constants.PROFILE_PHOTO_BASE_URL + this.state.message.username + '.png")', backgroundSize: "cover", backgroundPosition: "center center", backgroundRepeat: "no-repeat", transform: this.state.transform}}></div> 
          </div>
        )
      }
    }

    let m = (<div></div>)
    if(this.props.isself){
      m =(
      <div className="col-12" style={{padding: "0px"}}>
        <div style={{padding: "0px", borderRadius: "5px"}}>
        <div style={{float: "right"}}>
        <div  style={{float: "right"}}>
        <Timestamp time={message.created_at} style={styles.timestamp} format="time" />
        <div className="username" style={styles.username}>{message.username}</div>
        {user_profile_photo}
        </div>
        <div></div>
        <div className="chat-bubble-right" >
          <div style={styles.text}>{message.text}</div>
        </div>
        </div>
        </div>

      </div>
      )
      } else {
        m = (
      <div className="col-12" style={{padding: "00px"}}>
        <div style={{padding: "0px", borderRadius: "5px"}}>
        {user_profile_photo}
        <div className="username" style={styles.username} >{message.username}</div>
        <Timestamp time={message.created_at} style={styles.timestamp} format="time" />
        <div></div>
        <div className="chat-bubble" >
          <div style={styles.text}>{message.text}</div>
        </div>
        </div>
      </div>
      )

    }

    let previousTime = (<div></div>);
    let current_time = message.created_at;
    var msg_time = new Date(current_time);
    var now_time = new Date();
    var previous_time = new Date(this.state.previoustime);
    if(!this.compareDates(msg_time, previous_time)){
      let time = (<Timestamp time={current_time} format="date" />)
      if(this.compareDates(msg_time, now_time)){
        time = (<div>Today</div>)
      }
      previousTime = (
        <div className="row">
          <div className="col-12" style={{color: '#CCC', padding: "10px", textAlign: "center"}}>
            {time}
          </div>
        </div>
      )            
    }

    return (
      <div>
      {previousTime}
      <div className="row">
        {m}
      </div>
      </div>
    )
  }
}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateChatViewMessage);


const styles = {
  username: {
    fontSize: "14px",
    fontWeight: "bold",
    cursor: "pointer",
    display: "inline-block",
    marginLeft: "10px",
    marginRight: "10px"
  },
  timestamp: {
    fontSize: "12px",
    display: 'inline-block',
    marginLeft: "10px",
    color: "#ccc"
  }
}

