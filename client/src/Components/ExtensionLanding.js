import React, { Component } from 'react';
import  {getClientId } from '../Utils'
import Login from './Login';

class ExtensionLanding extends Component {
  constructor(props) {
    super(props);
    

    this.state = {
      client_id: null,
      timestamp: 'no timestamp yet',
      ips: [],
      tab: 'unknown',
      location: 'unknown',
      redirect_url: 'unknown'
    };
  }

  componentDidMount(){
    let $this = this;
    getClientId().then((client_id)=>{
      if(client_id){
        $this.setState({client_id: client_id}, ()=>{     
          $this.nav('main');
        });
      }
    });
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  render() {

    return (
      <div >
        <Login history={this.props.history} />
      </div>
    );
  }
}

export default ExtensionLanding;
         