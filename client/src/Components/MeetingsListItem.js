
import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import Api from '../Api'
import moment from 'moment';
import ProfilePhoto from "./ProfilePhoto";
import _ from "lodash";


class MeetingsListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      meeting: this.props.meeting,
      user: this.props.user,
      showAll: false
    };
  }

  componentDidMount(){
  }

  callApi(){
    Api.fetchMeetingDetails({meeting_id: this.props.meeting._id}).then((res)=>{
      console.log('res data', res.data);
      if(res.status === 200){
        var attending = _.find(res.data.attending, (u)=>{
            return u === this.state.user._id;
          });
        res.data.user_attending = attending;
        this.setState({meeting: JSON.parse(JSON.stringify(res.data)), showAll: true});
      }
    });
  }

  toggleAll(){
    var show_state = (this.state.showAll) ? false : true;
    if(show_state){
      this.callApi();
    } else {
      this.setState({showAll: show_state})
    }
  }

  joinMeetingOrLeave(){
    if(this.state.meeting.user_attending){
      console.log('leave meeting');
      Api.leaveMeeting({meeting_id: this.props.meeting._id, user_id: this.state.user._id}).then((res)=>{
        // console.log('res', res);
        this.callApi();
      });
    } else {
      Api.joinMeeting({meeting_id: this.props.meeting._id, user_id: this.state.user._id}).then((res)=>{
        // console.log('res', res);
        this.callApi();
      });
    }
  }

 
  render() {
    let meetingRow = (<div></div>)
    if(!this.state.showAll){
      meetingRow = (
       <div className="row list-hover" style={{padding: "10px"}}>
          <div className="col-8">
            <div>{moment(this.state.meeting.date).format("MMM Do YY")}</div>
            <div>{this.state.meeting.time}</div>
          </div>
          <div className="col-4">
            <button 
              className="general-button color-border center-item button-max"
              // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
              onClick={this.toggleAll.bind(this)}
              type="submit"> Details </button>
          </div>
        </div>
      )
    } else {
      let profilePhotos = (<div></div>)

      if(this.state.meeting.users && this.state.meeting.users.length > 0){
        profilePhotos = this.state.meeting.users.map((u) =>(
            <div style={{float: "left"}} key={u._id}>
            <ProfilePhoto user={u} size="50"/>
          </div>
        ));
      }

      meetingRow = (
        <div style={{backgroundColor: Constants.BRAND_LIGHTESTGRAY, borderBottom: '1px solid ' + Constants.BRAND_RED, borderTop: '1px solid ' + Constants.BRAND_RED}}>
       <div className="row " style={{padding: "10px"}}>
          <div className="col-8" style={{fontWeight: "700"}}>
            <div>{moment(this.state.meeting.date).format("MMM Do YY")}</div>
            <div>{this.state.meeting.time}</div>
          </div>
          <div className="col-4">
            <button 
              className="general-button color-border center-item button-max"
              // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
              onClick={this.toggleAll.bind(this)}
              type="submit"> Less </button>
          </div>
        </div>
        <div className="row " style={{padding: "10px"}}>
          <div className="col-12">
            <div> <b>Location:</b> {(this.state.meeting.place) ? this.state.meeting.place.address : 'Open'}</div>
          </div>
        </div>
        <div className="row " style={{padding: "10px"}}>
          <div className="col-12">
            <div><b>Goal & info:</b> {this.state.meeting.goal}</div>
          </div>
        </div>
        <div className="row " style={{padding: "10px"}}>
          <div className="col-12">
            <div><b>Number of people attending:</b> {this.state.meeting.attending.length} </div>
          </div>
        </div>
        <div className="row " style={{padding: "10px"}}>
          <div className="col-12">
            {profilePhotos}
          </div>
        </div>
        <div className="row " style={{padding: "10px"}}>
          <div className="col-12">
            <button 
              className="general-button color-border "
              // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
              onClick={this.joinMeetingOrLeave.bind(this)}
              type="submit"> {(this.state.meeting.user_attending) ? 'Leave' : 'Join' }</button>          
              </div>
        </div>
        </div>
      )
    }
    return (
      <div className="learnlabs-app">
       {meetingRow}
      </div>
    );
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(MeetingsListItem);


