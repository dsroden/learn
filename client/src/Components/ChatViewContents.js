import React, {Component} from 'react'
import { connect } from 'react-redux';
// import Constants from "../Constants"
import ReactChatView from 'react-chatview'
import ChatViewMessage from './ChatViewMessage'
import {receiveMessageAction } from "../Actions/actionCreator";
import Api from "../Api"
import {createRandomId } from "../Utils"

class ChatViewContents extends Component {
   constructor (props) {
    super(props);
    this.state = { 
      messages: null, // _.range(50).map(v => 'hi')
      user: this.props.user,
      channel: this.props.channel,
      channel_id: this.props.channel._id
    };
  }


  componentWillReceiveProps(newProps){
    if(newProps.received_message){
      this.addMessage(newProps.received_message);
    }
  }

  componentDidMount(){
    let data = {channel_id: this.state.channel_id};
    this.callApi(data);
  }

  componentWillUnmount(){
    this.setState({messages: []})
  }
 
  callApi = (data) => {
    let $this = this;
    Api.loadChannelMessages(data).then(res=>{
      if(res.status === 200){
        $this.setState({messages: res.data})
      }
    })
  };


  addMessage(messageData){
    messageData._id = createRandomId();
    // var messages = this.state.messages.concat([messageData]);
    // var messages = [messageData].concat(this.state.messages);
    let messages = [messageData].concat(this.state.messages);
    // console.log(messages);
    // this.setState({messages: []}, ()=>{
      this.setState({messages: messages})
    // });
  }

  loadMoreHistory () {
    return new Promise((resolve, reject) => {
      let data = {channel_id: this.state.channel_id, last_id: this.state.messages[this.state.messages.length -1]._id};
      Api.loadChannelMessages(data).then(res=>{
        if(res.status === 200){
          if(res.data.length === 1){
            return resolve();
          }
          let msgs = res.data;
          // let more = _.range(20).map(v=>'yo');
          this.setState({ messages: this.state.messages.concat(msgs)});
          resolve();      
        }
      })      
    });
  }

  render () {
    let chatlist = (<div></div>)
    if(this.state.messages && this.state.messages.length > 0){
      chatlist = (
          <ReactChatView 
            className="content"
            flipped={true}
            scrollLoadThreshold={50}
            onInfiniteLoad={this.loadMoreHistory.bind(this)}>
          {this.state.messages.map((m, ix) => <ChatViewMessage isself={((this.state.user) ? (m.username === this.state.user.username) : false)} previoustime={(this.state.messages[ix + 1]) ? this.state.messages[ix  + 1].created_at : ''} message={m} key={m._id}/>)}
        </ReactChatView>
        )
    }
    return (
      <div style={styles.chatViewContainer}>
        {chatlist}
      </div>
    )  
  }
}




const mapStateToProps = state => ({
  received_message: state.ChatReducer.received_message
});

const mapDispatchToProps = {
  receiveMessageAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatViewContents);


const styles = {
  chatViewContainer: {
    width: "100%",
    height: window.innerHeight - 160 - 40 - 30 + 'px',
    overflow: "auto"
  }
}

