import React, {Component} from 'react'
import { connect } from 'react-redux';
import {  } from "../Actions/actionCreator";
import Constants from "../Constants"
import FA from '@fortawesome/react-fontawesome';
import { faTimes, faShareAlt} from '@fortawesome/fontawesome-free-solid';
import { SocialIcon } from 'react-social-icons';

class ShareButtons extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      share_url: this.props.share_url,
      showShareOptions: false,
    }
  }

  showOptions(){
    let share = this.state.showShareOptions;
    share = (share) ? false : true;
    this.setState({showShareOptions: share});
  }

  render(){
    let url = this.state.share_url;

    return (
      <div style={{zIndex: '2'}}>
      <button onClick={this.showOptions.bind(this)} style={{height: "40px", width: "40px", backgroundColor: Constants.BRAND_BLUE, color: "white", float: "right", marginRight: "15px", borderRadius: "100%", boxShadow: "0px 0px 10px #ccc"}}> <FA icon={(this.state.showShareOptions) ? faTimes : faShareAlt} /> </button>
      <div style={{...styles.shareStripContainer, ...{display: (this.state.showShareOptions) ? 'inherit' : 'none'}}}>
        <div style={{marginTop: "5px"}}><SocialIcon style={{ height: 30, width: 30, marginTop: "3px" }} url={"https://www.facebook.com/dialog/share?app_id=" + Constants.FB_APP_ID + "&href=" + encodeURIComponent(url)} network="facebook" /></div>
         <div style={{marginTop: "5px"}}><SocialIcon style={{ height: 30, width: 30 }} url={"https://twitter.com/intent/tweet?url=" + encodeURIComponent(url)} network="twitter" /> </div>
         <div style={{marginTop: "5px"}}><SocialIcon style={{ height: 30, width: 30 }} url={"https://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(url)} network="linkedin" /> </div>
         <div style={{height: "5px"}}></div>
      </div>
      </div>
    )
  }

}

         // <SocialIcon style={{ height: 30, width: 30 }} url={"mailto:?subject=I wanted you to see this site&amp;body=Check out this site " + encodeURIComponent(url)} network="mailto" />


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ShareButtons);


const styles = {
  shareStripContainer: {
    height: "120px",
    width: "35px",
    position: "absolute",
    top: "40px",
    right: "15px",
    // backgroundColor: "white",
    color: "white",
  },
  shareItem: {
    width: "30px",
    height: "50px",
    backgroundColor: Constants.BRAND_BLUE,
    border: "1px solid " + Constants.BRAND_BLUE
  }
}

