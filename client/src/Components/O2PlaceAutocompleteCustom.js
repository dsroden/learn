
//from extension
import React, { Component } from 'react';
import Constants from '../Constants';
import axios from 'axios';
import { connect } from 'react-redux';
import Autosuggest from 'react-autosuggest';
import { updateSearchPlaceAction } from "../Actions/actionCreator";
import  {getLocalIPs } from '../Utils'

import FA from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/fontawesome-free-solid'

let topics = [
];

function getMatchingTopics(value) {
  // console.log('value', value);
  const escapedValue = escapeRegexCharacters(value.trim());
  
  if (escapedValue === '') {
    return [];
  }
  
  const regex = new RegExp('^' + escapedValue, 'i');


  let suggestions = topics.filter(topic => regex.test(topic.description));

  // let suggestions = topics.filter(topic => regex.test(topic.City));
  // console.log('suggestions', suggestions);
  return suggestions;
}

/* ----------- */
/*    Utils    */
/* ----------- */

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

/* --------------- */
/*    Component    */
/* --------------- */

function getSuggestionValue(suggestion) {
  return suggestion.description;
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion.description}</span>
  );
}


class O2PlaceAutocompleteCustom extends Component {
  constructor (props) {
    super(props)
    this.state = {topic: '', value: '', suggestions: [], isLoading: false};
  }


  componentDidMount(){
    let $this = this;
    getLocalIPs(function(ips) { // <!-- ips is an array of local IP addresses.
      // document.body.textContent = 'Local IP addresses:\n ' + ips.join('\n ');
        // freegeoip.net/{format}/{IP_or_hostname}

      axios.get(`https://freegeoip.net/json/${ips[1]}`).then((res)=>{
      console.log('ip locaiton', res);
        if(res.data.city){
          let address = res.data.city + ", " + res.data.country_name;
          $this.setState({value: address, location: {lat: res.data.latitude, lng: res.data.longitude}}, ()=>{
            $this.props.updateSearchPlaceAction({address: address, location: {lat: res.data.latitude, lng: res.data.longitude}})
          })
        }
      }).catch((err)=>{
         console.log('error creating example', err);
      })
    })
  }

  loadSuggestions(value) {
    console.log('load suggestion', value);
    if (this.lastRequestId !== null) {
      clearTimeout(this.lastRequestId);
    }
    
    this.setState({
      isLoading: true
    });
    
    // Fake request
    // axios.get(Constants.AUTOCOMPLETE_URL  + this.state.value ).then((res)=>{
    //   console.log('res', res);
    //   topics = res.data.Cities.splice(0,50);
    //   console.log(topics.length);
    //   this.setState({
    //     isLoading: false,
    //     suggestions: getMatchingTopics(value)
    //   });
    // }).catch((err)=>{
    //   console.log('error creating example', err);
    // })

    axios.post(Constants.API_URL + '/googleapi/places', {query: this.state.value}).then((res)=>{
      console.log('social login res', res);
      topics = res.data.predictions;
      this.setState({
        isLoading: false,
        suggestions: getMatchingTopics(value)
      });

    }).catch((err)=>{
      console.log('error creating example', err);
    });
  }

  onTopicChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    }, ()=>{
      console.log('new value', newValue);

      axios.post(Constants.API_URL + '/googleapi/geo', {query: newValue}).then((res)=>{
        console.log('google api geo', res);
        let place = {};
        if(res.status === 200){
          place = res.data.results[0] 
        } else {
          place = {formatted_address: null, location: {geometry: null}};
        }
        this.props.updateSearchPlaceAction({address: place.formatted_address, location: place.geometry.location})
      }).catch((err)=>{
        console.log('error creating example', err);
        let place = {formatted_address: null, geometry: {location: null}};
        this.props.updateSearchPlaceAction({address: place.formatted_address, location: place.geometry.location})

      });
    });
  };

    
  onSuggestionsFetchRequested = ({ value }) => {
    this.loadSuggestions(value);
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  renderSuggestionsContainer = ({ containerProps , children, query })=>{
    return (
      <div {... containerProps}>
        {children}
        <div className="dropdown-footer">
          <div>
            <img style={{width: '150px', float: 'right'}} alt="google-logo" src={require('../assets/powered_by_google_on_white_hdpi.png')} />
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { value, suggestions } = this.state;
    const topicsProps = {
      placeholder: "Search places",
      value,
      onChange: this.onTopicChange
    };

    let autocompleteTopic = (
      <div >
        <FA style={styles.inputIcon} icon={faMapMarkerAlt} />
        <Autosuggest 
          id="placesAutosuggest"
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          renderSuggestionsContainer={this.renderSuggestionsContainer}
          inputProps={topicsProps} />
      </div>
    )



    return (
        <div>
          {autocompleteTopic}
        </div>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  updateSearchPlaceAction
};

export default connect(mapStateToProps, mapDispatchToProps)(O2PlaceAutocompleteCustom);





const styles = {
  searchContainer: {
    "position": "relative",
    "width": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "borderRadius": "5px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  inputIcon: {
    width: "10%",
    textAlign: 'center',
    display: 'inline-block',
    color: Constants.BRAND_RED,
    fontSize: "1.5em"

  },
  input: {
    "width": "90%",
    "height": "50px",
    marginTop: "10px",
    marginBottom: "10px",
    border: 'none',
    borderBottom: '2px solid ' + Constants.BRAND_RED
  },
  btn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_BLACK,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}
//from original 

// import React, { Component } from 'react';
// import Constants from '../Constants';
// import axios from 'axios';
// import { connect } from 'react-redux';
// import Autosuggest from 'react-autosuggest';
// import { updateSearchPlaceAction } from "../Actions/actionCreator";
// import  {getLocalIPs } from '../Utils'

// import FA from '@fortawesome/react-fontawesome'
// import { faMapMarkerAlt } from '@fortawesome/fontawesome-free-solid'

// let topics = [
// ];

// function getMatchingTopics(value) {
//   console.log('value', value);
//   const escapedValue = escapeRegexCharacters(value.trim());
  
//   if (escapedValue === '') {
//     return [];
//   }
  
//   const regex = new RegExp('^' + escapedValue, 'i');


//   let suggestions = topics.filter(topic => regex.test(topic.description));

//   // let suggestions = topics.filter(topic => regex.test(topic.City));
//   console.log('suggestions', suggestions);
//   return suggestions;
// }

// /* ----------- */
// /*    Utils    */
// /* ----------- */

// // https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
// function escapeRegexCharacters(str) {
//   return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
// }

// /* --------------- */
// /*    Component    */
// /* --------------- */

// function getSuggestionValue(suggestion) {
//   return suggestion.description;
// }

// function renderSuggestion(suggestion) {
//   return (
//     <span>{suggestion.description}</span>
//   );
// }


// class O2PlaceAutocompleteCustom extends Component {
//   constructor (props) {
//     super(props)
//     this.state = {topic: '', value: '', suggestions: [], isLoading: false};
//   }


//   componentDidMount(){
//     let $this = this;
//     getLocalIPs(function(ips) { // <!-- ips is an array of local IP addresses.
//       // document.body.textContent = 'Local IP addresses:\n ' + ips.join('\n ');
//         // freegeoip.net/{format}/{IP_or_hostname}

//       axios.get(`https://freegeoip.net/json/${ips[1]}`).then((res)=>{
//       console.log('ip locaiton', res);
//         if(res.data.city){
//           let address = res.data.city + ", " + res.data.country_name;
//           $this.setState({value: address, location: {lat: res.data.latitude, lng: res.data.longitude}}, ()=>{
//             $this.props.updateSearchPlaceAction({address: address, location: {lat: res.data.latitude, lng: res.data.longitude}})
//           })
//         }
//       }).catch((err)=>{
//          console.log('error creating example', err);
//       })
//     })
//   }

//   loadSuggestions(value) {
//     console.log('load suggestion', value);
//     if (this.lastRequestId !== null) {
//       clearTimeout(this.lastRequestId);
//     }
    
//     this.setState({
//       isLoading: true
//     });
    
//     // Fake request
//     // axios.get(Constants.AUTOCOMPLETE_URL  + this.state.value ).then((res)=>{
//     //   console.log('res', res);
//     //   topics = res.data.Cities.splice(0,50);
//     //   console.log(topics.length);
//     //   this.setState({
//     //     isLoading: false,
//     //     suggestions: getMatchingTopics(value)
//     //   });
//     // }).catch((err)=>{
//     //   console.log('error creating example', err);
//     // })

//     axios.post(Constants.API_URL + '/googleapi/places', {query: this.state.value}).then((res)=>{
//       console.log('social login res', res);
//       topics = res.data.predictions;
//       this.setState({
//         isLoading: false,
//         suggestions: getMatchingTopics(value)
//       });

//     }).catch((err)=>{
//       console.log('error creating example', err);
//     });
//   }

//   onTopicChange = (event, { newValue }) => {
//     this.setState({
//       value: newValue
//     }, ()=>{
//       console.log('new value', newValue);

//       axios.post(Constants.API_URL + '/googleapi/geo', {query: newValue}).then((res)=>{
//         console.log('google api geo', res);
//         let place = {};
//         if(res.status === 200){
//           place = res.data.results[0] 
//         } else {
//           place = {formatted_address: null, location: {geometry: null}};
//         }
//         this.props.updateSearchPlaceAction({address: place.formatted_address, location: place.geometry.location})
//       }).catch((err)=>{
//         console.log('error creating example', err);
//         let place = {formatted_address: null, geometry: {location: null}};
//         this.props.updateSearchPlaceAction({address: place.formatted_address, location: place.geometry.location})

//       });
//     });
//   };

    
//   onSuggestionsFetchRequested = ({ value }) => {
//     this.loadSuggestions(value);
//   };

//   onSuggestionsClearRequested = () => {
//     this.setState({
//       suggestions: []
//     });
//   };

//   renderSuggestionsContainer = ({ containerProps , children, query })=>{
//     return (
//       <div {... containerProps}>
//         {children}
//         <div className="dropdown-footer">
//           <div>
//             <img style={{width: '150px', float: 'right'}} alt="google-logo" src={require('../assets/powered_by_google_on_white_hdpi.png')} />
//           </div>
//         </div>
//       </div>
//     );
//   }

//   render() {
//     const { value, suggestions } = this.state;
//     const topicsProps = {
//       placeholder: "Search places",
//       value,
//       onChange: this.onTopicChange
//     };

//     let autocompleteTopic = (
//       <div >
//         <FA style={styles.inputIcon} icon={faMapMarkerAlt} />
//         <Autosuggest 
//           id="placesAutosuggest"
//           suggestions={suggestions}
//           onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
//           onSuggestionsClearRequested={this.onSuggestionsClearRequested}
//           getSuggestionValue={getSuggestionValue}
//           renderSuggestion={renderSuggestion}
//           renderSuggestionsContainer={this.renderSuggestionsContainer}
//           inputProps={topicsProps} />
//       </div>
//     )



//     return (
//         <div>
//           {autocompleteTopic}
//         </div>
//     );
//   }
// }

// const mapStateToProps = state => ({
// });

// const mapDispatchToProps = {
//   updateSearchPlaceAction
// };

// export default connect(mapStateToProps, mapDispatchToProps)(O2PlaceAutocompleteCustom);





// const styles = {
//   searchContainer: {
//     "position": "relative",
//     "width": "100%",
//     "backgroundColor": Constants.BRAND_WHITE,
//     "borderRadius": "5px",
//     maxWidth: "900px",
//     display: "block",
//     marginLeft: "auto",
//     marginRight: "auto"
//   },
//   inputIcon: {
//     width: "10%",
//     textAlign: 'center',
//     display: 'inline-block',
//     color: Constants.BRAND_RED,
//     fontSize: "1.5em"

//   },
//   input: {
//     "width": "90%",
//     "height": "50px",
//     marginTop: "10px",
//     marginBottom: "10px",
//     border: 'none',
//     borderBottom: '2px solid ' + Constants.BRAND_RED
//   },
//   btn: {
//     height: "50px",
//     width: "100%",
//     marginTop: "10px",
//     marginBottom: "10px",
//     backgroundColor: Constants.BRAND_GREEN,
//     borderRadius: '5px',
//     cursor: "pointer",
//     color: Constants.BRAND_WHITE,
//     fontWeigt: '700'
//   },
//   btnHover: {
//     height: "50px",
//     width: "100%",
//     marginTop: "10px",
//     marginBottom: "10px",
//     backgroundColor: Constants.BRAND_BLACK,
//     borderRadius: '5px',
//     cursor: "pointer",
//     color: Constants.BRAND_WHITE,
//     fontWeigt: '700'
//   }
// }







