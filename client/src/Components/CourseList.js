import React, { Component } from 'react';
import { connect } from 'react-redux';
import Constants from '../Constants';
import Course from './CourseItem';
import { placeUpdatedAction, topicsUpdatedAction} from "../Actions/actionCreator";
import Api from "../Api";
// import Infinite from 'react-infinite'
import _ from "lodash"
import loadingGif from "../assets/loading.gif"
class CourseList extends Component {
  constructor (props) {
    super(props)
    this.state = {coach: this.props.coach || null, courses: [], place: null, topics: null, loading: false};
  }

 componentDidMount() {
    // window.addEventListener('scroll', this.onScroll, false);
    this.callApi('mount')
    this.setState({mounted: true})
 }



  componentWillUnmount() {
    // window.removeEventListener('scroll', this.onScroll, false);
        this.props.topicsUpdatedAction(null);

  }

  // onScroll = () => {
  //   if (
  //     (window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 500) &&
  //     this.state.courses.length && !this.state.loading
  //   ) {
  //     this.onPaginatedSearch();
  //   }
  // }

  onPaginatedSearch = ()=>{
    let $this = this;
    if($this.state.loading){
      // console.log('still loading');
    }
    $this.setState({loading: true}, ()=>{
      // console.log('paginate');
      Api.search({topics: $this.state.topics, place: $this.state.place, last_id: $this.state.courses[$this.state.courses.length - 1]._id}).then((res)=>{
        // console.log('loaded more', res.data.courses.length);
        if(res.data.courses.length < 1){
          // $this.setState({loading: false})
          return;
        } else {
          let courses = $this.state.courses;
          // courses = courses.concat(res.data.courses);
          let merge = _.unionBy(courses, res.data.courses, '_id');
          // console.log(merge);
          // $this.cleanCourses(courses, (cleanCourses)=>{
            setTimeout(()=>{
              // console.log('length ', cleanCourses.length);
              // console.log('merge length', merge.length);
              $this.setState({courses: merge, last_id: null, loading: false});
            }, 2000)
          // });
        }
      });
    })
    
  }

  cleanCourses(courses, cb){
    // console.log('coures lengh', courses.length);
    let clean =  _.uniqBy(courses, function (e) {

      return e._id;
    });
    // console.log('clean', clean.length);
    cb(clean)
  }

 componentWillReceiveProps(newProps){
    // console.log('new props in course list', newProps);
    let $this = this;
    let place = (!newProps.place || newProps.place.address.length < 4) ? null : newProps.place;
    let searchData = {topics: [].concat.apply([], newProps.topics), place: place};
    $this.setState({topics: searchData.topics, place: searchData.place}, ()=>{
      $this.callApi(searchData);
    })
 }

 callApi = (data) => {
    let $this = this;
    $this.setState({courses: []}, ()=>{
      data = (data !== 'mount') ? data : {topics: null, place: null};
      // console.log('data for classes call', data);
      Api.search(data).then(res =>{
        if(res.status === 200){
          // $this.props.courseSearchDoneAction(res.data.courses);
          $this.setState({courses: res.data.courses})
        }
      });
    })

  };

 render() {
    let list = (<div></div>);
    let courses = (<div></div>)
    if(this.state.courses.length > 0){
      let place = this.state.place;
      if(!place){
        place = null
      }
      var allCourses = _.filter(this.state.courses, (c)=>{
        return c.type !== 'category';
      })
      courses = allCourses.map((course) =>(
        <Course coach={this.state.coach} history={this.props.history} course={course} key={course._id} place={place}/>
      ))
      let loading = (<div></div>)
      if(this.state.loading){
        loading = (<div className="row">
            <div className="col-12">
              <div style={{textAlign: 'center', padding: "30px"}}><img alt="loading" src={loadingGif} width="80" height="80" style={{display: "block", marginLeft: "auto", marginRight: "auto"}}/></div>
            </div>
          </div>)
      }
      let loadMore = (<div></div>)
      if(!this.state.loading){
        loadMore = (
          <button onClick={this.onPaginatedSearch.bind(this)} className="general-button color-border center-item" style={{marginTop: "10px", maxWidth: "300px", display: "block", marginLeft: "auto", marginRight: "auto"}}>Load More</button>
        )
      }
      list = (
          <div className="container-fluid" style={styles.listContainer}>
          {courses}
          {loadMore}
          {loading}
          </div>
      )
    }

    return (
        <div>
        {list}
        </div>
    );
  }
}


const mapStateToProps = state => ({
  courses: state.SearchReducer.courses,
  topics: state.SearchReducer.topics,
  place: state.SearchReducer.place

});

const mapDispatchToProps = {
  placeUpdatedAction,
  topicsUpdatedAction
  
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseList);


const styles = {
  rowDescriptionContainer: {
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    overflow: "hidden"
  },
  rowDescription: {
    backgroundColor: Constants.BRAND_GREEN,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
    color: Constants.BRAND_WHITE,
    fontSize: "1.3em",
    fontWeight: "700"
  },
  colSeparator: {
    borderLeft: "1px solid " + Constants.BRAND_WHITE,
    borderRight: "1px solid " + Constants.BRAND_WHITE
  },
  listContainer: {
    padding: "0px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  }
}

// <div style={styles.rowDescriptionContainer}>
//         <div className="row" style={styles.rowDescription}>
//           <div className="col-5" >
//             <div>Course</div>
//           </div>
//           <div className="col-4" style={styles.colSeparator}>
//             <div>Location</div>
//           </div>
//           <div className="col-3" >
//             <div>Status</div>
//           </div>
//         </div>
//         </div>