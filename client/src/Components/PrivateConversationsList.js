
import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import Api from '../Api';
import FA from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';
import {   closePrivateConversationsListAction  } from "../Actions/actionCreator";
import PrivateConversationsListItem from "./PrivateConversationsListItem"

class PrivateConversationsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
      listState: 'people-list-close',
      contacts: [],
    };
  }

  componentWillReceiveProps(newProps){
    let listState = '';
    if(newProps.private_conversations_list_state){
      listState =  'people-list-open';
    } else {
       listState = 'people-list-close'
    }
    this.setState({listState: listState});
    if(listState === 'people-list-open'){
      this.callApi();
    }
  }

  componentDidMount(){
    this.callApi();
  }

  callApi(){
    let $this = this;
    Api.fetchRecentConversations($this.state.user).then((contacts)=>{
      $this.setState({contacts: []}, ()=>{
        $this.setState({contacts: contacts.data})
      })
    });
  }

  closeList(){
    this.props.closePrivateConversationsListAction(false);
  }

  render() {

    let contacts = (<div></div>)
      if(this.state.contacts && this.state.contacts.length > 0){
        contacts = this.state.contacts.map((contact) =>(
          <PrivateConversationsListItem user={this.state.user} contact={contact}  key={contact._id}/>
        ));
      }

    return (
      <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{margin: "0", borderTop: '1px solid #ffffff42', color: "white", backgroundColor: Constants.BRAND_RED, }}>
          <div className="col-12">
          <div onClick={this.closeList.bind(this)} style={{textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
            Contacts
            <FA icon={faTimes} style={{float: "left", marginLeft: "10px"}} />
          </div>
          </div>
        </div>
        <div style={styles.innerSideMenuContents}>
        {contacts}
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  private_conversations_list_state: state.ChatReducer.private_conversations_list_state
});

const mapDispatchToProps = {
  closePrivateConversationsListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateConversationsList);


const styles = {
    listContainer: {
     top: "60px",
      width: "100%",
      maxWidth: "400px",
      position: "fixed",
      // height: window.innerHeight - 40 + "px",
      // overflow: "auto",
      right:  "-415px",
      // paddingBottom: "40px",
      background: Constants.BRAND_LIGHTGRAY,
      zIndex: "2147483647",
      boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
    },
    innerSideMenuContents: {
    height: window.innerHeight - 100 + "px",
    overflowY: "scroll",
    paddingBottom: "40px"
  }
}

