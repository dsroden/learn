import React, {Component} from 'react'
import { connect } from 'react-redux';
import { toggleMenuAction } from "../Actions/actionCreator";


class SideMenu extends Component  {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  nav = (location) =>{
    this.props.history.push('/' + location);
  }

  selectMenuItem(val){
    this.nav(val);
    this.closeMenu()
  }

  logout(){
    window.localStorage.clear()
    window.location.href = '/'
  }

  closeMenu(){
    this.props.toggleMenuAction('close');
  }

  render(){
    let browse = (
        <div className="row">
          <div className="col-12">
            <button onClick={this.selectMenuItem.bind(this, 'browse')} className="general-button color-border center-item" style={{margin: "10px", width: "90%", display: "block", marginLeft: "auto", marginRight: "auto"}}>Browse</button>
          </div>
        </div>
      )
    let groups = (
        <div className="row">
          <div className="col-12">
            <button onClick={this.selectMenuItem.bind(this, 'chat')} className="general-button color-border center-item" style={{margin: "10px", width: "90%", display: "block", marginLeft: "auto", marginRight: "auto"}}>My Groups</button>
          </div>
        </div>
      )

    let profile = (
        <div className="row">
          <div className="col-12">
            <button onClick={this.selectMenuItem.bind(this, 'profile')} className="general-button color-border center-item" style={{margin: "10px", width: "90%", display: "block", marginLeft: "auto", marginRight: "auto"}}>Profile</button>
          </div>
        </div>
      )

    let logout = (        
      <div className="row" >
        <div className="col-12">    
          <button className="general-button color-border center-item" style={{margin: "10px", width: "90%", display: "block", marginLeft: "auto", marginRight: "auto"}}  onClick={this.logout.bind(this)} >Logout</button>
        </div>
      </div>
      )
    return (
      <div className="container-fluid">
      <div style={{height: "20px"}}></div>
      {browse}
      {groups}
      {profile}
      {logout}
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  toggleMenuAction
};

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);


// const styles = {

// }

