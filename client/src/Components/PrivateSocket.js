import React, {Component} from 'react'
import { connect } from 'react-redux';
import { receivePrivateMessageAction} from "../Actions/actionCreator";
import socketCluster from 'socketcluster-client';
import _ from "lodash";
import Api from "../Api";
import debug from "../Constants/debug.js"
import Constants from '../Constants/index.js'

class PrivateSocket extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      private_message: null,
      blocked_users: this.props.user.blocked
    }
  } 

  componentWillReceiveProps(newProps){
    if(newProps.blocked_user){
      let blocked_users = this.state.blocked_users || [];
      blocked_users.push(newProps.blocked_user);
      this.setState({blocked_users: blocked_users});
    }
    if(newProps.private_message !== this.state.private_message){
      this.setState({"private_message": newProps.private_message})
      this.publishPrivateMessage(newProps.private_message);
    }
  }

  checkIfBlocked(username){
    let blocked = _.find(this.state.blocked_users, (u)=>{
      return username === u;
    });
    return blocked;
  }


  componentDidMount(){
    let $this = this;
    var options;
    // console.log('mounting private socket', debug.debug)
    if(!debug.debug){
      options = {
        hostname: Constants.SOCKET_HOST,
        secure: true,
        // port: Constants.SOCKET_PORT,
        rejectUnauthorized: false
      };      
    } else {
      options = {
        port: 5000
      };
    }
  

    // Initiate the connection to the server
    $this.socket = socketCluster.create(options);
    
    $this.socket.on('connect', function (status) {
      //check authentication
      // console.log('status of connection', status);
      if(!status.isAuthenticated){
        $this.authenticate()
      } else { 
        $this.registerEvents();
      }
    });

  }

  authenticate(){
    let $this = this;
    var credentials = {
      username: $this.props.user.username,
      access_token: $this.props.user.access_token
    };
    $this.socket.emit('setAuth', credentials, function (err) {
      if (err) {
        //err would be caused if login failed
        console.log('err', err)
      } else {
        $this.registerEvents();
      }
    });
  }

  registerEvents(){
    // console.log('register events')
    let $this = this;

    //subscribe the user to his/her own channel
    $this.socket.username = {username: $this.state.user.username};
    $this.socket.subscribe('personal-'+ $this.state.user.username).watch(function (data) {
      // console.log('subscribed to self', data);
      let blocked = $this.checkIfBlocked(data.username);
      if(blocked){
        //should delete the message
        Api.expireMessage(data).then((res)=>{

        });
        return;
      } else {
        $this.props.receivePrivateMessageAction(data);
      }
    });
  }

  publishPrivateMessage(messageData){
    // Client code
    let $this = this;
    messageData.created_at = new Date();
    if(messageData.username !== messageData.receiver){
      $this.props.receivePrivateMessageAction(messageData);
    }
    // console.log('message ', messageData);
    $this.socket.emit('private_message', messageData, function(err) {
      // $this.socket.publish(messageData.channelId, messageData, function (err) {
        if (err) {
          // console.log('failed', err);
          // Failed to publish event, retry or let the user know and keep going?
        } else {
          // console.log('success');
          // Event was published successfully
        }
      // });
    });
  }

  destroy(){
    // destroying the socket will destory the connection and authentication will need to happen again
    this.socket.destroy();    
  }

  componentWillUnmount(){
    //destroying the socket will destory the connection and authentication will need to happen again
    this.socket.destroy();
  }

  render(){
    return (
      <div>
      </div>
    )
  } 
}


const mapStateToProps = state => ({
    private_message: state.ChatReducer.private_message,
    blocked_user: state.AccountReducer.blocked_user
});

const mapDispatchToProps = {
    receivePrivateMessageAction

};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateSocket);



