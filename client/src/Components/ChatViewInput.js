import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
// import FA from '@fortawesome/react-fontawesome';
// import { faArrowRight } from '@fortawesome/fontawesome-free-solid';
import { sendMessageAction, accountStateUpdatedAction} from "../Actions/actionCreator";
import sendIcon from '../assets/airplane-icon.png';
import Api from '../Api';

class ChatViewInput extends Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this);
    this._handleKeyPress = this._handleKeyPress.bind(this);

    this.state = {
      message: '',
      user: this.props.user,
      update: false,
    }
  } 

  componentWillReceiveProps(newProps){
    // var update = this.state.update;
    Api.fetchUserSelf({access_token: window.localStorage.getItem('access_token')}).then((user_res)=>{
      if(user_res.status === 200){
        this.setState({user: user_res.data})
      }
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  }

  _handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.sendMessage(this.state.message)
    }
  }

  sendMessage(message){
    if(!message){
      message = this.state.message;
    }
    if(message === ''){
      return;
    }
    let data = {text: message};
    this.props.sendMessageAction(data);
    this.setState({message: ''})
  }


  login(){
        this.props.accountStateUpdatedAction('not_logged_in')
  }

  render(){
    let sendButton = (<div></div>)
    // if(window.innerWidth <= 768){
      sendButton = (
        <button style={{position: "absolute", top: "7px", right: "-2px", backgroundColor: "transparent", border: "none"}} onClick={this.sendMessage.bind(this, null)} >
        <img alt="send" width="35" height="35" src={sendIcon} style={{ borderRadius: "100%", marginTop: "0px", backgroundColor: Constants.BRAND_RED}} />
        </button>
      )
    // }
    var valid = (<React.Fragment></React.Fragment>)
    if(!window.localStorage.getItem("access_token")){
      valid = (
        <div style={{position: "absolute", top: "-5px", bottom: "-5px", left: "-5px", right: "-5px", background: 'rgba(255, 255, 255, 0.8)'}}>
          <button onClick={this.login.bind(this)} className="general-button color-border">Login to chat</button>
        </div>
        )
    }
    return (
      <div className="row" style={styles.inputContainer}>
        <div className="color-border" style={{width: "100%", position: "relative", height: "50px", backgroundColor: "white", marginTop: "20px", marginLeft: "10px", marginRight: "10px"}}>
        <input  style={styles.input} type="text" placeholder="Message..." ref="message" name="message" value={this.state.message} onChange={this.handleInputChange} onKeyPress={this._handleKeyPress} />
        {sendButton}
        {valid}
        </div>
      </div>
    )
  } 
}


const mapStateToProps = state => ({
  account_state: state.AccountReducer.account_state
});

const mapDispatchToProps = {
  sendMessageAction,
  accountStateUpdatedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatViewInput);


const styles = {
  inputContainer: {
    padding: "0",
    bottom: "0",
    width: "100%",
    height: "130px",
    margin: "0"
  },
  input: {
    width: window.innerWidth - 80 + "px",
    margin: "10px",
    borderRadius: "5px",
    padding: "5px",
    paddingRight: "4%",
    border: "none"
  },
  sendButton: {
    position: "absolute",
    right: "20px",
    display: "inline-block",
    width: "15%",
    height: "50px",
    marginTop: "10px",
    borderTopRightRadius: "5px",
    borderBottomRightRadius: "5px",
    backgroundColor: Constants.BRAND_GREEN
  }
}


