import React, {Component} from 'react'
import { connect } from 'react-redux';
import {  } from "../Actions/actionCreator";
// import Constants from "../Constants"
import Nav from "./Nav"

class TermsOfUse extends Component  {
  constructor (props) {
    super(props)
    this.state = {
    }
  }


  render(){
    return (
      <div className="learnlabs-app">
        <Nav safe="true" history={this.props.history} />
        <div style={{height: "70px"}} ></div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <h3>
              LearnLabs Terms of Use
              </h3>
            </div>
          </div>          

          <div className="row">
            <div className="col-12">
              <p>
              LearnLabs’s mission is to empower learners by helping to connect them to each other, learning providers and coaches, and learning opportunities. We enable anyone anywhere to join learning circles and connect with other learners and learning coaches. We need rules to keep our platform and services safe for you, us, learn coaches, and our learning community. These Terms apply to all your activities on the LearnLabs website, the LearnLabs mobile applications, our Browser Extensions, our APIs and other related services (“Services”).
              </p>
              <p>
              We provide details regarding our processing of personal data of our learners and learning coaches in our Privacy Policy. If you do not agree and consent, discontinue use of the Services.
              </p>
              <p>
              If you live in the United States or Canada, by agreeing to these Terms, you agree to resolve disputes with LearnLabs through binding arbitration (with very limited exceptions, not in court), and you waive certain rights to participate in class actions, as detailed in the Dispute Resolution section.
              </p>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>Table of Contents</p>
              <ul>
                <li>1. Accounts</li>
                <li>2. Content and Behavior Rules</li>
                <li>3. LearnLabs’s Rights to Content You Post</li>
                <li>4. Using LearnLabs at Your Own Risks</li>
                <li>5. LearnLabs’s Rights</li>
                <li>6. Miscellaneous Legal Terms</li>
                <li>7. Dispute Resolution</li>
                <li>8.Updating These Terms</li>
                <li>9. How to Contact Us </li>
              </ul>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
                <b> 
                1. Accounts
                </b>
              </p>
              <p>
              You need an account for most activities on our platform. Keep your password somewhere safe, because you’re responsible for all activity associated with your account. If you suspect someone else is using your account, let us know by contacting support@learnlabs.city. You must have reached the age of consent for online services in your country to use LearnLabs.
              </p>
              <p>
              You need an account for most activities on our platform, including to joining a learning group, contacting other users, and contacting learning coaches. When setting up and maintaining your account, you must provide and continue to provide accurate and complete information, including a valid email address. You have complete responsibility for your account and everything that happens on your account, including for any harm or damage (to us or anyone else) caused by someone using your account without your permission. This means you need to be careful with your password. You may not transfer your account to someone else or use someone else’s account without their permission. If you contact us to request access to an account, we will not grant you such access unless you can provide us the login credential information for that account. In the event of the death of a user, the account of that user will be closed.
              </p>
              <p>
              If you share your account login credential with someone else, you are responsible for what happens with your account and LearnLabs will not intervene in disputes between students or instructors who have shared account login credentials. You must notify us immediately upon learning that someone else may be using your account without your permission (or if you suspect any other breach of security) by contacting our  support@learnlabs.city. We may request some information from you to confirm that you are indeed the owner of your account.
              </p>
              <p>
              Users and learning coaches must be at least 18 years of age to create an account on LearnLabs and use the Services. If you are younger than the required age, you may not set up an account, but we encourage you to invite a parent or guardian to open an account and help you join learning groups that are appropriate for you. If we discover that you have created an account and you are younger than the required age for consent to use online services (for example, 13 in the US), we will terminate your account. Under our coach agreement, you may be requested to verify your identity before you are authorized to become a learning coach on LearnLabs.
              </p>
              <p>
              You can terminate your account at any time by going to your profile and selecting ‘Delete Account’. Check our Privacy Policy to see what happens when you terminate your account.
              </p>
              
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              2. Content and Behavioral Rules
              </b>
              </p>
              <p>
              You can only use LearnLabs for lawful purposes. You’re responsible for all the content that you post on our platform. You should keep the reviews, questions, posts, courses and other content you upload in line with restriction guidelines listed below and the law, and respect the intellectual property rights of others. We can ban your account for repeated or major offenses. If you think someone is infringing your copyright on our platform, let us know. 
              </p>

              <p>
              You may not do any of the following things in connection with the Services:
              </p>

              <ul>
                <li>
                Abuse, harass, threaten or intimidate other users.

                </li>
                <li> 
                Create or submit spam to any user.

                </li>
                <li>
                Upload, post or otherwise include any content that is defamatory, libelous, tortuous, vulgar, obscene, invasive of privacy, racially or ethnically objectionable, hateful, promotes or provides instructional information about illegal activities, or promotes any act of cruelty to animals.

                </li>
                <li>
                Impersonate another user or any employee of LearnLabs.

                </li>
                <li>
                Use the Services for any illegal, political, commercial, or unauthorized purpose, or to promote or make solicitations for any illegal or unauthorized activity.

                </li>
                <li> 
                Use the Service to distribute, share or solicit content belonging to third parties without proper authorization from the owner(s) of that content.

                </li>
                <li>
                Transmit content in breach of a confidentiality or fiduciary obligation you may have.

                </li>
                <li>
                Violate any Federal, state or local laws.

                </li>
                <li>
                Modify, reverse engineer, adapt, or hack the Service, or engage in any activity in connection with the Services that is objectionable to LearnLabs.

                </li>
                <li> 
                Reproduce, duplicate, copy, sell, resell or exploit any portion of the Services. 

                </li>
                <li>
                Attempt to automatically redirect users to other domains, or mislead users into navigating away from our Services.

                </li>
                <li>
                Affect the way the Services operates (including the security measures used by the Service) or displays its pages, such as by framing the Service or placing pop-up windows over its pages, or otherwise adversely affect the display of any advertising or promotional links on the Service.
                </li>

                <li>
                Use any automated means to access the Services or collect any information from the Services.
                </li>

                <li>
                Collect or request any personal information about users through the Services.
                </li>

                <li>
                Publish or use files or processes that pose a threat to the proper technical operation of the Services.
                </li>

                <li>
                Transmit any worms or viruses or any code of a destructive nature.
                </li>

                <li>
                Provide any content that contains viruses, Trojan horses, worms, time bombs, cancelbots or other computer programming routines that are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information.
                </li>
              </ul>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <p>
              <b>
              3. LearnLabs’s Rights to Content You Post
              </b>
              </p>
              <p>
              You retain ownership of content you post to our platform. We’re allowed to share your content to anyone through any media, including promoting it via advertising on other websites.
              </p>
              <p>
              When you post chat messages, comments, questions, reviews, and when you submit to us ideas and suggestions for new features or improvements, you authorize LearnLabs to use and share this content with anyone, distribute it and promote it on any platform and in any media, and to make modifications or edits to it as we see fit. In legal language, by submitting or posting content on or through the platforms, you grant us a worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display, and distribute your content in any and all media or distribution methods (existing now or later developed). This includes making your content available to other companies, organizations, or individuals who partner with LearnLabs for the syndication, broadcast, distribution, or publication of content on other media. You represent and warrant that you have all the rights, power, and authority necessary to authorize us to use any content that you submit. You also agree to all such uses of your content with no compensation paid to you.
              </p>
            </div>
          </div>
            
          <div className="row">
            <div className="col-12">
              <p>
              <b>
              4. Using LearnLabs at Your Own Risk
              </b>
              </p>
              <p>
              Anyone can use LearnLabs to create and publish courses and instructors and we enable instructors and students to interact for teaching and learning. Like other platforms where people can post content and interact, some things can go wrong, and you use LearnLabs at your own risk.
              </p>
              <p>
              By using the Services, you may be exposed to content that you consider offensive, indecent, or objectionable. LearnLabs has no responsibility to keep such content from you and no liability for your access or participation in any group, to the extent permissible under applicable law. 
              </p>
              <p>
              When you interact directly with a student or a learning coach, you must be careful about the types of personal information that you share. We do not control what students and learn coaches do with the information they obtain from other users on the platform. You should not share your email or other personal information about you for your safety.
              </p>
              <p>
              We do not hire or employ learn coaches nor are we responsible or liable for any interactions involved between coaches and students. We are not liable for disputes, claims, losses, injuries, or damage of any kind that might arise out of or relate to the conduct of coaches or students.
              </p>
              <p>
              When you use our Services, you will find links to other websites that we don’t own or control. We are not responsible for the content or any other aspect of these third-party sites, including their collection of information about you. You should also read their terms and conditions and privacy policies.
              </p>
            </div>
          </div>


          <div className="row">
            <div className="col-12">
              <p>
              <b>
              5. LearnLabs’s Rights
              </b>
              </p>
              <p>
              We own the LearnLabs platform and Services, including the website, present or future apps and services, and things like our logos, API, code, and content created by our employees. You can’t tamper with those or use them without authorization.
              </p>
              <p>
              All right, title, and interest in and to the LearnLabs platform and Services, including our website, our existing or future applications, our APIs, databases, and the content our employees or partners submit or provide through our Services (but excluding content provided by instructors and students) are and will remain the exclusive property of LearnLabs and its licensors. Our platforms and services are protected by copyright, trademark, and other laws of both the United States and foreign countries. Nothing gives you a right to use the LearnLabs name or any of the LearnLabs trademarks, logos, domain names, and other distinctive brand features. Any feedback, comments, or suggestions you may provide regarding LearnLabs or the Services is entirely voluntary and we will be free to use such feedback, comments, or suggestions as we see fit and without any obligation to you.
              </p>
              <p>
              You may not do any of the following while accessing or using the LearnLabs platform and Services:
              </p>
              <ul>
                <li>
                access, tamper with, or use non-public areas of the platform, LearnLabs’s computer systems, or the technical delivery systems of LearnLabs’s service providers.
                </li>
                <li>
                disable, interfere with, or try to circumvent any of the features of the platforms related to security or probe, scan, or test the vulnerability of any of our systems.
                </li>
                <li>
                copy, modify, create a derivative work of, reverse engineer, reverse assemble, or otherwise attempt to discover any source code of or content on the LearnLabs platform or Services.
                </li>
                <li>
                access or search or attempt to access or search our platform by any means (automated or otherwise) other than through our currently available search functionalities that are provided via our website, mobile apps, or API (and only pursuant to those API terms and conditions). You may not scrape, spider, use a robot, or use other automated means of any kind to access the Services.
                </li>
                <li>
                in any way use the Services to send altered, deceptive, or false source-identifying information (such as sending email communications falsely appearing as LearnLabs); or interfere with, or disrupt, (or attempt to do so), the access of any user, host, or network, including, without limitation, sending a virus, overloading, flooding, spamming, or mail-bombing the platforms or services, or in any other manner interfering with or creating an undue burden on the Services.
                </li>
              </ul>
            </div>
          </div>

           <div className="row">
            <div className="col-12">
              <p>
              <b>
              7. Miscellaneous Legal Terms
              </b>
              </p>
              <p>
              These Terms are like any other contract, and they have boring but important legal terms that protect us from the countless things that could happen and that clarify the legal relationship between us and you.
              </p>
              <p>
              7.1 Binding Agreement
              </p>
              <p>
              You agree that by registering, accessing or using our Services, you are agreeing to enter into a legally binding contract with LearnLabs. If you do not agree to these Terms, do not register, access, or otherwise use any of our Services.
              </p>
              <p>
              If you are a learning coach accepting these Terms and using our Services on behalf of a company, organization, government, or other legal entity, you represent and warrant that you are authorized to do so.
              </p>
              <p>
              Any version of these Term in a language other than English is provided for convenience and you understand and agree that the English language will control if there is any conflict.
              </p>
              <p>
              These Terms (including any agreements and policies linked from these Terms) constitute the entire agreement between you and us.
              </p>
              <p>
              These Terms (including any agreements and policies linked from these Terms) constitute the entire agreement between you and us.
              </p>
              <p>
              If any part of these Terms is found to be invalid or unenforceable by applicable law, then that provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of these Terms will continue in effect.
              </p>
              <p>
              Even if we are delayed in exercising our rights or fail to exercise a right in one case, it doesn’t mean we waive our rights under these Terms, and we may decide to enforce them in the future. If we decide to waive any of our rights in a particular instance, it doesn’t mean we waive our rights generally or in the future.
              </p>
              <p>
              The following sections shall survive the expiration or termination of these Terms: Sections 5 (LearnLabs's Rights to Content You Post), 6 (Using LearnLabs at Your Own Risk), 7 (LearnLabs's Rights), 8 (Miscellaneous Legal Terms), and 9 (Dispute Resolution).
              </p>
              <p>
              7.2 Disclaimers
              </p>
              <p>
              It may happen that our platform is down, either for planned maintenance or because something goes down with the site. It may happen that one of our instructors is making misleading statements in their course. It may also happen that we encounter security issues. These are just examples. You accept that you will not have any recourse against us in any of these types of cases where things don’t work out right. In legal, more complete language, the Services and their content are provided on an “as is” and “as available” basis. We (and our affiliates, suppliers, partners, and agents) make no representations or warranties about the suitability, reliability, availability, timeliness, security, lack of errors, or accuracy of the Services or their content, and expressly disclaim any warranties or conditions (express or implied), including implied warranties of merchantability, fitness for a particular purpose, title, and non-infringement. We (and our affiliates, suppliers, partners, and agents) make no warranty that you will obtain specific results from use of the Services. Your use of the Services (including any content) is entirely at your own risk. Some jurisdictions don’t allow the exclusion of implied warranties, so some of the above exclusions may not apply to you.
              </p>
              <p>
              We may decide to cease making available certain features of the Services at any time and for any reason. Under no circumstances will LearnLabs or its affiliates, suppliers, partners or agents be held liable for any damages due to such interruptions or lack of availability of such features.
              </p>
              <p>
              We are not responsible for delay or failure of our performance of any of the Services caused by events beyond our reasonable control, like an act of war, hostility, or sabotage; natural disaster; electrical, internet, or telecommunication outage; or government restrictions.
              </p>
              <p>
              7.3 Limitation of Liability
              </p>
              <p>
              There are risks inherent into using our Services, for example, if you enroll in a health and wellness course like yoga, and you injure yourself. You fully accept these risks and you agree that you will have no recourse to seek damages against even if you suffer loss or damage from using our platform and Services. In legal, more complete language, to the extent permitted by law, we (and our group companies, suppliers, partners, and agents) will not be liable for any indirect, incidental, punitive, or consequential damages (including loss of data, revenue, profits, or business opportunities, or personal injury or death), whether arising in contract, warranty, tort, product liability, or otherwise, and even if we’ve been advised of the possibility of damages in advance. Our liability (and the liability of each of our group companies, suppliers, partners, and agents) to you or any third parties under any circumstance is limited to the greater of one hundred dollars ($100) or the amount you have paid us in the twelve (12) months before the event giving rise to your claims. Some jurisdictions don’t allow the exclusion or limitation of liability for consequential or incidental damages, so some of the above may not apply to you.
              </p>
              <p>
              We are not responsible for delay or failure of our performance of any of the Services caused by events beyond our reasonable control, like an act of war, hostility, or sabotage; natural disaster; electrical, internet, or telecommunication outage; or government restrictions.
              </p>
              <p>
              7.4 Indemnification
              </p>
              <p>
              If you behave in a way that gets us in legal trouble, we may exercise legal recourse against you. You agree to indemnify, defend (if we so request), and hold harmless LearnLabs, our group companies, and their officers, directors, suppliers, partners, and agents from an against any third-party claims, demands, losses, damages, or expenses (including reasonable attorney fees) arising from (a) the content you post or submit, (b) your use of the Services (c) your violation of these Terms, or (d) your violation of any rights of a third party. Your indemnification obligation will survive the termination of these Terms and your use of the Services.
              </p>
              <p>
              7.5 Governing Law and Jurisdiction
              </p>
              <p>
              These Terms are governed by the laws of the State of California, USA without reference to its choice or conflicts of law principles. Where the “Dispute Resolution” section below does not apply, you and we consent to the exclusive jurisdiction and venue of federal and state courts in San Francisco, California, USA.
              </p>
              <p>
              7.6 Legal Actions and Notices
              </p>
              <p>
              No action, regardless of form, arising out of or relating to this Agreement may be brought by either party more than one (1) year after the cause of action has accrued.
              </p>
              <p>
              Any notice or other communication to be given hereunder will be in writing and given by registered or certified mail return receipt requested, or email (by us to the email associated with your account or by you to notices@LearnLabs.com).
              </p>
              <p>
              7.7 Relationship Between Us
              </p>
              <p>
              Any notice or other communication to be given hereunder will be in writing and given by registered or certified mail return receipt requested, or email (by us to the email associated with your account or by you to notices@LearnLabs.com).
              </p>
              <p>
              7.8 No Assignment
              </p>
              <p>
              You may not assign or transfer these Terms (or the rights and licenses granted under them). For example, if you registered an account as an employee of a company, your account cannot be transferred to another employee. We may assign these Terms (or the rights and licenses granted under them) to another company or person without restriction. Nothing in these Terms confers any right, benefit, or remedy on any third-party person or entity. You agree that your account is non-transferable and that all rights to your account and other rights under these Terms terminate upon your death.
              </p>
            </div>
          </div>       

           <div className="row">
            <div className="col-12">
              <p>
              <b>
              8. Dispute Resolution
              </b>
              </p>
              <p>
              If there’s a dispute, our Support Team is happy to help resolve the issue. If that doesn’t work and you live in the United States or Canada, your options are to go to small claims court or bring a claim in binding arbitration; you may not bring that claim in another court or participate in a non-individual class action claim against us.              </p>
              <p>
              <b>This Dispute Resolution section applies only if you live in the United States or Canada. </b> Most disputes can be resolved, so before bringing a formal legal case, please first try contacting our Support Team.
              </p>
              <p>
              8.1 Small Claims
              </p>
              <p>
              Either of us can bring a claim in small claims court in (a) San Francisco, California, (b) the county where you live, or (c) another place we both agree on, as long as it qualifies to be brought in that court.
              </p>
              <p>
              8.2 Going to Arbitration
              </p>
              <p>
              If we can’t resolve our dispute amicably, you and LearnLabs agree to resolve any claims related to these Terms (or our other legal terms) through final and binding arbitration, regardless of the type of claim or legal theory. If one of us brings a claim in court that should be arbitrated and the other party refuses to arbitrate it, the other party can ask a court to force us both to go to arbitration (compel arbitration). Either of us can also ask a court to halt a court proceeding while an arbitration proceeding is ongoing.
              </p>
              <p>
              8.3 The Arbitration Process
              </p>
              <p>
              Any disputes that involve a claim of less than $10,000 USD must be resolved exclusively through binding non-appearance-based arbitration. A party electing arbitration must initiate proceedings by filing an arbitration demand with the American Arbitration Association (AAA). The arbitration proceedings shall be governed by the AAA Commercial Arbitration Rules, Consumer Due Process Protocol, and Supplementary Procedures for Resolution of Consumer-Related Disputes. You and we agree that the following rules will apply to the proceedings: (a) the arbitration will be conducted by telephone, online, or based solely on written submissions (at the choice of the party seeking relief); (b) the arbitration must not involve any personal appearance by the parties or witnesses (unless we and you agree otherwise); and (c) any judgment on the arbitrator’s rendered award may be entered in any court with competent jurisdiction. Disputes that involve a claim of more than $10,000 USD must be resolved per the AAA’s rules about whether the arbitration hearing has to be in-person.
              </p>
              <p>
              8.4 No Class Actions
              </p>
              <p>
              We both agree that we can each only bring claims against the other on an individual basis. This means: (a) neither of us can bring a claim as a plaintiff or class member in a class action, consolidated action, or representative action; (b) an arbitrator can’t combine multiple people’s claims into a single case (or preside over any consolidated, class, or representative action); and (c) an arbitrator’s decision or award in one person’s case can only impact that user, not other users, and can’t be used to decide other users’ disputes. If a court decides that this “No class actions” clause isn’t enforceable or valid, then this “Dispute Resolution” section will be null and void, but the rest of the Terms will still apply.
              </p>
              <p>
              8.5 Changes
              </p>
              <p>
              Notwithstanding the “Updating these Terms” section below, if LearnLabs changes this "Dispute Resolution" section after the date you last indicated acceptance to these Terms, you may reject any such change by providing LearnLabs written notice of such rejection by email from the email address associated with your Account to: support@LearnLabs.city, within 30 days of the date such change became effective, as indicated by the "last updated on" language above. To be effective, the notice must include your full name and clearly indicate your intent to reject changes to this "Dispute Resolution" section. By rejecting changes, you are agreeing that you will arbitrate any dispute between you and LearnLabs in accordance with the provisions of this "Dispute Resolution" section as of the date you last indicated acceptance to these Terms.
              </p>
              <p>
              10. Updating These Terms
              </p>
              <p>
              From time to time, we may update these Terms to clarify our practices or to reflect new or different practices (such as when we add new features), and LearnLabs reserves the right in its sole discretion to modify and/or make changes to these Terms at any time. If we make any material change, we will notify you using prominent means such as by email notice sent to the email address specified in your account or by posting a notice through our Services. Modifications will become effective on the day they are posted unless stated otherwise.
              </p>
              <p>
              Your continued use of our Services after changes become effective shall mean that you accept those changes. Any revised Terms shall supersede all previous Terms.
              </p>
            </div>
          </div>       

           <div className="row">
            <div className="col-12">
              <p>
              <b>
              9. How to Contact Us
              </b>
              </p>
              <p>
              If there’s a dispute, our Support Team is happy to help resolve the issue. If that doesn’t work and you live in the United States or Canada, your options are to go to small claims court or bring a claim in binding arbitration; you may not bring that claim in another court or participate in a non-individual class action claim against us.              </p>
              <p>
              The best way to get in touch with us is to contact our support@learnlabs.city. We’d love to hear your questions, concerns, and feedback about our Services.
              </p>
            </div>
          </div>       

        </div>
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(TermsOfUse);


// const styles = {
//   row: {padding: "20px", border: "1px solid #ccc"},
//   td1: {padding: "20px", fontWeight: "700", borderRight: "1px solid #ccc"},
//   td2: {padding: "20px"}
// }

