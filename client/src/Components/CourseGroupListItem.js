
import React, { Component } from 'react';
import Constants from '../Constants';
import { switchCourseAction, switchGroupAction, closeLocalListAction, closeCourseListAction } from "../Actions/actionCreator";
import { connect } from 'react-redux';
import Api from '../Api'


class CourseGroupListItem extends Component {
  constructor(props) {
    super(props);
    this.switchCourse = this.switchCourse.bind(this)
    this.switchToLocalGroup = this.switchToLocalGroup.bind(this)
    this.state = {
      group: this.props.group,
      course_info: this.props.course_info,
      courseIsHovered: false,
      courseIsSelected: false,
      type: this.props.type,
      buttonIsHovered: false,
    };
  }

  componentDidMount(){

  }

  setDivHovered = (val) =>{
    this.setState({courseIsHovered: val});
  }

  setButtonHovered = (val) => {
    this.setState({buttonIsHovered: val});
  }


  switchToLocalGroup(){
    this.props.switchGroupAction(this.state.group);
    this.props.closeLocalListAction(false);
  }
  switchCourse(){
    this.props.switchCourseAction(this.state.course_info)
    this.props.closeCourseListAction(false);
  }
  viewLocalGroup(){
    let $this = this;
    // Api.joinLocalGroup({course_id: $this.state.course_info._id, group_id: $this.state.group._id}).then((joinedGroup)=>{
    //   //update the local list 
    //   if(joinedGroup.status !== 200){
    //     return;
    //   }
    //   $this.props.switchGroupAction(joinedGroup.data);
    //   $this.props.closeLocalListAction(false);

    // });
    // console.log($this.state.course_info);
    Api.viewLocalGroup({course_id: $this.state.course_info._id, group_id: $this.state.group._id}).then((res)=>{
      if(res.status !== 200){
        return;
      }
      console.log('response', res);
      $this.props.switchGroupAction(res.data);
      $this.props.closeLocalListAction(false);
    })
  }

  render() {
    let item = (<div></div>);
    if(this.state.type === 'global'){
      item = (
        <div  onMouseEnter={this.setDivHovered.bind(this, true)} 
              onMouseLeave={this.setDivHovered.bind(this, false)}
              onClick={this.switchCourse}
              style={((this.state.courseIsHovered) ? styles.courseContainerHover : styles.courseContainer)} 
                        >
          <div>{this.state.course_info.title}</div>
        </div>
        )
    } else if(this.state.type === 'local'){
      item = (
        <div  onMouseEnter={this.setDivHovered.bind(this, true)} 
              onMouseLeave={this.setDivHovered.bind(this, false)}
              onClick={this.switchToLocalGroup}
              style={((this.state.courseIsHovered) ? styles.courseContainerHover : styles.courseContainer)} 
                        >
          <div>{this.state.group.address}</div>
          <div>{this.state.group.members.length} Members</div>
        </div>
        )
    } else if(this.state.type === 'localsearch') {
      item = (
        <div  onMouseEnter={this.setDivHovered.bind(this, true)} 
              onMouseLeave={this.setDivHovered.bind(this, false)}
              // onClick={this.joinLocalGroup}
              style={((this.state.courseIsHovered) ? styles.courseContainerHover : styles.courseContainer)} 
              className="row"
          >
          <div className="col-9">
            <div>{this.state.group.address}</div>
            <div>{this.state.group.members.length} Members</div>
          </div>
          <div className="col-3">
            <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
              onMouseLeave={this.setButtonHovered.bind(this, false)}
              onClick={this.viewLocalGroup.bind(this)}
              className="general-button color-border center-item button-max"
              // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
              type="submit"> View </button>
          </div>
        </div>
        )
    }
    return (
      <div>
        {item}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  lists_state: state.ChatReducer.lists_state
});

const mapDispatchToProps = {
  switchGroupAction,
  switchCourseAction,
  closeLocalListAction,
  closeCourseListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseGroupListItem);

         

const styles = {
  courseContainer: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_LIGHTGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
  },
  courseContainerHover: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_MEDIUMGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
  },
  courseContainerSelected: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_LIGHTESTGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
  },
  colSeparator: {
    borderLeft: "1px solid " + Constants.BRAND_WHITE,
    borderRight: "1px solid " + Constants.BRAND_WHITE
  },
  courseTitle: {
    color: Constants.BRAND_BLACK,
    fontWeight: '600',
    fontSize: "1.2em"
  },
  courseDescription: {
    color: Constants.BRAND_BLACK,
    fontWeight: '600',
  },
  locationIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_RED,
    fontSize: "1.5em"

  },
  courseLocation: {
    display: "inline-block",
    marginLeft: "5px"
  },
  membersIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_GREEN,
    fontSize: "1.5em"

  },
  courseMembersCount: {
    display: "inline-block",
    marginLeft: "5px"
  },
  statusIconOff:{
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_RED,
    fontSize: "1.5em"    
  },
  statusIconOn:{
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_GREEN,
    fontSize: "1.5em"    
  },
  courseStatus: {
    display: "inline-block",
    marginLeft: "5px"
  },
  starIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_BLACK,
    fontSize: "1.5em"

  },
  btn: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeight: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeight: '700'
  }
}

