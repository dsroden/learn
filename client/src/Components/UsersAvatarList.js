import React, { Component } from 'react';
import Api from "../Api"
import ProfilePhoto from "./ProfilePhoto"

class UsersAvatarList extends Component {
  state = {
    group: this.props.group,
    users: []
  };

  componentDidMount() {
    this.callApi()
  }

  componentWillReceiveProps(newProp){
    //called 
    this.callApi();
  }

  callApi = () => {
    Api.getGroupMembers({group: this.state.group}).then((res)=>{
      this.setState({users: res.data});
    });
  };

  render() {
    let usersAvatars = (<div></div>)
    if(this.state.users && this.state.users.length > 0){
      usersAvatars = this.state.users.map((u) =>(
        <div style={{float: "left"}} key={u._id}>
        <ProfilePhoto user={u} size="50"/>
      </div>
    ));
    }
    return (
      <div className="row">
        <div className="col-12" style={{padding: "10px"}}>
            {usersAvatars}
        </div>
      </div>
    );
  }
}


export default UsersAvatarList;


// const styles = {
// }