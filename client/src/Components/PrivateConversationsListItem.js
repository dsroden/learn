
import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import Timestamp from 'react-timestamp';
import Api from '../Api';
import {  openUserProfileViewAction, showUserProfileAction } from '../Actions/actionCreator.js'
import _ from "lodash";
import ProfilePhoto from './ProfilePhoto'

class PrivateConversationsListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
      contact: this.props.contact,
      contactIsHovered: false,
      blocked: false,
    };
  }

  componentDidMount(){
    let $this = this;
    let blocked = null;
    if($this.state.user.blocked_by){
      blocked = _.find($this.state.user.blocked_by, (u)=>{
        return u === $this.state.contact._id;
      });
    }
    if(blocked){
      $this.setState({blocked: true});
    }
    if($this.state.contact._id){
      var contact = this.state.contact
      contact.username = this.state.contact._id;
      $this.setState({contact: JSON.parse(JSON.stringify(contact))});
    }
  }

  callApi(){

  }

  showUserProfile(){

    let $this = this;
    if($this.state.blocked){
      return;
    }

    Api.fetchUserProfile($this.state.contact._id).then((response)=>{
      $this.props.openUserProfileViewAction(true)
      response.data.details = false
      $this.props.showUserProfileAction(response.data)
    });
  }


  setDivHovered = (val) =>{
    this.setState({contactIsHovered: val});
  }

  getAlphabetIndexColor(letter){
    let ALPHABET = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let index = ALPHABET.indexOf(letter);
    let color = Constants.ALPHABET_COLORS[index];
    return color;
  }

  render() {
    let item = (<div></div>);
    if(this.state.contact._id){
      item = (
          <div  onMouseEnter={this.setDivHovered.bind(this, true)} 
                onMouseLeave={this.setDivHovered.bind(this, false)}
                onClick={this.showUserProfile.bind(this)}
                style={((this.state.contactIsHovered) ? styles.courseContainerHover : styles.courseContainer)} 
                          >
            <div className="row">
            <div className="col-2">
            <ProfilePhoto user={{username: this.state.contact._id}} size="40" />
            </div>
            <div className="col-10" style={{paddingLeft: 0}}>
            <div style={{display: "inline-block", width: "100%"}}>
              <div><b>{this.state.contact._id}</b></div>
              <div style={styles.lastText} >{(this.state.contact.sender_username === this.state.user.username) ? 'You' : this.state.contact.sender_username}: {this.state.contact.text}</div>
              <Timestamp time={this.state.contact.created_at} style={{float: 'right', marginRight: '10px'}} format="time" />
            </div>
            </div>
            </div>
          </div>
      )
    }
    return (
      <div>
        {item}
      </div>
    );
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  showUserProfileAction
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateConversationsListItem);

         

const styles = {
  courseContainer: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_LIGHTGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
  },
  courseContainerHover: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_MEDIUMGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
  },
  courseContainerSelected: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_LIGHTESTGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid #eee",
    height: "auto",
    minHeight: '70px',
    padding: '15px',
  },
  colSeparator: {
    borderLeft: "1px solid " + Constants.BRAND_WHITE,
    borderRight: "1px solid " + Constants.BRAND_WHITE
  },
  courseTitle: {
    color: Constants.BRAND_BLACK,
    fontWeight: '600',
    fontSize: "1.2em"
  },
  courseDescription: {
    color: Constants.BRAND_BLACK,
    fontWeight: '600',
  },
  locationIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_YELLOW,
    fontSize: "1.5em"

  },
  courseLocation: {
    display: "inline-block",
    marginLeft: "5px"
  },
  membersIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_GREEN,
    fontSize: "1.5em"

  },
  courseMembersCount: {
    display: "inline-block",
    marginLeft: "5px"
  },
  statusIconOff:{
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_RED,
    fontSize: "1.5em"    
  },
  statusIconOn:{
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_GREEN,
    fontSize: "1.5em"    
  },
  courseStatus: {
    display: "inline-block",
    marginLeft: "5px"
  },
  starIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_BLACK,
    fontSize: "1.5em"

  },
  btn: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  lastText: {
    width: "70%",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "inline-block"
  }
}

