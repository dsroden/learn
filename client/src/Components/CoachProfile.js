import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import {openSuggestionBoxAction, updateUserAction} from "../Actions/actionCreator";
import PlaceSearch from "./PlaceSearch"
import TopicSearch from "./TopicSearch"
import CourseList from "./CourseList"
import CoachGroupList from "./CoachGroupList"
import Api from "../Api"
import CourseSuggestionBox from "./CourseSuggestionBox"
import  { o2LoadImage } from '../Utils'

class CoachProfile extends Component  {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      user: this.props.user,
      coach: this.props.user.coach,
      coachingbioValue: this.props.user.coaching_bio || '',
      coachingstatusValue: this.props.user.coaching_status || '' ,
      coachingSocialLinkValue: this.props.user.coaching_social_link || '',
      coachingPersonalLinkValue: this.props.user.coaching_personal_link || '',
      coachingProfessionalBackgroundValue: this.props.user.coaching_resume || '',
      fileName: 'No file uploaded',
      place: this.props.user.place || {place: null},
      loading: false,
      errorMsg: false,
      successMsg: false,
      skills: this.props.user.coaching_skills || [],
      currentlyEditing: 'profile_details',
      isMounted: false,
      minValue: 0,
      maxValue: 200,
      currencyValue: this.props.user.currency_value || 'usd',
      rateValue: this.props.user.coaching_rate || {min: 10, max: 35},
      rateMinValue: (this.props.user.coaching_rate) ? (this.props.user.coaching_rate.min || 0) : 0,
      rateMaxValue: (this.props.user.coaching_rate) ? (this.props.user.coaching_rate.max || 0) : 0,
      curentProgress: 0,
      progressRequirements: '',
    }
  }
  componentDidMount(){
    this.setState({isMounted: true});
    this.calculateProgress();
    let $this = this;
    let imageUrl = Constants.PROFILE_PHOTO_BASE_URL + $this.state.user.username + '.png?raw=true';

    o2LoadImage(imageUrl).then((result)=>{
      if(result.image_valid){
        $this.setState({image_valid: true, transform: result.transform})
      }
    });
  }
  componentWillUnmount(){
    console.log('unmounting coach profile');
    this.setState({isMounted: false})
  }

  calculateProgress(){
    if(this.state.coach === 'pending'){
      this.setState({currentlyEditing: 'pending'});
      return;
    }

    let total = 7;
    let listOfRequirements = [];
    
    if(!this.state.user.profile_photo){
      total = total - 1;
      listOfRequirements.push('profile photo ');
    }
    
    if(!this.state.place.address || !this.state.place.address === ''){
      total = total - 1;
      listOfRequirements.push('location ');      
    }

    if(!this.state.coachingbioValue || this.state.coachingbioValue === ''){
      total = total - 1;
      listOfRequirements.push('coach bio ');      
    }
    
    if(!this.state.coachingSocialLinkValue || this.state.coachingSocialLinkValue === ''){
      total = total - 1;
      listOfRequirements.push('social profile link ');      
    }
    
    if(!this.state.skills || this.state.skills.length < 1){

      total = total - 1;
      listOfRequirements.push('tag your skills ');      
    }
    
    if(!this.state.coachingProfessionalBackgroundValue || this.state.coachingProfessionalBackgroundValue === ''){
      total = total - 1;
      listOfRequirements.push('professional and project background ');      
    }
    
    if(!this.state.currencyValue || this.state.currencyValue === '' || !this.state.rateValue){
      total = total - 1;
      listOfRequirements.push('hourly rate ');      
    }

    this.setState({progressRequirements: listOfRequirements, currentProgress: (Math.ceil(total/7 * 100))});
  }

 componentWillReceiveProps(newProps){
    // console.log('new props in course list', newProps);
    let $this = this;
    // let user = $this.state.user;
    if(!$this.state.place || (newProps.place && newProps.place.address !== $this.state.place.address)){
      // user.place = newProps.place
      // console.log('new place', user.place);
      $this.setState({place: newProps.place});
    }
    if($this.state.currentlyEditing === 'profile_skills'){
      // user.coaching_skills = searchData.topics;
      $this.setState({skills: newProps.topics}, ()=>{
        $this.handleSubmit();
      })
    }
 }

 nav(location){
  this.props.history.push('/' + location)
 }

 updateRate(val){
  console.log('val updated', val);
  this.handleSubmit();
 }

  handleSubmit(event) {
    //prevent from reload
    if(!this.state.isMounted){
      return;
    }
    if(event){
      event.preventDefault();
    }
    let $this = this;
    let data = {};
    data.coaching_status = $this.state.coachingstatusValue;
    data.coaching_bio = $this.state.coachingbioValue;
    data.place = $this.state.place;
    data.coaching_skills = $this.state.skills;
    data.coaching_social_link = $this.state.coachingSocialLinkValue;
    data.coaching_personal_link = $this.state.coachingPersonalLinkValue;
    data.currency_value = $this.state.currencyValue;
    data.coaching_rate = {min: $this.state.rateMinValue, max: $this.state.rateMaxValue};
    data.coaching_resume = $this.state.coachingProfessionalBackgroundValue
    console.log('data.coaching_resume', data.coaching_resume);
    Api.updateUserProfile(data).then((res)=>{
      //user profile updated
      if(res.status === 200){
        console.log('res', res.data);
        $this.calculateProgress();
        $this.setState({ successMsg: 'Profile saved'}, ()=>{
          setTimeout(()=>{
            $this.setState({successMsg: false});
          }, 3000)
        })
      } else {
        $this.setState({errorMsg: 'There was an error. Please try again'}, ()=>{
          setTimeout(()=>{
            $this.setState({errorMsg: false})
          }, 3000)
        })
      }
    });

  }


  handleInputChange(event) {
    event.preventDefault();

    const target = event.target;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if(name === 'fileName'){
      console.log('this.fileinput', this.fileInput);
      // value = this.fileInput.files[0].name;
      if(this.fileInput.files[0]){
        value = this.fileInput.files[0].name;
      }
    }

    console.log('name, value', name, value)

    this.setState({
      [name]: value
    });
      // this.handleSubmit()


    if(this.fileInput && this.fileInput.files[0]){
      let data = new FormData()
      data.set('file', this.fileInput.files[0]);
      data.set('filename', this.fileInput.files[0].name);
      this.setState({loading: true},()=>{
        Api.updateUserProfilePhoto(data).then((res)=>{
          if(res.status === 200){
            let user = res.data;
            let temp_photo = user.profile_photo;
            let edited_user = JSON.parse(JSON.stringify(user));
            edited_user.profile_photo = null
            this.setState({user: edited_user, [name]: null}, ()=>{
              edited_user = JSON.parse(JSON.stringify(edited_user));
              edited_user.profile_photo = temp_photo;
              this.setState({user: edited_user, [name]: null, loading: false})
            });
          }
        });
      });
    } 
          // console.log('submit');
  }



  switchEditing(val, reset){
    if(reset){
      this.setState({currentlyEditing: 'reset'}, ()=>{
        this.setState({currentlyEditing: val})
      })
    } else {
        this.setState({currentlyEditing: val})
    }
  }

  createGroup(){
    if(!this.state.user.coach_status || this.state.user.coach_stats === 'pending'){
      return;
    }
    this.props.openSuggestionBoxAction(true)
  }


  submitApplication(){
    this.setState({coach: 'pending', currentlyEditing: 'pending'});
  }


  render() {
    let profilePhoto = (<div className="color-border" style={styles.profilePhoto}>Add photo</div>)
    if(this.state.user && this.state.user.profile_photo){
      profilePhoto = (<div style={{...styles.profilePhoto, ...{backgroundImage: 'url(' + this.state.user.profile_photo + ')', backgroundSize: 'cover', backgroundPosition: "center center", backgroundRepeat: 'no-repeat', transform: this.state.transform}}}> </div>)
      // profilePhoto = (<img src={this.state.user.profile_photo} />)
    }
    let loading = (<div></div>)
    if(this.state.loading){
      loading = (<div style={{padding: "10px", display: 'inline-block'}}>Loading...</div>)
    }

    let coachingStatus = (<div></div>)
    if(this.state.coach === 'approved'){
      coachingStatus = (
            <div className="row" style={styles.formRow}>
              <div className="col-12">        
                <label style={styles.label}>
                  Coaching status:
                  <textarea  placeholder="Give a brief description of your coaching availability (i.e Available twice a week in the evenings.)" style={styles.textarea} name="coachingstatusValue" value={this.state.coachingstatusValue} onChange={this.handleInputChange} />
                </label>
              </div>
            </div>
      )
    } else if(this.state.coach === 'pending'){
       coachingStatus = 
      (
      <div>
      <div className="row" style={{padding: "20px"}}>
        <div className="col-12" >
          <div style={{whiteSpace: "pre-line", fontWeight: "500"}}>Thank you for applying to become a LearnLabs Coach! <b> Your application is now in review. </b> This can take a couple of days. You will receive an email notifiction when the review is completed.</div>
        </div>
      </div>
      </div>
      )     
    } else {
      let coachingProgressDesc = (<div></div>)
      if(this.state.progressRequirements.length > 0){
          coachingProgressDesc = (
            <div>
              <div style={{fontWeight: "700"}}>Please complete the following:</div>
              <div>{this.state.progressRequirements.join('\r\n')}</div>
            </div>
          )
      } else {
        coachingProgressDesc = (
          <div className="row" style={styles.formRow}>
            <div className="col-12">    
              <button className="general-button color-border-green" onClick={this.submitApplication.bind(this)}>Submit for review</button>
            </div>
          </div>          
        )
      }
      coachingStatus = 
      (
      <div>
      <div className="row" style={{padding: "20px"}}>
        <div className="col-12" >
          <div style={{whiteSpace: "pre-line", fontWeight: "500"}}>{coachingProgressDesc}</div>
        </div>
      </div>
      <div className="row" style={{padding: "0px 20px 20px 20px"}}>
        <div className="col-12" style={{overflow: "hidden", position: "relative", height: "30px", width: "100%", borderRadius: "15px", border: "1px solid #ccc",}}>
          <div style={{position: "absolute", top: "0", bottom: "0", left: "0", width: this.state.currentProgress + '%', backgroundColor: Constants.BRAND_GREEN}}></div>
        </div>
      </div>
      </div>
      )
    }

    let editprofilePhoto = (<div></div>)
    editprofilePhoto = (
        <div>
        <form style={styles.formContainer}>
          <div className="row" style={styles.formRow}>
            <div className="col-12">           
              <label>
                <div style={{padding: "0px 10px 10px 0px"}}>Upload a profile image: (required)</div>
                <input 
                  style={{opacity: "0", position: "absolute", width: "300px"}}
                  type="file"
                  ref={input => {
                    this.fileInput = input;
                  }}
                  onChange={this.handleInputChange}
                  name="fileName"
                />
                {profilePhoto}
                {loading}
              </label>
            </div>
          </div>
        </form>
        </div>
    )

    let bioAndStatus = (<div></div>)
    bioAndStatus = (
        <div>
        <br/>
        <div className="row" style={styles.formContainer}>
          <div className="col-12">
            <label style={styles.label}>
              <div style={{fontWeight: "bold"}}>Add location:</div>
              <PlaceSearch color={Constants.BRAND_GREEN} address={(this.state.user.place) ? this.state.user.place.address : null}/>
            </label>
          </div>
        </div>
        <br/>
        <form onSubmit={this.handleSubmit} style={styles.formContainer}>
          <div className="row" style={styles.formRow}>
            <div className="col-12">        
              <label style={styles.label}>
                Add a coaching bio: (required)
                <textarea placeholder="Tell learners why you want to be their coach and what you can offer" style={styles.textarea} name="coachingbioValue" value={this.state.coachingbioValue} onChange={this.handleInputChange} />
              </label>
            </div>
          </div>
          <br/>
          <div className="row" style={styles.formRow}>
            <div className="col-12">        
              <label style={styles.label}>
                Add social link (Facebook, Twitter, LinkedIn, Dribble, etc): (required)
                <input  placeholder="Add a link to another social profile" style={styles.input} name="coachingSocialLinkValue" value={this.state.coachingSocialLinkValue} onChange={this.handleInputChange} />
              </label>
            </div>
          </div>
          <br/>
          <div className="row" style={styles.formRow}>
            <div className="col-12">        
              <label style={styles.label}>
                Add a website: (recommended)
                <input  placeholder="Add a link to your personal website" style={styles.input} name="coachingPersonalLinkValue" value={this.state.coachingPersonalLinkValue} onChange={this.handleInputChange} />
              </label>
            </div>
          </div>
          <br/>
          <div className="row" style={styles.formRow}>
            <div className="col-12">    
              <button className="general-button color-border-green"  type="submit">Save</button>
            </div>
          </div>
        </form>
        </div>
    )


    let skills = (<div></div>)
    let search = (<div></div>)
    if(this.state.currentlyEditing === 'profile_skills'){

      search = (<TopicSearch origin="skills" color={Constants.BRAND_GREEN} tags={this.state.user.coaching_skills}/>)

      skills = (
          <div>
            <div className="row" style={styles.formContainer}>
              <div className="col-12">
                <label style={styles.label}>
                  <div style={{fontWeight: "bold"}}>Add skill tags: (required)</div>
                </label>
                {search}
              </div>
            </div>
            <br/>
            <div className="row" style={styles.formRow}>
              <div className="col-12">        
                <label style={styles.label}>
                  List your professional and academic background. Include links to projects where available. This list will be displayed to learners after review: (required)
                </label>
                  <textarea  placeholder="List experiences and projects here" style={styles.textarea} name="coachingProfessionalBackgroundValue" value={this.state.coachingProfessionalBackgroundValue} onChange={this.handleInputChange} />
              </div>
            </div>
            <br/>
            <div className="row" style={styles.formRow}>
              <div className="col-12">    
                <button className="general-button color-border-green"  onClick={this.handleSubmit}>Save</button>
              </div>
            </div>
          </div>
      )
    }

    let content = (<div></div>)
    if(this.state.currentlyEditing === 'profile_content'){
      search = (
        <div>
        <div style={{height: "10px"}}></div>
        <PlaceSearch color={Constants.BRAND_RED}/>    
        <div style={{height: "10px"}}></div>
        <TopicSearch origin="content" color={null} tags={[]}/>
        </div>
        )
      content = (
          <div>
            <div className="row" style={styles.formContainer}>
              <CourseSuggestionBox coach={true} history={this.props.history}/>
              <div className="col-12">
                <label style={styles.label}>
                  <div style={{fontWeight: "bold"}}>Start a custom group </div>
                </label>
                <button 
                  className={("general-button color-border-green " + (!this.stateuser.coach_status || this.state.user.coach_status === 'pending') ? 'inactive-button' : '')}
                  style={{maxWidth: "200px"}}
                  onClick={this.createGroup.bind(this)}
                > Custom Group
                </button>
                <div style={{display: "inline-block", marginLeft: "10px"}}> Available after review</div>
              </div>
            </div>
            <br/>
            <div className="row" style={styles.formContainer}>
              <div className="col-12">
                <label style={styles.label}>
                  <div style={{fontWeight: "bold"}}>Groups you have joined as a coach:</div>
                </label>
                <CoachGroupList history={this.props.history} user={this.state.user} />
              </div>
            </div>

            <br/>
            <div className="row" style={styles.formContainer}>
              <div className="col-12">
                <label style={styles.label}>
                  <div style={{fontWeight: "bold"}}>Join existing groups as a coach: (recommended)</div>
                </label>
                {search}
              </div>
            </div>
            <br/>
            <CourseList coach={true} history={this.props.history}/>
          </div>
      )
    }

    let earn = (<div></div>)
    earn = (
        <div className="learnlabs-app">
        <form onSubmit={this.handleSubmit} style={styles.formContainer}>
          <div className="row">
            <div className="col-12">
              <div >
              <div>
                <label style={styles.label}>
                  <div style={{fontWeight: "bold"}}>Currency: (required)</div>
                </label>
                <div className="select-container">
                <select  value={this.state.currencyValue} name="currencyValue" onChange={this.handleInputChange}>
                  <option value="usd">USD</option>
                  <option value="euro">EURO</option>
                </select>
                <div className="select-arrow"></div>
                </div>
                </div>
                <br/>
                <label style={styles.label}>
                  <div style={{fontWeight: "bold"}}>What is your hourly rate? (required)</div>
                </label>
                <div style={{height: "15px"}}></div>
                <div className="row">
                  <div className="col-2">
                    <div style={{lineHeight: "40px", fontWeight: "500"}}>Min</div>
                  </div>
                  <div className="col-4">
                    <input 
                    style={styles.input}
                    onChange={this.handleInputChange}
                    name="rateMinValue"
                    value={this.state.rateMinValue}
                    placeholder="0"
                    />
                  </div>
                  <div className="col-2">
                    <div style={{lineHeight: "40px", fontWeight: "500"}}>Max</div>
                  </div>
                  <div className="col-4">
                    <input 
                    style={styles.input}
                    onChange={this.handleInputChange}
                    value={this.state.rateMaxValue}
                    name="rateMaxValue"
                    placeholder="0"
                    />
                  </div>
                </div>
            </div>
            </div>
          </div>
          <br/>
          <div className="row" style={styles.formRow}>
            <div className="col-12">    
              <button className="general-button color-border-green"  type="submit">Save</button>
            </div>
          </div>
        </form>
        </div>
    )


    let currentlyEditing = (<div></div>)
    switch(this.state.currentlyEditing){
      case 'profile_details': 
        currentlyEditing = (
          <div>
            {editprofilePhoto}
            {bioAndStatus}

          </div>
        )
        break;
      case 'profile_skills': 
        currentlyEditing = (
          <div>
            {skills}
          </div>
        )
        break;

      case 'profile_content':
        currentlyEditing = (
          <div>
            {content}
          </div>
        ) 
        break; 
      case 'profile_earn':
        currentlyEditing = (
          <div>
            {earn}
          </div>
        ) 
        break; 
      case 'reset':
         currentlyEditing = (
          <div>
            <div>Loading</div>
          </div>
        )   
      break;  
      case 'pending':
        currentlyEditing = (
          <div>

          </div>
        )
      break;     
      default: 
        currentlyEditing = (
          <div>

          </div>
        )
        break;
    }


    let profileBar = (<div></div>)
    if(this.state.currentlyEditing !== 'pending'){
    profileBar = (
          <div className="row" style={{padding: "10px", marginBottom: "15px"}}>
            <div className="col-12 color-border center-item " style={{border: "1px solid #ccc", overflow: "hidden", backgroundColor: "white", maxWidth: "600px", marginLeft: "auto", marginRight: "auto", display: "block", padding: "0"}}>
              <div className="row">
                <div className="col-3" style={{padding: "0px", backgroundColor: (this.state.currentlyEditing === 'profile_details') ? Constants.BRAND_GREEN : 'white', fontWeight: (this.state.currentlyEditing === 'profile_details') ? '700' : '400', color: (this.state.currentlyEditing === 'profile_details') ? 'white' : ''  }}>
                  <div className="center-item general-hover" style={{padding: "15px", textAlign: "center", borderRight: "1px solid #ccc" }} onClick={this.switchEditing.bind(this, 'profile_details')}>Details</div>
                </div>
                <div className="col-3" style={{padding: "0px", backgroundColor: (this.state.currentlyEditing === 'profile_skills') ? Constants.BRAND_GREEN : 'white', fontWeight: (this.state.currentlyEditing === 'profile_skills') ? '700' : '400', color: (this.state.currentlyEditing === 'profile_skills') ? 'white' : ''  }}>
                  <div className="center-item general-hover" style={{padding: "15px", textAlign: "center", borderRight: "1px solid #ccc" }} onClick={this.switchEditing.bind(this, 'profile_skills', true)}>Skills</div>
                </div>
                <div className="col-3" style={{padding: "0px", backgroundColor: (this.state.currentlyEditing === 'profile_content') ? Constants.BRAND_GREEN : 'white', fontWeight: (this.state.currentlyEditing === 'profile_content') ? '700' : '400', color: (this.state.currentlyEditing === 'profile_content') ? 'white' : ''  }}>
                  <div className="center-item general-hover" style={{padding: "15px", textAlign: "center", borderRight: "1px solid #ccc" }} onClick={this.switchEditing.bind(this, 'profile_content', true)}>Groups</div>
                </div>
                <div className="col-3" style={{padding: "0px", backgroundColor: (this.state.currentlyEditing === 'profile_earn') ? Constants.BRAND_GREEN : 'white', fontWeight: (this.state.currentlyEditing === 'profile_earn') ? '700' : '400', color: (this.state.currentlyEditing === 'profile_earn') ? 'white' : ''  }}>
                  <div className="center-item general-hover" style={{padding: "15px", textAlign: "center",  }} onClick={this.switchEditing.bind(this, 'profile_earn')}>Rate</div>
                </div>
              </div>
            </div>
          </div>
        )
    } else {
      profileBar = (
        <div>
        <div className="row" style={{padding: "20px"}}>
          <div className="col-12" >
            <div style={{whiteSpace: "pre-line", fontWeight: "500"}}>You can edit your coach profile anytime after the review, or if there will be a need for corrections.</div>
          </div>
        </div>
          <div className="row" style={styles.formRow}>
            <div className="col-12">    
              <button className="general-button color-border-green" onClick={this.nav.bind(this, '')}>Browse Groups</button>
            </div>
          </div>    
        </div>
      )

    }

    return (
        <div>
        <div className="container-fluid" style={styles.editProfileContainer}>
          {coachingStatus}

          {profileBar}
          <div className="row">
            <div className="col-12">
              
              {currentlyEditing}
              
              <div style={{maxWidth: "600px", marginLeft: "auto", marginRight: "auto", display: "block", marginTop: "15px", borderRadius: "15px"}} >
                <div style={{display: (this.state.errorMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',   borderRadius: "15px", backgroundColor: Constants.BRAND_RED, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.errorMsg}</div>             
                <div style={{display: (this.state.successMsg) ? 'inherit' : 'none', padding: '5px', width: '100%',  borderRadius: "15px", backgroundColor: Constants.BRAND_GREEN, color: Constants.BRAND_WHITE, textAlign: 'center'}}> {this.state.successMsg}</div>             
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
    // place: state.PlacesReducer.place,
    place: state.SearchReducer.place,
    // topic: state.TopicsReducer.topic,
    topics: state.SearchReducer.topics,

});

const mapDispatchToProps = {
  openSuggestionBoxAction,
  updateUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CoachProfile);



const styles = {
  editProfileContainer: {   
    backgroundColor: Constants.BRAND_LIGHTESTGRAY,
    boxShadow: "0 -1px 10px rgb(175, 173, 173)", //+ Constants.BRAND_DARKGRAY,
    borderTop: "1px solid " + Constants.BRAND_LIGHTGRAY,
    maxWidth: "600px",
    padding: "10px 0 20px 0",
    marginBottom: "20px",
    marginTop: "10px"
  },
  formContainer: {maxWidth: "600px", marginLeft: "auto", marginRight: "auto"},
  "profilePhoto": {width: "100px", height: "100px", borderRadius: "100%", backgroundColor: Constants.BRAND_LIGHTGRAY, textAlign: "center",
  lineHeight: "100px",
  fontSize: "12px", cursor: "pointer"},
  "header": {padding: '10px', backgroundColor: 'black', fontWeight: 'bold', fontSize: '1.1em'},
  "formRow": {padding: '0px', fontWeight: 'bold',},
  "label": {width: '100%', marginBottom: "10px"},
  "select": {width: '200px', height: '40px', marginLeft: '10px'},
  "input": {border: "1px solid #ccc", borderRadius: "5px", padding: "15px", width: '100%', height: '40px'},
  "textarea": {border: "1px solid #ccc", width: '100%', height: '70px', borderRadius: "5px", padding: "15px", minHeight: "200px"},
  "submitBtn": {width: '200px', display: 'block', marginLeft: 'auto', marginRight: 'auto'}
}
