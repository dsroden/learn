import React, { Component } from 'react';
import Constants from '../Constants';
import axios from 'axios';
import { connect } from 'react-redux';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import Autosuggest from 'react-autosuggest';
import { courseSearchDoneAction } from "../Actions/actionCreator";

import O2PlaceAutocomplete from "./O2PlaceAutocomplete";
import O2TopicAutocomplete from "./O2TopicAutocomplete";

const FA = require('react-fontawesome')

let topics = [
];

function getMatchingTopics(value) {
  const escapedValue = escapeRegexCharacters(value.trim());
  
  if (escapedValue === '') {
    return [];
  }
  
  const regex = new RegExp('^' + escapedValue, 'i');

  return topics.filter(topic => regex.test(topic.name));
}

/* ----------- */
/*    Utils    */
/* ----------- */

// https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

/* --------------- */
/*    Component    */
/* --------------- */

function getSuggestionValue(suggestion) {
  return suggestion.name;
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion.name}</span>
  );
}


class Search extends Component {
  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {topic: '', address: '', location: null , buttonIsHovered: false, value: '', suggestions: [], isLoading: false};
  }

  handleSubmit(event) {
    //prevent from reload
    event.preventDefault();
    //create form data 
    let $this = this;
    let data = new FormData();
    // console.log($this.state.value, $this.state.location, $this.state.address);
    let value = $this.state.value;
    data.append('topic', value);
    data.append('location', $this.state.location);
    data.append('address', $this.state.address);
    axios.post(Constants.API_URL + '/search', data).then((res)=>{
      let courses = res.data.courses;
      $this.props.courseSearchDoneAction(courses);

    }).catch((err)=>{
      console.log('error creating example', err);
    })

  }

  setButtonHovered = (val) => {
    this.setState({buttonIsHovered: val});
  }

  renderFooter = () => (
    <div className="dropdown-footer">
      <div>
        <img style={{width: '150px', float: 'right'}} alt="google-logo" src={require('../assets/powered_by_google_on_white_hdpi.png')} />
      </div>
    </div>
  )

  locationSelection = (address) =>{
    let $this = this;
    $this.setState({address}, ()=>{
     geocodeByAddress($this.state.address)
      .then(results => getLatLng(results[0]))
      .then(latLng => console.log('Success', this.setState({location: JSON.stringify(latLng)}, ()=>{console.log(this.state.address, this.state.location)})))
      .catch(error => console.error('Error', error))
    })
  }


  
  loadSuggestions(value) {
    // Cancel the previous request
    if (this.lastRequestId !== null) {
      clearTimeout(this.lastRequestId);
    }
    
    this.setState({
      isLoading: true
    });
    
    // Fake request
    axios.get(Constants.API_URL + '/search/topics/?q=' + this.state.value ).then((res)=>{
      // console.log('res', res);
      topics = res.data;
      this.setState({
        isLoading: false,
        suggestions: getMatchingTopics(value)
      });
    }).catch((err)=>{
      console.log('error creating example', err);
    })
  }

  onTopicChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };
    
  onSuggestionsFetchRequested = ({ value }) => {
    this.loadSuggestions(value);
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };


  render() {
    const inputProps = {
      value: this.state.address,
      onChange: this.locationSelection,
      placeholder: 'Search Places...',
    }
    const autocompletePlacesStyle = {
      root: {
            display: 'inline-block',
            "width": "90%",
            "height": "50px",
            marginTop: "10px",
            marginBottom: "10px",
            border: 'none',
            borderBottom: '2px solid ' + Constants.BRAND_YELLOW,
            zIndex: '9'

      },
      input: { width: '100%', border: 'none' },
      autocompleteContainer: { backgroundColor: Constants.BRAND_LIGHTGRAY },
      autocompleteItem: { color: 'black' },
      autocompleteItemActive: { color: Constants.BRAND_GREEN },
    }
  

    // let autocompletePlaces = (
    //       <div className="col-12 col-md-5">
    //         <FA style={styles.inputIcon} name="map-marker" />
    //         <PlacesAutocomplete  styles={autocompletePlacesStyle} inputProps={inputProps} renderFooter={this.renderFooter} />
    //       </div>
    // )


    const { value, suggestions } = this.state;
    const topicsProps = {
      placeholder: "Search a topic (i.e Coding, Cooking)",
      value,
      onChange: this.onTopicChange
    };

    // let autocompleteTopic = (
    //   <div className="col-12 col-md-5">
    //     <FA style={styles.inputIcon} name="search" />
    //     <Autosuggest 
    //       suggestions={suggestions}
    //       onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
    //       onSuggestionsClearRequested={this.onSuggestionsClearRequested}
    //       getSuggestionValue={getSuggestionValue}
    //       renderSuggestion={renderSuggestion}
    //       inputProps={topicsProps} />
    //   </div>
    // )


    let autocompletePlaces = (
      <div className="col-12 col-md-5">
        <O2PlaceAutocomplete />
      </div>
      )

    let autocompleteTopics = (
      <div className="col-12 col-md-5">
        <O2TopicAutocomplete />
      </div>
    )

    return (
        <div className="container-fluid" style={styles.searchContainer}>
            <div>
              <form onSubmit={this.handleSubmit}>
                <div className="row">
                  {autocompleteTopics}
                  {autocompletePlaces}
                  <div className="col-12 col-md-2">    
                    <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
                            onMouseLeave={this.setButtonHovered.bind(this, false)}
                            style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} type="submit">Search</button>
                  </div>
                </div>
              </form>
              <div className="row">
                
              </div>
            </div>
        </div>
    );
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  courseSearchDoneAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);




const styles = {
  searchContainer: {
    "position": "relative",
    "width": "100%",
    "backgroundColor": Constants.BRAND_WHITE,
    "borderRadius": "5px",
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  inputIcon: {
    width: "10%",
    textAlign: 'center',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_YELLOW,
    fontSize: "1.8em"

  },
  input: {
    "width": "90%",
    "height": "50px",
    marginTop: "10px",
    marginBottom: "10px",
    border: 'none',
    borderBottom: '2px solid ' + Constants.BRAND_YELLOW
  },
  btn: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_BLACK,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}


