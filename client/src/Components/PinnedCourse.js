import React, {Component} from 'react'
import { connect } from 'react-redux';
import Api from "../Api"
import CourseItem from "./CourseItem"

class PinnedCourse extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      course: false,
      title: this.props.title
    }
  }

  componentDidMount(){
    console.log('component mounted');
    Api.fetchCourseByTitle({course_title: this.state.title}).then((res)=>{
      console.log('res', res);
      if(res.status === 200){
        console.log('response', res);
        this.setState({course: res.data})
      }
    });
  }

  render(){
    let course = (<div></div>)
    if(this.state.course){
      course = (<CourseItem coach={this.state.coach} history={this.props.history} course={this.state.course} key={this.state.course._id}/>)
    }
    return (
      <div >
        {course}
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(PinnedCourse);

