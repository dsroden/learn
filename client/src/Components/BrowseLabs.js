import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import CourseList from './CourseList'
import PlaceSearch from './PlaceSearch.js'
import TopicSearch from './TopicSearch.js'
import PinnedCourse from './PinnedCourse.js'

class BrowseLabs extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      pinned_title: false,
    }
  }

  componentDidMount(){
    // this.setState({pinned_title: 'Udemy'})
  }

  render(){

    var pinned = (<div></div>)
    if(this.state.pinned_title){
      pinned = (<PinnedCourse history={this.props.history} title={this.state.pinned_title}/>)
    }
    return (
      <div>
        <div style={{maxWidth: "900px",
              display: "block",
              marginLeft: "auto",
              marginRight: "auto"}}>
        {pinned}
        </div>
        <div className="row" style={styles.descriptionContainer}>
          <div className="col-12">
            <div>Group channels by online or offline classes and locations</div>
          </div>
        </div>

        <div style={{padding: "0px 10px 0px 10px"}}>
          <PlaceSearch color={Constants.BRAND_RED}/>    
        </div>     
        <div style={{height: "10px"}}></div>
        <div style={{padding: "0px 10px 0px 10px"}}>
          <TopicSearch />
        </div>
        <div style={{height: "10px"}}></div>
        <CourseList history={this.props.history}/>
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  
};

export default connect(mapStateToProps, mapDispatchToProps)(BrowseLabs);


const styles = {
  descriptionContainer: {
    maxWidth: "900px",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    borderTopRightRadius: "10px",
    borderTopLeftRadius: "10px",
    overflow: "hidden",
    marginTop: "10px",
    marginBottom: "10px",
    textAlign: "center",
    fontWeight: "500"
  },
}
