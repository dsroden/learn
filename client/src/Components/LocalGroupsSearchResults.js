import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants";
import Api from "../Api"
import CourseGroupListItem from "./CourseGroupListItem"
import {  switchGroupAction, closeLocalListAction } from "../Actions/actionCreator";


class LocalGroupsSearchResults extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      course_info: this.props.course_info,
      place: null,
      local_groups: null,
      buttonIsHovered: false
    }
  }

  componentWillReceiveProps(newProps){
    if(newProps.place && newProps.place !== this.state.place){
      this.setState({place: newProps.place}, ()=>{
        this.callApi();
      });
    }
  }

  callApi(data){
    let $this = this;
    let place = $this.state.place;
    Api.findLocalGroupsForCourseBasedOnLocation({course_id: this.state.course_info._id, place: place}).then((local_groups)=>{
      $this.setState({local_groups: local_groups.data})
    });
  }

  setButtonHovered = (val) => {
    this.setState({buttonIsHovered: val});
  }

  startLocalGroup = () =>{
    let $this = this;
    Api.createLocalGroupForCourse({course: $this.state.course_info, place: $this.state.place}).then((new_group)=>{
      if(!new_group.data){
        return;
      }
      $this.props.switchGroupAction(new_group.data);
      $this.props.closeLocalListAction(false);
      $this.setState({local_groups: null})
    });

    // Api.viewGroup({course: $this.state.course_info, place: $this.state.place}).then((results)=>{
    //   console.log('view group', results);
    //   if(results.status === 200){
    //   $this.props.switchGroupAction(results.data.course);
    //   $this.props.closeLocalListAction(false);
    //   $this.setState({local_groups: null})        
    //   }
    // });
  }

  render(){
    let searchResults = (<div></div>)

    if(this.state.local_groups && this.state.local_groups.length === 0){
      searchResults = (
        <div style={styles.createGroupContainer}>
        <div className="row">
          <div className="col-12">
            <div style={{padding: "10px"}}>{this.state.course_info.title} does not have any local groups near </div>
            <div style={{padding: "10px"}}> {this.state.place.address} </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
              onMouseLeave={this.setButtonHovered.bind(this, false)}
              onClick={this.startLocalGroup.bind(this)}
              // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
              className="general-button color-border center-item button-max"
              type="submit"
              > Start the group! </button>
          </div>
        </div>
        </div>
      )
    }
    if(this.state.local_groups && this.state.local_groups.length > 0){
      searchResults = this.state.local_groups.map((group) =>(
        <CourseGroupListItem course_info={this.state.course_info} type="localsearch" group={group} key={group._id}/>
      ));
    }

    return (
      <div>
        {searchResults}
      </div>
    )
  }

}


const mapStateToProps = state => ({
    place: state.SearchReducer.place
});

const mapDispatchToProps = {
    switchGroupAction,
    closeLocalListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(LocalGroupsSearchResults);


const styles = {
  createGroupContainer: {
    padding: "20px",
    textAlign: "center"
  },
  btn: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}

