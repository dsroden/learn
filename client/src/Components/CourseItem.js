import React, {Component} from 'react'
import Constants from "../Constants"
import Tag from "./Tag"
import { accountStateUpdatedAction, coachGroupAddedAction } from "../Actions/actionCreator";
import { connect } from 'react-redux';
// import axios from 'axios';
import Api from "../Api";
import FA from '@fortawesome/react-fontawesome';
import { faUsers, faGlobe, faMapMarker } from '@fortawesome/fontawesome-free-solid';
import RecentActivityPreview from './RecentActivityPreview';

class CourseItem extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      course: this.props.course,
      courseIsHovered: false,
      courseIsSelected: false,
      buttonIsHovered: false,
      place: this.props.place,
      showRecentActivity: false,
      coach: this.props.coach,
      coach_confirmed: false,
    }
  }

  setDivHovered = (val) =>{
    this.setState({courseIsHovered: val});
  }

  toggleCourseSelected = () =>{
    let courseState = this.state.courseIsSelected;
    let newState = (courseState) ? false : true;
    this.setState({courseIsSelected: newState})
  }

  setButtonHovered = (val) =>{
    this.setState({buttonIsHovered: val});
  }

  // joinCourse = (e) =>{
  //   let accessToken = window.localStorage.getItem('access_token');

  //   if(!accessToken){
  //     //user not logged in, show login modal
  //     this.props.accountStateUpdatedAction('not_logged_in')
  //     return;
  //   }

  //   e.stopPropagation();

  //   // if user logged in:
  //   // add the user as member to the local group if one exists
  //   // or create a local group around the user's location
  //   // same for global group - add the user as a member or create the group. 
  //   if(this.state.place){
  //     // this.state.course.place = this.state.place;
  //     // console.log('place', this.state.place);
  //     let course = this.state.course;
  //     course.place = this.state.place
  //     this.setState({"course": course}, ()=>{
  //       this.join(accessToken, this.state.course);
  //     });
  //   } else {
  //     this.join(accessToken, this.state.course);
  //   }

  // }

  // nav = (course) =>{
  //   // console.log(course);
  //   if(this.state.place){
  //     this.props.history.push('/chat/?course=' + encodeURIComponent(course.title) + '&place=' + this.state.place.address + '&lng=' + this.state.place.location.lng + '&lat=' + this.state.place.location.lat);
  //   } else {
  //     this.props.history.push('/chat/?course=' + encodeURIComponent(course.title));      
  //   }
  // }

  // join = (access_token, course) => {
  //   // console.log('course', course);
  //   let $this = this;
  //   let data = {course: course};
  //   Api.joinGroup(data).then((results)=>{
  //     if(results.status === 200){
  //       if($this.state.coach){
  //         // console.log('res data', results.data);
  //         Api.coachJoinGroup(data).then(coachResults =>{
  //           if(coachResults.status === 200){
  //             $this.setState({coach_confirmed: true}, ()=>{
  //               $this.props.coachGroupAddedAction(coachResults.data)
  //             });
  //           }
  //         })
  //       } else {
  //         $this.nav(course);
  //       }
  //     } else {
  //       window.localStorage.removeItem('access_token');
  //       $this.joinCourse()       
  //     }
  //   });

  // };

 joinCourse = (e) =>{
    let accessToken = window.localStorage.getItem('access_token');

    // if(!accessToken){
    //   //user not logged in, show login modal
    //   this.props.accountStateUpdatedAction('not_logged_in')
    //   return;
    // }

    // e.stopPropagation();

    // if user logged in:
    // add the user as member to the local group if one exists
    // or create a local group around the user's location
    // same for global group - add the user as a member or create the group. 
    var course = null;
    course  = this.state.course;

    if(this.state.place){
      // this.state.course.place = this.state.place;
      // console.log('place', this.state.place);
      course = this.state.course;
      course.place = this.state.place
      this.setState({"course": course}, ()=>{
        this.join(accessToken, this.state.course);
      });
    } else {
      this.join(accessToken, this.state.course);
    }

  }

  nav = (course) =>{
    // console.log(category, this.state.place);
    if(this.state.place){
      this.props.history.push('/chat/?course=' + encodeURIComponent(course.title) + '&place=' + this.state.place.address + '&lng=' + this.state.place.location.lng + '&lat=' + this.state.place.location.lat);
    } else {
      this.props.history.push('/chat/?course=' + encodeURIComponent(course.title));      
    }
  }

  join = (access_token, course) => {
    console.log('course', course);
    let $this = this;
    let data = {course: course};
    Api.viewGroup(data).then((results)=>{
      // console.log('view group', results);
      if(results.status === 200){
        if($this.state.coach){
          // console.log('res data', results.data);
          Api.coachJoinGroup(data).then(coachResults =>{
            if(coachResults.status === 200){
              $this.setState({coach_confirmed: true}, ()=>{
                $this.props.coachGroupAddedAction(coachResults.data)
              });
            }
          })
        } else {
          $this.nav(course);
        }
      } else {
        window.localStorage.removeItem('access_token');
        $this.joinCourse()       
      }
    });


  };

  recentActivity(e){
    // console.log('recent activity')
    e.stopPropagation();
    let $this = this;
    let show = (this.state.showRecentActivity) ? false : true;
    $this.setState({showRecentActivity: show})
  }

  openUrl(url){
    // console.log('url', url);
    window.open(url, '_blank');
  }

  render(){
    let course = this.state.course;
    let tags = (<div></div>)

    if(course.tags.length > 0){
      tags = course.tags.map((tag, index) =>(
        <Tag tag={tag} key={index}/>
      ))
    }

    let global_members = (course.global_group_views) ? course.global_group_views + ' Total Views': 'Start a group';
    let local_members = (course.local_group_views) ? course.local_group_views  + ' Views in ' + this.state.place.address : 'Start a local group';
    let local_address = (course.local_group) ? this.state.place.address : null;

    // console.log('course', course);
    let membersInfo = (
       <div className="row">
          <div className="col-12 col-md-4">
            <FA style={styles.globalIcon} icon={faGlobe} />
            <div style={styles.courseLocation}>{global_members} </div>
          </div>
          <div className="col-12 col-md-4">
          </div>
          <div className="col-12 col-md-4">        
          </div>
        </div>
    );

    if(this.state.place){
      membersInfo = (
         <div className="row">
            <div className="col-12 col-md-4">
              <FA style={styles.globalIcon} icon={faGlobe} />
              <div style={styles.courseLocation}>{global_members}</div>
            </div>
            <div className="col-12 col-md-4">
              <FA style={styles.locationIcon} icon={faMapMarker} />
              <div style={styles.courseLocation}>{this.state.place.address}</div>          
            </div>
            <div className="col-12 col-md-4">
              <FA style={styles.membersIcon} icon={faUsers} />
              <div style={styles.courseMembersCount}>{local_members}</div>           
            </div>
          </div>      

      );
    }

    if(local_address){
      membersInfo = (
        <div className="row">
          <div className="col-12 col-md-4">
            <FA style={styles.globalIcon} icon={faGlobe} />
            <div style={styles.courseLocation}>{global_members}</div>
          </div>
          <div className="col-12 col-md-4">
            <FA style={styles.locationIcon} icon={faMapMarker}/>
            <div style={styles.courseLocation}>{local_address}</div>
          </div>
          <div className="col-12 col-md-4">
            <FA style={styles.membersIcon} icon={faUsers} />
            <div style={styles.courseMembersCount}>{local_members}</div>
          </div>
        </div>
      )
    }

    let url = encodeURI(course.url);
    // console.log('url', url);
    let activityPreview = (<div></div>)
    if(this.state.showRecentActivity){
      activityPreview = (
        <RecentActivityPreview course={this.state.course} place={this.state.place} /> 
      )
    } 

    let coachJoinConfirmation = (<div></div>)
    if(this.state.coach_confirmed){

      coachJoinConfirmation = (
        <div className="row">
          <div className="col-12">
            <div style={{marginTop: "15px", padding: "10px", borderRadius: "5px", backgroundColor: Constants.BRAND_GREEN, color: "white", fontWeight: "700"}}>You have joined the group. When your coaching status has been approved you'll be listed in the group as a coach.</div>
          </div>
        </div>
      )


    } 

    let joinButton = (<div></div>)
    if(this.state.coach ) {
      joinButton = (
        <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
                onMouseLeave={this.setButtonHovered.bind(this, false)}
                // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
                onClick={this.joinCourse.bind(this)}
                className="general-button color-border"
                style={{maxWidth: "100px", backgroundColor: (this.state.coach_confirmed) ? Constants.BRAND_GREEN : 'white', color: (this.state.coach_confirmed) ? 'white' : '', border: "1px solid " + ((this.state.coach_confirmed) ? Constants.BRAND_GREEN : Constants.BRAND_RED)}}
                type="submit">Coach</button>
      )
    } else {
      joinButton = (
        <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
                onMouseLeave={this.setButtonHovered.bind(this, false)}
                // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
                onClick={this.joinCourse.bind(this)}
                className="general-button color-border waves-effect waves-light"
                style={{maxWidth: "100px"}}
                type="submit">View</button>
      )
    }

    return (
      <div className={"gray-white-gradient waves-effect waves-light " + ((this.state.courseIsSelected) ? 'list-item-selected' : '')} 
            // onMouseEnter={this.setDivHovered.bind(this, true)} 
            // onMouseLeave={this.setDivHovered.bind(this, false)}
            onClick={this.toggleCourseSelected.bind(this)}
            style={(((this.state.courseIsSelected) ? styles.courseContainerSelected : ((this.state.courseIsHovered) ? styles.courseContainerHover : styles.courseContainer)))} 
      >
        <div style={{display: ((this.state.courseIsSelected) ? '' : 'none')}}>
          <div className="row">
            <div className="col-12">
              {joinButton}
            </div>
            {coachJoinConfirmation}
            {activityPreview}
            <div style={{paddingLeft: "20px", marginTop: "15px", marginBottom: "15px", color: Constants.BRAND_BLUE}}><a onClick={this.openUrl.bind(this, url)} target="_blank">{course.url}</a></div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div style={styles.courseTitle}>{course.title}</div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div style={(!this.state.courseIsSelected) ? styles.courseDescription : styles.courseDescriptionOpened}>{course.description}</div>
          </div>
        </div>
        <div className="row">
          <div className="col-12" style={{marginTop: "15px"}}>
            {tags}
          </div>
        </div>   
        <br/>
        {membersInfo}
      </div>
    )
  }

}



const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  accountStateUpdatedAction,
  coachGroupAddedAction
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseItem);


const styles = {
  courseContainer: {
    cursor: "pointer",
    // backgroundColor: Constants.BRAND_LIGHTGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid " + Constants.BRAND_LIGHTGRAY,
    height: "auto",
    minHeight: '70px',
    padding: '15px',
    width: "100%"
  },
  courseContainerHover: {
    cursor: "pointer",
    backgroundColor: Constants.BRAND_MEDIUMGRAY,
    boxShadow: "inset 0 1px 0 #fff",
    borderTop: "1px solid " + Constants.BRAND_LIGHTGRAY,
    height: "auto",
    minHeight: '70px',
    padding: '15px',
    width: "100%"

  },
  courseContainerSelected: {
    marginTop: "5px",
    marginBottom: "5px",
    cursor: "pointer",
    backgroundColor: Constants.BRAND_LIGHTESTGRAY + ' !important',
    boxShadow: "0 -1px 10px rgb(175, 173, 173)", //+ Constants.BRAND_DARKGRAY,
    borderTop: "1px solid " + Constants.BRAND_LIGHTGRAY,
    // borderBottom: "1px solid " + Constants.BRAND_DARKGRAY,
    height: "auto",
    minHeight: '70px',
    padding: '15px',
    width: "100%"

  },
  colSeparator: {
    borderLeft: "1px solid " + Constants.BRAND_WHITE,
    borderRight: "1px solid " + Constants.BRAND_WHITE
  },
  courseTitle: {
    color: Constants.BRAND_BLACK,
    fontWeight: '600',
    fontSize: "1.2em"
  },
  courseDescriptionOpened: {
    color: Constants.BRAND_BLACK,
    fontWeight: '500',
    marginTop: "10px"    
  },
  courseDescription: {
    color: Constants.BRAND_BLACK,
    fontWeight: '500',
    maxHeight: "80px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    marginTop: "10px"
  },
  globalIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    // marginBottom: "10px",
    color: Constants.BRAND_BLUE,
    fontSize: "1.5em"

  },
    locationIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    // marginBottom: "10px",
    color: Constants.BRAND_YELLOW,
    fontSize: "1.5em"

  },
  courseLocation: {
    display: "inline-block",
    marginLeft: "5px"
  },
  membersIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    // marginBottom: "10px",
    color: Constants.BRAND_GREEN,
    fontSize: "1.5em"

  },
  courseMembersCount: {
    display: "inline-block",
    marginLeft: "5px"
  },
  statusIconOff:{
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    // marginBottom: "10px",
    color: Constants.BRAND_RED,
    fontSize: "1.5em"    
  },
  statusIconOn:{
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    // marginBottom: "10px",
    color: Constants.BRAND_GREEN,
    fontSize: "1.5em"    
  },
  courseStatus: {
    display: "inline-block",
    marginLeft: "5px"
  },
  starIcon: {
    textAlign: 'left',
    display: 'inline-block',
    marginTop: "10px",
    marginBottom: "10px",
    color: Constants.BRAND_BLACK,
    fontSize: "1.5em"

  },
  btn: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_GREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  },
  btnHover: {
    height: "50px",
    width: "100%",
    maxWidth: "200px",
    marginTop: "10px",
    marginBottom: "10px",
    backgroundColor: Constants.BRAND_DARKGREEN,
    borderRadius: '5px',
    cursor: "pointer",
    color: Constants.BRAND_WHITE,
    fontWeigt: '700'
  }
}

//               <button onMouseEnter={this.setButtonHovered.bind(this, true)} 
//                       onMouseLeave={this.setButtonHovered.bind(this, false)}
//                       // style={((this.state.buttonIsHovered) ? styles.btnHover : styles.btn)} 
//                       onClick={this.recentActivity.bind(this)}
//                       className="general-button color-border waves-effect waves-light"
//                       style={{maxWidth: "100px", marginLeft: "10px"}}
//                       type="submit">Activity</button>