
import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import Api from '../Api';
import FA from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';
import {   toggleMeetingsListAction  } from "../Actions/actionCreator";
import MeetingsListItem from "./MeetingsListItem"

class MeetingsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      group: this.props.group,
      listState: 'people-list-close',
      meetings: [],
      user: this.props.user
    };
  }

  componentWillReceiveProps(newProps){

    if(newProps.list_state === 'open'){
      this.setState({listState: 'people-list-open'})
      this.callApi();
      this.props.toggleMeetingsListAction('reset');
    }

  }

  componentDidMount(){
    this.callApi();
  }

  callApi(){
    let $this = this;
    Api.fetchGroupMeetings({group_id: $this.state.group._id}).then((response)=>{
      $this.setState({meetings: []}, ()=>{
        $this.setState({meetings: response.data})
      });
    });
  }

  closeList(){
    this.setState({listState: 'people-list-close'});
    // this.props.closePrivateConversationsListAction(false);
  }

  render() {

    let meetings = (<div></div>)
    if(this.state.meetings && this.state.meetings.length > 0){
      meetings = this.state.meetings.map((meeting) =>(
        <MeetingsListItem user={this.state.user} meeting={meeting} key={meeting._id}/>
      ));
    } else {
      meetings = (<div className="row" style={{margin: "0", borderTop: '1px solid #ffffff42', color: "white", backgroundColor: Constants.BRAND_RED, }}>
          <div className="col-12">
            <div>No upcoming meetings</div>
          </div>
          </div>
      )
    }

    return (
      <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{margin: "0", borderTop: '1px solid #ffffff42', color: "white", backgroundColor: Constants.BRAND_RED, }}>
          <div className="col-12">
          <div onClick={this.closeList.bind(this)} style={{textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
            Meetings
            <FA icon={faTimes} style={{float: "left", marginLeft: "10px"}} />
          </div>
          </div>
        </div>
        <div style={styles.innerSideMenuContents}>
        {meetings}
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  list_state: state.MeetingsReducer.meetings_list
});

const mapDispatchToProps = {
  toggleMeetingsListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetingsList);


const styles = {
    listContainer: {
     top: "60px",
      width: "100%",
      maxWidth: "400px",
      position: "fixed",
      // height: window.innerHeight - 40 + "px",
      // overflow: "auto",
      right:  "-415px",
      // paddingBottom: "40px",
      background: Constants.BRAND_LIGHTGRAY,
      zIndex: "2147483647",
      boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
    },
    innerSideMenuContents: {
    height: window.innerHeight - 100 + "px",
    overflowY: "scroll",
    paddingBottom: "40px"
  }
}
