import React, { Component } from 'react';
import Constants from '../Constants';
import { connect } from 'react-redux';
import CourseGroupListItem from './CourseGroupListItem';
import Api from '../Api';
import FA from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';
import { closeLocalListAction } from "../Actions/actionCreator";
import PlaceSearch from "./PlaceSearch";
import LocalGroupsSearchResults from "./LocalGroupsSearchResults";

class LocalGroupsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course_info: this.props.course_info,
      local_groups: [],
      user: this.props.user,
      listState: 'local-list-close'
    };
  }

  componentWillReceiveProps(newProps){
    let listState = '';
    if(newProps.local_list_state){
      listState =  'local-list-open';
    } else {
       listState = 'local-list-close'
    }
    this.setState({listState: listState});
    if(listState === 'local-list-open'){
      if(this.state.user){
      let access_token = window.localStorage.getItem('access_token');
      let data = {access_token: access_token, user_id: this.state.user._id, location: this.state.user.location, course_id: this.state.course_info._id}
      this.callApi(data);
      }
    }
  }

  componentDidMount(){
    //get the local groups of this course of which the user is a member
    let access_token = window.localStorage.getItem('access_token');
    // if(!access_token){

    // }
    // if(!this.state.user.location){
    //   //if the user has not added a location give option to ad a location or search by a locatio near them

    // }
    if(this.state.user){
    let data = {access_token: access_token, user_id:  this.state.user._id, location: this.state.user.location, course_id: this.state.course_info._id}
    this.callApi(data);
    }
  }

  callApi = (data) => {
    let $this = this;
    Api.loadCourseLocalGroupsMember(data).then((local_groups)=>{
      // $this.setState({local_groups: []}, ()=>{
        $this.setState({local_groups: local_groups.data})
      // });
    });

  };

  closeList(){
    // this.props.coursesToggleAction(null);
    this.props.closeLocalListAction(false);
  }

  render() {
      let groups = (<div className="row">
          <div className="col-12"> 
          <div style={{ padding: "10px"}}>You have not joined any local chats groups for this group yet</div>
          </div>
        </div>)
      if(this.state.local_groups && this.state.local_groups.length > 0){
        groups = this.state.local_groups.map((group) =>(
          <CourseGroupListItem course_info={this.state.course_info} type="local" group={group} key={group._id}/>
        ));
      }
    return (
        <div style={styles.listContainer} className={this.state.listState}>
        <div className="row" style={{margin: "0", borderTop: '1px solid #ffffff42', color: "white", backgroundColor: Constants.BRAND_RED}}>
            <div className="col-12">
            <div onClick={this.closeList.bind(this)} style={{textAlign: 'center', padding: "20px", width: "100%", display: "block", marginLeft: "auto", marginRight: "auto", fontWeight: "bold", cursor: "pointer"}}>
              Your local chats
              <FA icon={faTimes} style={{float: "left", marginLeft: "10px"}} />
            </div>
            </div>
          </div>
          <div style={styles.innerSideMenuContents}>
          {groups}
          <div className="row">
            <div className="col-12">
              <div style={{padding: "10px", textAlign: 'center',boxShadow: 'rgb(255, 255, 255) 0px 1px 0px inset', borderTop: '1px solid rgb(238, 238, 238)'}}> Find a local chat group for <b> {this.state.course_info.title} </b></div>
            </div>
          </div>
          <PlaceSearch />
          <LocalGroupsSearchResults course_info={this.state.course_info} />
          </div>        
        </div>
    );
  }
}


const mapStateToProps = state => ({
    global_local_toggle: state.ChatReducer.global_local_toggle,
    local_list_state: state.ChatReducer.local_list_state,

});

const mapDispatchToProps = {
  closeLocalListAction
};

export default connect(mapStateToProps, mapDispatchToProps)(LocalGroupsList);


const styles = {
    listContainer: {
      top: "60px",
      width: "100%",
      maxWidth: "400px",
      position: "fixed",
      // height: window.innerHeight - 40 + "px",
      // overflow: "auto",
      right:  "-415px",
      paddingBottom: "40px",
      background: Constants.BRAND_LIGHTGRAY,
      zIndex: "2147483647",
      boxShadow: '2px 2px 4px ' + Constants.BRAND_DARKGRAY
    },
    innerSideMenuContents: {
    height: window.innerHeight - 100 + "px",
    overflowY: "scroll",
    paddingBottom: "40px"
  }
}


