import React, {Component} from 'react'
import { connect } from 'react-redux';
import ChatViewInfoBar from './ChatViewInfoBar'
import ChatViewContents from './ChatViewContents'
import ChatViewInput from './ChatViewInput'

class ChatView extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      course_info: this.props.course_info,
      current_group: this.props.current_group,
      chat_state: this.props.chat_state
    }
  }

  componentDidMount(){
  }

  componentWillReceiveProps(newProps){

  }

  render(){

    return (
      <div >
        <ChatViewInfoBar 
          courseinfo={this.state.course_info} 
          current_group={this.state.current_group} 
          user={this.state.user}
          chat_state={this.state.chat_state}
        />
        <ChatViewContents user={this.state.user} channel={this.state.current_group} style={{height: "600px"}}/>
        <ChatViewInput user={this.state.user} />
      </div>
    )
  }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatView);


