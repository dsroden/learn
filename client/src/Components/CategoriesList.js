import React, { Component } from 'react';
import { connect } from 'react-redux';
import { topicsUpdatedAction} from "../Actions/actionCreator";
import Api from "../Api";
// import Infinite from 'react-infinite'
// import loadingGif from "../assets/loading.gif"
import CategoriesListItem from "./CategoriesListItem";
import _ from "lodash"
import loadingGif from "../assets/loading.gif"



class CategoriesList extends Component {
  constructor (props) {
    super(props)
    this.state = {categories: [], place: null, topics: null, loading: false, loadMore: true};
  }

 componentDidMount() {
      this.callApi()

 }



  componentWillUnmount() {
    // window.removeEventListener('scroll', this.onScroll, false);
    // console.log('unmounting');
    this.setState({topics: null});
    this.props.topicsUpdatedAction(null);
  }


  onPaginatedSearch = ()=>{
    let $this = this;
    if($this.state.loading){
      // console.log('still loading');
    }
    $this.setState({loading: true}, ()=>{
      var last_id = $this.state.categories[$this.state.categories.length - 1]._id
      // console.log('last id', last_id);
      Api.search({categories: true, topics: $this.state.topics, place: $this.state.place, last_id: last_id}).then((res)=>{
        // console.log('loaded more', res.data.courses.length);
        if(res.data.length < 1){
          // $this.setState({loading: false})
          return;
        } else {
          let categories = $this.state.categories;
          // courses = courses.concat(res.data.courses);
          // console.log(res.data)
          let merge = _.unionBy(categories, res.data.courses, '_id');
          // console.log(merge);
          $this.cleanCourses(categories, (cleanCategories)=>{
            setTimeout(()=>{
              // console.log('length ', cleanCourses.length);
              // console.log('merge length', merge.length);
              $this.setState({loadMore: false, categories: merge, last_id: null, loading: false});
            }, 2000)
          });
        }
      });
    })
    
  }


  cleanCourses(courses, cb){
    // console.log('coures lengh', courses.length);
    let clean =  _.uniqBy(courses, function (e) {

      return e._id;
    });
    // console.log('clean', clean.length);
    cb(clean)
  }

 
 componentWillReceiveProps(newProps){
    let $this = this;
    let place = (!newProps.place || newProps.place.address.length < 4) ? null : newProps.place;
    let searchData = {topics: [].concat.apply([], newProps.topics), place: place};
    $this.setState({topics: searchData.topics, place: searchData.place}, ()=>{
      $this.callApi(searchData);
    })
 }

 callApi = (data) => {
    let $this = this;
    // $this.setState({categories: []}, ()=>{
      data = (data) ? data : {topics: null};
      data.categories = true;
      Api.search(data).then(res =>{
        if(res.status === 200){
    $this.setState({categories: []}, ()=>{

              $this.setState({categories: res.data.courses, loading: false})
                  })

        }
      });

  };

 render() {
    // let list = (<div></div>);
    // console.log('history', this.props.history);
    var categories = (<div></div>)
    if(this.state.categories.length > 0){
      var place = this.state.place;
      if(!place){
        place = null
      }

      categories = this.state.categories.map((category) =>(
        <CategoriesListItem history={this.props.history} category={category} key={category._id} place={place} />
      ))

    } else {
        categories = (
          <div className="row" style={{width: "100%"}}>
            <div className="col-12">
              <div style={{width: "100%", textAlign: 'center', padding: "30px"}}>No matching results at the moment.</div>
            </div>
          </div>
          )
      }

      var loading = (<div></div>)
      if(this.state.loading){
        loading = (<div className="row">
            <div className="col-12">
              <div style={{textAlign: 'center', padding: "30px"}}><img alt="loading" src={loadingGif} width="80" height="80" style={{display: "block", marginLeft: "auto", marginRight: "auto"}}/></div>
            </div>
          </div>)
      }

      let loadMore = (<div></div>)
      if(!this.state.loading && this.state.categories.length > 0 ){
        loadMore = (
          <div>
          <button onClick={this.onPaginatedSearch.bind(this)} className="general-button color-border center-item" style={{marginTop: "10px", maxWidth: "300px", display: "block", marginLeft: "auto", marginRight: "auto"}}>Load More</button>
          </div>
        )
      }


    return (
        <div className="container-fluid">
          <div className="row" style={{maxWidth: "900px",  margin: "0 auto"}}>
            {categories}
          </div>
            {loadMore}
            {loading}
        </div>
    );
  }
}


const mapStateToProps = state => ({
  topics: state.SearchReducer.topics,
  place: state.SearchReducer.place
});

const mapDispatchToProps = {
  topicsUpdatedAction
  
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesList);

