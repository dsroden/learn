import React, {Component} from 'react'
import { connect } from 'react-redux';
import ReactChatView from 'react-chatview'
import PreviewChatMessage from './PreviewChatMessage'


class ChatViewContents extends Component {
   constructor (props) {
    super(props);
    this.state = { 
      messages: this.props.messages, // _.range(50).map(v => 'hi')
    };
  }


  loadMoreHistory () {
    return;
  }

  render () {
    let chatlist = (<div></div>)
    if(this.state.messages && this.state.messages.length > 0){
      chatlist = (
        <ReactChatView 
            className="content-preview"
            flipped={true}
            scrollLoadThreshold={50}
            onInfiniteLoad={this.loadMoreHistory.bind(this)}
            >
          {this.state.messages.map((m, ix) => <PreviewChatMessage  previoustime={(this.state.messages[ix + 1]) ? this.state.messages[ix  + 1].created_at : ''} message={m} key={ix}/>)}
        </ReactChatView>
      )
    }
    return (
      <div style={styles.chatViewContainer} >
        {chatlist}
      </div>
    )  
  }
}




const mapStateToProps = state => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatViewContents);


const styles = {
  chatViewContainer: {
    position: "relative",
    width: "100%",
    height: "300px",
    overflow: "auto",
    marginTop: "20px",
    marginBottom: "20px",
    boxShadow: "0px 1px 1px #f1f1f1",
    borderTop: "1px solid #edecec",
    backgroundColor: "white"
    // border: "1px solid " + Constants.BRAND_BLUE
  }
}
