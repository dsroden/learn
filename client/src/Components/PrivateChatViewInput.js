import React, {Component} from 'react'
import { connect } from 'react-redux';
import Constants from "../Constants"
import { sendPrivateMessageAction} from "../Actions/actionCreator";
import _ from "lodash";
import sendIcon from '../assets/airplane-icon.png';

class PrivateChatViewInput extends Component {
  constructor (props) {
    super(props)
    this.handleInputChange = this.handleInputChange.bind(this);
    this._handleKeyPress = this._handleKeyPress.bind(this);

    this.state = {
      message: '',
      sender: this.props.sender,
      receiver: this.props.receiver
    }
  } 

  componentWillReceiveProps(newProps){
    if(newProps.unblocked_user){
      let user = this.state.sender;
      let index = this.state.sender.blocked.indexOf(newProps.unblocked_user);
      user.blocked.splice(index, 1);
      this.setState({sender: user});
    }
  }
  componentDidMount(){
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  }

  _handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.sendMessage(this.state.message)
    }
  }

  sendMessage(message){
    if(!message){
      message = this.state.message;
    }

    if(message === ''){
      return;
    }
    let data = {text: message};
    data.channelId = 'personal-' + this.state.receiver.username;
    data.username = this.state.sender.username;
    data.receiver = this.state.receiver.username;
    let blocked = this.checkIfBlocked(data.receiver);
    if(blocked){
      this.setState({message: ''});
      return;
    }
    this.props.sendPrivateMessageAction(data);
    this.setState({message: ''})
  }


  checkIfBlocked(username){
    let blocked = _.find(this.state.sender.blocked, (u)=>{
      return username === u;
    });
    return blocked;
  }

  render(){
    let sendButton = (<div></div>)
    sendButton = (
      <button style={{position: "absolute", top: "7px", right: "-2px", backgroundColor: "transparent", border: "none"}} onClick={this.sendMessage.bind(this, null)} >
      <img alt="send" width="35" height="35" src={sendIcon} style={{ borderRadius: "100%", marginTop: "0px", backgroundColor: Constants.BRAND_RED}} />
      </button>
    )
    return (
      <div className="row" style={styles.inputContainer}>
        <div className="color-border" style={{width: "100%", position: "relative", height: "50px", backgroundColor: "white"}}>
        <input  style={styles.input} type="text" placeholder="Message..." ref="message" name="message" value={this.state.message} onChange={this.handleInputChange} onKeyPress={this._handleKeyPress} />
        {sendButton}
        </div>
      </div>
    )
  } 
}


const mapStateToProps = state => ({
  unblocked_user: state.AccountReducer.unblocked_user
});

const mapDispatchToProps = {
  sendPrivateMessageAction
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateChatViewInput);


const styles = {
  inputContainer: {
    padding: "10px",
    bottom: "0",
    width: "100%",
    backgroundColor: "white",
    margin: "0"
  },
  input: {
    width: "85%",
    margin: "10px",
    borderRadius: "5px",
    padding: "5px",
    paddingRight: "4%",
    border: "none"
  },
  sendButton: {
    position: "absolute",
    right: "20px",
    display: "inline-block",
    width: "15%",
    height: "50px",
    marginTop: "10px",
    borderTopRightRadius: "5px",
    borderBottomRightRadius: "5px",
    backgroundColor: Constants.BRAND_GREEN
  }
}

