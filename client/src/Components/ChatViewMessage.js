import React, {Component} from 'react'
import { connect } from 'react-redux';
import Timestamp from 'react-timestamp';
import Api from '../Api'
import { showUserProfileAction, openUserProfileViewAction } from "../Actions/actionCreator";
import ProfilePhoto from "./ProfilePhoto"

class ChatViewMessage extends Component { 
  constructor(props){
    super(props)
    this.state ={
      message: this.props.message,
      isself: this.props.isself,
      previoustime: this.props.previoustime,
    } 
  }

  componentDidMount(){
  }

  showUserProfile(){
    if(!window.localStorage.getItem('access_token')){
      return;
    }
    Api.fetchUserProfile(this.state.message.username).then((user)=>{
      this.props.openUserProfileViewAction(true)
      user.data.details = false;
      this.props.showUserProfileAction(user.data)
    });
  }


  compareDates(d1, d2){
   return d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate();
  }

  render() {

    let message = this.state.message;
    let previousTime = (<div></div>);
    let current_time = message.created_at;
    var msg_time = new Date(current_time);
    var now_time = new Date();
    var previous_time = new Date(this.state.previoustime);
    
    if(!this.compareDates(msg_time, previous_time)){
      let time = (<Timestamp time={current_time} format="date" />)
      if(this.compareDates(msg_time, now_time)){
        time = (<div>Today</div>)
      }
      previousTime = (
        <div className="row">
          <div className="col-12" style={{color: '#ccc', padding: "10px", textAlign: "center"}}>
            {time}
          </div>
        </div>
      )            
    }

    return (
      <div>
      {previousTime}
      <div className="row">
      <div className="col-12" style={{padding: "0px"}}>
      <div style={{padding: "0px", borderRadius: "5px", boxShadow: (this.props.isself1) ? "0px 0px 10px #ccc" : 'none'}}>
      <div style={{display: "inline-block"}}><ProfilePhoto user={this.state.message} size="40" /></div>
      <div className="username" style={styles.username} onClick={this.showUserProfile.bind(this)}>{message.username}</div>
      <Timestamp time={message.created_at} style={styles.timestamp} format="time" />
      <div></div>
      <div className="chat-bubble" >
        <div style={styles.text}>{message.text}</div>
      </div>
      </div>
      </div>
      </div>
      </div>
    )
  }
}


const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  openUserProfileViewAction,
  showUserProfileAction
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatViewMessage);


const styles = {
  username: {
    fontSize: "14px",
    fontWeight: "bold",
    cursor: "pointer",
    display: "inline-block",
    marginLeft: "10px"
  },
  timestamp: {
    fontSize: "12px",
    // float: "right",
    // marginTop: "20px",
    display: 'inline-block',
    marginLeft: "10px",
    color: "#ccc"
  }
}

