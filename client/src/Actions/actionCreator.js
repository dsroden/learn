import { 
  exampleCreated,
  exampleDeleted, 
  courseSearchDone, 
  updateSearchTopic, 
  updateSearchPlace, 
  removeTag,
  topicsUpdated,
  placeUpdated,
  accountStateUpdated,
  sendMessage,
  receiveMessage,
  globalLocalToggle,
  courseToggle,
  switchCourse,
  switchGroup,
  closeCourseList,
  openCourseList,
  closeLocalList,
  openLocalList,
  closePrivateConversationsList,
  openPrivateConversationsList,
  openUserProfileView,
  closeUserProfileView,
  showUserProfile,
  receivePrivateMessage,
  sendPrivateMessage,
  reloadChatPage,
  blockUser,
  unblockUser,
  createdUsername,
  toggleMenu,
  coachGroupAdded,
  openSuggestionBox,
  updateUser,
  toggleCreateMeeting,
  toggleMeetingsList
} from "./actionTypes";

const exampleCreatedAction = (example) => ({
  type: exampleCreated,
  data: example
});

const exampleDeletedAction = (example) => ({
  type: exampleDeleted,
  data: example
});

const courseSearchDoneAction = (courses) => ({
  type: courseSearchDone,
  data: courses
});

const updateSearchTopicAction = (topic) => ({
  type: updateSearchTopic,
  data: topic
});

const updateSearchPlaceAction = (place) => ({
  type: updateSearchPlace,
  data: place
});

const removeTagAction = (tag) => ({
  type: removeTag,
  data: tag
});

const topicsUpdatedAction = (topics) => ({
  type: topicsUpdated,
  data: topics
});

const placeUpdatedAction = (place) => ({
  type: placeUpdated,
  data: place
});

const accountStateUpdatedAction = (account_state) => ({
  type: accountStateUpdated,
  data: account_state
});

const sendMessageAction = (message)=>({
  type: sendMessage,
  data: message
});

const receiveMessageAction = (message)=>({
  type: receiveMessage,
  data: message
});

const globalLocalToggleAction = (toggleState)=>({
  type: globalLocalToggle,
  data: toggleState
});

const coursesToggleAction = (toggleState)=>({
  type: courseToggle,
  data: toggleState
});

const switchCourseAction = (course)=>({
  type: switchCourse,
  data: course
});

const switchGroupAction = (group)=>({
  type: switchGroup,
  data: group
});

const closeCourseListAction = (list_state)=>({
  type: closeCourseList,
  data: list_state
});
const openCourseListAction = (list_state)=>({
  type: openCourseList,
  data: list_state
});
const closeLocalListAction = (list_state)=>({
  type: closeLocalList,
  data: list_state
});
const openLocalListAction = (list_state)=>({
  type: openLocalList,
  data: list_state
});
const closePrivateConversationsListAction = (list_state)=>({
  type: closePrivateConversationsList,
  data: list_state
});
const openPrivateConversationsListAction = (list_state)=>({
  type: openPrivateConversationsList,
  data: list_state
});

const openUserProfileViewAction = (user_profile_view)=>({
  type: openUserProfileView,
  data: user_profile_view
});
const closeUserProfileViewAction = (user_profile_view)=>({
  type: closeUserProfileView,
  data: user_profile_view
});
const showUserProfileAction = (user)=>({
  type: showUserProfile,
  data: user
});
const receivePrivateMessageAction = (message)=>({
  type: receivePrivateMessage,
  data: message
});
const sendPrivateMessageAction = (message)=>({
  type: sendPrivateMessage,
  data: message
});
const reloadChatPageAction = (pageState)=>({
  type: reloadChatPage,
  data: pageState
});
const blockUserAction = (username)=>({
  type: blockUser,
  data: username
});
const unblockUserAction = (username)=>({
  type: unblockUser,
  data: username
});

const createdUsernameAction = (username)=>({
  type: createdUsername,
  data: username
});

const toggleMenuAction = (menu_state)=>({
  type: toggleMenu,
  data: menu_state
});

const coachGroupAddedAction = (new_group)=>({
  type: coachGroupAdded,
  data: new_group
});

const openSuggestionBoxAction = (open)=>({
  type: openSuggestionBox,
  data: open
});

const updateUserAction = (user)=>({
  type: updateUser,
  data: user
});

const toggleCreateMeetingAction = (state)=>({
  type: toggleCreateMeeting,
  data: state
})

const toggleMeetingsListAction = (state)=>({
  type: toggleMeetingsList,
  data: state
})


export { 
  exampleCreatedAction,
  exampleDeletedAction,
  courseSearchDoneAction,
  updateSearchTopicAction,
  updateSearchPlaceAction,
  removeTagAction,
  topicsUpdatedAction,
  placeUpdatedAction,
  accountStateUpdatedAction,
  sendMessageAction,
  receiveMessageAction,
  globalLocalToggleAction,
  coursesToggleAction,
  switchCourseAction,
  switchGroupAction,
  closeCourseListAction,
  openCourseListAction,
  closeLocalListAction,
  openLocalListAction,
  closePrivateConversationsListAction,
  openPrivateConversationsListAction,
  openUserProfileViewAction,
  closeUserProfileViewAction,
  showUserProfileAction,
  receivePrivateMessageAction,
  sendPrivateMessageAction,
  reloadChatPageAction,
  blockUserAction,
  unblockUserAction,
  createdUsernameAction,
  toggleMenuAction,
  coachGroupAddedAction,
  openSuggestionBoxAction,
  updateUserAction,
  toggleCreateMeetingAction,
  toggleMeetingsListAction
};



