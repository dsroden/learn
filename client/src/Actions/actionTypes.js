
const exampleCreated = "EXAMPLE_CREATED";
const exampleDeleted = "EXAMPLE_DELETED";
const courseSearchDone = "COURSE_SEARCH_DONE";
const updateSearchTopic = "UPDATE_SEARCH_TOPIC";
const updateSearchPlace = "UPDATE_SEARCH_PLACE";
const removeTag = "REMOVE_TAG";
const topicsUpdated = "TOPICS_UPDATED";
const placeUpdated = "PLACE_UPDATED";
const accountStateUpdated = "ACCOUNT_STATE_UPDATED";
const sendMessage = "SEND_MESSAGE";
const receiveMessage = "RECEIVE_MESSAGE";
const globalLocalToggle = "GLOBAL_LOCAL_TOGGLE";
const courseToggle = "COURSE_TOGGLE";
const switchCourse = "SWITCH_COURSE";
const switchGroup = "SWITCH_GROUP";
const closeCourseList = "CLOSE_COURSE_LIST";
const openCourseList = "OPEN_COURSE_LIST";
const closeLocalList = "CLOSE_LOCAL_LIST";
const openLocalList = "OPEN_LOCAL_LIST";
const openPrivateConversationsList = "OPEN_PRIVATE_CONVERSATIONS_LIST";
const closePrivateConversationsList = "CLOSE_PRIVATE_CONVERSATIONS_LIST";
const openUserProfileView = "OPEN_USER_PROFILE_VIEW";
const closeUserProfileView = "CLOSE_USER_PROFILE_VIEW";
const showUserProfile = "SHOW_USER_PROFILE";
const receivePrivateMessage = "RECEIVE_PRIVATE_MESSAGE";
const sendPrivateMessage = "SEND_PRIVATE_MESSAGE";
const reloadChatPage = "RELOAD_CHAT_PAGE";
const blockUser = "BLOCK_USER";
const unblockUser = "UNBLOCK_USER";
const createdUsername = "CREATED_USERNAME";
const toggleMenu = "TOGGLE_MENU";
const coachGroupAdded = "COACH_GROUP_ADDED";
const openSuggestionBox = "OPEN_SUGGESTION_BOX";
const updateUser = "UPDATE_USER";
const toggleMeetingsList = "TOGGLE_MEETINGS_LIST";
const toggleCreateMeeting = "TOGGLE_CREATE_MEETING";

export {
	exampleCreated,
	exampleDeleted, 
	courseSearchDone,
	updateSearchTopic, 
	updateSearchPlace,
	removeTag,
	topicsUpdated,
	placeUpdated,
	accountStateUpdated,
	sendMessage,
	receiveMessage,
	globalLocalToggle,
	courseToggle,
	switchCourse,
	switchGroup,
	closeCourseList,
	openCourseList,
	closeLocalList,
	openLocalList,
	openPrivateConversationsList,
	closePrivateConversationsList,
	showUserProfile,
	openUserProfileView,
	closeUserProfileView,
	receivePrivateMessage,
	sendPrivateMessage,
	reloadChatPage,
	blockUser,
	unblockUser,
	createdUsername,
	toggleMenu,
	coachGroupAdded,
	openSuggestionBox,
	updateUser,
	toggleMeetingsList,
	toggleCreateMeeting,

};