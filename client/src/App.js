import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'

import createHistory from 'history/createBrowserHistory'
import { Switch, Route, Router } from 'react-router'

import store from './store'

import BrowsePage from './Components/BrowsePage.js'
import ChatPage from './Components/ChatPage.js'
import ProfilePage from './Components/ProfilePage.js'
import AccountVerification from './Components/AccountVerification.js'
import ResetPassword from './Components/ResetPassword.js'
import PrivacyPolicy from './Components/PrivacyPolicy'
import TermsOfUse from './Components/TermsOfUse'
import Landing from './Components/Landing'
import Constants from './Constants'

import ReactGA from 'react-ga';
ReactGA.initialize(Constants.GA_ID, {debug: false}); 

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory()

history.listen((location, action) => {
    // console.log('location', location)
  ReactGA.set({ page: location.pathname })
  ReactGA.pageview(location.pathname)
})

export default function() {
    return ReactDOM.render(
        <div className="learnlabs-app">
        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route exact path="/" component={Landing} />
                    <Route exact path="/browse" component={BrowsePage} />
                    <Route exact path="/chat" component={ChatPage} />
                    <Route exact path="/profile" component={ProfilePage} />
                    <Route exact path="/verification" component={AccountVerification} />
                    <Route exact path="/reset" component={ResetPassword} />
                    <Route exact path="/privacy" component={PrivacyPolicy} />
                    <Route exact path="/terms" component={TermsOfUse} />

                </Switch>
            </Router>
        </Provider>
        </div>,
        document.getElementById('root')
    );
}


