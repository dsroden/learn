/*global chrome */
import axios from 'axios';
import Constants from '../Constants';
import * as loadImage from 'blueimp-load-image'


export function authHeaders() {
    var token = localStorage.getItem("token");
    if (token) {
        return {"Authorization": "Basic " + btoa(unescape(encodeURIComponent(token + ':')))};
    }
    return {};
}

export function getLocalIPs(callback) {
    var ips = [];

    var RTCPeerConnection = window.RTCPeerConnection ||
        window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

    var pc = new RTCPeerConnection({
        // Don't specify any stun/turn servers, otherwise you will
        // also find your public IP addresses.
        iceServers: []
    });
    // Add a media line, this is needed to activate candidate gathering.
    pc.createDataChannel('');
    
    // onicecandidate is triggered whenever a candidate has been found.
    pc.onicecandidate = function(e) {
        if (!e.candidate) { // Candidate gathering completed.
            pc.close();
            callback(ips);
            return;
        }
        var ip = /^candidate:.+ (\S+) \d+ typ/.exec(e.candidate.candidate)[1];
        if (ips.indexOf(ip) === -1) // avoid duplicate entries (tcp/udp)
            ips.push(ip);
    };
    pc.createOffer(function(sdp) {
        pc.setLocalDescription(sdp);
    }, function onerror() {});
}


export function notify(contents) {
    var options = {
      type: "basic",
      title: "activated",
      message: "You have successfully created a notification!",
      iconUrl: "favicon.ico"
    };
    chrome.notifications.create('11234', options, (id)=>{
        // chrome.browserAction.setBadgeText({ text: '1' });
         setTimeout(function(){
            chrome.notifications.clear(id, ()=>{

            })
         }, 1000)
    });
}


export function getCurrentUrl(){
    return new Promise((resolve, reject)=>{
        chrome.runtime.sendMessage({get_current_url: 'yes'}, function(response) {
            if(response){
                resolve(response);
            } else {
                reject();
            }
        });
    }).then((result)=>{
        return result;
    }).catch((err)=>{
        console.log('error', err);
        return err;
    });
}


export function clearChromeStorage(){
    chrome.storage.sync.clear(()=>{
    });
}

export function getClientId (){
    console.log('going to uitls to get client id')
    return new Promise((resolve, reject)=>{
        console.log('going into chrome storage to get client id');
        chrome.storage.sync.get('client_id', function(items) {
            console.log('items from chrome storage>>>', items);
            var client_id = items.client_id;
            if(client_id){
                console.log('resolving get client id', client_id);
                resolve(client_id);
            } else {
                console.log('rejecting get client id');
                reject();
            }
        });
    }).then((client_id)=>{
        return client_id;
    }).catch((err)=>{
        console.log('Error getting client id ' + JSON.stringify(err));
        // return err;
    });

}

export function setClientId (){
    return new Promise((resolve, reject)=>{
        console.log('utils setting client id');
        let client_id = getRandomToken();
        chrome.storage.sync.set({client_id: client_id}, function() {
            axios.post(Constants.API_URL + '/extension/createone', {client_id: client_id}).then((res)=>{
                console.log('client id set', client_id)
                resolve(res.data.client_id);
            }).catch((err)=>{
              reject(err);
            })
        });
    }).then((client_id)=>{
        return client_id;
    }).catch((err)=>{
        console.log('Error etting client id ' + JSON.stringify(err));
    });
}

function getRandomToken() {
    // E.g. 8 * 32 = 256 bits token
    var randomPool = new Uint8Array(32);
    crypto.getRandomValues(randomPool);
    var hex = '';
    for (var i = 0; i < randomPool.length; ++i) {
        hex += randomPool[i].toString(16);
    }
    // E.g. db18458e2782b2b77e36769c569e263a53885a9944dd0a861e5064eac16f1a
    return hex;
}

export function createRandomId(){
    return Math.random().toString(36).substr(2); // remove `0.`
}

export function o2LoadImage(imageUrl){
    let prom = new Promise((res, rej)=>{
        const ORIENT_TRANSFORMS = {
            1: '',
            2: 'rotateY(180deg)',
            3: 'rotate(180deg)',
            4: 'rotate(180deg) rotateY(180deg)',
            5: 'rotate(270deg) rotateY(180deg)',
            6: 'rotate(90deg)',
            7: 'rotate(90deg) rotateY(180deg)',
            8: 'rotate(270deg)'
        }

        var xhr = new XMLHttpRequest();
        xhr.open('GET', imageUrl, true);
        xhr.responseType = 'arraybuffer';

        xhr.onload = function(e) {
          if (this.status === 200) {
            var arrayBufferView = new Uint8Array(this.response);
            var blob = new Blob([arrayBufferView], { type: "image/jpeg" });

            // console.log("about to parse blob:" + _.pairs(this.response));
            loadImage.parseMetaData(blob, function (data) {
                // console.log("EXIF:" + _.pairs(data))
                var ori ="initial";
                if (data.exif) {
                    ori = data.exif.get('Orientation');
                }
                // console.log("ori is:" + ori);
                res({image_valid: true, transform: ORIENT_TRANSFORMS[ori]});
            });
          } else {
            // console.log('no image');
            res({image_valid: false});
          }
        };
        xhr.send();
    })

    prom.then((result)=>{
        return result;
    }).catch((err)=>{
        console.log('Error loading image', err);
        return {image_valid: false};
    });
    
    return prom;

  }



